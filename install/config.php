<?php
defined('InYUNYECMS') or exit('Direct access not allowed');
define('YUNYECMSConfig',TRUE);
$yunyecms_dbconfig=array();
$ecms_adminloginr=array();
//数据库设置
$yunyecms_dbconfig['type']='mysql';	//数据库类型
$yunyecms_dbconfig['ver']='5.0';	//数据库版本
$yunyecms_dbconfig['hostname']='#DB_HOST#';	//数据库登录地址
$yunyecms_dbconfig['port']='#DB_PORT#';	//端口，不填为按默认
$yunyecms_dbconfig['username']='#DB_USER#';	//数据库用户名
$yunyecms_dbconfig['password']='#DB_PWD#';	//数据库密码
$yunyecms_dbconfig['database']='#DB_NAME#';	//数据库名
$yunyecms_dbconfig['dbchar']='utf8';	//数据库默认编码
$yunyecms_dbconfig['tablepre']='#DB_PREFIX#';	//数据表前缀
$yunyecms_dbconfig['pconnect']=0;	//数据表前缀
$yunyecms_dbconfig['autoconnect']=0;	//数据表前缀
$yunyecms_dbconfig['debug']=true;	//显示SQL错误提示(0为不显示,1为显



//网站防火墙配置
$yunyecms_config['firewall']['open']=0;	//开启防火墙(0为关闭,1为开启)
$yunyecms_config['firewall']['pwd']='';	//防火墙加密密钥(填写10~50个任意字符，最好多种字符组合)
$yunyecms_config['firewall']['adminallowdomain']='';	//允许后台登陆的域名,设置后必须通过这个域名才能访问后台
$yunyecms_config['firewall']['adminhour']='';	//允许登陆后台的时间：0~23小时，多个时间点用半角逗号格开
$yunyecms_config['firewall']['adminweek']='';	//允许登陆后台的星期：星期0~6，多个星期用半角逗号格开
$yunyecms_config['firewall']['adminckpassvar']='';	//后台预登陆验证变量名
$yunyecms_config['firewall']['adminckpassval']='';	//后台预登陆认证码
$yunyecms_config['firewall']['Sensitivecharacter']='';	//屏蔽提交敏感字符，多个用半角逗号格






//Cookie配置
define('COOKIE_DOMAIN','');//Cookie 作用域
define('COOKIE_PATH','/');//Cookie 作用路径
define('COOKIE_PRE','YUNYECMS_'); //Cookie 前缀
define('COOKIE_ADMINPRE','YUNYECMSADM_'); //Cookie 后台前缀
define('COOKIE_EXPIRE',0); //Cookie 生命周期
define('COOKIE_RND','LZ7ujhcmo4hrSItm8zZfFr5vq1QI6c'); //cookie随机验证码

//Cookie配置
define('ADMLOGIN_NUMS',5);//Cookie 作用域
define('ADMLOGIN_MINUTES',60);//分钟数
define('ADMLOGIN_OVERTIME',60);//分钟数


define('SAFE_LOGINLOG',1);//是否记录登录日志
define('SAFE_ADMCOOKIERND','yOxMRqyNrUmITUxU5Jlu67nBKNE9Ee');//后台登录COOKIE认证码(填写10~50个任意字符，最好多种字符组合)
define('SAFE_CKHASH',0);//启用后台来源认证码,0为开启来源验证,1为关闭
define('SAFE_ISOPLOG',1);//启用后台操作日期,1为启用,0为关闭


?>