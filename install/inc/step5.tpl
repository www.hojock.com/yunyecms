
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title><?php echo $steps[$step];?>-云业内容管理系统2.0</title>
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="../public/ui/bootstrap/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link href="../public/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" />
  <link rel="stylesheet" href="../public/ui/plugins/font-awesome-4.7.0/css/font-awesome.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="../public/ui/dist/css/AdminLTE.min.css">

  <link rel="stylesheet" href="../public/ui/dist/css/skins/_all-skins.min.css">
  <link href="../public/ui/dist/css/install.css" rel="stylesheet" type="text/css" />
  <link href="../public/ui/dist/css/step.css" rel="stylesheet" type="text/css" />
  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
</head>
<body class="hold-transition layout-top-nav skin-blue ">
<div class="wrapper">
	<?php include_once ('./inc/head.tpl'); ?>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
   <div class="container">
    <!-- Main content -->
    <section class="invoice">
      <!-- title row -->
      <div class="row">
        <div class="col-xs-12">
          <h2 class="page-header">
            <i class="fa fa-check font-blue"></i>第 <?php echo $step;?> 步：<?php echo $steps[$step];?>
          </h2>
        </div>
        <!-- /.col -->
      </div>
      <!-- Table row -->
      <div class="row">
       <div class="mt-element-step">
                                                      
                                                            <div class="row step-line">
                                                                <div class="col-md-3 mt-step-col first  done step-width">
                                                                    <div class="mt-step-number bg-white">1</div>
                                                                    <div class="mt-step-title uppercase font-grey-cascade">安装须知</div>
                                                                </div>
                                                                <div class="col-md-3 mt-step-col step-width done">
                                                                    <div class="mt-step-number bg-white">2</div>
                                                                    <div class="mt-step-title uppercase font-grey-cascade">环境检测</div>
                                                                </div>
                                                                <div class="col-md-3 mt-step-col step-width done">
                                                                    <div class="mt-step-number bg-white">3</div>
                                                                    <div class="mt-step-title uppercase font-grey-cascade">参数配置</div>
                                                                </div>
                                                                <div class="col-md-3 mt-step-col  step-width done">
                                                                    <div class="mt-step-number bg-white">4</div>
                                                                    <div class="mt-step-title uppercase font-grey-cascade">开始安装</div>
                                                                </div>
                                                                 <div class="col-md-3 mt-step-col step-width active last">
                                                                    <div class="mt-step-number bg-white">5</div>
                                                                    <div class="mt-step-title uppercase font-grey-cascade">安装完成</div>
                                                                </div>
                                                            </div>
       
                                                       
  	<div class="wrap">
  	  <div class="well" style="background: none;">
		<h1 class="font-green text-center">恭喜您，安装成功！</h1>	 
		<p> 
				<div class="text-center"><a class="btn btn-lg  btn-success btn-flat" href="http://<?php echo $domain ?>/admin.php" target="_blank" > <i class="fa fa-arrow-right"> </i> 进入后台管理</a> </div>
						<div class="note note-info margin-top-20">
                        <h4 class="block">温馨提示：</h4>
                        <p>
                        <span class="font-red">1、</span> 安装完毕后请进入后台更新缓存<br/>
						<span class="font-red">2、</span> 为了您站点的安全，安装完成后即可将网站根目录下的“<span class="font-green">Install</span>”文件夹删除。
                           </p>
                        </div>
                    
         </p>
  	  </div>
         
     </div>
                                                       
                                     
                                                       
                                                       
                                                        </div>
       
      </div>
    </section>
    <!-- /.content -->
    <div class="clearfix"></div>
  </div>
  <!-- /.content-wrapper -->
	<?php include_once ('./inc/foot.tpl'); ?>
 </div>
</div>
<!-- ./wrapper -->
<!-- jQuery 2.2.3 -->
<script src="../public/ui/plugins/jQuery/jquery-2.2.3.min.js"></script>
<!-- Bootstrap 3.3.6 -->
<script src="../public/ui/bootstrap/js/bootstrap.min.js"></script>
<!-- FastClick -->
<script src="../public/ui/plugins/fastclick/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="../public/ui/dist/js/app.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="../public/ui/dist/js/demo.js"></script>
</body>
</html>
