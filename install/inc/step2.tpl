
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title><?php echo $steps[$step];?>-云业内容管理系统2.0</title>
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="../public/ui/bootstrap/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link href="../public/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" />
  <link rel="stylesheet" href="../public/ui/plugins/font-awesome-4.7.0/css/font-awesome.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="../public/ui/dist/css/AdminLTE.min.css">

  <link rel="stylesheet" href="../public/ui/dist/css/skins/_all-skins.min.css">
  <link href="../public/ui/dist/css/install.css" rel="stylesheet" type="text/css" />
  <link href="../public/ui/dist/css/step.css" rel="stylesheet" type="text/css" />
  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
</head>
<body class="hold-transition layout-top-nav skin-blue ">
<div class="wrapper">
	<?php include_once ('./inc/head.tpl'); ?>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
   <div class="container">
    <!-- Main content -->
    <section class="invoice">
      <!-- title row -->
      <div class="row">
        <div class="col-xs-12">
          <h2 class="page-header">
            <i class="fa fa-bullhorn font-blue"></i>第 <?php echo $step;?> 步：<?php echo $steps[$step];?>
          </h2>
        </div>
        <!-- /.col -->
      </div>
      <!-- Table row -->
      <div class="row">
       <div class="mt-element-step">
                                                      
                                                            <div class="row step-line">
                                                                <div class="col-md-3 mt-step-col first  done step-width">
                                                                    <div class="mt-step-number bg-white">1</div>
                                                                    <div class="mt-step-title uppercase font-grey-cascade">安装须知</div>
                                                                </div>
                                                                <div class="col-md-3 mt-step-col step-width active">
                                                                    <div class="mt-step-number bg-white">2</div>
                                                                    <div class="mt-step-title uppercase font-grey-cascade">环境检测</div>
                                                                </div>
                                                                <div class="col-md-3 mt-step-col step-width">
                                                                    <div class="mt-step-number bg-white">3</div>
                                                                    <div class="mt-step-title uppercase font-grey-cascade">参数配置</div>
                                                                </div>
                                                                <div class="col-md-3 mt-step-col  step-width">
                                                                    <div class="mt-step-number bg-white">4</div>
                                                                    <div class="mt-step-title uppercase font-grey-cascade">开始安装</div>
                                                                </div>
                                                                 <div class="col-md-3 mt-step-col step-width last">
                                                                    <div class="mt-step-number bg-white">5</div>
                                                                    <div class="mt-step-title uppercase font-grey-cascade">安装完成</div>
                                                                </div>
                                                            </div>
       <div class="wrap">
	<h3  class="font-green">服务器信息</h3>	
	<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0" class="table table-striped table-hover">
		<tr>
			<th width="300" align="left"><strong>参数</strong></th>
			<th width="424" align="left"><strong>值</strong></th>
		</tr>
		<tr>
				<td><strong>服务器域名/IP地址</strong></td>
				<td><?php echo $name."/".$host; ?></td>
		</tr>
		<tr>
				<td><strong>服务器操作系统</strong></td>
				<td><?php echo $os; ?></td>
		</tr>
		<tr>
				<td><strong>服务器解译引擎</strong></td>
				<td><?php echo $server; ?></td>
		</tr>
		<tr>
				<td><strong>PHP版本</strong></td>
				<td><?php echo $phpv; ?></td>
		</tr>
		<tr>
				<td><strong>安装路径</strong></td>
				<td><?php echo YUNYECMS_ROOT; ?></td>
		</tr>
		<tr>
				<td><strong>脚本超时时间</strong></td>
				<td><?php echo $max_execution_time; ?> 秒</td>
		</tr>

	</table>
	<h3 class="font-green">系统环境要求</h3> 
	<table width="100%" border="0" align="center" cellpadding="2" cellspacing="0" class="table table-striped table-hover">
		<tr>
			<th width="230"><strong>需开启的变量或函数</strong></th>
			<th width="100"><strong>系统要求</strong></th>
			<th width="400"><strong>实际状态及建议</strong></th>
		</tr>
		<tr>
				<td>GD 支持 </td>
				<td>支持</td>
				<td><?php echo $gd; ?></td>
		</tr>
		<!--<tr>
				<td>MySQL 支持</td>
				<td>支持</td>
				<td><?php echo $mysql; ?></td>
		</tr>-->
		<tr>
				<td>MySQLi 支持</td>
				<td>支持</td>
				<td><?php echo $mysqli; ?></td>
		</tr>

		<tr> 
			<td>upload</td>
			<td>支持</td>
			<td><?php echo $uploadSize ?></td>
			</tr>
		<tr>
		  <td>session</td>
		  <td>支持</td>
		  <td><?php echo $session ?></td>
		</tr>
	</table>
	<h3  class="font-green">目录权限检测</h3> 
	<div style="margin:10px auto; color:#666;">
	系统要求必须满足下列所有的目录权限全部可读写的需求才能使用。</div>
	
	<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0" class="table table-striped table-hover">
		<tr>
			<th width="230"><strong>目录名</strong></th>
			<th width="100"><strong>系统要求</strong></th>
			<th width="400"><strong>实际状态及建议</strong></th>
		</tr>
		<?php
		foreach($folder as $dir)
		{
			$Testdir = YUNYECMS_ROOT.$dir;
			dir_create($Testdir);
			if(TestWrite($Testdir)){
				$w = '<span class="lable lable-success"><i class="fa fa-check  font-green"></i> 写 </span>';
			}else{
				$w = '<span class="lable label-danger "><i class="fa fa-remove font-red"></i> 写 </span> ';
				$err++;
			}
			if(is_readable($Testdir)){
				$r  = '<span class="lable lable-success "><i class="fa fa-check font-green"></i> 读 </span>';
			}else{
				$r = '<span class="lable label-danger "><i class="fa fa-remove font-red"></i> 读 </span> ';
				$err++;
			}
		?>
		<tr>
		  <td><?php echo $dir; ?></td>
		  <td>读写</td>
		  <td><?php echo $r." ".$w; ?></td>
		</tr>
		<?php } ?>
	</table>
	<div class="text-center">
		<button type="button" class="btn btn-lg  btn-primary btn-flat"  style="margin-right:20px" onclick="history.back();"  /><i class="icon-action-undo"></i> 上一步</button>
		
	<button type="button" class="btn btn-lg  btn-primary btn-flat"   onclick="window.location.href='index.php?step=3';" <?php if($err>0){ ?> id="nonext"  disabled<?php } ?> /><i class="icon-control-forward"></i> 下一步</button>		
		
		<button type="button" class="btn btn-lg  btn-success btn-flat" onclick="document.location.reload();" style="margin-left:20px" /><i class="icon-reload"></i> 重新检测</button>
	</div>
</div>
                                                        </div>
       
      </div>
    </section>
    <!-- /.content -->
    <div class="clearfix"></div>
  </div>
  <!-- /.content-wrapper -->
	<?php include_once ('./inc/foot.tpl'); ?>
 </div>
</div>
<!-- ./wrapper -->
<!-- jQuery 2.2.3 -->
<script src="../public/ui/plugins/jQuery/jquery-2.2.3.min.js"></script>
<!-- Bootstrap 3.3.6 -->
<script src="../public/ui/bootstrap/js/bootstrap.min.js"></script>
<!-- FastClick -->
<script src="../public/ui/plugins/fastclick/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="../public/ui/dist/js/app.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="../public/ui/dist/js/demo.js"></script>
</body>
</html>
