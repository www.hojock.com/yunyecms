<?php 
if (!file_exists('../../data/install.lock')){
Header("HTTP/1.1 303 See Other"); 
Header("Location: ../index.php"); 
exit; 
}
?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>出错了，您已经安装过该系统了-云业内容管理系统2.0.2</title>
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="../../public/ui/bootstrap/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link href="../../public/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" />
  <link rel="stylesheet" href="../../public/ui/plugins/font-awesome-4.7.0/css/font-awesome.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="../../public/ui/dist/css/AdminLTE.min.css">

  <link rel="stylesheet" href="../../public/ui/dist/css/skins/_all-skins.min.css">
  <link href="../../public/ui/dist/css/install.css" rel="stylesheet" type="text/css" />
  <link href="../../public/ui/dist/css/step.css" rel="stylesheet" type="text/css" />
  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
</head>
<body class="hold-transition layout-top-nav skin-blue ">
<div class="wrapper">
	<?php include_once ('head.tpl'); ?>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
   <div class="container">
    <!-- Main content -->
    <section class="invoice">
      <!-- title row -->
      <div class="row">
        <div class="col-xs-12">
          <h2 class="page-header">
            <i class="fa fa-bullhorn font-blue"></i> 提示信息
          </h2>
        </div>
        <!-- /.col -->
      </div>
      <!-- Table row -->
      <div class="row">
                                             <div class="alert alert-block alert-info fade in">
                                                            <h4 class="alert-heading text-center">出错了!</h4>
                                                            <p class="text-center"> 你已经安装过该系统了，如果想重新安装，请先删除Public目录下的 install.lock 文件，然后再安装。 </p>
                                                            <p class="text-center margin-top-15">
                                                               <button type="reset" name="reset" class="btn btn-circle green hidden" onclick="history.go(-1)"> <i class="   icon-action-undo"> </i> 返 回</button>
                                                            </p>
                                         </div>
       
      </div>
    </section>
    <!-- /.content -->
    <div class="clearfix"></div>
  </div>
  <!-- /.content-wrapper -->
	<?php include_once ('foot.tpl'); ?>
 </div>
</div>
<!-- ./wrapper -->
<!-- jQuery 2.2.3 -->
<script src="../../public/ui/plugins/jQuery/jquery-2.2.3.min.js"></script>
<!-- Bootstrap 3.3.6 -->
<script src="../../public/ui/bootstrap/js/bootstrap.min.js"></script>
<!-- FastClick -->
<script src="../../public/ui/plugins/fastclick/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="../../public/ui/dist/js/app.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="../../public/ui/dist/js/demo.js"></script>
</body>
</html>
