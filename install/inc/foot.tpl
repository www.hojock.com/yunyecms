 <footer class="main-footer no-print" style="position: fixed; bottom: 0px; width: 100%;">
    <div class="pull-right hidden-xs">
      <b>Version</b> 2.0
    </div>
    <strong>Copyright  &copy; 2019  <a target="_blank" href="http://www.yunyecms.com">云业CMS  V2.0</a> All Rights Reserved.
  </footer>