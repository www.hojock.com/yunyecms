
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title><?php echo $steps[$step];?>-云业内容管理系统2.0</title>
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="../public/ui/bootstrap/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link href="../public/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" />
  <link rel="stylesheet" href="../public/ui/plugins/font-awesome-4.7.0/css/font-awesome.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="../public/ui/dist/css/AdminLTE.min.css">

  <link rel="stylesheet" href="../public/ui/dist/css/skins/_all-skins.min.css">
  <link href="../public/ui/dist/css/install.css" rel="stylesheet" type="text/css" />
  <link href="../public/ui/dist/css/step.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="../public/ui/validator/dist/css/bootstrapValidator.css"/>
  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
</head>
<body class="hold-transition layout-top-nav skin-blue ">
<div class="wrapper">
	<?php include_once ('./inc/head.tpl'); ?>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
   <div class="container">
    <!-- Main content -->
    <section class="invoice">
      <!-- title row -->
      <div class="row">
        <div class="col-xs-12">
          <h2 class="page-header">
            <i class="icon-settings font-blue"></i>第 <?php echo $step;?> 步：<?php echo $steps[$step];?>
          </h2>
        </div>
        <!-- /.col -->
      </div>
      <!-- Table row -->
      <div class="row">
       <div class="mt-element-step">
                                                      
                                                            <div class="row step-line">
                                                                <div class="col-md-3 mt-step-col first  done step-width">
                                                                    <div class="mt-step-number bg-white">1</div>
                                                                    <div class="mt-step-title uppercase font-grey-cascade">安装须知</div>
                                                                </div>
                                                                <div class="col-md-3 mt-step-col done step-width ">
                                                                    <div class="mt-step-number bg-white">2</div>
                                                                    <div class="mt-step-title uppercase font-grey-cascade">环境检测</div>
                                                                </div>
                                                                <div class="col-md-3 mt-step-col  active step-width">
                                                                    <div class="mt-step-number bg-white">3</div>
                                                                    <div class="mt-step-title uppercase font-grey-cascade">参数配置</div>
                                                                </div>
                                                                <div class="col-md-3 mt-step-col  step-width">
                                                                    <div class="mt-step-number bg-white">4</div>
                                                                    <div class="mt-step-title uppercase font-grey-cascade">开始安装</div>
                                                                </div>
                                                                 <div class="col-md-3 mt-step-col step-width last">
                                                                    <div class="mt-step-number bg-white">5</div>
                                                                    <div class="mt-step-title uppercase font-grey-cascade">安装完成</div>
                                                                </div>
                                                            </div>
                                                            
<script language="javascript" type="text/javascript">
<!--
	$(document).ready(function(){
		$('#dbPwd').blur();
		$(".table tr").each(function(){ $(this).children("td").eq(0).addClass("on");});
		$("input[type='text']").addClass("input_blur").mouseover(function(){ $(this).addClass("input_focus");}).mouseout(function(){$(this).removeClass("input_focus");});
		$(".table tr").mouseover(function(){ $(this).addClass("mouseover");}).mouseout(function(){$(this).removeClass("mouseover");	});
	});
	$.ajaxSetup ({ cache: false });
	function TestDbPwd()
	{
		var dbHost = $('#dbHost').val();
		var dbUser = $('#dbUser').val();
		var dbPwd = $('#dbPwd').val();
		var dbName = $('#dbName').val();
		var dbPort = $('#dbPort').val();
		data={'dbHost':dbHost,'dbUser':dbUser,'dbPwd':dbPwd,'dbName':dbName,'dbPort':dbPort};
		var url =  "./index.php?step=3&testdbpwd=1";
		$.ajax({
			 type: "POST",
			 url: url,
			 data: data,
			 beforeSend:function(){				 
			 },
			 success: function(msg){
				 if(msg){
					$('#pwd_msg').html("数据库配置正确");
					$('#dosubmit').attr("disabled",false);
					$('#dosubmit').removeAttr("disabled");				
					$('#dosubmit').removeClass("nonext");
				}else{
					$('#dosubmit').attr("disabled",true);
					$('#dosubmit').addClass("nonext");
					$('#pwd_msg').html("数据库链接配置失败");	
				}
			},
			complete:function(){
			},
			error:function(){
				$('#pwd_msg').html("数据库链接配置失败");						
			}
		});
	}

-->
</script>                                                            
            
                                                                                                                                                                                                                                                                                                                                                       
       <div class="wrap">
	   <div class="row">
                                            <div class="col-md-12">
                                                <!-- BEGIN VALIDATION STATES-->
                                                <div class="portlet light portlet-fit portlet-form ">
                                                    <div class="portlet-body">
                                                        <!-- BEGIN FORM-->
                                                  
                                                          <form action="index.php?step=4" id="defaultForm" method="post" class="form-horizontal" novalidate> 
                                                            <div class="form-body">
															<h3 class="form-section">填写数据库信息</h3>                                                               
                                                               <div class="form-group">
                                                                    <label class="control-label col-md-3">数据库主机
                                                                        <span class="required"> * </span>
                                                                    </label>
                                                                    <div class="col-md-4">
                                                                        <input type="text" name="dbHost" id="dbHost"  class="required form-control font-dark" style="font-size: 1em;" placeholder="一般为localhost" value="localhost"  /></div>
                                                                </div>
                                                                
															  <div class="form-group">
                                                                    <label class="control-label col-md-3">数据库端口
                                                                        <span class="required"> * </span>
                                                                    </label>
                                                                    <div class="col-md-4">
                                                                        <input type="text"  name="dbPort"  id="dbPort"  data-required="1" class="digits form-control font-dark" style="font-size: 1em;" placeholder="一般为3306" value="3306"  /> </div>
                                                                </div>
                                                                <div class="form-group">
                                                                    <label class="control-label col-md-3">数据库名称
                                                                        <span class="required"> * </span>
                                                                    </label>
                                                                    <div class="col-md-4">
                                                                        <input type="text"  name="dbName"  id="dbName"  data-required="1" class="required form-control font-dark" style="font-size: 1em;" placeholder="您的数据库名称" value=""  /> </div>
                                                                </div>
                                                                <div class="form-group">
                                                                    <label class="control-label col-md-3">数据库用户
                                                                        <span class="required"> * </span>
                                                                    </label>
                                                                    <div class="col-md-4">
                                                                        <input type="text"  name="dbUser"  id="dbUser"  data-required="1" class="required form-control font-dark" style="font-size: 1em;" placeholder="您的数据库用户名" value="root"/> </div>
                                                                </div> 
                                                                <div class="form-group">
                                                                    <label class="control-label col-md-3">数据库密码
                                                                        <span class="required"> * </span>
                                                                    </label>
                                                                    <div class="col-md-4">
                                                                        <input type="password"  name="dbPwd"  id="dbPwd"  data-required="1" class="required form-control font-dark" style="font-size: 1em;" placeholder="您的数据库密码" onblur="TestDbPwd()"/> </div>
                                                                          <div class="col-md-4">                                                <span class="font-red" id='pwd_msg'></span> </div>  
                                                                    </div>  
                                                                <div class="form-group">
                                                                    <label class="control-label col-md-3">数据表前缀
                                                                        <span class="required"> * </span>
                                                                    </label>
                                                                    <div class="col-md-4">
                                                                        <input type="text"  name="dbPrefix"  id="dbPrefix"  data-required="1" class="required form-control font-dark" style="font-size: 1em;" placeholder="如无特殊需要,请不要修改" value="yunyecms_"/> </div>
                                                                </div>    
                                                                <h3 class="form-section">填写管理员帐号</h3>   
                                                          
                                                               <div class="form-group">
                                                                    <label class="control-label col-md-3">管理员用户名
                                                                        <span class="required"> * </span>
                                                                    </label>
                                                                    <div class="col-md-4">
                                                                        <div class="input-group">
                                                                            <span class="input-group-addon">
                                                                                <i class="fa fa-user"></i>
                                                                            </span>
                                                                            <input type="text" class="form-control" value="admin" id="username" name="username" placeholder="管理员用户名"> </div>
                                                                    </div>
                                                                </div> 
															    <div class="form-group">
                                                                    <label class="control-label col-md-3">密　码：
                                                                        <span class="required"> * </span>
                                                                    </label>
                                                                    <div class="col-md-4">
                                                                        <input type="password"  name="password"  id="password"  data-required="1" class="required form-control font-dark" style="font-size: 1em;" placeholder="您的用户密码" /> </div>
                                                                         <div class="col-sm-5">
                      <span class="help-block"><span class="font_red14"> * </span> 密码设置6位以上，且密码不能包含：$ & * # < > ' " / \ % ; 空格 </span>
                    </div>

                                                                 </div> 
																<div class="form-group">
                                                                    <label class="control-label col-md-3">管理员邮箱
                                                                        <span class="required"> * </span>
                                                                    </label>
                                                                    <div class="col-md-4">
                                                                        <div class="input-group">
                                                                            <span class="input-group-addon">
                                                                                <i class="fa fa-envelope"></i>
                                                                            </span>
                                                                            <input type="email" class="form-control" id="email" name="email" value="kefu@xxx.com" placeholder="管理员邮箱"> </div>
                                                                    </div>
                                                                </div>  
                                                           <h3 class="form-section">网站基本信息</h3>   
																<div class="form-group">
                                                                    <label class="control-label col-md-3">网站名称：
                                                                        <span class="required"> * </span>
                                                                    </label>
                                                                       <div class="col-md-4">
                                                                        <input type="text"  name="sitename"  id="sitename"   data-required="1" class="required form-control font-dark" style="font-size: 1em;" placeholder="网站名称" value="某某建筑五金制品有限公司"  /></div>
                                                                </div>  
                                                                
															<div class="form-group">
                                                                    <label class="control-label col-md-3">关键词：
                                                                        <span class="required"> * </span>
                                                                    </label>
                                                                    <div class="col-md-6">
                <textarea name="seokeywords" class="required form-control font-dark" rows="3"   id="seokeywords" >五金、建筑、门窗五金、执手、滑撑、条锁、合页、拉手
                </textarea>
                                                                        </div>
                                                                </div>  
                                                                                                                      
                                                                
															<div class="form-group">
                                                                    <label class="control-label col-md-3">描述：
                                                                        <span class="required"> * </span>
                                                                    </label>
                                                                    <div class="col-md-6">
                <textarea  class="required form-control font-dark" rows="4"  id="seodescription" name="seodescription">某某建筑五金制品有限公司，创建于1981年，三十多年来致力于铝合金门窗五金系统产品的研发、生产及销售，是现时中国最大的铝塑门窗配件生产企业，也是国内唯一一家同时拥有门窗五金配件及密封配件双重研发生产和大型制造能力的企业，是门窗配件领域最具影响力品牌，同时也是行业标准的起草参编企业之一。</textarea>
                                                                        </div>
                                                                </div>                                                                                                                   
                                                                                                                                                                                                                                     
                                                            </div>
                                                            <div class="form-actions">
                                                                <div class="row">
                                                                    <div class="col-md-offset-3 col-md-9">
                                                                        <button type="button" onclick="history.back();"  class="btn grey-salsa btn-outline btn-lg" ><i class="icon-action-undo"></i> 上一步</button>
                                                                          <button type="submit" style="margin-left: 20px;" class="btn btn-lg  btn-primary btn-flat" disabled  id="dosubmit" > <i class="icon-check"></i> 下一步</button>
                                                                        
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </form>
                                                        <!-- END FORM-->
                                                    </div>
                                                </div>
                                                <!-- END VALIDATION STATES-->
                                            </div>
                                        </div>
	   
	   
	   
	   

       
 

</div>
                                                        </div>
       
      </div>
    </section>
    <!-- /.content -->
    <div class="clearfix"></div>
  </div>
  <!-- /.content-wrapper -->
	<?php include_once ('./inc/foot.tpl'); ?>
 </div>
</div>
<!-- ./wrapper -->
<!-- jQuery 2.2.3 -->
<script src="../public/ui/plugins/jQuery/jquery-2.2.3.min.js"></script>
<!-- Bootstrap 3.3.6 -->
<script src="../public/ui/bootstrap/js/bootstrap.min.js"></script>
<!-- FastClick -->
<script src="../public/ui/plugins/fastclick/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="../public/ui/dist/js/app.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="../public/ui/dist/js/demo.js"></script>
<script type="text/javascript" src="../public/ui/validator/dist/js/bootstrapValidator.js"></script>
<script type="text/javascript">
$(document).ready(function() {
    $('#defaultForm').bootstrapValidator({
        message: 'This value is not valid',
//        live: 'disabled',
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
			dbName: {
                validators: {
                    notEmpty: {
                        message: '数据库名称不能为空'
                    }
                }
            },
				username: {
                validators: {
                    notEmpty: {
                        message: '管理员用户名不能为空'
                    },
					 stringLength: {
                        min: 5,
                        max: 30,
                        message: '用户名至少是5位，且小于30个字符'
                    },
                    different: {
                        field: 'password',
                        message: '用户名和密码不能相同'
                    }
                }
            },
			 password: {
                validators: {
                    notEmpty: {
                        message: '密码不能为空'
                    },
                    different: {
                        field: 'username',
                        message: '用户名和密码不能相同'
                    }
                }
            },
			
          }
    });
});
</script>

</body>
</html>
