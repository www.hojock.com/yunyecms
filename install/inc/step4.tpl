
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title><?php echo $steps[$step];?>-云业内容管理系统2.0</title>
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="../public/ui/bootstrap/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link href="../public/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" />
  <link rel="stylesheet" href="../public/ui/plugins/font-awesome-4.7.0/css/font-awesome.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="../public/ui/dist/css/AdminLTE.min.css">

  <link rel="stylesheet" href="../public/ui/dist/css/skins/_all-skins.min.css">
  <link href="../public/ui/dist/css/install.css" rel="stylesheet" type="text/css" />
  <link href="../public/ui/dist/css/step.css" rel="stylesheet" type="text/css" />
  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
</head>
<body class="hold-transition layout-top-nav skin-blue ">
<div class="wrapper">
	<?php include_once ('./inc/head.tpl'); ?>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
   <div class="container">
    <!-- Main content -->
    <section class="invoice">
      <!-- title row -->
      <div class="row">
        <div class="col-xs-12">
          <h2 class="page-header">
            <i class="fa fa-bullhorn font-blue"></i>第 <?php echo $step;?> 步：<?php echo $steps[$step];?>
          </h2>
        </div>
        <!-- /.col -->
      </div>
      <!-- Table row -->
      <div class="row">
       <div class="mt-element-step">
                                                      
                                                            <div class="row step-line">
                                                                <div class="col-md-3 mt-step-col first  done step-width">
                                                                    <div class="mt-step-number bg-white">1</div>
                                                                    <div class="mt-step-title uppercase font-grey-cascade">安装须知</div>
                                                                </div>
                                                                <div class="col-md-3 mt-step-col step-width done">
                                                                    <div class="mt-step-number bg-white">2</div>
                                                                    <div class="mt-step-title uppercase font-grey-cascade">环境检测</div>
                                                                </div>
                                                                <div class="col-md-3 mt-step-col step-width done">
                                                                    <div class="mt-step-number bg-white">3</div>
                                                                    <div class="mt-step-title uppercase font-grey-cascade">参数配置</div>
                                                                </div>
                                                                <div class="col-md-3 mt-step-col  step-width active">
                                                                    <div class="mt-step-number bg-white">4</div>
                                                                    <div class="mt-step-title uppercase font-grey-cascade">开始安装</div>
                                                                </div>
                                                                 <div class="col-md-3 mt-step-col step-width last">
                                                                    <div class="mt-step-number bg-white">5</div>
                                                                    <div class="mt-step-title uppercase font-grey-cascade">安装完成</div>
                                                                </div>
                                                            </div>
       
                                                       
        	<div class="wrap">
	 
		<div class="well well-lg text-center" id="setupinfo">
				<h1 class="text-center">正在安装</h1>

		正在开始安装...<br>
		</div>
		  <div class="text-center"><button type="button" onclick="window.location.href='index.php?step=3';"   class="btn btn-lg  btn-default btn-flat" ><i class="icon-action-undo"></i> 上一步</button>
          <button type="submit" style="margin-left: 20px;" class="btn btn-lg  btn-primary btn-flat" disabled  id="dosubmit" > <i class="icon-check"></i> 下一步</button></div>
		
	</div>        
                                                       
                                     
                                                       
                                                       
                                                        </div>
       
      </div>
    </section>
    <!-- /.content -->
    <div class="clearfix"></div>
  </div>
  <!-- /.content-wrapper -->
	<?php include_once ('./inc/foot.tpl'); ?>
 </div>
</div>
<!-- ./wrapper -->
<!-- jQuery 2.2.3 -->
<script src="../public/ui/plugins/jQuery/jquery-2.2.3.min.js"></script>
<!-- Bootstrap 3.3.6 -->
<script src="../public/ui/bootstrap/js/bootstrap.min.js"></script>
<!-- FastClick -->
<script src="../public/ui/plugins/fastclick/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="../public/ui/dist/js/app.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="../public/ui/dist/js/demo.js"></script>

<script language="javascript" type="text/javascript">
var n=0;
var data = <?php echo json_encode($_POST);?>;
$.ajaxSetup ({ cache: false });
function reloads(n) {
		var url =  "./index.php?step=4&install=1&n="+n;
		$.ajax({
			 type: "POST",		
			 url: url,
			 data: data,
			 dataType: 'json',
			 beforeSend:function(){
			 },
			 success: function(msg){
				 console.log(msg);
				if(msg.n=='999999'){
					$('#setupinfo').append(msg.msg);
					$('#dosubmit').attr("disabled",false);
					$('#dosubmit').removeAttr("disabled");				
					$('#dosubmit').removeClass("nonext");
					setTimeout('gonext()',2000);
					return false;
				}
				if(msg.n || msg.n==0){
					$('#setupinfo').append(msg.msg);
					reloads(msg.n);
				}else{
					 //alert('指定的数据库不存在，系统也无法创建，请先通过其他方式建立好数据库！');
					 alert(msg.msg);
				}			 
			}
		});
}
function gonext(){
	window.location.href='index.php?step=5';
}
$(document).ready(function(){
		reloads(n);
})
</script>   
</body>
</html>
