// JavaScript Document
 
function  ConfirmDel (key,value,modaltitle,modalbody,id,strcss){
var modaltitle_arg = arguments[2] ? arguments[2] : "删除确认";
var modalbody_arg = arguments[3] ? arguments[3] : "您确定要删除该信息吗？";
var id = arguments[4] ? arguments[4] : "delmodal";
var strcss = arguments[5] ? arguments[5] : "modal modal-primary";
curmodal=$("#"+id);
curmodal.modal('show');
curmodal.attr("class",strcss);
curmodal.find('.modal-title').text(modaltitle_arg); 
curmodal.find('.modal-body').find("p").html(modalbody_arg);
curmodal.data(key,value);
}

function  CloseAndJump(key,id){
var id = arguments[1] ? arguments[1] : "delmodal";
curmodal=$("#"+id);
curmodal.modal('hide');
key_value=curmodal.data(key);
location.href = key_value;
}

function  CloseAndSubmit(key,id,formid){
var id = arguments[1] ? arguments[1] : "delmodal";
curmodal=$("#"+id);
curmodal.modal('hide');
key_value=curmodal.data(key);
curform=$("#"+formid);
if(key_value){ curform.attr("action",key_value);}
curform.submit();
}

function  AlertModal (modaltitle,modalbody,id){
var modaltitle_arg = arguments[0] ? arguments[0] : "提示信息：";
var modalbody_arg = arguments[1] ? arguments[1] : "";
var id = arguments[2] ? arguments[2] : "alertmodal";
curmodal=$("#"+id);
curmodal.modal('show');
curmodal.find('.modal-title').text(modaltitle_arg); 
curmodal.find('.modal-body').find("p").html(modalbody_arg);
}

function  CloseModal(id){
var id = arguments[1] ? arguments[1] : "alertmodal";
curmodal=$("#"+id);
curmodal.modal('hide');
}
				
				
