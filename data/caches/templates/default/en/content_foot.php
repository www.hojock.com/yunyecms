<?php defined('IN_YUNYECMS') or exit('No permission.'); ?>    <!-- Footer -->
       <footer class="footer">
        <div class="pt-50 pb-30 foot-bg">
            <div class="container">
                <div class="row">
                    <!-- Footer widget -->
                     <?php $tag_parse = core::load_class('tag_parse');$data=$tag_parse->cat_tag(array('catid'=>'1,3,6,32','level'=>'2'));?>
                         <?php if(is_array($data)): $i = -1; $__DATA__ = $data;if(count($__DATA__)==0 )  echo "" ;foreach($__DATA__ as $key=>$v): $mod = ($i % 2 );++$i;?>
						 <?php if($key==2){ $footcsswidth='footwidth2';} else{ $footcsswidth='footwidth';}?>
						<div class="col-lg-2 col-sm-6 <?php echo $footcsswidth;?> ">
						    <div class="footer-widget" data-animate="fadeInUp" data-delay=".3">
							 <h3 class="footer-title"><a href="<?php echo $v["url"] ;?>"><?php echo $v["title"] ;?></a></h3>
							  <?php if(!empty($v["child"])): ?>  	
							 <div class="menu-wrap">
                                <ul class="menu">
							 <?php if(is_array($v['child'])): $k2 = -1; $__DATA__ = $v['child'];if(count($__DATA__)==0 )  echo "" ;foreach($__DATA__ as $key=>$v): $mod = ($k2 % 2 );++$k2;?>
								<li><a href="<?php echo $v["url"] ;?>"> <?php echo $v["title"] ;?></a></li>
								 </volist>
								<?php if(count($v["child"])>=12) echo '<li><a href="'.$v[url].'"><i class="fa fa-plus"></i> See All >> </a> </li>';?>
							 <?php  endforeach;  endif; ?>
                            </div>
							 <?php endif; ?>  
						    </div>
						      </div>
						  <?php  endforeach;  endif; ?>
					              
					 <div class="col-lg-2 col-sm-6 footwidthlast">
                        <div class="footer-widget" data-animate="fadeInUp">
                            <h3 class="footer-title">Contact Us</h3>
                            <div class="menu-wrap">
                            <ul class="menu">
								<li>Tel：<a href="tel:<?php echo $lang["tel"] ;?>"><?php echo $lang["tel"] ;?></a></li>
								<li>E-mail：<a href="mailto:<?php echo $lang["mail"] ;?>"><?php echo $lang["mail"] ;?></a></li>
								<li>Mobile：<a href="tel:<?php echo $lang["mobile"] ;?>"><?php echo $lang["mobile"] ;?></a></li>
								<li>Address：<?php echo $lang["address"] ;?></li>
                            </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-2 col-sm-6">
                        <div class="footer-widget" data-animate="fadeInUp" data-delay=".3">
                            <h3 class="footer-title text-center">Fllow Us</h3>
                              <div style="width: 100%; line-height: 2.5;" class="text-center">
                       	        <img src="<?php echo $lang["qrcode"] ;?>" alt="" style="width:80%;"/>
                       	        <br/>
                       	         Official WeChat
                       	      </div>
                        </div>
                    </div>
                    <!-- End of Footer widget -->
                </div>
            </div>
        </div>
        <div class="bottom-footer foot-bg">
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-lg-8 order-last order-lg-first">
                        <!--Start footer text -->
                        <div class="copyright text-center text-lg-left">
                           <?php echo $lang["copyright"] ;?>
                        </div>
                        <!-- End Footer text -->
                              <div class="footer-widget">
                                <ul class="social-icons nav mt-10" style="font-size: 20px;">
                                    <li><a href="http://wpa.qq.com/msgrd?v=3&amp;uin=527150978&amp;site=qq&amp;menu=yes" target="_blank"><i class="fa fa-qq"></i></a></li>
                                    <li><a href="#" target="_blank"><i class="fa fa-weibo"></i></a></li>
                                    <li><a href="#" target="_blank"><i class="fa fa-weixin"></i></a></li>
                                    <li><a href="#" target="_blank"><i class="fa fa-plus-circle"></i></a></li>
                                </ul>
                            </div>
                    </div>
                    <div class="col-lg-4 order-first order-lg-last" style="text-align: right;">
                           <select onchange="javascript:if(this.value!='')window.open(this.value)" style="border: #ccc 1px solid; background: none; width: 80%; color:#aaaaaa; line-height: 45px; padding: 5px;"><option selected="" value="" style="color:#000;">Links</option>
                            	<?php $tag_parse = core::load_class('tag_parse');$data="";$data=$tag_parse->list_tag(array('catid'=>'45'));?>
								 <?php if(is_array($data)): $i = -1; $__DATA__ = $data;if(count($__DATA__)==0 )  echo "" ;foreach($__DATA__ as $key=>$v): $mod = ($i % 2 );++$i;?>
                                  <option value="<?php echo $v["url"] ;?>" style="color:#000;"> <?php echo $v["title"] ;?></option> 
								 <?php  endforeach;  endif; ?>
								  
                           </select>
                    </div>
                </div>
            </div>
        </div>
    </footer>
    <!-- End of Footer -->
     <!-- Back to top -->
    <div class="back-to-top">
        <a href="#" class="d-flex align-items-center justify-content-center">
            <img src="<?php echo TPL;?>static/images/up-arrow.svg" alt="" class="svg">
        </a>
    </div>