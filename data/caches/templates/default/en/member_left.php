<?php defined('IN_YUNYECMS') or exit('No permission.'); ?>	<aside>
                         <!-- start single sidebar widget -->
                        <div class="catebox widget_categories mb-30" data-animate="fadeInUp">
                            <h3 class="category-title">Member Center </h3>
                            <ul>
                            <li><a href="<?php echo url('member/member/myinfo');?>" <?php if(ROUTE_A=='myinfo') { ?> class="active" <?php } ?>  >  <span> <i class="fa fa-user"></i> My Profile</span></a></li>
							<li> <a href="<?php echo url('shop/cart/index');?>"  <?php if(ROUTE_C=='cart') { ?> class="active" <?php } ?>  > <span> <i class="fa fa-shopping-cart"></i> Shopping cart</span></a></li>
							 <li> <a href="<?php echo url('shop/orders/index');?>" <?php if(ROUTE_C=='orders') { ?> class="active" <?php } ?>  > <span> <i class="fa fa-list"></i> My Order</span></a></li>
							<li> <a href="<?php echo url('member/notice/index');?>" <?php if(ROUTE_C=='notice') { ?> class="active" <?php } ?>  > <span> <i class="fa fa-bullhorn"></i> Notice</span></a></li>
							<?php $tag_parse = core::load_class('tag_parse');$data=$tag_parse->cat_tag(array('pid'=>'11'));?>
								 <?php if(is_array($data)): $i = -1; $__DATA__ = $data;if(count($__DATA__)==0 )  echo "" ;foreach($__DATA__ as $key=>$v): $mod = ($i % 2 );++$i;?>
									 <li><a href="<?php echo url('member/member/customform',array('catid'=>$v['id']));?>" <?php if(ROUTE_A=='customform') { ?> class="active" <?php } ?>>  <i class="fa fa-commenting-o"></i> My <?php echo $v["title"] ;?> </a>
									 </li>
								 <?php  endforeach;  endif; ?>
							 
							 <li> <a href="<?php echo url('member/member/pwd');?>" <?php if(ROUTE_A=='pwd') { ?> class="active" <?php } ?>   >   <span> <i class=" fa fa-lock"></i> Change Password</span></a></li>
							 <li> <a href="<?php echo url('member/member/logout');?>" <?php if(ROUTE_A=='logout') { ?> class="active" <?php } ?>  >   <span> <i class="fa fa-sign-out"></i> Sign out</span></a></li>
                           </ul>
                        </div>
                        <!-- End of single sidebar widget -->
   </aside>