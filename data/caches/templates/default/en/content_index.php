<?php defined('IN_YUNYECMS') or exit('No permission.'); ?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- Document Title -->
	<title><?php echo $seo["title"] ;?></title>
	<meta name="keywords" content="<?php echo $seo["keywords"] ;?>" />
	<meta name="description" content="<?php echo $seo["description"] ;?>" />
       <!-- Favicon -->
    <link rel="shortcut icon" type="image/png" href="favicon.png">
    <link href="<?php echo YUNYECMS_PUBLIC;?>plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" />
    <!-- CSS Files -->
    <link rel="stylesheet" href="<?php echo TPL;?>static/css/font-awesome.min.css">
    <!--==== Bootstrap css file ====-->
    <link rel="stylesheet" href="<?php echo TPL;?>static/css/bootstrap.min.css">
    <!--==== Swiper css file ====-->
    <link rel="stylesheet" href="<?php echo TPL;?>static/css/swiper.min.css">
    <!--==== Style css file ====-->
    <link rel="stylesheet" href="<?php echo TPL;?>static/css/style.css?201901">
    <!--==== Responsive css file ====-->
    <link rel="stylesheet" href="<?php echo TPL;?>static/css/responsive.css?201901">
    <!--==== Theme Color 1 css file ====-->
    <link rel="stylesheet" href="<?php echo TPL;?>static/css/theme-color-1.css">
    <!--==== Custom css file ====-->
    <link rel="stylesheet" href="<?php echo TPL;?>static/css/custom.css">
    <link rel="shortcut icon" href="<?php echo YUNYECMS_PUBLIC;?>img/favicon.ico" /> 
</head>
<body>
    
    <!-- Main header -->
      <?php include tpl("head"); ?>
    <!-- End of Main header -->
    <!-- Banner -->
<section class="slider">
        <div class="main-slider swiper-container">
            <div class="swiper-wrapper">
                <!-- Single slide -->
					<?php $tag_parse = core::load_class('tag_parse');$data="";$data=$tag_parse->list_tag(array('catid'=>'41','pic'=>'1','order'=>'ordernum asc'));?>
								 <?php if(is_array($data)): $i = -1; $__DATA__ = $data;if(count($__DATA__)==0 )  echo "" ;foreach($__DATA__ as $key=>$v): $mod = ($i % 2 );++$i;?>
                      <div class="swiper-slide position-relative">
                    <!-- slider img -->
                    <a href="javascript:void(0)" target="_blank"><img src="<?php echo $v["pic"] ;?>" data-rjs="2" alt="Slider Img"></a>
                    <!-- End of Slider img -->
                    <div class="container">
                        <div class="row align-items-center">
                            <div class="col-xl-6 col-lg-8">
                                <!-- Start Slider Content -->
                                <div class="slide-content">
                                   
                                </div>
                                <!-- End Slider Content -->
                            </div>
                        </div>
                    </div>
                </div>
				
				
								 <?php  endforeach;  endif; ?>
					  
                <!-- End of Single slide -->
            </div>
            <!-- Banner Pagination -->
            <div class="swiper-pagination main-slider-pagination slider-pagination-style" ></div>
        </div>
    </section>    
    
    
    <!-- End of Banner -->

    <!-- 关于我们开始 -->
    <section class="about" > 
        <div class="container">
            <div class="row">
			   <div class="col-xl-12 col-lg-8 pt-80">
                    <div class="section-title text-center center" data-animate="fadeInUp">
                        <h2>About us</h2>
                         <p>Adhere to the whole heart to provide quality service to customers</p>
                    </div>
                </div>  
                 <?php $find_parse = core::load_class('tag_parse');$data=$find_parse->find_tag(array('catid'=>'43'));?>
                <div class="col-xl-6 col-lg-6 col-md-6">
                      <img src=" <?php echo $data["pic"] ;?>">
                </div> 
                <div class="col-xl-6 col-lg-6 col-md-6" style="flex: 0 0 auto">
                     <div class="title"> <?php echo $data["summary"] ;?></div>
                     <div class="about-info">  
                        <?php echo $data["content"] ;?>
                     </div>
                   <a href="<?php echo url('singlepage',array('catid'=>2))?>" class="btn btn-primary btn-flat  animated fadeInUp" >More+</a>
                </div> 
              
            </div>
       
            
        </div>
    </section>
    <!-- 关于我们结束 -->
   
    <!-- 产品展示开始 -->
    <section class="pt-80 products" >
        <div class="container">
            <div class="row align-items-center">
            	 <div class="col-xl-12 col-lg-8">
                    <div class="section-title text-center center" data-animate="fadeInUp">
                        <h2>Product center</h2>
                         <p>Concentrate, focus, professional</p>
                    </div>
                </div>  
            </div>
            <div class="row">
                <div class="col">
                    <!-- Service Carousel -->
                    <div class="swiper-container-wrap" data-animate="fadeInUp" data-delay=".5">
                        <div class="service-carousel swiper-container">
                            <div class="swiper-wrapper">
								<?php $tag_parse = core::load_class('tag_parse');$data="";$data=$tag_parse->list_tag(array('catid'=>'6','num'=>'16','isgood'=>'1'));?>
								 <?php if(is_array($data)): $i = -1; $__DATA__ = $data;if(count($__DATA__)==0 )  echo "" ;foreach($__DATA__ as $key=>$v): $mod = ($i % 2 );++$i;?>
									  <div class="swiper-slide">
														<!-- Single Service Carousel -->
														<div class="single-service mb-30 text-center">
														   <a href="<?php echo $v["url"] ;?>" target="_blank" > <img src="<?php echo $v["pic"] ;?>" class="pic"></a>
															 <a href="<?php echo $v["title"] ;?>" class="link-title"> <?php echo strcut($v["title"],15);?></a>
														</div>
														<!--End Single Service Carousel -->
									  </div> 
								 <?php  endforeach;  endif; ?>
								  
                             </div>
                        </div>
                    </div>
                    
                    <!-- End of Service Carousel -->
                </div>
            </div>
               <div class="col-xl-12 col-lg-12 col-md-12 carousel-control-d-pc">
                    <ul class="service-controls nav justify-content-end" data-animate="fadeInUp" data-delay=".3" style="width: 40px; float:left; margin-left: -100px; margin-top: -200px;">
                        <li class="prev-service carousel-control d-flex align-items-center justify-content-center">
                            <img src="<?php echo TPL;?>static/images/left-arrow.svg" alt="" class="svg">
                        </li>
                    </ul>
                </div>
                  <div class="col-xl-12 col-lg-12 col-md-12 carousel-control-d-pc">
                    <ul class="service-controls nav justify-content-end" data-animate="fadeInUp" data-delay=".3" style="width: 40px; float:right; margin-right: -100px; margin-top: -200px;">
                        <li class="next-service carousel-control d-flex align-items-center justify-content-center">
                            <img src="<?php echo TPL;?>static/images/right-arrow.svg" alt="" class="svg">
                        </li>
                    </ul>
                </div>
                <div class="col-xl-12 col-lg-12 col-md-12 carousel-control-d-mobile">
                    <ul class="service-controls nav justify-content-end" data-animate="fadeInUp" data-delay=".3">
                        <li class="prev-service carousel-control d-flex align-items-center justify-content-center">
                            <img src="<?php echo TPL;?>static/images/left-arrow.svg" alt="" class="svg">
                        </li>
                        <li class="next-service carousel-control d-flex align-items-center justify-content-center">
                            <img src="<?php echo TPL;?>static/images/right-arrow.svg" alt="" class="svg">
                        </li>
                    </ul>
                </div>
        </div>
    </section>
    <!-- 产品展示结束 -->

    <!-- 成功案例开始 -->
    <section class="pt-30 cases"  >
        <div class="container">
            <div class="row align-items-center">
            	 <div class="col-xl-12 col-lg-8">
                    <div class="section-title text-center center" data-animate="fadeInUp">
                        <h2>Case</h2>
                           <p>Concentrate, focus, professional</p>
                    </div>
                </div>  
            </div>
            <div class="row" data-animate="fadeInUp">
                               	<?php $tag_parse = core::load_class('tag_parse');$data="";$data=$tag_parse->list_tag(array('catid'=>'32','num'=>'20','isgood'=>'1'));?>
								 <?php if(is_array($data)): $i = -1; $__DATA__ = $data;if(count($__DATA__)==0 )  echo "" ;foreach($__DATA__ as $key=>$v): $mod = ($i % 2 );++$i;?>
								<div class="col-md-3 col-sm-6">
								 <div class="single-cases mb-30 text-center">
								   <a href="<?php echo $v["url"] ;?>"><img src="<?php echo $v["pic"] ;?>" class="pic"></a>
								   <a href="<?php echo $v["url"] ;?>" class="link-title"> <p><?php echo strcut($v["title"],15);?></p> </a>
								 </div>
							     </div> 
								 <?php  endforeach;  endif; ?>
								  
                                <div class="col-xl-12 col-lg-12 text-center">
                                <a href="<?php echo url('lists',array('catid'=>32))?>" class="btn btn-sm  btn-transparent animated fadeInUp text-white" style="border:#fff 1px solid; margin-top: 10px;">More+</a>
                    </div>                                                          
            </div>
        </div>
    </section>
    <!-- 成功案例结束 -->

    <!-- News -->
    <section class="pt-30 pb-40 news-bg">
        <div class="container">
                <div class="row align-items-center">
            	 <div class="col-xl-12 col-lg-8">
                    <div class="section-title text-center center" data-animate="fadeInUp">
                        <h2>News</h2>
                         <p>Keep up with the times</p>
                    </div>
                </div>  
            </div>
            <div class="row">
                <div class="col">
                    <!-- News carousel -->
                    <div class="swiper-container-wrap" data-animate="fadeInUp" data-delay=".3">
                        <div class="news-carousel swiper-container">
                            <div class="swiper-wrapper">
                                <!-- Single news -->
                               	<?php $tag_parse = core::load_class('tag_parse');$data="";$data=$tag_parse->list_tag(array('catid'=>'3','num'=>'3','isgood'=>'1'));?>
								 <?php if(is_array($data)): $i = -1; $__DATA__ = $data;if(count($__DATA__)==0 )  echo "" ;foreach($__DATA__ as $key=>$v): $mod = ($i % 2 );++$i;?>
                           <div class="swiper-slide">
											  <div class="single-post" data-animate="fadeInUp">
							<a href="<?php echo $v["url"] ;?>" target="_blank">
								<ul class="post-date text-center m-0 p-0 list-unstyled">
									<li><span><?php echo date("m",$v["addtime"]);?></span> <?php echo date("d",$v["addtime"]);?></li>
									<li><?php echo date("Y",$v["addtime"]);?></li>
								</ul>
							</a>
							<a href="<?php echo $v["url"] ;?>" target="_blank"> <img src="<?php echo $v["pic"] ;?>" data-rjs="2" alt="<?php echo $v["title"] ;?>"></a>
							<div class="post-content">
								<h2 class="h4">
									<a href="<?php echo $v["url"] ;?>" target="_blank"><?php echo $v["title"] ;?></a>
								</h2>
								<hr>
								<p><?php echo strcut(strip_tags($v["content"]),100);?></p>
								<a href="<?php echo $v["url"] ;?>" target="_blank" class="btn btn-transparent">More+</a>
							</div>
						</div>
									</div>  
                            <?php  endforeach;  endif; ?>
								   
                                                         
                                                                                       
                                                                                                                     
                                                                                                                                                                                 
                            </div>
                        </div>
                    </div>
                    <!-- End of News carousel -->
                    <!-- Banner Pagination -->
                    <div class="swiper-pagination news-pagination slider-pagination-style"></div>
                </div>
            </div>
        </div>
    </section>
    <!-- End of News -->
    <!-- Footer -->
   <?php include tpl('foot'); ?>
    <!-- End of Footer -->

    <!-- Back to top -->
    <div class="back-to-top">
        <a href="#" class="d-flex align-items-center justify-content-center">
            <img src="<?php echo TPL;?>static/images/up-arrow.svg" alt="" class="svg">
        </a>
    </div>
    <!-- JS Files -->
    <!-- ==== JQuery 3.3.1 js file==== -->
    <script src="<?php echo TPL;?>static/js/jquery-3.3.1.min.js"></script>

    <!-- ==== Bootstrap js file==== -->
    <script src="<?php echo TPL;?>static/js/bootstrap.bundle.min.js"></script>

    <!-- ==== JQuery Waypoint js file==== -->
    <script src="<?php echo TPL;?>static/js/jquery.waypoints.min.js"></script>

    <!-- ==== Sticky js file==== -->
    <script src="<?php echo TPL;?>static/js/sticky.min.js"></script>
    <!-- ==== Swiper js file==== -->
    <script src="<?php echo TPL;?>static/js/swiper.min.js"></script>
    <!-- ==== Parsley js file==== -->
    <script src="<?php echo TPL;?>static/js/parsley.min.js"></script>

    <!-- ==== Menu  js file==== -->
    <script src="<?php echo TPL;?>static/js/menu.min.js"></script>

    <!-- ==== Scrippt js file==== -->
    <script src="<?php echo TPL;?>static/js/scripts.js?20190123"></script>

    <!-- ==== Custom js file==== -->
<script src="<?php echo TPL;?>static/js/custom.js"></script>
</body>
</html>