<?php defined('IN_YUNYECMS') or exit('No permission.'); ?><header class="header">
        <!-- Start Header Top topbar -->
        <div class="header-top dark-bg" data-animate="fadeInDown" data-delay=".5">
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-md-5">
                        <!-- start Header Top Info  -->
                        <ul class="header-top-info nav pt-sans justify-content-center justify-content-md-start">
                            <li>Welcome to <?php echo $lang["sitename"] ;?>！</li>
                        </ul>
                         <!-- End Header Top Info  -->
                    </div>
                    <div class="col-md-7">
                       <!-- Start Header Top toolbar-->
                        <div class="header-top-settings-wrap">
                            <ul class="header-top-settings pt-sans nav justify-content-center justify-content-md-end">
                                <?php if($this->member) { ?>
				                <li class="dropdown dropdown-extended dropdown-notification " id="header_notification_bar">
                                            <a href="javascript:;" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                                                <i class="icon-bell"></i>
                                                <span class="badge badge-danger" style="font-weight: normal;"><?php echo $this->member['noticenum'];?></span>
                                                <i class="fa fa-angle-down"></i>
                                            </a>
                                            <ul class="dropdown-menu">
                                                <li class="external">
                                                    <h3>You have  <strong> <?php echo $this->member['noticenum'];?>  </strong> new messages</h3>
                                                    <a href="<?php echo url('member/notice/index');?>"> view all </a>
                                                </li>
                                                <ul class="dropdown-menu-list scroller" style="height: 250px;" data-handle-color="#637283">
	<?php $tag_parse = core::load_class('tag_parse');$data="";$data=$tag_parse->query_tag(array('sql'=>'select * from #yunyecms_notice where status=1 order by addtime desc','num'=>'10'));?>
		 <?php if(is_array($data)): $i = -1; $__DATA__ = $data;if(count($__DATA__)==0 )  echo "" ;foreach($__DATA__ as $key=>$v): $mod = ($i % 2 );++$i;?>
			   <li>
																	<a href="<?php echo url('member/notice/show',array('id'=>$v['id']));?>">
																		<span class="time pull-right"><?php echo udate($v['addtime']);?></span>
																		<span class="details">
																			<span class="label label-sm label-icon label-info">
																				<i class="fa fa-bullhorn"></i>
																			</span><?php echo $v["title"] ;?></span>
																			<div class="clearfix"></div>
																	</a>
			  </li>
		 <?php  endforeach;  endif; ?>
	          
                                                  </ul>
                                             </ul>
                                        </li>
                                        <li class="topcart">
                                            <a href="<?php echo url('shop/cart/index');?>">
                                                <i class="icon-basket"></i> <span class="text-white"><?php if($this->member['cartnum']) echo $this->member['cartnum'];?></span>
                                            </a>
                                        </li>
									    <li class="dropdown dropdown-user">
                                               <a href="javascript:;"  data-toggle="dropdown" data-hover="dropdown" data-close-others="true" aria-expanded="true">
                                                <i class="icon-user"></i> <?php echo $this->member['username'];?>  <i class="fa fa-angle-down"></i>
                                               </a>
                                              <ul class="dropdown-menu dropdown-menu-default">
                                                <li>
                                                    <a href="<?php echo url('member/member/index');?>">
                                                     <i class="icon-user"></i> Member Centre </a>
                                                </li>
                                                <li>
                                                    <a href="<?php echo url('member/member/myinfo');?>">
                                                     <i class="icon-user"></i> My Profile </a>
                                                </li>
                                                <li>
                                                    <a href="<?php echo url('shop/cart/index');?>">
                                                        <i class="icon-basket"></i> shopping cart
                                                        <span class="badge badge-danger"> <?php echo $this->member['cartnum'];?> </span>
                                                    </a>
                                                </li>
                                                 <li>
                                                    <a href="<?php echo url('member/notice/index');?>">
                                                        <i class="icon-volume-1"></i> My message
                                                        <span class="badge badge-danger">  <?php echo $this->member['noticenum'];?>   </span>
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="<?php echo url('shop/orders/index');?>">
                                                        <i class="fa fa-list"></i> My Order
                                                        <span class="badge badge-success"> <?php echo $this->member['ordernum'];?> </span>
                                                    </a>
                                                </li>
                                                <li class="divider"> </li>
                                                <li>
                                                    <a href="<?php echo url('member/member/pwd');?>">
                                                        <i class="icon-lock"></i> change Password </a>
                                                 </li>
                                                <li>
                                                    <a href="<?php echo url('member/member/logout');?>">
                                                        <i class="icon-key"></i> Sign out </a>
                                                </li>
                                            </ul>
                                        </li>                                
                                <?php } else { ?>
                                <li><a  href="<?php echo url('member/member/login');?>"  >  <i class="icon-user"></i> Login</a></li>
                                <li><a  href="<?php echo url('member/member/register');?>">  <i class="icon-user-follow "></i> Sign up</a></li>
                                <?php } ?>
                                  <?php $tag_parse = core::load_class('tag_parse');$data=$tag_parse->lang_tag(array());?>
									 <?php if(is_array($data)): $i = -1; $__DATA__ = $data;if(count($__DATA__)==0 )  echo "" ;foreach($__DATA__ as $key=>$v): $mod = ($i % 2 );++$i;?>
										<?php if($v['id']!=$lang['id']) { ?>
											<li><a  href="<?php echo $v["url"] ;?>"  >  <i class="icon-globe"></i> <?php echo $v["title_en"] ;?> </a></li>
										<?php } ?>
									 <?php  endforeach;  endif; ?>
								  
                            <!--    <li><a  href="javascript:"  onclick="SetHome(this,window.location)" >设为首页</a></li>
                                <li ><a href="javascript:" onclick="javascript:addFavorite2()">加为收藏</a></li>-->
                                <li class="bg-red" style="color:#fff; font-size: 18px;">
                                      <div  style="font-size: 15px; background: #fff; color:#2981ff; width: 22px; height: 22px; border-radius: 11px;"><i class="fa fa-phone" style="color:#2981ff; margin-left: 5px; margin-top: 4px;"></i></div>
                                      &nbsp;<?php echo $cfg["tel"] ;?>
                                </li>
                            </ul>
                        </div>
                        <!-- Start Header Top toolbar-->
                    </div>
                </div>
            </div>
        </div>
        <!-- End Header Top topbar -->
        <!-- Start Header Navbar-->
        <div class="main-header light-bg">
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-xl-4 col-lg-4 col-md-5 col-sm-10 col-10">
                        <div class="row" >
										 <!-- Logo -->
									<div class="logo" data-animate="fadeInUp" data-delay=".65">
										<a href="/">
											<img src="<?php echo $lang["logo"] ;?>" data-rjs="2" alt="<?php echo $lang["sitename"] ;?>">
										</a>
									</div>
									<!-- End of Logo -->
                        </div>
                    </div>
                    <div class="col-xl-8 col-lg-8 col-md-7 col-sm-2 col-2">
                        <div class="menu--inner-area clear-fix">
                            <div class="menu-wraper">
                                <nav data-animate="fadeInUp" data-delay=".8">
                                    <!-- Header-menu -->
                                    <div class="header-menu pt-sans">
                                    <ul>
                                        <li  <?php if(ROUTE_A=='index')  echo ' class="active"';	 ?> ><a href="<?php echo ROOT;?>index.php?lang=<?php echo $lang["id"] ;?>">Home</a></li>
											<?php $tag_parse = core::load_class('tag_parse');$data=$tag_parse->cat_tag(array('level'=>'3'));?>
											 <?php if(is_array($data)): $i = -1; $__DATA__ = $data;if(count($__DATA__)==0 )  echo "" ;foreach($__DATA__ as $key=>$v): $mod = ($i % 2 );++$i;?>
												<li <?php if(!empty($rootcatid)&&$rootcatid==$v['id'])  echo ' class="active"';	 ?>><a href="<?php echo $v["url"] ;?>"> <?php echo $v["title"] ;?> <span class="arrow"></span> </a>
												  <ul class="dropdown-menu pull-left">
														<?php if(is_array($v['child'])): $k2 = -1; $__DATA__ = $v['child'];if(count($__DATA__)==0 )  echo "" ;foreach($__DATA__ as $key=>$v): $mod = ($k2 % 2 );++$k2;?>
													     <li><a href="<?php echo $v["url"] ;?>"><?php echo $v["title"] ;?></a>
															<ul class="dropdown-menu pull-left">
															   <?php if(is_array($v['child'])): $k3 = -1; $__DATA__ = $v['child'];if(count($__DATA__)==0 )  echo "" ;foreach($__DATA__ as $key=>$v): $mod = ($k3 % 2 );++$k3;?>
																	<li><a href="<?php echo $v["url"] ;?>"><?php echo $v["title"] ;?></a></li>
															   <?php  endforeach;  endif; ?>
															</ul>
														  </li>
												   <?php  endforeach;  endif; ?>
												   </ul>
												</li>
											 <?php  endforeach;  endif; ?>
										   
                                    </ul>
                                    </div>
                                    <!-- End of Header-menu -->
                                </nav>


                            </div>
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>
        <!-- End Header Navbar-->
    </header>