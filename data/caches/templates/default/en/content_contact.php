<?php defined('IN_YUNYECMS') or exit('No permission.'); ?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- Document Title -->
	<title><?php echo $seo["title"] ;?></title>
	<meta name="keywords" content="<?php echo $seo["keywords"] ;?>" />
	<meta name="description" content="<?php echo $seo["description"] ;?>" />
       <!-- Favicon -->
   <link rel="shortcut icon" type="image/png" href="favicon.png">
    <link href="<?php echo YUNYECMS_PUBLIC;?>plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" />
    <!-- CSS Files -->
    <link rel="stylesheet" href="<?php echo TPL;?>static/css/font-awesome.min.css">
    <!--==== Bootstrap css file ====-->
    <link rel="stylesheet" href="<?php echo TPL;?>static/css/bootstrap.min.css">
    <!--==== Swiper css file ====-->
    <link rel="stylesheet" href="<?php echo TPL;?>static/css/swiper.min.css">
    <!--==== Style css file ====-->
    <link rel="stylesheet" href="<?php echo TPL;?>static/css/style.css?201901">
    <!--==== Responsive css file ====-->
    <link rel="stylesheet" href="<?php echo TPL;?>static/css/responsive.css?201901">
    <!--==== Theme Color 1 css file ====-->
    <link rel="stylesheet" href="<?php echo TPL;?>static/css/theme-color-1.css">
    <!--==== Custom css file ====-->
    <link rel="stylesheet" href="<?php echo TPL;?>static/css/custom.css">
</head>
<body>
    
    <!-- Main header -->
    <!-- End of Main header -->
     <?php include tpl("head"); ?>
    <!-- Page title -->
     <?php 
    	if($rootcat["pic"]) :
    ?>
    <section class="page-title-wrap" data-bg-img="<?php echo $rootcat["pic"] ;?>" data-rjs="2">
        <div class="container">
            <div class="row align-items-center">
                <div class="col">
                    <div class="page-title" data-animate="fadeInUp" data-delay="1.05">
                        <h1><?php echo $rootcat["title"] ;?></h1>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <?php 
    	endif;
    ?> 
     <!-- End of Page title -->
<div class="breadcumb-wrapper">
    <div class="container">
        <div class="pull-left">
            <ul class="list-inline link-list">
                <li><a href="<?php echo ROOT;?>index.php?lang=<?php echo $lang["id"] ;?>">Home</a></li>
				</li>
                <?php $tag_parse = core::load_class('tag_parse');$data=$tag_parse->breadcumb_tag(array('catid'=>$catid));?>
                     <?php if(is_array($data)): $i = -1; $__DATA__ = $data;if(count($__DATA__)==0 )  echo "" ;foreach($__DATA__ as $key=>$v): $mod = ($i % 2 );++$i;?>
                         <?php if($v['id']!=$catid ) { ?>
						   <li><a href="<?php echo $v["url"] ;?>"><?php echo $v["title"] ;?></a></li> 
						  <?php } else { ?>
						   <li><span><?php echo $v["title"] ;?></span></li> 
						<?php } ?>
					 <?php  endforeach;  endif; ?>
                
            </ul>
        </div>
        <div class="pull-right">
        </div>
        <div class="clearfix"></div>
    </div>
</div>     
    <!-- Gallery isotope Section -->
    <section class="pt-50 pb-120 default-bg">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <!-- gallery button inner-->
                    <div class="row">
                        <div class="col">
                            <div class="gallery--button-inner text-center" data-animate="fadeInUp">
                                <div class="gallery-button">
                                    <!-- Start Gallery nav -->
                                    <nav>
                                        <ul class="gallery_filter">
											  <?php $tag_parse = core::load_class('tag_parse');$data=$tag_parse->cat_tag(array('pid'=>$rootcatid));?>
												 <?php if(is_array($data)): $i = -1; $__DATA__ = $data;if(count($__DATA__)==0 )  echo "" ;foreach($__DATA__ as $key=>$v): $mod = ($i % 2 );++$i;?>
													<li  <?php if($catid==$v['id']||$rootcatid==$v['id'])  echo ' class="active"';	 ?>
	><a href="<?php echo $v["url"] ;?>"><span class="gallery-btn pt-sans"><?php echo $v["title"] ;?></span></a>
													 </li> 
												 <?php  endforeach;  endif; ?>
											  
										</ul>
                                    </nav>
                                    <!-- Start Gallery nav -->
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- End Of gallery button inner-->

                    
   <section class="pt-10 pb-90">
        <div class="container">
            <div class="row align-items-center">
               <div class="col-lg-5">
                    <!-- Start Contact Info -->
                    <div class="contact-info mb-30" data-animate="fadeInUp" data-delay=".3">
                        <h1>Contact Us</h1>
                           <?php $find_parse = core::load_class('tag_parse');$data=$find_parse->find_tag(array('catid'=>$catid));?>
								  <?php echo $data["content"] ;?>
							
                    </div>
                     <!-- End of Contact Info -->
                </div>
                <div class="col-lg-7">
                    <!-- Map -->
                    <div class="map" id="map" data-map-latitude="23.790546" data-map-longitude="90.375583" data-map-zoom="16" data-map-marker="[[23.790546, 90.375583]]" style="overflow: hidden;">
                    	 <iframe width=100% height=100% frameborder=0 scrolling=no src="<?php echo TPL;?>map.html"></iframe>
                    </div>
                    <!-- Map End -->
                </div>
            </div>
        </div>
    </section> 
    <!-- Contact form -->
    <section class="pb-120">
        <div class="container">
            <div class="row">
                <div class="col">
                    <!-- Contact form -->
                    <div class="contact-form contact-page-form parsley-validate" data-animate="fadeInUp">
                        <h1>Feedback</h1>
                        <span>With * are required</span>
                        <div class="form-response"></div>
                          <?php $form_parse = core::load_class('tag_parse');$dataparm =array('catid'=>'12','onsubmit'=>'return check();','class'=>'form-horizontal');$onsubmit='onsubmit="'.$dataparm["onsubmit"].'"';$_SESSION["token"]=make_rand_letternum(21); $token=$_SESSION["token"];$data=$form_parse->form_tag(array('catid'=>'12','onsubmit'=>'return check();','class'=>'form-horizontal'));echo '<form action="'.url('formadd').'"  class="'.$dataparm["class"].'"  name="form'.$dataparm["catid"].'"   id="form'.$dataparm["catid"].'" method="post" enctype="multipart/form-data"   '.$onsubmit.'  >';echo '<input type="hidden" name="catid" id="catid" value="'.$dataparm["catid"].'">';echo '<input type="hidden" name="token" id="token" value="'.$token.'">';?>            
                            <div class="row half-gutter">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Name *</label>
                                        <input type="text" name="name"  id="name" placeholder="Please enter your name" class="theme-input-style"   <?php if($this->member) { ?> value="<?php echo $this->member['name'];?>"   <?php } ?> >
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Mobile *</label>
                                        <input type="mobile" name="mobile" id="mobile" placeholder="Mobile phone" class="theme-input-style"  <?php if($this->member) { ?> value="<?php echo $this->member['mobile'];?>"   <?php } ?>  >
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Title*</label>
                                        <input type="text" name="title" id="title"  placeholder="Please enter a feedback title" class="theme-input-style">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label>Content</label>
                                <textarea name="remark" placeholder="Message content" class="theme-input-style" ></textarea>
                            </div>
                            <button class="btn btn-primary" type="submit"><i class="fa fa-send-o "></i> Submit </button>
                         <script language="javascript">   
						 function check() {
								<?php if(is_array($data)): $i = -1; $__DATA__ = $data;if(count($__DATA__)==0 )  echo "" ;foreach($__DATA__ as $key=>$v): $mod = ($i % 2 );++$i;?>
								<?php if($v['isrequired'] ) { ?>
								if (document.getElementById("<?php echo $v["fdname"] ;?>").value==""){
									window.alert("<?php echo $v["fdtitle"] ;?> Can not be empty!");
									document.getElementById("<?php echo $v["fdname"] ;?>").focus();
									return false ;
								 }
								 <?php } ?>
							  <?php  endforeach;  endif; ?>
							}
					 </script> 
                     </form>
                    </div>
                    <!-- End of Contact form -->
                </div>
            </div>
        </div>
    </section>
    <!-- End of Contact form -->                                                     
                </div>
            </div>
        </div>
    </section>
    <!-- End Of GAllery Isotope Section -->
   <?php include tpl('foot'); ?>
    <!-- JS Files -->
    <!-- ==== JQuery 3.3.1 js file==== -->
    <script src="<?php echo TPL;?>static/js/jquery-3.3.1.min.js"></script>

    <!-- ==== Bootstrap js file==== -->
    <script src="<?php echo TPL;?>static/js/bootstrap.bundle.min.js"></script>

    <!-- ==== JQuery Waypoint js file==== -->
    <script src="<?php echo TPL;?>static/js/jquery.waypoints.min.js"></script>

    <!-- ==== Sticky js file==== -->
    <script src="<?php echo TPL;?>static/js/sticky.min.js"></script>
    <!-- ==== Swiper js file==== -->
    <script src="<?php echo TPL;?>static/js/swiper.min.js"></script>
    <!-- ==== Parsley js file==== -->
    <script src="<?php echo TPL;?>static/js/parsley.min.js"></script>

    <!-- ==== Menu  js file==== -->
    <script src="<?php echo TPL;?>static/js/menu.min.js"></script>

    <!-- ==== Scrippt js file==== -->
    <script src="<?php echo TPL;?>static/js/scripts.js?20190123"></script>

    <!-- ==== Custom js file==== -->
    <script src="<?php echo TPL;?>static/js/custom.js"></script>
</body>
</html>