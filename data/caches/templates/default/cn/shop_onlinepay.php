<?php defined('IN_YUNYECMS') or exit('No permission.'); ?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- Document Title -->
	<title><?php echo $seo["title"] ;?></title>
	<meta name="keywords" content="<?php echo $seo["keywords"] ;?>" />
	<meta name="description" content="<?php echo $seo["description"] ;?>" />
       <!-- Favicon -->
    <link rel="shortcut icon" type="image/png" href="favicon.png">
    <link href="<?php echo YUNYECMS_PUBLIC;?>plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" />
    <!-- CSS Files -->
    <link rel="stylesheet" href="<?php echo TPL;?>static/css/font-awesome.min.css">
    <!--==== Bootstrap css file ====-->
    <link rel="stylesheet" href="<?php echo TPL;?>static/css/bootstrap.min.css">
    <!--==== Shop Style css file ====-->
    <link rel="stylesheet" href="<?php echo TPL;?>static/css/shop-style.css">
    <!--==== Style css file ====-->
    <link rel="stylesheet" href="<?php echo TPL;?>static/css/style.css?<?php echo time();?>">
    <!--==== Responsive css file ====-->
    <link rel="stylesheet" href="<?php echo TPL;?>static/css/responsive.css?201901">
    <!--==== Theme Color 1 css file ====-->
    <link rel="stylesheet" href="<?php echo TPL;?>static/css/theme-color-1.css">
    <!--==== Custom css file ====-->
    <link rel="stylesheet" href="<?php echo TPL;?>static/css/custom.css">
    <link rel="stylesheet" href="<?php echo TPL;?>static/css/member.css">
</head>
<body>
    <div class="preLoader"></div>
    <!-- Main header -->
    <!-- End of Main header -->
     <?php include tpl("head",'content'); ?>
    <!-- Page title -->
    <?php $find_parse = core::load_class('tag_parse');$data=$find_parse->find_tag(array('catid'=>'42'));?>
    <section class="page-title-wrap" data-bg-img="<?php echo $data["pic"] ;?>" data-rjs="2">
        <div class="container">
            <div class="row align-items-center">
                <div class="col">
                    <div class="page-title" data-animate="fadeInUp" data-delay="1.05">
                        <h1>在线支付</h1>
                    </div>
                </div>
            </div>
        </div>
    </section>
   
     <!-- End of Page title -->
<div class="breadcumb-wrapper">
    <div class="container">
        <div class="pull-left">
            <ul class="list-inline link-list">
                <li><a href="/">首页</a></li>
				</li>
				 <?php if(is_array($breadcumb)): $i = -1; $__DATA__ = $breadcumb;if(count($__DATA__)==0 )  echo "" ;foreach($__DATA__ as $key=>$v): $mod = ($i % 2 );++$i;?>
				       <?php if($i+1!=count($breadcumb)) { ?>
					   <li><a href="<?php echo $v["url"] ;?>"><?php echo $v["title"] ;?></a></li> 
					   <?php } else { ?>
					   <li><span><?php echo $v["title"] ;?></span></li> 
					   <?php } ?>
				 <?php  endforeach;  endif; ?>
            </ul>
        </div>
        <div class="pull-right">
        </div>
        <div class="clearfix"></div>
    </div>
</div>   

<section class="pt-30 default-bg">
            <div class="container">
                <div class="row">
                    
                <div class="col-md-12">
                    <!-- shop your order table -->
                    <div class="shop--your-order" data-animate="fadeInUp" data-delay=".8">
                        <h4 class="font-weight-bold PT-sens text-center">您的订单已提交，订单号为<span class="text-danger"><?php echo $orderitem["sn"] ;?></span>
</h4>
                        <table class="order-table table PT-sens">
                            <tbody>
                                <tr>
                                    <td>
                                       产品
                                    </td>
                                    <td>
                                        总计
                                    </td>
                                </tr>
                                <?php if(is_array($orderitem['goods'])): $i = -1; $__DATA__ = $orderitem['goods'];if(count($__DATA__)==0 )  echo "" ;foreach($__DATA__ as $key=>$v): $mod = ($i % 2 );++$i;?>
                                <tr>
                                    <td>
                                        <a href="<?php echo $v["url"] ;?>" target="_blank"> <?php echo $v["title"] ;?>   <span><svg 
                                             xmlns="http://www.w3.org/2000/svg"
                                             xmlns:xlink="http://www.w3.org/1999/xlink"
                                             width="8px" height="8px">
                                            <path fill-rule="evenodd"  fill="rgb(130, 135, 167)"
                                             d="M0.070,7.929 C0.118,7.976 0.179,8.000 0.241,8.000 C0.302,8.000 0.364,7.976 0.411,7.929 L3.992,4.341 L7.573,7.929 C7.620,7.976 7.682,8.000 7.743,8.000 C7.805,8.000 7.867,7.976 7.914,7.929 C8.008,7.835 8.008,7.682 7.914,7.588 L4.333,4.000 L7.914,0.412 C8.008,0.318 8.008,0.165 7.914,0.070 C7.820,-0.024 7.667,-0.024 7.573,0.070 L3.992,3.658 L0.411,0.071 C0.317,-0.024 0.165,-0.024 0.070,0.071 C-0.024,0.165 -0.024,0.318 0.070,0.412 L3.651,4.000 L0.070,7.588 C-0.024,7.682 -0.024,7.835 0.070,7.929 Z"/>
                                            </svg></span>    <?php echo $v["num"] ;?> </a>
                                    </td>
                                    <td>
                                        ￥<?php echo $v["price"]*$v["num"]?>元<br>
                                    </td>
                                </tr>
                                <?php  endforeach;  endif; ?>
                                <tr>
                                    <td>
                                       总计 :
                                    </td>
                                    <td class="text-danger" style="font-size: 20px;">
                                        ￥<?php echo $orderitem["money"] ;?>元
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <div class="shop-payment-method pb-30 text-center">
								地址：<?php echo $orderitem["address"] ;?> &nbsp;&nbsp; 姓名： <?php echo $orderitem["name"] ;?> &nbsp;&nbsp; 电话： <?php echo $orderitem["mobile"] ;?>
                        </div>
                        
     <div class="well well-lg">		    
    <form action="<?php echo url('shop/alipay/doalipay')?>"  method="post" target="_blank">
    <div class="containter text-center">
                <input type="hidden" name="WIDout_trade_no" id="out_trade_no" value="<?php echo $orderitem["sn"] ;?>">
                <input type="hidden" name="WIDsubject" value="<?php echo $lang["sitename"] ;?>订单-<?php echo $orderitem["sn"] ;?>">
                <input type="hidden" name="WIDtotal_fee" value="<?php echo $orderitem["money"] ;?>">
                <input type="hidden" name="WIDbody" value='<?php echo $lang["sitename"] ;?>订单-<?php echo $orderitem["sn"] ;?>-<?php echo $orderitem["name"] ;?>-<?php echo $orderitem["mobile"] ;?>-<?php echo $orderitem["address"] ;?>'>
                 <button type="submit" name="button" value="Send" class="btn btn-primary btn-flat"><i class="  icon-paper-plane"></i> 支付宝付款  </button>
</div>         
	</form>
</div> 
                        
                        
                        
                        
                    </div>
                    <!--ENd of shop your order table -->
                </div>
            </div>
        </div>
    </section>

    <!-- Subscription -->
    <section class="primary-bg pt-55 pb-55" style="display: none;">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-lg-6">
                    <div class="subscription-heading" data-animate="fadeInUp">
                        <h1 class="text-white mb-0">邮件订阅</h1>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="primary-form parsley-validate" data-animate="fadeInUp" data-delay=".3">
                        <form action="#" method="post" name="mc-embedded-subscribe-form" target="_blank">
                            <input type="email" name="EMAIL" class="theme-input-style" placeholder="请输入您的电子邮箱" required>
                            <button class="btn btn-primary" type="submit">订阅</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- End of Subscription -->  
   <?php include tpl('foot','content'); ?>

    <!-- JS Files -->
    <!-- ==== JQuery 3.3.1 js file==== -->
    <script src="<?php echo TPL;?>static/js/jquery-3.3.1.min.js"></script>
        <!-- ==== Bootstrap js file==== -->
    <script src="<?php echo TPL;?>static/js/bootstrap.bundle.min.js"></script>
    <!-- ==== JQuery Waypoint js file==== -->
    <script src="<?php echo TPL;?>static/js/jquery.waypoints.min.js"></script>
    <!-- ==== Sticky js file==== -->
    <script src="<?php echo TPL;?>static/js/sticky.min.js"></script>
    <!-- ==== Menu  js file==== -->
    <script src="<?php echo TPL;?>static/js/menu.min.js"></script>
    <!-- ==== Scrippt js file==== -->
    <script src="<?php echo TPL;?>static/js/scripts.js?20190123"></script>
    <!-- ==== Custom js file==== -->
    <script src="<?php echo TPL;?>static/js/custom.js"></script>

</body>
</html>