<?php defined('IN_YUNYECMS') or exit('No permission.'); ?><!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- Document Title -->
	<title><?php echo $seo["title"] ;?></title>
	<meta name="keywords" content="<?php echo $seo["keywords"] ;?>" />
	<meta name="description" content="<?php echo $seo["description"] ;?>" />
       <!-- Favicon -->
   <link rel="shortcut icon" type="image/png" href="favicon.png">
    <link href="<?php echo YUNYECMS_PUBLIC;?>plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" />
    <!-- CSS Files -->
    <link rel="stylesheet" href="<?php echo TPL;?>static/css/font-awesome.min.css">
    <!--==== Bootstrap css file ====-->
    <link rel="stylesheet" href="<?php echo TPL;?>static/css/bootstrap.min.css">
    <!--==== Swiper css file ====-->
    <link rel="stylesheet" href="<?php echo TPL;?>static/css/swiper.min.css">
    <!--==== Style css file ====-->
    <link rel="stylesheet" href="<?php echo TPL;?>static/css/style.css?201901">
    <!--==== Responsive css file ====-->
    <link rel="stylesheet" href="<?php echo TPL;?>static/css/responsive.css?201901">
    <!--==== Theme Color 1 css file ====-->
    <link rel="stylesheet" href="<?php echo TPL;?>static/css/theme-color-1.css">
    <!--==== Custom css file ====-->
    <link rel="stylesheet" href="<?php echo TPL;?>static/css/custom.css">
    <link rel="stylesheet" href="<?php echo TPL;?>static/css/member.css">
</head>
<body>
    <div class="preLoader"></div>
    <!-- Main header --> 
    <!-- End of Main header -->
     <?php include tpl("head",'content'); ?>
    <!-- Page title -->
    <?php $find_parse = core::load_class('tag_parse');$data=$find_parse->find_tag(array('catid'=>'42'));?>
    <section class="page-title-wrap" data-bg-img="<?php echo $data["pic"] ;?>" data-rjs="2">
        <div class="container">
            <div class="row align-items-center">
                <div class="col">
                    <div class="page-title" data-animate="fadeInUp" data-delay="1.05">
                        <h1>我的订单</h1>
                    </div>
                </div>
            </div>
        </div>
    </section>
   
     <!-- End of Page title -->
<div class="breadcumb-wrapper">
    <div class="container">
        <div class="pull-left">
            <ul class="list-inline link-list">
                <li><a href="/">首页</a></li>
				</li>
				 <?php if(is_array($breadcumb)): $i = -1; $__DATA__ = $breadcumb;if(count($__DATA__)==0 )  echo "" ;foreach($__DATA__ as $key=>$v): $mod = ($i % 2 );++$i;?>
				       <?php if($i+1!=count($breadcumb)) { ?>
					   <li><a href="<?php echo $v["url"] ;?>"><?php echo $v["title"] ;?></a></li> 
					   <?php } else { ?>
					   <li><span><?php echo $v["title"] ;?></span></li> 
					   <?php } ?>
				 <?php  endforeach;  endif; ?>
            </ul>
        </div>
        <div class="pull-right">
        </div>
        <div class="clearfix"></div>
    </div>
</div>   
    <!-- Gallery isotope Section -->
    <section class="pt-50 pb-90 default-bg">
        <div class="container">
            <div class="row">
             <!-- Start Side bar widget -->
                <div class="col-lg-3">
				   <?php include tpl("left","member"); ?>
                </div>
                <!-- End of Side bar widget -->
                <div class="col-lg-9">
   <div class="row"><a href="#" class="link-procat">通知消息</a></div>
<div class="blog-wrap single-post-page">
                        <!-- Blog details -->
                        <div class="post-content-block post-main-content mb-60 clearfix">
                            <h1 class="h2 text-center" data-animate="fadeInUp"> <?php echo $row["title"] ;?></h1>
          <ul class="post-meta nav mb-4 text-center" data-animate="fadeInUp">
                                <li>点击：<span><?php echo $row["hits"] ;?></span></li>
                                <li>时间 <a href="#"><?php echo $row["time"] ;?></a></li>
                            </ul>
                            <?php if($row["pic"]!='') { ?>
                             <img src=" <?php echo $row["pic"] ;?>" data-rjs="2" alt="" data-animate="fadeInUp"> 
                            <?php } ?>
                            <div class="post-main-content-inner" data-animate="fadeInUp">
                               <?php echo $row["content"] ;?>
                            </div>
                            <div class="d-md-flex align-items-center justify-content-between" data-animate="fadeInUp">
                                <!-- Start Blog details share -->
                           
                                <!-- End of Blog details share -->
                            </div>
                        </div>
               <div class="row margin-top-20">
			   <?php if(!empty($prev)) { ?>
				<span class="detail_prev col-xs-12 col-sm-6 col-md-6">
				上一条：
			    <a class="fy-left" href="<?php echo $prev["url"] ;?>">
				   <?php echo $prev["title"] ;?> </a>
			    </p>
				</span>
			   <?php } ?>
              <?php if(!empty($next)) { ?>
                     <span class="detail_next col-xs-12 col-sm-6 col-md-6">
                     	下一条：
					<a  class="fy-right" href="<?php echo $next["url"] ;?>">
					<?php echo $next["title"] ;?></a>
                     </span>
               <?php } ?>     
             </div>
                        <!-- End of Blog details -->
                    </div>
  
  
   </div>
            </div>
        </div>
    </section>
    <!-- End Of GAllery Isotope Section -->
   <?php include tpl('foot','content'); ?>
    <!-- JS Files -->
    <!-- ==== JQuery 3.3.1 js file==== -->
    <script src="<?php echo TPL;?>static/js/jquery-3.3.1.min.js"></script>

    <!-- ==== Bootstrap js file==== -->
    <script src="<?php echo TPL;?>static/js/bootstrap.bundle.min.js"></script>

    <!-- ==== JQuery Waypoint js file==== -->
    <script src="<?php echo TPL;?>static/js/jquery.waypoints.min.js"></script>

    <!-- ==== Sticky js file==== -->
    <script src="<?php echo TPL;?>static/js/sticky.min.js"></script>
    <!-- ==== Swiper js file==== -->
    <script src="<?php echo TPL;?>static/js/swiper.min.js"></script>
    <!-- ==== Parsley js file==== -->
    <script src="<?php echo TPL;?>static/js/parsley.min.js"></script>

    <!-- ==== Menu  js file==== -->
    <script src="<?php echo TPL;?>static/js/menu.min.js"></script>

    <!-- ==== Scrippt js file==== -->
    <script src="<?php echo TPL;?>static/js/scripts.js?20190123"></script>

    <!-- ==== Custom js file==== -->
    <script src="<?php echo TPL;?>static/js/custom.js"></script>
</body>
</html>