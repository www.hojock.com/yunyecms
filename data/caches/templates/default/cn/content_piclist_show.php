<?php defined('IN_YUNYECMS') or exit('No permission.'); ?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- Document Title -->
	<title><?php echo $seo["title"] ;?></title>
	<meta name="keywords" content="<?php echo $seo["keywords"] ;?>" />
	<meta name="description" content="<?php echo $seo["description"] ;?>" />
       <!-- Favicon -->
   <link rel="shortcut icon" type="image/png" href="favicon.png">
    <link href="<?php echo YUNYECMS_PUBLIC;?>plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" />
    <!-- CSS Files -->
    <link rel="stylesheet" href="<?php echo TPL;?>static/css/font-awesome.min.css">
    <!--==== Bootstrap css file ====-->
    <link rel="stylesheet" href="<?php echo TPL;?>static/css/bootstrap.min.css">
    <!--==== Swiper css file ====-->
    <link rel="stylesheet" href="<?php echo TPL;?>static/css/swiper.min.css">
       <!--==== Shop Style css file ====-->
    <link rel="stylesheet" href="<?php echo TPL;?>/static/css/shop-style.css">
    <!--==== Style css file ====-->
    <link rel="stylesheet" href="<?php echo TPL;?>static/css/style.css?201901">
    <!--==== Responsive css file ====-->
    <link rel="stylesheet" href="<?php echo TPL;?>static/css/responsive.css?201901">
    <!--==== Theme Color 1 css file ====-->
    <link rel="stylesheet" href="<?php echo TPL;?>static/css/theme-color-1.css">
    <!--==== Custom css file ====-->
    <link rel="stylesheet" href="<?php echo TPL;?>static/css/custom.css">
</head>
<body>
    <div class="preLoader"></div>
    <!-- Main header -->
    <!-- End of Main header -->
     <?php include tpl("head"); ?>
    <!-- Page title -->
     <?php 
    	if($rootcat["pic"]) :
    ?>
    <section class="page-title-wrap" data-bg-img="<?php echo $rootcat["pic"] ;?>" data-rjs="2">
        <div class="container">
            <div class="row align-items-center">
                <div class="col">
                    <div class="page-title" data-animate="fadeInUp" data-delay="1.05">
                        <h1><?php echo $rootcat["title"] ;?></h1>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <?php 
    	endif;
    ?> 
     <!-- End of Page title -->
<div class="breadcumb-wrapper">
    <div class="container">
        <div class="pull-left">
            <ul class="list-inline link-list">
                <li><a href="/">首页</a></li>
				</li>
                <?php $tag_parse = core::load_class('tag_parse');$data=$tag_parse->breadcumb_tag(array('catid'=>$catid));?>
                     <?php if(is_array($data)): $i = -1; $__DATA__ = $data;if(count($__DATA__)==0 )  echo "" ;foreach($__DATA__ as $key=>$v): $mod = ($i % 2 );++$i;?>
                         <?php if($v['id']!=$catid ) { ?>
						   <li><a href="<?php echo $v["url"] ;?>"><?php echo $v["title"] ;?></a></li> 
						  <?php } else { ?>
						   <li><span><?php echo $v["title"] ;?></span></li> 
						<?php } ?>
					 <?php  endforeach;  endif; ?>
                
            </ul>
        </div>
        <div class="pull-right">
        </div>
        <div class="clearfix"></div>
    </div>
</div>     
    <!-- Gallery isotope Section -->
    <section class="pt-50 pb-120 default-bg">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <!-- gallery sorting area -->
                    <div class="row">  
                        <div class="col-md-12">
      <div class="shop-details">
                        <div class="row">
                            <div class="col-lg-9">
                                <div class="blog-wrap single-post-page">
                        <!-- Blog details -->
                        <div class="post-content-block post-main-content mb-60 clearfix">
                            <h1 class="h2 text-center" data-animate="fadeInUp"> <?php echo $row["title"] ;?></h1>
          <ul class="post-meta nav mb-4 text-center" data-animate="fadeInUp">
                                <li>点击：<span><?php echo $row["hits"] ;?></span></li>
                                <li>时间 <a href="#"><?php echo $row["time"] ;?></a></li>
                            </ul>
                            <?php if($row["pic"]!='') { ?>
                             <img src=" <?php echo $row["pic"] ;?>" data-rjs="2" alt="" data-animate="fadeInUp"> 
                            <?php } ?>
                            <div class="post-main-content-inner" data-animate="fadeInUp">
                               <?php echo $row["content"] ;?>
                            </div>
                            <div class="d-md-flex align-items-center justify-content-between" data-animate="fadeInUp">
                                <!-- Start Blog details share -->
                                <ul class="post-share-icons social-icons social-icons-light nav justify-content-md-end">
                                    <div class="bdsharebuttonbox"><a href="#" class="bds_more" data-cmd="more"></a><a href="#" class="bds_qzone" data-cmd="qzone" title="分享到QQ空间"></a><a href="#" class="bds_tsina" data-cmd="tsina" title="分享到新浪微博"></a><a href="#" class="bds_tqq" data-cmd="tqq" title="分享到腾讯微博"></a><a href="#" class="bds_renren" data-cmd="renren" title="分享到人人网"></a><a href="#" class="bds_weixin" data-cmd="weixin" title="分享到微信"></a></div>
<script>window._bd_share_config={"common":{"bdSnsKey":{},"bdText":"","bdMini":"2","bdPic":"","bdStyle":"0","bdSize":"16"},"share":{}};with(document)0[(getElementsByTagName('head')[0]||body).appendChild(createElement('script')).src='http://bdimg.share.baidu.com/static/api/js/share.js?v=89860593.js?cdnversion='+~(-new Date()/36e5)];</script>
                                </ul>
                                <!-- End of Blog details share -->
                            </div>
                        </div>
               <div class="row margin-top-20">
			   <?php if(!empty($prev)) { ?>
				<span class="detail_prev col-xs-12 col-sm-6 col-md-6">
				上一条：
			    <a class="fy-left" href="<?php echo $prev["url"] ;?>">
				   <?php echo $prev["title"] ;?> </a>
			    </p>
				</span>
			   <?php } ?>
              <?php if(!empty($next)) { ?>
                     <span class="detail_next col-xs-12 col-sm-6 col-md-6">
                     	下一条：
					<a  class="fy-right" href="<?php echo $next["url"] ;?>">
					<?php echo $next["title"] ;?></a>
                     </span>
               <?php } ?>     
             </div>
                        <!-- End of Blog details -->
                    </div>
                  <!-- Start shop related product -->
                       <?php if(!empty($row['seokeywords'])):?>
                                       <div class="related-product">
                                        <h4>相关信息</h4>
                                        <div class="row">
                                            <?php $tag_parse = core::load_class('tag_parse');$data=$tag_parse->relation_tag(array('catid'=>$catid,'num'=>'20','id'=>$id,'keywords'=>$row['seokeywords']));?> 
                               <?php if(is_array($data)): $i = -1; $__DATA__ = $data;if(count($__DATA__)==0 )  echo "" ;foreach($__DATA__ as $key=>$v): $mod = ($i % 2 );++$i;?>
                                            <div class="col-lg-3 col-md-3">
                                                <!-- single product -->
					            <div class="sigle--product mb-40">
											<div class="product--item text-center" data-animate="fadeInUp" data-delay=".2">
												<a href="<?php echo $v["url"] ;?>" target="_blank">
													<img src="<?php echo $v["pic"] ;?>" data-rjs="2" alt="Product Image">
													<h5 class="pt-sans">
													   <?php echo strcut($v["title"],15); ?>
													</h5>
												</a>
											</div>
                                 </div>
                                                                          
                                                <!-- single product -->
                                            </div>
                                                <?php  endforeach;  endif; ?>
				                   

                                        </div>
                            </div>
                           <?php endif;?>
                                    <!-- ENd shop related product -->                           
                           
                           
                            </div>
                            <div class="col-lg-3">
					      <aside>
                         <!-- start single sidebar widget -->
                        <div class="catebox widget_categories mb-30" data-animate="fadeInUp">
                            <h3 class="category-title"><?php echo $rootcat["title"] ;?> <br/><span></span></h3>
                            <ul>
                       <?php $tag_parse = core::load_class('tag_parse');$data=$tag_parse->cat_tag(array('pid'=>$rootcatid));?>
							<?php if(is_array($data)): $i = -1; $__DATA__ = $data;if(count($__DATA__)==0 )  echo "" ;foreach($__DATA__ as $key=>$v): $mod = ($i % 2 );++$i;?>
							  <li><a href="<?php echo $v["url"] ;?>"  <?php if($catid==$v['id']||$rootcatid==$v['id'])  echo ' class="active"';	 ?>><span class="nav-text"><?php echo $v["title"] ;?></span></a>
							  </li> 
							<?php  endforeach;  endif; ?>
					   
                           </ul>
                        </div>
                        <!-- End of single sidebar widget -->
                        
                        <!-- start single sidebar widget -->
                     <div class="single-sidebar-widget mb-30" data-animate="fadeInUp">
                            <h3 class="h4 font-weight-bold"><?php echo $rootcat["title"] ;?>搜索</h3>
                            <div class="search-form parsley-validate">
                                <form action="<?php echo url('content/index/lists');?>" method="POST">
                                     <input type="hidden" name="catid" value="<?php echo $rootcat["id"] ;?>">
                                    <input type="text" name="searchkey" placeholder="关键词 ..." required class="theme-input-style">
                                    <button type="submit" class="btn btn-flat btn-primary"><i class="fa fa-search mr-0"></i></button>
                                </form>
                            </div>
                        </div>
                       <!-- End of single sidebar widget -->
                    </aside>
                </div>
                        </div>
                    </div>
                        </div>
                    </div>
                    <!-- End of gallery sorting area -->
                </div>
            </div>
        </div>
    </section>
    <!-- End Of GAllery Isotope Section -->
   <?php include tpl('foot'); ?>
    <!-- JS Files -->
    <!-- ==== JQuery 3.3.1 js file==== -->
    <script src="<?php echo TPL;?>static/js/jquery-3.3.1.min.js"></script>

    <!-- ==== Bootstrap js file==== -->
    <script src="<?php echo TPL;?>static/js/bootstrap.bundle.min.js"></script>

    <!-- ==== JQuery Waypoint js file==== -->
    <script src="<?php echo TPL;?>static/js/jquery.waypoints.min.js"></script>

    <!-- ==== Sticky js file==== -->
    <script src="<?php echo TPL;?>static/js/sticky.min.js"></script>
    <!-- ==== Swiper js file==== -->
    <script src="<?php echo TPL;?>static/js/swiper.min.js"></script>
    <!-- ==== Parsley js file==== -->
    <script src="<?php echo TPL;?>static/js/parsley.min.js"></script>

    <!-- ==== Menu  js file==== -->
    <script src="<?php echo TPL;?>static/js/menu.min.js"></script>

    <!-- ==== Scrippt js file==== -->
    <script src="<?php echo TPL;?>static/js/scripts.js?20190123"></script>

    <!-- ==== Custom js file==== -->
    <script src="<?php echo TPL;?>static/js/custom.js"></script>
</body>
</html>