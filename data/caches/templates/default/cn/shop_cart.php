<?php defined('IN_YUNYECMS') or exit('No permission.'); ?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- Document Title -->
	<title><?php echo $seo["title"] ;?></title>
	<meta name="keywords" content="<?php echo $seo["keywords"] ;?>" />
	<meta name="description" content="<?php echo $seo["description"] ;?>" />
       <!-- Favicon -->
    <link rel="shortcut icon" type="image/png" href="favicon.png">
    <link href="<?php echo YUNYECMS_PUBLIC;?>plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" />
    <!-- CSS Files -->
    <link rel="stylesheet" href="<?php echo TPL;?>static/css/font-awesome.min.css">
    <!--==== Bootstrap css file ====-->
    <link rel="stylesheet" href="<?php echo TPL;?>static/css/bootstrap.min.css">
    <!--==== Shop Style css file ====-->
    <link rel="stylesheet" href="<?php echo TPL;?>static/css/shop-style.css">
    <!--==== Style css file ====-->
    <link rel="stylesheet" href="<?php echo TPL;?>static/css/style.css?<?php echo time();?>">
    <!--==== Responsive css file ====-->
    <link rel="stylesheet" href="<?php echo TPL;?>static/css/responsive.css?201901">
    <!--==== Theme Color 1 css file ====-->
    <link rel="stylesheet" href="<?php echo TPL;?>static/css/theme-color-1.css">
    <!--==== Custom css file ====-->
    <link rel="stylesheet" href="<?php echo TPL;?>static/css/custom.css">
    <link rel="stylesheet" href="<?php echo TPL;?>static/css/member.css">
</head>
<body>
    <div class="preLoader"></div>
    <!-- Main header -->
    <!-- End of Main header -->
     <?php include tpl("head",'content'); ?>
    <!-- Page title -->
    <?php $find_parse = core::load_class('tag_parse');$data=$find_parse->find_tag(array('catid'=>'42'));?>
    <section class="page-title-wrap" data-bg-img="<?php echo $data["pic"] ;?>" data-rjs="2">
        <div class="container">
            <div class="row align-items-center">
                <div class="col">
                    <div class="page-title" data-animate="fadeInUp" data-delay="1.05">
                        <h1>购物车</h1>
                    </div>
                </div>
            </div>
        </div>
    </section>
   
     <!-- End of Page title -->
<div class="breadcumb-wrapper">
    <div class="container">
        <div class="pull-left">
            <ul class="list-inline link-list">
                <li><a href="/">首页</a></li>
				</li>
				 <?php if(is_array($breadcumb)): $i = -1; $__DATA__ = $breadcumb;if(count($__DATA__)==0 )  echo "" ;foreach($__DATA__ as $key=>$v): $mod = ($i % 2 );++$i;?>
				       <?php if($i+1!=count($breadcumb)) { ?>
					   <li><a href="<?php echo $v["url"] ;?>"><?php echo $v["title"] ;?></a></li> 
					   <?php } else { ?>
					   <li><span><?php echo $v["title"] ;?></span></li> 
					   <?php } ?>
				 <?php  endforeach;  endif; ?>
            </ul>
        </div>
        <div class="pull-right">
        </div>
        <div class="clearfix"></div>
    </div>
</div>   

    <!-- cart section-->
    <section class="pt-60 pb-60 default-bg"> 
    <form id="cartform" method="post" action="<?php echo url('shop/cart/pay')?>" target="_blank">
            <div class="container">
                <div class="row">
     <?php if(!empty($list)) { ?>
                    <div class="col-md-12">
                        <!-- Cart Product -->
                        <div class="cart-product" data-animate="fadeInUp" data-delay=".2">
                            <div class="table-responsive">
                                 <table class="sope--cart-table table pt-sans">
                                    <!-- start cart table body -->
                                    <tbody>
                                        <tr>
                                            <td><input type="checkbox" name="checkall" id="checkall"  onclick="return checkall_onclick()" checked></td>
                                            <td>商品</td>
                                            <td>价格</td>
                                            <td>数量</td>
                                            <td>总价</td>
                                            <td>删除</td>
                                        </tr>
                                            <?php if(is_array($list)): $i = -1; $__DATA__ = $list;if(count($__DATA__)==0 )  echo "" ;foreach($__DATA__ as $key=>$vo): $mod = ($i % 2 );++$i;?>
                                        <tr>
                                            <td><input type="checkbox" name="selcart[]"  value="<?php echo $vo["id"] ;?>"  onclick="select_cart_goods();" checked></td>
                                            <td>
                                                <!-- product details/image -->
                                                <div class="product-dtls">
                                                    <div class="item-left">
                                                        <a href="<?php echo $vo["goods"]["url"] ;?>" target="_blank"><img src="<?php echo $vo["goods"]["pic"] ;?>" data-rjs="2" alt="Cart Image"></a>
                                                    </div>
                                                    <div class="item-right">
                                                        <a href="<?php echo $vo["goods"]["url"] ;?>" target="_blank"><?php echo $vo["goods"]["title"] ;?></a>
                                                    </div>
                                                </div>
                                                <!--end of product details/image -->
                                            </td>
                                            <td class="text-danger text-center" id="price<?php echo $vo["id"] ;?>">
                                               ￥<?php echo $vo["price"] ;?>元
                                            </td>
                                            <td>
                                      <div class="btn-group btn-group-sm">
									  <button type="button" class="btn btn-flat" onClick="changenum(<?php echo $vo["id"] ;?>,<?php echo $vo["price"] ;?>,2);">-</button>
									  <input type="text" class="text-center"  id="num<?php echo $vo["id"] ;?>" name="num[]" value="<?php echo $vo["num"] ;?>" readonly style="width: 50px;border-radius: 0; border:#CCC 1px solid;">
									  <button type="button" class="btn btn-flat" onClick="changenum(<?php echo $vo["id"] ;?>,<?php echo $vo["price"] ;?>,1);">+</button>
									  </div>
                                            </td>
                                            <td class="text-danger"  id="total<?php echo $vo["id"] ;?>">
                                                ￥<?php echo $vo["totalprice"] ;?>元
                                            </td>
                                            <td>                                                
                                                <a href="javascript:if (confirm('您确实要把该商品移出购物车吗？')) location.href='<?php echo url('shop/cart/remove',array("id"=>$vo['id']));?>'; " class="click-hide">
                                                    <i class="fa fa-times"></i>
                                                </a>
                                            </td>
                                        </tr>
                                          <?php  endforeach;  endif; ?> 
                                    </tbody>
                                    <!-- end of cart table body -->
                                </table>
                            </div>
                        </div>
                     <?php if(!empty($member["name"])&&!empty($member["address"])&&!empty($member["mobile"])) { ?>
                    <p class="text-dark light-bg p-3">收货人信息：地址：<?php echo $member["address"] ;?>，<?php echo $member["name"] ;?> ，<?php echo $member["mobile"] ;?></p>
                     <?php } ?>
                        <!--End of Cart Product -->
                    </div>
                 
                   <div class="col-md-6 col-lg-6">
					<ul class="pagination pull-left">
					   <?php echo $page;?>
					</ul>
                   </div>
             <!--       <div class="col-md-4">
                        <div class="product-cupon-apply" data-animate="fadeInUp" data-delay=".2">
                            <label class="pt-sans">Apply Coupon Code If You Have One</label>
                            <div class="cupon-mail">
                                <input type="number" class="theme-input-style" name="apply" required="" placeholder="Cupon Code">
                                <button class="btn">Apply</button>
                            </div>
                        </div>
                    </div>-->
                     <div class="col-md-6 col-lg-6 ">
                        <!-- shop and cart total -->
                        <div class="shop--cart-total" data-animate="fadeInUp" data-delay=".6">
                            <h4 class="pt-sans font-weight-bold text-right">总计</h4>
                            <table class="cart--total-table table text-right">
                                <tbody>
                                    <tr>
                                        <td id="totalmoney" class="text-danger h3">
                                            ￥<?php echo $totalmoney;?>元
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                            <div class="text-right">
                                 <input type="hidden" name="token" id="token" value="<?php echo $token;?>">
                                 <input type="hidden" name="totalamount" id="totalamount" value="<?php echo $totalmoney;?>">
                                 <a href="javascript:history.back();" class="btn btn-outline-primary btn-flat"> <i class="fa fa-mail-reply"></i> 继续购物</a>&nbsp;&nbsp;
                                 <button  type="submit" class="btn btn-danger btn-flat"> <i class="fa fa-check"></i> 结 算 </button>
                            </div>
                        </div>
                        <!--End of shop and cart total -->
                    </div>
                    
      <?php } else { ?>
               <div class="container">
                <div class="note note-info">
                <h4><i class="icon fa fa-info"></i> 提示信息：</h4>
                   您的购物车是空的<br/>
               </div>
               </div>
     <?php } ?>
                    
                    
                </div>
            </div>
         </form>
    </section>
    <!-- End Of cart Area -->

    <!-- Subscription -->
    <section class="primary-bg pt-55 pb-55" style="display: none;">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-lg-6">
                    <div class="subscription-heading" data-animate="fadeInUp">
                        <h1 class="text-white mb-0">邮件订阅</h1>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="primary-form parsley-validate" data-animate="fadeInUp" data-delay=".3">
                        <form action="#" method="post" name="mc-embedded-subscribe-form" target="_blank">
                            <input type="email" name="EMAIL" class="theme-input-style" placeholder="请输入您的电子邮箱" required>
                            <button class="btn btn-primary" type="submit">订阅</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- End of Subscription -->  
   <?php include tpl('foot','content'); ?>

    <!-- JS Files -->
    <!-- ==== JQuery 3.3.1 js file==== -->
    <script src="<?php echo TPL;?>static/js/jquery-3.3.1.min.js"></script>
        <!-- ==== Bootstrap js file==== -->
    <script src="<?php echo TPL;?>static/js/bootstrap.bundle.min.js"></script>
    <!-- ==== JQuery Waypoint js file==== -->
    <script src="<?php echo TPL;?>static/js/jquery.waypoints.min.js"></script>
    <!-- ==== Sticky js file==== -->
    <script src="<?php echo TPL;?>static/js/sticky.min.js"></script>
    <!-- ==== Menu  js file==== -->
    <script src="<?php echo TPL;?>static/js/menu.min.js"></script>
    <!-- ==== Scrippt js file==== -->
    <script src="<?php echo TPL;?>static/js/scripts.js?20190123"></script>
    <!-- ==== Custom js file==== -->
    <script src="<?php echo TPL;?>static/js/custom.js"></script>
    <script language="javascript">
	     $(document).ready(function(){
		   $("#cartform").submit(function(e){
			     if(!$("input[name='selcart[]']").is(':checked')){
	                  alert('您至少应该选择一个产品！');
					  return false;
					}
			});  
		 });
		
	function checkall_onclick() 
	{
		var obj = document.getElementById('checkall');
		var obj_cartgoods = document.getElementsByName("selcart[]");
		for (var i=0;i<obj_cartgoods.length;i++)
		{
			var e = obj_cartgoods[i];
			if (e.name != 'checkall'){
				e.checked = obj.checked;
			}
		}
		select_cart_goods();
	}
		
	function select_cart_goods()
	{
	      var sel_goods = new Array();
	      var obj_cartgoods = document.getElementsByName("selcart[]");
	      var j=0;
	      var c = true;
	      for (i=0;i<obj_cartgoods.length;i++)
	      {
			if(obj_cartgoods[i].checked == true)
			{	
				sel_goods[j] = obj_cartgoods[i].value;
				j++;
			}else{
				c = false;
			}
	      }
	      document.getElementById('checkall').checked = c;
					  $.ajax({
							data: {
								sel_goods: sel_goods
							},
							type: "post",
							dataType: "text",
							async: true,
							url: "<?php echo url('shop/cart/changecart');?>",
							success: function(data)
							{
								console.log(data);
								if(data==0){
								$('#totalmoney').html("您至少应该选择一个商品");
								}else{
									var pricedata = JSON.parse(data); 
									$('#totalmoney').html("￥"+pricedata.totalmoney+"元");
								}
							},
							error: function ()
							{
								return false;
							}
						 })			
	}
	function changenum(id,price,mode){
						  objnum=$('#num'+id);
						  objprice=$('#price'+id);
						  objtotal=$('#total'+id);
						  objtotalmoney=$('#totalmoney');
						  cur=objnum.val();
						  //curtotalmoney=objtotalmoney.html();
						  //curtotalmoney=parseInt(curtotalmoney.replace("￥","").replace("元",""));
						  cur= parseInt(cur);
						  if(mode==1){
							objnum.val(cur+1);
							/*objtotal.html("￥"+Math.round((cur+1)*price*100)/100+"元");
							totalamount=Math.round((curtotalmoney+price)*100)/100;
							$('#totalamount').val(totalamount);
							objtotalmoney.html("￥"+totalamount+"元");*/
						  }
						  if(mode==2){
							  if(cur>1){
							   objnum.val(cur-1);
		                   /*  objtotal.html("￥"+Math.round((cur-1)*price*100)/100+"元");
							   totalamount=Math.round((curtotalmoney-price)*100)/100;
							   $('#totalamount').val(totalamount);
							   objtotalmoney.html("￥"+totalamount+"元");	*/							  
								  }
						  }
						var sel_goods = new Array();
						var obj_cartgoods = document.getElementsByName("selcart[]");
						var j=0;
						  for (i=0;i<obj_cartgoods.length;i++)
						  {
							if(obj_cartgoods[i].checked == true)
							{	
								sel_goods[j] = obj_cartgoods[i].value;
								j++;
							}
						  }
						  $.ajax({
							data: {
								cartid:id,
								mode: mode,
								sel_goods: sel_goods
							},
							type: "post",
							dataType: "text",
							async: true,
							url: "<?php echo url('shop/cart/changenum');?>",
							success: function(data)
							{
								//console.log(data);
								var pricedata = JSON.parse(data); 
								objtotal.html("￥"+pricedata.totalprice+"元");
								objtotalmoney.html("￥"+pricedata.totalmoney+"元");
								//objtotalmoney.val(pricedata.num);
							},
							error: function ()
							{
								return false;
							}
						 })						
						
					}
		
  </script>
</body>
</html>