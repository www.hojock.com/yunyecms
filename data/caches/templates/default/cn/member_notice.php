<?php defined('IN_YUNYECMS') or exit('No permission.'); ?><!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- Document Title -->
	<title><?php echo $seo["title"] ;?></title>
	<meta name="keywords" content="<?php echo $seo["keywords"] ;?>" />
	<meta name="description" content="<?php echo $seo["description"] ;?>" />
       <!-- Favicon -->
   <link rel="shortcut icon" type="image/png" href="favicon.png">
    <link href="<?php echo YUNYECMS_PUBLIC;?>plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" />
    <!-- CSS Files -->
    <link rel="stylesheet" href="<?php echo TPL;?>static/css/font-awesome.min.css">
    <!--==== Bootstrap css file ====-->
    <link rel="stylesheet" href="<?php echo TPL;?>static/css/bootstrap.min.css">
    <!--==== Swiper css file ====-->
    <link rel="stylesheet" href="<?php echo TPL;?>static/css/swiper.min.css">
    <!--==== Style css file ====-->
    <link rel="stylesheet" href="<?php echo TPL;?>static/css/style.css?201901">
    <!--==== Responsive css file ====-->
    <link rel="stylesheet" href="<?php echo TPL;?>static/css/responsive.css?201901">
    <!--==== Theme Color 1 css file ====-->
    <link rel="stylesheet" href="<?php echo TPL;?>static/css/theme-color-1.css">
    <!--==== Custom css file ====-->
    <link rel="stylesheet" href="<?php echo TPL;?>static/css/custom.css">
    <link rel="stylesheet" href="<?php echo TPL;?>static/css/member.css">
</head>
<body>
    <div class="preLoader"></div>
    <!-- Main header --> 
    <!-- End of Main header -->
     <?php include tpl("head",'content'); ?>
    <!-- Page title -->
    <?php $find_parse = core::load_class('tag_parse');$data=$find_parse->find_tag(array('catid'=>'42'));?>
    <section class="page-title-wrap" data-bg-img="<?php echo $data["pic"] ;?>" data-rjs="2">
        <div class="container">
            <div class="row align-items-center">
                <div class="col">
                    <div class="page-title" data-animate="fadeInUp" data-delay="1.05">
                        <h1>通知消息</h1>
                    </div>
                </div>
            </div>
        </div>
    </section>
   
     <!-- End of Page title -->
<div class="breadcumb-wrapper">
    <div class="container">
        <div class="pull-left">
            <ul class="list-inline link-list">
                <li><a href="/">首页</a></li>
				</li>
				 <?php if(is_array($breadcumb)): $i = -1; $__DATA__ = $breadcumb;if(count($__DATA__)==0 )  echo "" ;foreach($__DATA__ as $key=>$v): $mod = ($i % 2 );++$i;?>
				       <?php if($i+1!=count($breadcumb)) { ?>
					   <li><a href="<?php echo $v["url"] ;?>"><?php echo $v["title"] ;?></a></li> 
					   <?php } else { ?>
					   <li><span><?php echo $v["title"] ;?></span></li> 
					   <?php } ?>
				 <?php  endforeach;  endif; ?>
            </ul>
        </div>
        <div class="pull-right">
        </div>
        <div class="clearfix"></div>
    </div>
</div>   
    <!-- Gallery isotope Section -->
    <section class="pt-50 pb-90 default-bg">
        <div class="container">
            <div class="row">
             <!-- Start Side bar widget -->
                <div class="col-lg-3">
				   <?php include tpl("left","member"); ?>
                </div>
                <!-- End of Side bar widget -->
                <div class="col-lg-9">
   <div class="row"><a href="#" class="link-procat">通知消息</a></div>
    <?php if(!empty($list)) { ?>
    <div class="box-body">
	<table class="table table-bordered table-striped ">
        <thead>
        <tr>
            <th width="75%">标题</th>
            <th  width="25%">时间</th>
        </tr>
        </thead>
        <tbody id="catlist">
             <?php if(is_array($list)): $i = -1; $__DATA__ = $list;if(count($__DATA__)==0 )  echo "" ;foreach($__DATA__ as $key=>$vo): $mod = ($i % 2 );++$i;?>
             <tr>
                <td><a href="<?php echo $vo["url"] ;?>" target="_blank"><?php echo $vo["title"] ;?></a></td>
                <td align="left" >
                  <?php echo udate($vo['addtime']);?>
                 </td>  
            </tr>
          <?php  endforeach;  endif; ?>
        </tbody>
    </table>
		<ul class="pagination pull-right">
		     <?php echo $page;?>
		</ul>
      </div>
     <?php } else { ?>
               <div class="container">
               <div class="callout callout-info">
                <h4><i class="icon fa fa-info"></i> 提示信息：</h4>
                暂时没有任何订单信息<br/>
               </div>
               </div>
     <?php } ?>
                </div>
            </div>
        </div>
    </section>
    <!-- End Of GAllery Isotope Section -->
   <?php include tpl('foot','content'); ?>
    <!-- JS Files -->
    <!-- ==== JQuery 3.3.1 js file==== -->
    <script src="<?php echo TPL;?>static/js/jquery-3.3.1.min.js"></script>

    <!-- ==== Bootstrap js file==== -->
    <script src="<?php echo TPL;?>static/js/bootstrap.bundle.min.js"></script>

    <!-- ==== JQuery Waypoint js file==== -->
    <script src="<?php echo TPL;?>static/js/jquery.waypoints.min.js"></script>

    <!-- ==== Sticky js file==== -->
    <script src="<?php echo TPL;?>static/js/sticky.min.js"></script>
    <!-- ==== Swiper js file==== -->
    <script src="<?php echo TPL;?>static/js/swiper.min.js"></script>
    <!-- ==== Parsley js file==== -->
    <script src="<?php echo TPL;?>static/js/parsley.min.js"></script>

    <!-- ==== Menu  js file==== -->
    <script src="<?php echo TPL;?>static/js/menu.min.js"></script>

    <!-- ==== Scrippt js file==== -->
    <script src="<?php echo TPL;?>static/js/scripts.js?20190123"></script>

    <!-- ==== Custom js file==== -->
    <script src="<?php echo TPL;?>static/js/custom.js"></script>
</body>
</html>