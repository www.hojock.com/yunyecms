<?php defined('IN_YUNYECMS') or exit('No permission.'); ?><!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta name="applicable-device" content="pc">
<title><?php echo $seo["title"] ;?></title>
<meta name="keywords" content="<?php echo $seo["keywords"] ;?>" />
<meta name="description" content="<?php echo $seo["description"] ;?>" />
<link href="<?php echo TPL;?>static/css/style.css" rel="stylesheet" />
<link rel="canonical" href="<?php echo ROOT;?>"/>
<link rel="alternate" media="only screen and(max-width: 640px)" href="<?php echo ROOT;?>" >
<meta http-equiv="mobile-agent" content="format=xhtml;url=<?php echo ROOT;?>">
<script type="text/javascript">if(window.location.toString().indexOf('pref=padindex') != -1){}else{if(/AppleWebKit.*Mobile/i.test(navigator.userAgent) || (/MIDP|SymbianOS|NOKIA|SAMSUNG|LG|NEC|TCL|Alcatel|BIRD|DBTEL|Dopod|PHILIPS|HAIER|LENOVO|MOT-|Nokia|SonyEricsson|SIE-|Amoi|ZTE/.test(navigator.userAgent))){if(window.location.href.indexOf("?mobile")<0){try{if(/Android|Windows Phone|webOS|iPhone|iPod|BlackBerry/i.test(navigator.userAgent)){window.location.href="<?php echo ROOT;?>";}else if(/iPad/i.test(navigator.userAgent)){}else{}}catch(e){}}}}</script>

<!--IE8以下 圆角等补丁-->
<!--[if lt IE 9]>
<script type="text/javascript" src="<?php echo TPL;?>static/js/pie.js"></script>
<![endif]-->
</head>
<body>
 <?php include tpl("top"); ?>
 <?php include tpl("head"); ?>
<!--顶栏、头部、导航-->
<div class="index-page">
  <div class="wrap auto">
	   <?php $tag_parse = core::load_class('tag_parse');$data="";$data=$tag_parse->list_tag(array('catid'=>'41','order'=>'ordernum asc','num'=>'1'));?>
				 <?php if(is_array($data)): $i = -1; $__DATA__ = $data;if(count($__DATA__)==0 )  echo "" ;foreach($__DATA__ as $key=>$v): $mod = ($i % 2 );++$i;?>
		          <div class="AD_1200_108 ovh mb15"><a href="<?php echo $v["url"] ;?>" target="_blank"><img src="<?php echo $v["pic"] ;?>"></a></div>
				<?php  endforeach;  endif; ?>
			  
    <!-- 最新系统下载 -->
    <div class="section idx-zxxt fix">
      <div class="idx-zxxt-l">
        <div class="fix"> 
          
          <!--推荐幻灯-->
          <div class="idx-zxxt-a J_slide_idx">
            <ul class="bd J_slide_inner">
            <?php $tag_parse = core::load_class('tag_parse');$data="";$data=$tag_parse->list_tag(array('catid'=>'71','order'=>'ordernum asc','num'=>'4'));?>
				 <?php if(is_array($data)): $i = -1; $__DATA__ = $data;if(count($__DATA__)==0 )  echo "" ;foreach($__DATA__ as $key=>$v): $mod = ($i % 2 );++$i;?>
			<li><a href="<?php echo $v["url"] ;?>" title="<?php echo $v["title"] ;?>" target="_blank"><img src="<?php echo $v["pic"] ;?>" width="420" height="314" alt="<?php echo $v["title"] ;?>" /></a></li>	
				<?php  endforeach;  endif; ?>
			  
            </ul>
            <ul class="thumb fix">
			 <?php $tag_parse = core::load_class('tag_parse');$data="";$data=$tag_parse->list_tag(array('catid'=>'71','order'=>'ordernum asc','num'=>'4'));?>
				 <?php if(is_array($data)): $i = -1; $__DATA__ = $data;if(count($__DATA__)==0 )  echo "" ;foreach($__DATA__ as $key=>$v): $mod = ($i % 2 );++$i;?>
		             <li class="thumb-item J_slide_tmb on"> <img src="<?php echo $v["pic"] ;?>" width="100" height="74" alt="<?php echo $v["title"] ;?>" /> <span class="arrow"><i class="ui-bg"></i></span> </li>
				<?php  endforeach;  endif; ?>
			  
            </ul>
          </div>
          <!--推荐幻灯--> 
          <!--最新系统-->
          <div class="idx-zxxt-b">
            <div class="g-title fix"> <a href="<?php echo url('lists',array('catid'=>32))?>" class="more fr">更多+</a>
              <h2 class="title-txt c-g-blue">最新系统下载</h2>
            </div>
            <div class="b-list">
              <ul class="fix">
	   <?php $tag_parse = core::load_class('tag_parse');$data="";$data=$tag_parse->query_tag(array('sql'=>'select * from #yunyecms_m_download where status=1  order by addtime desc','num'=>'10'));?>
			   <?php if(is_array($data)): $i = -1; $__DATA__ = $data;if(count($__DATA__)==0 )  echo "" ;foreach($__DATA__ as $key=>$v): $mod = ($i % 2 );++$i;?>
				  <li class="list-item"> <span class="fr"><?php echo date("m-d",$v["addtime"]);?></span> <i></i> <a href="<?php echo $v["url"] ;?>" target="_blank" title="<?php echo $v["title"] ;?>" class="g-list-a"><?php echo getsubstr($v["title"],0,35); ?></a> </li>
				<?php  endforeach;  endif; ?>
	       
              </ul>
              <div class="bgw bgw-t"></div>
              <div class="bgw bgw-b"></div>
            </div>
          </div>
          <!--最新系统--> 
          
        </div>
        <div class="dotted ui-bg"></div>
        <div class="fix">
          <div class="idx-zxxt-c fl"> 
			<?php $tag_parse = core::load_class('tag_parse');$data="";$data=$tag_parse->list_tag(array('catid'=>'73','order'=>'ordernum asc','num'=>'2'));?>
				 <?php if(is_array($data)): $i = -1; $__DATA__ = $data;if(count($__DATA__)==0 )  echo "" ;foreach($__DATA__ as $key=>$v): $mod = ($i % 2 );++$i;?>
		           	<a class="m-ad-img" href="<?php echo $v["url"] ;?>" target="_blank"> <img class="img" src="<?php echo $v["pic"] ;?>" width="418" height="100" alt="<?php echo $v["title"] ;?>" /> </a>
				<?php  endforeach;  endif; ?>
			  
		 </div>
          <!--最新资讯-->
          <div class="idx-zxxt-d fr">
		   <?php $tag_parse = core::load_class('tag_parse');$data="";$data=$tag_parse->list_tag(array('catid'=>'3','order'=>'hits desc','isgood'=>'1','num'=>'1'));?> 
				<?php if(is_array($data)): $i = -1; $__DATA__ = $data;if(count($__DATA__)==0 )  echo "" ;foreach($__DATA__ as $key=>$v): $mod = ($i % 2 );++$i;?>
				<a href="<?php echo $v["url"] ;?>"  title="<?php echo $v["title"] ;?>" target="_blank">
				<div class="d-title fix"> <span class="title-tag"><?php echo $v["tag"] ;?></span>
				  <h3 class="title-txt ellipsis"><?php echo getsubstr($v["title"],0,15); ?></h3>
				</div>
				<p class="d-txt"><?php echo strcut(strip_tags($v["content"]),100);?></p>
				</a>
				<?php  endforeach;  endif; ?>
			  
            <ul class="d-list">
			 <?php $tag_parse = core::load_class('tag_parse');$data="";$data=$tag_parse->list_tag(array('catid'=>'3','order'=>'addtime desc','num'=>'4'));?> 
				<?php if(is_array($data)): $i = -1; $__DATA__ = $data;if(count($__DATA__)==0 )  echo "" ;foreach($__DATA__ as $key=>$v): $mod = ($i % 2 );++$i;?>
			       <li class="list-item"> <span class="fr"><?php echo date("m-d",$v["addtime"]);?></span> <i></i> <a href="<?php echo $v["url"] ;?>"  title="<?php echo $v["title"] ;?>" target="_blank" class="g-list-a"><?php echo getsubstr($v["title"],0,15); ?></a> </li>
				<?php  endforeach;  endif; ?>
			    
            </ul>
          </div>
          <!--最新资讯--> 
        </div>
      </div>
      <div class="idx-zxxt-r ">
        <div class="g-title fix">
          <h2 class="title-txt ">品牌系统</h2>
        </div>
        <div class="idx-zxxt-e ">
          <ul class="tab-cont-item" style="display: block">
			<?php $tag_parse = core::load_class('tag_parse');$data="";$data=$tag_parse->query_tag(array('sql'=>'select * from #yunyecms_m_download where status=1 and isgood=1 order by addtime desc','num'=>'10'));?>
				   <?php if(is_array($data)): $i = -1; $__DATA__ = $data;if(count($__DATA__)==0 )  echo "" ;foreach($__DATA__ as $key=>$v): $mod = ($i % 2 );++$i;?>
						<li class="e-item fix"> <img class="e-img" width="58" height="58" src="<?php echo $v["pic"] ;?>" alt="" />
						  <div class="e-info">
							<h4 class="e-name ellipsis"><?php echo getsubstr($v["title"],0,10); ?></h4>
							<p class="e-txt ellipsis"><?php echo getsubstr($v["summary"],0,13); ?></p>
							<p class="e-txt">评分：<?php echo $v["dengji"] ;?></p>
						  </div>
						</li>
				   <?php  endforeach;  endif; ?>
			    
          </ul>
        </div>
		  	<?php $tag_parse = core::load_class('tag_parse');$data="";$data=$tag_parse->list_tag(array('catid'=>'72','order'=>'ordernum asc','num'=>'1'));?>
				 <?php if(is_array($data)): $i = -1; $__DATA__ = $data;if(count($__DATA__)==0 )  echo "" ;foreach($__DATA__ as $key=>$v): $mod = ($i % 2 );++$i;?>
					<div class="ad250-70"> <a href="<?php echo $v["url"] ;?>" target="_blank"><img src="<?php echo $v["pic"] ;?>"></a> </div>
				<?php  endforeach;  endif; ?>
			 
      </div>
    </div>
    <!-- / 最新系统下载 --> 
    
    <!-- 系统分类 -->
    <div class="section idx-xtfl fix J_g_mouseover_tab">
      <div class="g-title fix">
        <div class="more-tabs fr J_extend_cont">
		  <?php $tag_parse = core::load_class('tag_parse');$data=$tag_parse->cat_tag(array('catid'=>'32,33,34,37'));?>
			 <?php if(is_array($data)): $i = -1; $__DATA__ = $data;if(count($__DATA__)==0 )  echo "" ;foreach($__DATA__ as $key=>$v): $mod = ($i % 2 );++$i;?>
		  <a href="<?php echo $v["url"] ;?>" class="item" <?php if($key==0) { ?>style="display: block"<?php } ?>>更多+</a>
			 <?php  endforeach;  endif; ?>
		  	
           </div>
        <h2 class="title-txt">系统分类</h2>
        <div class="g-tab sty-1 J_tab">
		  <?php $tag_parse = core::load_class('tag_parse');$data=$tag_parse->cat_tag(array('catid'=>'32,33,34,37'));?>
			 <?php if(is_array($data)): $i = -1; $__DATA__ = $data;if(count($__DATA__)==0 )  echo "" ;foreach($__DATA__ as $key=>$v): $mod = ($i % 2 );++$i;?>
				 <a href="<?php echo $v["url"] ;?>" target="_blank" class="tab-item <?php if($key==0) { ?> cur <?php } ?>"><?php echo $v["title"] ;?><i class="bot-arrow ui-bg"></i></a>
			 <?php  endforeach;  endif; ?>
		  
           </div>
      </div>
      <div class="fix">
        <div class="idx-xtfl-a fix J_tab_cont"> 
          <!--XP系统-->
		<?php $tag_parse = core::load_class('tag_parse');$data=$tag_parse->cat_tag(array('catid'=>'32,33,34,37'));?>
				 <?php if(is_array($data)): $i = -1; $__DATA__ = $data;if(count($__DATA__)==0 )  echo "" ;foreach($__DATA__ as $key=>$v): $mod = ($i % 2 );++$i;?>
          <div class="tab-cont-item fix" <?php if($key==0) { ?>style="display: block"<?php } ?>>
            <ul class="fix">
			  <?php $vatid=$v['id'];?>
				    <?php $tag_parse = core::load_class('tag_parse');$data="";$data=$tag_parse->list_tag(array('catid'=>$vatid,'num'=>'6'));?>
						 <?php if(is_array($data)): $i = -1; $__DATA__ = $data;if(count($__DATA__)==0 )  echo "" ;foreach($__DATA__ as $key=>$var): $mod = ($i % 2 );++$i;?>
			<li class="a-item fix"> <a href="<?php echo $var["url"] ;?>" target="_blank" title="<?php echo $var["title"] ;?>"><img class="item-img lazy" width="165" height="127" data-original="<?php echo $var["pic"] ;?>" alt="<?php echo $var["title"] ;?>"></a>
							<div class="item-info"> <a href="<?php echo $var["url"] ;?>" target="_blank" title="<?php echo $var["title"] ;?>">
							  <h4 class="item-name"><?php echo $var["title"] ;?></h4>
							  </a>
							  <div class="g-star sty-1 star- mb15"></div>
							  <p class="item-desc">热度：<?php echo $var["hits"] ;?>℃</p>
							</div>
			</li>	 <?php  endforeach;  endif; ?>
					  
            </ul>
          </div>
				 <?php  endforeach;  endif; ?>
	   	
			
        </div>
        <div class="idx-xtfl-b m-rank rank-toggle">
          <h3 class="rank-title ui-bg"><span class="c-g-blue">系统</span>总排行</h3>
          <ul>
			<?php $tag_parse = core::load_class('tag_parse');$data="";$data=$tag_parse->query_tag(array('sql'=>'select * from #yunyecms_m_download where catid in(32,33,34,37,39) and status=1 order by addtime desc','num'=>'10'));?>
				   <?php if(is_array($data)): $i = -1; $__DATA__ = $data;if(count($__DATA__)==0 )  echo "" ;foreach($__DATA__ as $key=>$v): $mod = ($i % 2 );++$i;?>
				 <li class="rank-item on"> <a href="<?php echo $v["url"] ;?>" title="<?php echo $v["title"] ;?>" class="item-name ellipsis" target="_blank"> <span class="g-sort-num no<?php echo $key+1;?>"><?php echo $key+1;?></span><?php echo getsubstr($v["title"],0,10); ?></a>
              <div class="item-info fix ml20"> <img class="fl vab mr10 lazy" data-original="<?php echo $v["pic"] ;?>" width="66" height="66" alt="<?php echo $v["title"] ;?>" />
                <p class="item-grade"> <span class="dib"><?php echo getsubstr($v["title"],0,10); ?></span> <span class="g-star sty-2 star-5"></span> </p>
                <a href="<?php echo $v["url"] ;?>" title="<?php echo $v["title"] ;?>" target="_blank" class="item-btn">下载</a> </div>
               </li>
				   <?php  endforeach;  endif; ?>
			     			  
          </ul>
        </div>
      </div>
    </div>
    <!-- / 系统分类 --> 
    
    <!-- 软件必备 -->
    <div class="section idx-rjbb fix J_g_mouseover_tab">
      <div class="g-title fix"> <a href="<?php echo url('lists',array('catid'=>63))?>" class="more fr">更多+</a>
        <h2 class="title-txt">软件必备</h2>
        <!--<ul class="g-tab sty-2 J_tab">
          <li class="tab-item cur">聊天工具<i class="bot-arrow ui-bg"></i></li><li class="tab-item ">视频播放<i class="bot-arrow ui-bg"></i></li><li class="tab-item ">音频播放<i class="bot-arrow ui-bg"></i></li><li class="tab-item ">网络电视<i class="bot-arrow ui-bg"></i></li><li class="tab-item ">网页浏览<i class="bot-arrow ui-bg"></i></li><li class="tab-item ">下载工具<i class="bot-arrow ui-bg"></i></li><li class="tab-item ">杀毒软件<i class="bot-arrow ui-bg"></i></li><li class="tab-item ">安全辅助<i class="bot-arrow ui-bg"></i></li>
        </ul>--> 
      </div>
      <div class="fix">
        <div class="idx-rjbb-a fix J_tab_cont">
          <div class="tab-cont-item fix" style="display: block;">
            <?php $tag_parse = core::load_class('tag_parse');$data=$tag_parse->cat_tag(array('pid'=>'63'));?>
				 <?php if(is_array($data)): $i = -1; $__DATA__ = $data;if(count($__DATA__)==0 )  echo "" ;foreach($__DATA__ as $key=>$v): $mod = ($i % 2 );++$i;?>
		    <dl class="a-cell">
              <dt class="a-title"> <i class="ui-facet c-<?php echo $key+1;?>"></i> <?php echo $v["title"] ;?> </dt>
              <dd class="cell-item">
			        <?php $vatid=$v['id'];?>
				    <?php $tag_parse = core::load_class('tag_parse');$data="";$data=$tag_parse->list_tag(array('catid'=>$vatid,'num'=>'5'));?>
						 <?php if(is_array($data)): $i = -1; $__DATA__ = $data;if(count($__DATA__)==0 )  echo "" ;foreach($__DATA__ as $key=>$var): $mod = ($i % 2 );++$i;?>
				  <a href="<?php echo $var["url"] ;?>" title="<?php echo $var["title"] ;?>" target="_blank" class="ellipsis db"> <img class="ico-20 lazy" data-original="<?php echo $var["pic"] ;?>" alt="<?php echo $var["title"] ;?>" style="display: inline;" width="20" height="20" src="<?php echo $var["pic"] ;?>"> <span><?php echo $var["title"] ;?></span></a>
						 <?php  endforeach;  endif; ?>
						 
				</dd>
            </dl>
				 <?php  endforeach;  endif; ?>
	     	
          </div>
        </div>
        <div class="idx-rjbb-b m-rank rank-toggle">
          <h3 class="rank-title ui-bg"><span class="c-g-blue">软件</span>总排行</h3>
          <ul>
		  <?php $tag_parse = core::load_class('tag_parse');$data="";$data=$tag_parse->list_tag(array('catid'=>'63','order'=>'hits desc','num'=>'10'));?> 
			   <?php if(is_array($data)): $i = -1; $__DATA__ = $data;if(count($__DATA__)==0 )  echo "" ;foreach($__DATA__ as $key=>$v): $mod = ($i % 2 );++$i;?>
			  <li class="rank-item on"> <a href="<?php echo $v["url"] ;?>" title="<?php echo $v["title"] ;?>" class="item-name ellipsis" target="_blank"> <span class="g-sort-num no<?php echo $key+1;?>"><?php echo $key+1;?></span><?php echo getsubstr($v["title"],0,10); ?></a>
              <div class="item-info fix ml20"> <img class="fl vab mr10 lazy" data-original="<?php echo $v["pic"] ;?>" width="66" height="66" alt="<?php echo $v["title"] ;?>" />
                <p class="item-grade"> <span class="dib"><?php echo getsubstr($v["title"],0,10); ?></span> <span class="g-star sty-2 star-5"></span> </p>
                <a href="<?php echo $v["url"] ;?>" title="<?php echo $v["title"] ;?>" target="_blank" class="item-btn">下载</a> </div>
               </li>
			  <?php  endforeach;  endif; ?>
	      
          </ul>
        </div>
      </div>
    </div>
    <!-- 软件必备 --> 
    
    <!-- 软件更新 -->
    <div class="section idx-rjgx fix J_g_mouseover_tab">
      <div class="g-title fix"> <a href="<?php echo url('lists',array('catid'=>63))?>" class="more fr">更多+</a>
        <h2 class="title-txt">软件更新</h2>
        <!--<div class="g-tab sty-1 J_tab">
          <a href="/19363/software/im/" target="_blank" class="tab-item cur">聊天工具<i class="bot-arrow ui-bg"></i></a><a href="/19363/software/shipinbofang/" target="_blank" class="tab-item ">视频播放<i class="bot-arrow ui-bg"></i></a><a href="/19363/software/yinpinbofang/" target="_blank" class="tab-item ">音频播放<i class="bot-arrow ui-bg"></i></a><a href="/19363/software/webtv/" target="_blank" class="tab-item ">网络电视<i class="bot-arrow ui-bg"></i></a><a href="/19363/software/browser/" target="_blank" class="tab-item ">网页浏览<i class="bot-arrow ui-bg"></i></a><a href="/19363/software/downtool/" target="_blank" class="tab-item ">下载工具<i class="bot-arrow ui-bg"></i></a>
       </div>--> 
      </div>
      <div class="fix">
        <div class="fix J_tab_cont"> 
          
          <!--开始-->
          <div class="tab-cont-item fix" style="display: block">
            <ul class="idx-rjgx-a fix">
				   <?php $tag_parse = core::load_class('tag_parse');$data="";$data=$tag_parse->list_tag(array('catid'=>'63','order'=>'addtime desc','num'=>'20'));?> 
						   <?php if(is_array($data)): $i = -1; $__DATA__ = $data;if(count($__DATA__)==0 )  echo "" ;foreach($__DATA__ as $key=>$v): $mod = ($i % 2 );++$i;?>
			<li class="a-item ellipsis"> <span class="fr"> <?php echo date("m-d",$v["addtime"]);?>
												</span> <a href="<?php echo $v["url"] ;?>" target="_blank" class="item-label"></a>  <a href="<?php echo $v["url"] ;?>" target="_blank" class="item-name"><?php echo getsubstr($v["title"],0,26); ?></a> </li>
							<?php  endforeach;  endif; ?>
				        
            </ul>
            <div class="idx-rjgx-b m-rank rank-toggle">
              <h3 class="rank-title ui-bg"><span class="c-g-blue">软件</span>最新发布</h3>
              <ul>
		  <?php $tag_parse = core::load_class('tag_parse');$data="";$data=$tag_parse->list_tag(array('catid'=>'63','order'=>'addtime desc','num'=>'10'));?> 
			   <?php if(is_array($data)): $i = -1; $__DATA__ = $data;if(count($__DATA__)==0 )  echo "" ;foreach($__DATA__ as $key=>$v): $mod = ($i % 2 );++$i;?>
			  <li class="rank-item <?php if($key==0) { ?>on<?php } ?>"> <a href="<?php echo $v["url"] ;?>" title="<?php echo $v["title"] ;?>" class="item-name ellipsis" target="_blank"> <span class="g-sort-num no<?php echo $key+1;?>"><?php echo $key+1;?></span><?php echo getsubstr($v["title"],0,10); ?></a>
              <div class="item-info fix ml20"> <img class="fl vab mr10 lazy" data-original="<?php echo $v["pic"] ;?>" width="66" height="66" alt="<?php echo $v["title"] ;?>" />
                <p class="item-grade"> <span class="dib"><?php echo getsubstr($v["title"],0,10); ?></span> <span class="g-star sty-2 star-5"></span> </p>
                <a href="<?php echo $v["url"] ;?>" title="<?php echo $v["title"] ;?>" target="_blank" class="item-btn">下载</a> </div>
               </li>
			  <?php  endforeach;  endif; ?>
	      				  
              </ul>
            </div>
          </div>
          <!--结束--> 
        </div>
      </div>
    </div>
    <!-- / 软件更新 --> 
    <!-- 最新教程 -->
    <div class="section idx-zxjc fix J_g_mouseover_tab">
      <div class="g-title fix"> <a href="<?php echo url('lists',array('catid'=>3))?>" class="more fr">更多+</a>
        <h2 class="title-txt">最新教程</h2>
        <div class="g-tab sty-2 J_tab">
			 <?php $tag_parse = core::load_class('tag_parse');$data=$tag_parse->cat_tag(array('pid'=>'3'));?>
			 <?php if(is_array($data)): $i = -1; $__DATA__ = $data;if(count($__DATA__)==0 )  echo "" ;foreach($__DATA__ as $key=>$v): $mod = ($i % 2 );++$i;?>
				<a href="<?php echo $v["url"] ;?>" target="_blank" class="tab-item <?php if($key==0) { ?> cur <?php } ?>"><?php echo $v["title"] ;?><i class="bot-arrow ui-bg"></i></a>
			 <?php  endforeach;  endif; ?>
		    
		  </div>
      </div>
      <div class="fix">
        <div class="fl J_tab_cont" >
			<?php $tag_parse = core::load_class('tag_parse');$data=$tag_parse->cat_tag(array('pid'=>'3'));?>
				 <?php if(is_array($data)): $i = -1; $__DATA__ = $data;if(count($__DATA__)==0 )  echo "" ;foreach($__DATA__ as $key=>$v): $mod = ($i % 2 );++$i;?>
			  <?php $vatid=$v['id'];?>
          <div class="tab-cont-item" <?php if($key==0) { ?>style="display: block"<?php } ?>>
            <div class="idx-zxjc-a"> 
				<?php $tag_parse = core::load_class('tag_parse');$data="";$data=$tag_parse->list_tag(array('catid'=>$vatid,'pic'=>'1','num'=>'2'));?>
						 <?php if(is_array($data)): $i = -1; $__DATA__ = $data;if(count($__DATA__)==0 )  echo "" ;foreach($__DATA__ as $key=>$var): $mod = ($i % 2 );++$i;?>
					  <a class="a-img" href="<?php echo $var["url"] ;?>" title="<?php echo $var["title"] ;?>" target="_blank"> <img class="lazy" data-original="<?php echo $var["pic"] ;?>" width="274" height="203" alt="<?php echo $var["title"] ;?>">
					  <div class="a-bg"></div>
					  <p class="a-title ellipsis"><?php echo $var["title"] ;?></p>
					  </a>
					 <?php  endforeach;  endif; ?>
				  
             </div>
            <div class="idx-zxjc-b">
				<?php $tag_parse = core::load_class('tag_parse');$data="";$data=$tag_parse->list_tag(array('catid'=>$vatid,'isgood'=>'1','num'=>'1'));?>
					  <?php if(is_array($data)): $i = -1; $__DATA__ = $data;if(count($__DATA__)==0 )  echo "" ;foreach($__DATA__ as $key=>$var): $mod = ($i % 2 );++$i;?>
					    <a href="<?php echo $var["url"] ;?>" title="<?php echo $var["title"] ;?>" target="_blank">
						  <div class="b-title fix">
							<h3 class="title-txt ellipsis"><?php echo $var["title"] ;?></h3>
						  </div>
						  <p class="b-txt"> <?php echo strcut(strip_tags($var["content"]),100);?></p>
						 </a>
					  <?php  endforeach;  endif; ?>
				  
              <ul class="b-list">
			    <?php $tag_parse = core::load_class('tag_parse');$data="";$data=$tag_parse->list_tag(array('catid'=>$vatid,'num'=>'10'));?>
					  <?php if(is_array($data)): $i = -1; $__DATA__ = $data;if(count($__DATA__)==0 )  echo "" ;foreach($__DATA__ as $key=>$var): $mod = ($i % 2 );++$i;?>
					    <li class="list-item"> <span class="fr"> <?php echo date("m-d",$var["addtime"]);?>
                                        </span> <i></i> <a href="<?php echo $var["url"] ;?>" title="<?php echo $var["title"] ;?>" target="_blank" class="g-list-a"><?php echo getsubstr($var["title"],0,25);?></a> </li>
					  <?php  endforeach;  endif; ?>
				  				  
              </ul>
            </div>
          </div>
			
		 <?php  endforeach;  endif; ?>
	   				
          
        </div>
        <div class="idx-zxjc-c m-rank">
          <h3 class="rank-title ui-bg"><span class="c-g-blue">教程</span>总排行</h3>
          <ul>
		 <?php $tag_parse = core::load_class('tag_parse');$data="";$data=$tag_parse->list_tag(array('catid'=>'3','order'=>'hits desc','num'=>'10'));?> 
		    <?php if(is_array($data)): $i = -1; $__DATA__ = $data;if(count($__DATA__)==0 )  echo "" ;foreach($__DATA__ as $key=>$v): $mod = ($i % 2 );++$i;?>
			 <li class="rank-item"> <a href="<?php echo $v["url"] ;?>" title="<?php echo $v["title"] ;?>" class="item-name ellipsis" target="_blank"> <span class="g-sort-num no<?php echo $key+1;?>"><?php echo $key+1;?></span> <?php echo getsubstr($v["title"],0,26); ?></a>
              <div class="item-info fix ml20"> <img class="fl vab mr10 lazy" data-original="<?php echo $v["pic"] ;?>" width="66" height="66" alt="<?php echo $v["title"] ;?>" />
                <p class="item-grade"> <span class="dib">系统教程 /</span> <span class="g-star sty-2 star-"></span> </p>
                <a href="<?php echo $v["url"] ;?>" title="<?php echo $v["title"] ;?>" target="_blank" class="item-btn">下载</a> </div>
            </li>
			<?php  endforeach;  endif; ?>
	      			  
          </ul>
        </div>
      </div>
    </div>
    <!-- / 最新教程 --> 
    
    <!-- 专题 -->
    <div class="section idx-zt J_g_mouseover_tab">
      <div class="g-title fix"> <a href="<?php echo url('lists',array('catid'=>66))?>" class="more fr">更多+</a>
        <h2 class="title-txt">主题下载</h2>
        <ul class="g-tab sty-2 J_tab">
		   <?php $tag_parse = core::load_class('tag_parse');$data=$tag_parse->cat_tag(array('pid'=>'66'));?>
			 <?php if(is_array($data)): $i = -1; $__DATA__ = $data;if(count($__DATA__)==0 )  echo "" ;foreach($__DATA__ as $key=>$v): $mod = ($i % 2 );++$i;?>
				<a href="<?php echo $v["url"] ;?>" target="_blank" class="tab-item <?php if($key==0) { ?> cur <?php } ?>"><?php echo $v["title"] ;?><i class="bot-arrow ui-bg"></i></a>
			 <?php  endforeach;  endif; ?>
		    			
        </ul>
      </div>
      <div class="J_tab_cont">
	     <?php $tag_parse = core::load_class('tag_parse');$data=$tag_parse->cat_tag(array('pid'=>'66'));?>
				 <?php if(is_array($data)): $i = -1; $__DATA__ = $data;if(count($__DATA__)==0 )  echo "" ;foreach($__DATA__ as $key=>$v): $mod = ($i % 2 );++$i;?>
			  <?php $vatid=$v['id'];?>
		  <div class="tab-cont-item" <?php if($key==0) { ?>style="display: block"<?php } ?>>
          <div class="J_slide_theme">
            <div class="J_slide_inner">
	<div class="zxzt-list fix">
		  <?php $tag_parse = core::load_class('tag_parse');$data="";$data=$tag_parse->list_tag(array('catid'=>$vatid,'pic'=>'1','num'=>'10'));?>
						 <?php if(is_array($data)): $i = -1; $__DATA__ = $data;if(count($__DATA__)==0 )  echo "" ;foreach($__DATA__ as $key=>$var): $mod = ($i % 2 );++$i;?>
						 <a href="<?php echo $var["url"] ;?>" title="<?php echo $var["title"] ;?>" target="_blank" class="list-img"> <img data-original="<?php echo $var["pic"] ;?>" alt="<?php echo $var["title"] ;?>"> <i class="img-cover"><?php echo $var["title"] ;?></i> </a>
		      <?php if(($key+1)%5==0):  ?>
		        </div><div class="zxzt-list fix"> 
		      <?php endif; ?>
			 <?php  endforeach;  endif; ?>
				  
	</div>
            </div>
            <div class="list-btn btn-prev J_prev"> <i class="btn-cover"></i> <i class="btn-ico"></i> </div>
            <div class="list-btn btn-next J_next"> <i class="btn-cover"></i> <i class="btn-ico"></i> </div>
          </div>
        </div>
		 <?php  endforeach;  endif; ?>
	   			  
		
      </div>
    </div>
    <!-- //专题 -->
    		<?php $tag_parse = core::load_class('tag_parse');$data="";$data=$tag_parse->list_tag(array('catid'=>'74','order'=>'ordernum asc','num'=>'1'));?>
				 <?php if(is_array($data)): $i = -1; $__DATA__ = $data;if(count($__DATA__)==0 )  echo "" ;foreach($__DATA__ as $key=>$v): $mod = ($i % 2 );++$i;?>
		              <div class="AD_1200_108 ovh mb40" id="index_ad1">	<a href="<?php echo $v["url"] ;?>" target="_blank"><img src="<?php echo $v["pic"] ;?>"></a></div>
				<?php  endforeach;  endif; ?>
			  
    <!-- 友情链接 -->
    <div class="idx-yqlj">
      <h2 class="g-title fix"> <span class="yqlj-tip">要求pr4>，权重≥7,日访问IP≥3W。百度权重小于7的链接会被暂时撤销，敬请谅解！ 联系QQ： <?php echo $lang["qq"] ;?></span> <span class="title-txt">友情链接</span> </h2>
      <div class="yqlj-link"> 
		  <?php $tag_parse = core::load_class('tag_parse');$data="";$data=$tag_parse->list_tag(array('catid'=>'45','order'=>'ordernum asc','num'=>'20'));?>
				 <?php if(is_array($data)): $i = -1; $__DATA__ = $data;if(count($__DATA__)==0 )  echo "" ;foreach($__DATA__ as $key=>$v): $mod = ($i % 2 );++$i;?>
		                   <a href="<?php echo $v["url"] ;?>" target="_blank"><?php echo $v["title"] ;?></a>
				<?php  endforeach;  endif; ?>
			  
         </div>
    </div>
    <!-- / 友情链接 --> 
  </div>
</div>
<!-- 页尾 -->
<!-- sidebar -->
<div class="m-sidebar">
  <div class="go-top J_gotop"><i class="ico"></i></div>
</div>
<!-- / sidebar --> 
<!-- 页尾 -->
 <?php include tpl("foot"); ?>

<!-- / 页尾 --> 

<script src="<?php echo TPL;?>static/js/jquery-1.7.2.min.js"></script> 
<script src="<?php echo TPL;?>static/js/lazyload.js"></script> 
<script src="<?php echo TPL;?>static/js/jquery.superslide.2.1.1.js"></script> 
<script src="<?php echo TPL;?>static/js/main.js"></script> 
<script type="text/javascript">
$(document).ready(function(){
  $(".search-option").toggle(function(){ 
  $(this).next(".select").slideDown();
  },function(){ 
  $(this).next(".select").slideUp();
  }); 
$(".select dd").click(function(){ 
  $a=$(this).html();
  if($a=='XP系统'){
  $("#typeid").val('1');
  $(".search-option").html($a+'<i><em></em><span></span></i>'); 
  }
  else  if($a=='Win7系统'){
  $("#typeid").val('2');
  $(".search-option").html($a+'<i><em></em><span></span></i>'); 
  }
  else  if($a=='Win8系统'){
  $("#typeid").val('2');
  $(".search-option").html($a+'<i><em></em><span></span></i>'); 
  }
  else  if($a=='Win10系统'){
  $("#typeid").val('38');
  $(".search-option").html($a+'<i><em></em><span></span></i>'); 
  }
  else  if($a=='装机软件'){
  $("#typeid").val('5');
  $(".search-option").html($a+'<i><em></em><span></span></i>'); 
  }
  else  if($a=='主题下载'){
  $("#typeid").val('4');
  $(".search-option").html($a+'<i><em></em><span></span></i>'); 
  }
  else  if($a=='教程资讯'){
  $("#typeid").val('6');
  $(".search-option").html($a+'<i><em></em><span></span></i>'); 
  }
 $(".select").slideUp();
}); 
  
});
</script>
</body>
</html>
