<?php defined('IN_YUNYECMS') or exit('No permission.'); ?>	<aside>
                         <!-- start single sidebar widget -->
                        <div class="catebox widget_categories mb-30">
                            <h3 class="category-title">会员中心 <br/><span>Member Center</span></h3>
                            <ul>
                            <li><a href="<?php echo url('member/member/myinfo');?>" <?php if(ROUTE_A=='myinfo') { ?> class="active" <?php } ?>  >  <span> <i class="fa fa-user"></i> 我的资料</span></a></li>
							<li> <a href="<?php echo url('member/notice/index');?>" <?php if(ROUTE_C=='notice') { ?> class="active" <?php } ?>  > <span> <i class="fa fa-bullhorn"></i> 通知消息</span></a></li>
							 <li> <a href="<?php echo url('member/member/pwd');?>" <?php if(ROUTE_A=='pwd') { ?> class="active" <?php } ?>   >   <span> <i class=" fa fa-lock"></i> 修改密码</span></a></li>
							 <li> <a href="<?php echo url('member/member/logout');?>" <?php if(ROUTE_A=='logout') { ?> class="active" <?php } ?>  >   <span> <i class="fa fa-sign-out"></i> 退出</span></a></li>
                           </ul>
                        </div>
                        <!-- End of single sidebar widget -->
   </aside>