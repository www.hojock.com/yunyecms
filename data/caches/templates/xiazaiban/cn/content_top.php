<?php defined('IN_YUNYECMS') or exit('No permission.'); ?><!-- 顶栏 -->
<div class="topbar">
  <div class="wrap auto fix">
    <p class="fl">  <?php echo $lang["sitename"] ;?> </p>
    <div class="topbar-menu">
  <?php if($this->member) { ?>
		<a href="<?php echo url('member/member/index');?>" class="menu-link">会员中心</a>	
	    <a href="<?php echo url('member/member/logout');?>" class="menu-link">退出</a>		
	<?php } else { ?>
		    <a href="<?php echo url('member/member/login');?>" class="menu-link">登录</a>	
	        <a href="<?php echo url('member/member/register');?>" class="menu-link">注册</a>
   <?php } ?>
		<a href="<?php echo url('sinpage',array('catid'=>1))?>" class="menu-link">本站简介</a>
      <a href="<?php echo url('sinpage',array('catid'=>46))?>" class="menu-link">广告投放</a>  | <a href="<?php echo ROOT;?>" target="_blank" class="menu-link">手机网站</a> |
      <div class="topbar-nav"> <a href="javascript:;" class="nav-btn ui-bg">网站导航</a>
        <div class="nav-main">
          <div class="inner"> 
		 <?php $tag_parse = core::load_class('tag_parse');$data=$tag_parse->cat_tag(array('level'=>'2'));?>
		   <?php if(is_array($data)): $i = -1; $__DATA__ = $data;if(count($__DATA__)==0 )  echo "" ;foreach($__DATA__ as $key=>$v): $mod = ($i % 2 );++$i;?>
				  <dl class="cate">
				  <dt class="title">
					<p class="item"><a href="<?php echo $v["url"] ;?>" title="<?php echo $v["title"] ;?>"><?php echo $v["title"] ;?></a></p>
				  </dt>
				  <dd class="sub">
					  <?php if(is_array($v['child'])): $k2 = -1; $__DATA__ = $v['child'];if(count($__DATA__)==0 )  echo "" ;foreach($__DATA__ as $key=>$v): $mod = ($k2 % 2 );++$k2;?>  
					  <a href="<?php echo $v["url"] ;?>" target="_blank" class="item"><?php echo $v["title"] ;?></a>
					  <?php  endforeach;  endif; ?> 
				  </dl>
		   <?php  endforeach;  endif; ?>
		   
		   </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- / 顶栏 --> 