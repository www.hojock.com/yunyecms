<?php defined('IN_YUNYECMS') or exit('No permission.'); ?><!-- 头部 -->
<div class="header">
  <div class="wrap auto fix">
    <div class="logo" style="background: url(<?php echo $lang["logo"] ;?>) no-repeat;"> <a href="<?php echo ROOT;?>" class="logo-link"><?php echo $lang["sitename"] ;?></a> </div>
    <div class="c-search">
      <form action="<?php echo url('content/index/lists');?>" name="formsearch" method="post" >
        <input type="hidden" name="kwtype" value="0" />
 <input type="hidden" name="catid" value="32">        <div class="search-wrap">
          <input type="hidden" name="typeid" value="1" id="typeid" />
          <div class="search-option">系统下载 <i><em></em><span></span></i></div>
          <dl class="select" style="display:none">
			  <?php $tag_parse = core::load_class('tag_parse');$data=$tag_parse->cat_tag(array('catid'=>'32,33,34,37,63,66,3'));?>
					 <?php if(is_array($data)): $i = -1; $__DATA__ = $data;if(count($__DATA__)==0 )  echo "" ;foreach($__DATA__ as $key=>$v): $mod = ($i % 2 );++$i;?>
					<dd ><?php echo $v["title"] ;?></dd>
					 <?php  endforeach;  endif; ?>
			   			  
          </dl>
          <input class="keyWord" type="text"  name="searchkey"  value="" placeholder="请输入搜索关键词！"   autocomplete="off"  />
        </div>
        <input type="submit" name="submit" class="submit sub-btn mod_search_btn" tabindex="9" value="搜&#160;索" />
      </form>
    </div>
  </div>
</div>
<!-- / 头部 --> 
<!-- 导航 -->
<div class="navbar">
  <div class="wrap auto fix">
  <a href="<?php echo ROOT;?>" class="nav-item  <?php if(ROUTE_A=='index')  echo ' cur';	 ?> ">首页</a> 
	  <?php $tag_parse = core::load_class('tag_parse');$data=$tag_parse->cat_tag(array('level'=>'1'));?>
		 <?php if(is_array($data)): $i = -1; $__DATA__ = $data;if(count($__DATA__)==0 )  echo "" ;foreach($__DATA__ as $key=>$v): $mod = ($i % 2 );++$i;?>
		 <a href="<?php echo $v["url"] ;?>" class="nav-item <?php if(!empty($rootcatid)&&$rootcatid==$v['id'])  echo ' cur';	 ?>"><?php echo $v["title"] ;?></a>
		 <?php  endforeach;  endif; ?>
	   
	</div>
</div>
<div class="sub-navbar">
  <div class="wrap auto fix sub-navbar-inner">
		<?php $tag_parse = core::load_class('tag_parse');$data="";$data=$tag_parse->query_tag(array('sql'=>'select * from #yunyecms_category where isgood=1 and pid in(32,33,34,37,63,66,3) order by ordernum desc','num'=>'6'));?>
		 <?php if(is_array($data)): $i = -1; $__DATA__ = $data;if(count($__DATA__)==0 )  echo "" ;foreach($__DATA__ as $key=>$v): $mod = ($i % 2 );++$i;?>
			<a href="<?php echo url('lists',array('catid'=>$v['id']))?>" class="sub-nav-item"><?php echo $v["title"] ;?></a>
		 <?php  endforeach;  endif; ?>
	 
	</div>
</div>
<!-- / 导航 --> 