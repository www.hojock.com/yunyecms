<?php defined('IN_YUNYECMS') or exit('No permission.'); ?><!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta name="applicable-device" content="pc">
<title><?php echo $seo["title"] ;?></title>
<meta name="keywords" content="<?php echo $seo["keywords"] ;?>" />
<meta name="description" content="<?php echo $seo["description"] ;?>" />
    <link rel="stylesheet" href="<?php echo TPL;?>member/css/font-awesome.min.css">
    <!--==== Bootstrap css file ====-->
    <link rel="stylesheet" href="<?php echo TPL;?>member/css/bootstrap.min.css">
    <!--==== Swiper css file ====-->
    <link rel="stylesheet" href="<?php echo TPL;?>member/css/swiper.min.css">
    <!--==== Style css file ====-->
    <link rel="stylesheet" href="<?php echo TPL;?>member/css/style.css?201901">
    <!--==== Responsive css file ====-->
    <link rel="stylesheet" href="<?php echo TPL;?>member/css/responsive.css?201901">
    <!--==== Theme Color 1 css file ====-->
    <link rel="stylesheet" href="<?php echo TPL;?>member/css/theme-color-1.css">
    <!--==== Custom css file ====-->
    <link rel="stylesheet" href="<?php echo TPL;?>member/css/custom.css">	
    <link rel="stylesheet" href="<?php echo TPL;?>member/css/member.css">
	
<link href="<?php echo TPL;?>static/css/style.css" rel="stylesheet" />
<link rel="alternate" media="only screen and(max-width: 640px)" href="<?php echo ROOT;?>" >
<meta http-equiv="mobile-agent" content="format=xhtml;url=<?php echo ROOT;?>">
<script type="text/javascript">if(window.location.toString().indexOf('pref=padindex') != -1){}else{if(/AppleWebKit.*Mobile/i.test(navigator.userAgent) || (/MIDP|SymbianOS|NOKIA|SAMSUNG|LG|NEC|TCL|Alcatel|BIRD|DBTEL|Dopod|PHILIPS|HAIER|LENOVO|MOT-|Nokia|SonyEricsson|SIE-|Amoi|ZTE/.test(navigator.userAgent))){if(window.location.href.indexOf("?mobile")<0){try{if(/Android|Windows Phone|webOS|iPhone|iPod|BlackBerry/i.test(navigator.userAgent)){window.location.href="<?php echo ROOT;?>";}else if(/iPad/i.test(navigator.userAgent)){}else{}}catch(e){}}}}</script>
	
<!--IE8以下 圆角等补丁-->
<!--[if lt IE 9]>
<script type="text/javascript" src="<?php echo TPL;?>static/js/pie.js"></script>
<![endif]-->
</head>
<body>
 <?php include tpl("top",'content'); ?>
 <?php include tpl("head",'content'); ?>

	<section class="pt-30 pb-90 default-bg">
        <div class="container">
            <div class="row">
             <!-- Start Side bar widget -->
                <div class="col-lg-3">
				   <?php include tpl("left","member"); ?>
                </div>
                <!-- End of Side bar widget -->
                <div class="col-lg-8" style="width: 70%;">
                   <div class="row"><a href="#" class="link-procat">会员中心</a></div>
                   <div class="note note-info">
                          <h4 class="block">会员基本信息</h4>
                          <div class="meminfo">
                        <ul>
                             <li>
                                <span>用户名：  <?php echo $member["username"] ;?> </span>&nbsp;&nbsp;
                                <span>
                                    <a href="<?php echo url('member/member/myinfo');?>">资料修改</a>&nbsp;&nbsp;
                                </span>
                                <span>
                                    <a href="<?php echo url('member/member/pwd');?>">密码修改</a>
                                </span>
                            </li>
                            <li>
                                <span>会员组：  <?php echo GetMemberGroupName($member["groupid"]);?> </span>
                            </li>
                            <?php if(!empty($member["mobile"])) { ?>
                            <li>
                                 <span>手机：  <?php echo $member["mobile"] ;?> </span>
                            </li>
                            <?php } ?>
                            <?php if(!empty($member["name"])) { ?>
                            <li>
                                 <span>姓名：  <?php echo $member["name"] ;?> </span>
                            </li>
                            <?php } ?>
                            <?php if(!empty($member["email"])) { ?>
                            <li>
                                 <span>E-mail：  <?php echo $member["email"] ;?> </span>
                            </li>
                            <?php } ?>
                            <li>
                                <span >最近登录时间：</span>
                                <?php if($member['lastlogintime']){echo date('Y-m-d H:i:s',$member['lastlogintime']);}else{
                                echo date('Y-m-d H:i:s',$member['addtime']);
                                }?>
                            </li>
                        </ul>
                    </div>
				  	
			  
                                                        </div>  
                   
                   
                   
                </div>


            </div>
        </div>
    </section>
	
	
<!-- 页尾 -->
<!-- sidebar -->


<div class="m-sidebar">
  <div class="go-top J_gotop"><i class="ico"></i></div>
</div>
<!-- / sidebar --> 
<!-- 页尾 -->
 <?php include tpl("foot",'content'); ?>
<!-- / 页尾 --> 

<script src="<?php echo TPL;?>static/js/jquery-1.7.2.min.js"></script> 
<script src="<?php echo TPL;?>static/js/lazyload.js"></script> 
<script src="<?php echo TPL;?>static/js/jquery.superslide.2.1.1.js"></script> 
<script src="<?php echo TPL;?>static/js/main.js"></script> 
<script type="text/javascript">
$(document).ready(function(){
  $(".search-option").toggle(function(){ 
  $(this).next(".select").slideDown();
  },function(){ 
  $(this).next(".select").slideUp();
  }); 
$(".select dd").click(function(){ 
  $a=$(this).html();
  if($a=='XP系统'){
  $("#typeid").val('1');
  $(".search-option").html($a+'<i><em></em><span></span></i>'); 
  }
  else  if($a=='Win7系统'){
  $("#typeid").val('2');
  $(".search-option").html($a+'<i><em></em><span></span></i>'); 
  }
  else  if($a=='Win8系统'){
  $("#typeid").val('2');
  $(".search-option").html($a+'<i><em></em><span></span></i>'); 
  }
  else  if($a=='Win10系统'){
  $("#typeid").val('38');
  $(".search-option").html($a+'<i><em></em><span></span></i>'); 
  }
  else  if($a=='装机软件'){
  $("#typeid").val('5');
  $(".search-option").html($a+'<i><em></em><span></span></i>'); 
  }
  else  if($a=='主题下载'){
  $("#typeid").val('4');
  $(".search-option").html($a+'<i><em></em><span></span></i>'); 
  }
  else  if($a=='教程资讯'){
  $("#typeid").val('6');
  $(".search-option").html($a+'<i><em></em><span></span></i>'); 
  }
 $(".select").slideUp();
}); 
  
});
</script>
</body>
</html>
