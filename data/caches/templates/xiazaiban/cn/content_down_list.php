<?php defined('IN_YUNYECMS') or exit('No permission.'); ?><!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta name="applicable-device" content="pc">
<title><?php echo $seo["title"] ;?></title>
<meta name="keywords" content="<?php echo $seo["keywords"] ;?>" />
<meta name="description" content="<?php echo $seo["description"] ;?>" />
<link href="<?php echo TPL;?>static/css/style.css" rel="stylesheet" />
<link rel="alternate" media="only screen and(max-width: 640px)"href="/19363/m/list.php?tid=1" >
<link rel="canonical" href="/19363/m/19363/xp/"/>
<meta http-equiv="mobile-agent" content="format=xhtml;url=/19363/m/list.php?tid=1">
<script type="text/javascript">if(window.location.toString().indexOf('pref=padindex') != -1){}else{if(/AppleWebKit.*Mobile/i.test(navigator.userAgent) || (/MIDP|SymbianOS|NOKIA|SAMSUNG|LG|NEC|TCL|Alcatel|BIRD|DBTEL|Dopod|PHILIPS|HAIER|LENOVO|MOT-|Nokia|SonyEricsson|SIE-|Amoi|ZTE/.test(navigator.userAgent))){if(window.location.href.indexOf("?mobile")<0){try{if(/Android|Windows Phone|webOS|iPhone|iPod|BlackBerry/i.test(navigator.userAgent)){window.location.href="/19363/m/list.php?tid=1";}else if(/iPad/i.test(navigator.userAgent)){}else{}}catch(e){}}}}</script>
<!--IE8以下 圆角等补丁-->
<!--[if lt IE 9]>
<script type="text/javascript" src="<?php echo TPL;?>static/js/pie.js"></script>
<![endif]-->
</head>

<body>
 <?php include tpl("top"); ?>
 <?php include tpl("head"); ?> 
<div class="page-sys-list mt20">
  <div class="wrap auto">
	<?php if($rootcat["pic"]) { ?>
    <div class="AD_1200_108 ovh mb15" id="top_gg"><img src="<?php echo $rootcat["pic"] ;?>"></div>
    <?php } ?>
    <!-- 系统列表页 -->
    <div class="section m-list-section fix">
      <div class="section-a fl">
        <div class="c-cur-pos"> <span class="pos-txt">当前位置：</span><a href='<?php echo ROOT;?>'>首页</a>&nbsp;>&nbsp;
			       <?php $tag_parse = core::load_class('tag_parse');$data=$tag_parse->breadcumb_tag(array('catid'=>$catid));?>
                     <?php if(is_array($data)): $i = -1; $__DATA__ = $data;if(count($__DATA__)==0 )  echo "" ;foreach($__DATA__ as $key=>$v): $mod = ($i % 2 );++$i;?>
                         <?php if($v['id']!=$catid ) { ?>
						 <a href="<?php echo $v["url"] ;?>"><?php echo $v["title"] ;?></a>  &nbsp;>&nbsp;
						  <?php } else { ?>
						  <span><?php echo $v["title"] ;?></span> 
						<?php } ?>
					 <?php  endforeach;  endif; ?>
                
			 </div>
        <ul class="a-list mb30 fix">
		<?php if(is_array($list)): $i = -1; $__DATA__ = $list;if(count($__DATA__)==0 )  echo "" ;foreach($__DATA__ as $key=>$v): $mod = ($i % 2 );++$i;?>
			<li class="list-item fix"> <a href="<?php echo $v["url"] ;?>" title="<b><?php echo $v["title"] ;?></b>" target="_blank">
            <h3 class="item-title"><b><?php echo $v["title"] ;?></b></h3>
            </a> <a  href="<?php echo $v["url"] ;?>" title="<?php echo $v["title"] ;?>" target="_blank" class="fl"> <img class="list-item-img" src="<?php echo $v["pic"] ;?>" alt="<?php echo $v["title"] ;?>" width="240" height="180" /> </a>
            <div class="item-info">
              <p class="info-detail"> <span>语言：<?php echo $v["yuyan"] ;?> </span> <span class="ml30">大小：<?php echo $v["filesize"] ;?> </span> <span class="ml30">时间：<?php echo date("Y-m-d",$v["addtime"]);?></span> </p>
              <p class="info-desc">　<?php echo strcut(strip_tags($v["content"]),100);?>　</p>
              <p class="info-other"> <a  href="<?php echo $v["url"] ;?>" title="" target="_blank" class="info-btn">前往下载</a> <span>人气：<?php echo $v["hits"] ;?></span> <span class="ml30">系统等级：<span class="g-star sty-3 star-3"></span></span> </p>
            </div>
          </li>
	   <?php  endforeach;  endif; ?>
        </ul>
        <div class="m-pagination">
          <ul>
			   <?php echo $page;?>
          </ul>
        </div>
      </div>
      <div class="m-side-col col-r-w">
  <div class="g-title fix">
    <h2 class="title-txt"><?php echo $rootcat["title"] ;?></h2>
  </div>
  <div class="tag-list fix">  
	    <?php $tag_parse = core::load_class('tag_parse');$data=$tag_parse->cat_tag(array('pid'=>$rootcatid));?>
			 <?php if(is_array($data)): $i = -1; $__DATA__ = $data;if(count($__DATA__)==0 )  echo "" ;foreach($__DATA__ as $key=>$v): $mod = ($i % 2 );++$i;?>
				<a href='<?php echo $v["url"] ;?>'  class='tag-link  <?php if($catid==$v['id']||$rootcatid==$v['id'])  echo ' cur';	 ?>'><?php echo $v["title"] ;?></a> 
			 <?php  endforeach;  endif; ?>
		  
  </div>
  <div class="g-title fix">
    <h2 class="title-txt"><?php echo $rootcat["title"] ;?>排行</h2>
  </div>
  <div class="m-rank mb40">
    <ul>
	   <?php $tag_parse = core::load_class('tag_parse');$data="";$data=$tag_parse->list_tag(array('catid'=>$catid,'order'=>'hits desc','num'=>'10'));?> 
			   <?php if(is_array($data)): $i = -1; $__DATA__ = $data;if(count($__DATA__)==0 )  echo "" ;foreach($__DATA__ as $key=>$v): $mod = ($i % 2 );++$i;?>
				   <li class="rank-item"> <a href="<?php echo $v["url"] ;?>" title="<?php echo $v["title"] ;?>" class="item-name ellipsis"> <span class="g-sort-num no<?php echo $key+1;?>"><?php echo $key+1;?></span> <?php echo getsubstr($v["title"],0,26); ?> </a> </li>
				<?php  endforeach;  endif; ?>
	                              
    </ul>
  </div>
		
		  	<?php $tag_parse = core::load_class('tag_parse');$data="";$data=$tag_parse->list_tag(array('catid'=>'76','order'=>'ordernum asc','num'=>'3'));?> 
				   <?php if(is_array($data)): $i = -1; $__DATA__ = $data;if(count($__DATA__)==0 )  echo "" ;foreach($__DATA__ as $key=>$v): $mod = ($i % 2 );++$i;?>
		          <div class="AD_360_300 mb40" id="article_system_ad4"><a href="<?php echo $v["url"] ;?>"><img src="<?php echo $v["pic"] ;?>"></a></div>
				   <?php  endforeach;  endif; ?>
			  		  
		  
		  
  <div class="g-title fix">
    <h2 class="title-txt">系统教程</h2>
  </div>
  <div class="m-rank mb40">
    <ul>
		<?php $tag_parse = core::load_class('tag_parse');$data="";$data=$tag_parse->list_tag(array('catid'=>'3','order'=>'hits desc','num'=>'10'));?> 
		    <?php if(is_array($data)): $i = -1; $__DATA__ = $data;if(count($__DATA__)==0 )  echo "" ;foreach($__DATA__ as $key=>$v): $mod = ($i % 2 );++$i;?>
			   <li class="rank-item"> <a href="<?php echo $v["url"] ;?>" title="<?php echo $v["title"] ;?>" class="item-name ellipsis"> <span class="g-sort-num no<?php echo $key+1;?>"><?php echo $key+1;?></span> <?php echo getsubstr($v["title"],0,26); ?> </a> </li>
			<?php  endforeach;  endif; ?>
	      
    </ul>
  </div>
  <div class="g-title fix">
    <h2 class="title-txt">主题下载</h2>
  </div>
  <div class="m-rank u-dashed mb40">
    <ul>
		<?php $tag_parse = core::load_class('tag_parse');$data="";$data=$tag_parse->list_tag(array('catid'=>'66','order'=>'hits desc','num'=>'10'));?> 
		    <?php if(is_array($data)): $i = -1; $__DATA__ = $data;if(count($__DATA__)==0 )  echo "" ;foreach($__DATA__ as $key=>$v): $mod = ($i % 2 );++$i;?>
			   <li class="rank-item"> <a href="<?php echo $v["url"] ;?>" title="<?php echo $v["title"] ;?>" class="item-name ellipsis"> <span class="g-sort-num no<?php echo $key+1;?>"><?php echo $key+1;?></span> <?php echo getsubstr($v["title"],0,26); ?> </a> </li>
			<?php  endforeach;  endif; ?>
	      
    </ul>
  </div>
  
  <!--装机必备-->
  <div class="m-side-zjbb">
    <div class="g-title fix">
      <h2 class="title-txt">装机软件</h2>
    </div>
    <ul class="zjbb-list mb20 fix">
		<?php $tag_parse = core::load_class('tag_parse');$data="";$data=$tag_parse->list_tag(array('catid'=>'63','order'=>'hits desc','pic'=>'1','num'=>'9'));?> 
		    <?php if(is_array($data)): $i = -1; $__DATA__ = $data;if(count($__DATA__)==0 )  echo "" ;foreach($__DATA__ as $key=>$v): $mod = ($i % 2 );++$i;?>
		     <li class="list-item"><a href="<?php echo $v["url"] ;?>" title="<?php echo $v["title"] ;?>"  target="_blank"> <img class="item-img" src="<?php echo $v["pic"] ;?>" alt="<?php echo $v["title"] ;?>" width="80" height="80">
        <p class="item-title"><?php echo $v["title"] ;?></p>
        </a></li>
			<?php  endforeach;  endif; ?>
	      
    </ul>
  </div>
  <!--装机必备--> 
</div>
 </div>
    <!-- / 系统列表页 --> 
  </div>
</div>
<!-- 页尾 -->
<!-- sidebar -->


<div class="m-sidebar">
  <div class="go-top J_gotop"><i class="ico"></i></div>
</div>
<!-- / sidebar --> 
<!-- 页尾 -->
 <?php include tpl("foot"); ?>
<!-- / 页尾 --> 

<script src="<?php echo TPL;?>static/js/jquery-1.7.2.min.js"></script> 
<script src="<?php echo TPL;?>static/js/lazyload.js"></script> 
<script src="<?php echo TPL;?>static/js/jquery.superslide.2.1.1.js"></script> 
<script src="<?php echo TPL;?>static/js/main.js"></script> 
<script type="text/javascript">
$(document).ready(function(){
  $(".search-option").toggle(function(){ 
  $(this).next(".select").slideDown();
  },function(){ 
  $(this).next(".select").slideUp();
  }); 
$(".select dd").click(function(){ 
  $a=$(this).html();
  if($a=='XP系统'){
  $("#typeid").val('1');
  $(".search-option").html($a+'<i><em></em><span></span></i>'); 
  }
  else  if($a=='Win7系统'){
  $("#typeid").val('2');
  $(".search-option").html($a+'<i><em></em><span></span></i>'); 
  }
  else  if($a=='Win8系统'){
  $("#typeid").val('2');
  $(".search-option").html($a+'<i><em></em><span></span></i>'); 
  }
  else  if($a=='Win10系统'){
  $("#typeid").val('38');
  $(".search-option").html($a+'<i><em></em><span></span></i>'); 
  }
  else  if($a=='装机软件'){
  $("#typeid").val('5');
  $(".search-option").html($a+'<i><em></em><span></span></i>'); 
  }
  else  if($a=='主题下载'){
  $("#typeid").val('4');
  $(".search-option").html($a+'<i><em></em><span></span></i>'); 
  }
  else  if($a=='教程资讯'){
  $("#typeid").val('6');
  $(".search-option").html($a+'<i><em></em><span></span></i>'); 
  }
 $(".select").slideUp();
}); 
  
});
</script>
</body>
</html>
