<?php defined('IN_YUNYECMS') or exit('No permission.'); ?><!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta name="applicable-device" content="pc">
<title><?php echo $seo["title"] ;?></title>
<meta name="keywords" content="<?php echo $seo["keywords"] ;?>" />
<meta name="description" content="<?php echo $seo["description"] ;?>" />
<link href="<?php echo TPL;?>static/css/style.css" rel="stylesheet" />
<script src="<?php echo TPL;?>static/js/jquery-1.7.2.min.js"></script>
<script src="<?php echo TPL;?>static/js/dedeajax2.js"></script>
<link rel="canonical" href="<?php echo ROOT;?>"/>
<link rel="alternate" media="only screen and(max-width: 640px)" href="<?php echo ROOT;?>" >
<meta http-equiv="mobile-agent" content="format=xhtml;url=<?php echo ROOT;?>">
<script type="text/javascript">if(window.location.toString().indexOf('pref=padindex') != -1){}else{if(/AppleWebKit.*Mobile/i.test(navigator.userAgent) || (/MIDP|SymbianOS|NOKIA|SAMSUNG|LG|NEC|TCL|Alcatel|BIRD|DBTEL|Dopod|PHILIPS|HAIER|LENOVO|MOT-|Nokia|SonyEricsson|SIE-|Amoi|ZTE/.test(navigator.userAgent))){if(window.location.href.indexOf("?mobile")<0){try{if(/Android|Windows Phone|webOS|iPhone|iPod|BlackBerry/i.test(navigator.userAgent)){window.location.href="<?php echo ROOT;?>";}else if(/iPad/i.test(navigator.userAgent)){}else{}}catch(e){}}}}</script>
<!--IE8以下 圆角等补丁-->
<!--[if lt IE 9]>
<script type="text/javascript" src="<?php echo TPL;?>static/js/pie.js"></script>
<![endif]-->
</head>

<body>
 <?php include tpl("top"); ?>
 <?php include tpl("head"); ?> 

<div class="page-sys-art">
  <div class="wrap auto">
	<?php if($rootcat["pic"]) { ?>
    <div class="AD_1200_108 ovh mb15" id="top_gg"><img src="<?php echo $rootcat["pic"] ;?>"></div>
    <?php } ?>
	     <div class="c-cur-pos mb0"> <span class="pos-txt">当前位置：</span><a href='<?php echo ROOT;?>'>首页</a>&nbsp;>&nbsp;
			       <?php $tag_parse = core::load_class('tag_parse');$data=$tag_parse->breadcumb_tag(array('catid'=>$catid));?>
                     <?php if(is_array($data)): $i = -1; $__DATA__ = $data;if(count($__DATA__)==0 )  echo "" ;foreach($__DATA__ as $key=>$v): $mod = ($i % 2 );++$i;?>
                         <?php if($v['id']!=$catid ) { ?>
						 <a href="<?php echo $v["url"] ;?>"><?php echo $v["title"] ;?></a>  &nbsp;>&nbsp;
						  <?php } else { ?>
						  <span><?php echo $v["title"] ;?></span> 
						<?php } ?>
					 <?php  endforeach;  endif; ?>
                 &nbsp;>&nbsp; <span><?php echo $row["title"] ;?></span>
		</div>
    <!-- 系统详情页 -->
    <div class="s-sys-art fix sys_902">
      <h1 class="art-title"><?php echo $row["title"] ;?></h1>
      <div class="art-main col-l-w">
        <div class="art-detail fix">
          <div class="fix mb20">
			   <?php if($row["pic"]!='') { ?>
			  <img class="art-img" src="<?php echo $row["pic"] ;?>" alt="<?php echo $row["title"] ;?>" width="256" height="192" >
                <?php } ?>
            <ul class="m-soft-detail fix">
              <li class="item"><span class="c-999">文件大小：</span><?php echo $row["filesize"] ;?></li>
              <li class="item"><span class="c-999">界面语言：</span><?php echo $row["yuyan"] ;?></li>
              <li class="item"><span class="c-999">文件类型：</span>.<?php echo $row["filetype"] ;?></li>
              <li class="item"><span class="c-999">授权方式：</span><?php echo $row["shouquan"] ;?></li>
              <li class="item"><span class="c-999">软件类型：</span><?php echo $row["softtype"] ;?></li>
              <li class="item"><span class="c-999">发布时间：</span><?php echo $row["fbtime"] ;?></li>
              <li class="item"><span class="c-999">运行环境：</span><?php echo $row["huanjing"] ;?></li>
              <li class="item"><span class="c-999">下载次数：</span><?php echo $row["hits"] ;?></li>
              <li class="item"><span class="c-999">软件等级：</span><span class="g-star sty-5 star-3"></span></li>
              <li class="item full-w">
				  <?php  $anquan=$row['anquan'];
				     if(!empty($anquan)){ $anquan =explode(",",$anquan); }
				  ?>
				  <span class="c-999 dib vam">安全检测：</span> 
				   <?php if(is_array($anquan)): $i = -1; $__DATA__ = $anquan;if(count($__DATA__)==0 )  echo "" ;foreach($__DATA__ as $key=>$var): $mod = ($i % 2 );++$i;?>
				     <?php if($var=="360安全卫士") { ?> <span> <i class="ico ico-1"></i><em class="dib vam"><?php echo $var;?></em><i class="ico ico-access"></i> </span> <?php } ?>
				  	 <?php if($var=="360杀毒") { ?> <span class="ml20"> <i class="ico ico-2"></i><em class="dib vam"><?php echo $var;?></em><i class="ico ico-access"></i> </span> <?php } ?>
				  	  <?php if($var=="电脑管家") { ?> <span class="ml20"> <i class="ico ico-3"></i><em class="dib vam"><?php echo $var;?></em><i class="ico ico-access"></i> </span> <?php } ?>
                   <?php  endforeach;  endif; ?>
			</li>
              <li class="item full-w fix"><span class="c-999 fl">分享好友：</span> 
                <!-- Baidu Button BEGIN -->
                <div class="bdsharebuttonbox" data-tag="bdshare"> <a class="bds_weixin" data-cmd="weixin"></a> <a class="bds_qzone" data-cmd="qzone" href="#"></a> <a class="bds_tsina" data-cmd="tsina"></a> <a class="bds_more" data-cmd="more"></a> </div>
                <!-- Baidu Button END --> 
              </li>
            </ul>
          </div>
          <div class="fix"> <a href="#J_download" class="btn-dl fl"></a> 
            <!--script language="javascript" type="text/javascript">getDigg(164724);</script-->
            <div class="sys_tag fl">
              <div class="ico_sys_tag fl"></div>
              <ul class="sys_item_link">
                <li style='display:none'>
                <a href="/19363/xp/ylmf/163.html" title="雨林木风 GHOST XP SP3 快速安全版 V201"></a>
                </li>
<li style='display:none'>
                <a href="/19363/xp/ylmf/162.html" title="雨林木风 GHOST XP SP3 暑假装机版 V201"></a>
                </li>
<li >
                <a href="/19363/xp/ylmf/161.html" title="雨林木风 GHOST XP SP3 极速增强版 V201">雨林木风 SP3</a>
                </li>
<li style='display:none'>
                <a href="/19363/xp/ylmf/160.html" title="雨林木风 GHOST XP SP3 经典旗舰版 V201"></a>
                </li>
<li style='display:none'>
                <a href="/19363/xp/ylmf/117.html" title="雨林木风 GHOST XP SP3 经典珍藏版 YN20"></a>
                </li>
<li style='display:none'>
                <a href="/19363/xp/ylmf/87.html" title="雨林木风Ghost XP_SP3 贺岁版"></a>
                </li>
<li style='display:none'>
                <a href="/19363/xp/ylmf/88.html" title="雨林木风Ghost XP_SP3 贺岁版"></a>
                </li>
<li style='display:none'>
                <a href="/19363/xp/ylmf/89.html" title="雨林木风Ghost XP_SP3 贺岁版"></a>
                </li>

              </ul>
            </div>
          </div>
        </div>
        <div class="g-title fix">
          <h2 class="title-txt c-g-blue">系统简介</h2>
        </div>
        <div class="m-art-cont">
           <p><?php echo $row["content"] ;?></p>
        </div>
        <div class="m-art-dl fix">
          <div class="g-title fix" id="J_download">
            <h2 class="title-txt">下载地址</h2>
            <!--<a class="goto-qq fr" target="_blank" href=""></a>--> 
          </div>
          <div class="inner fl fix">
            <ul class="media fix" id="localdown">
              <li><a href="<?php echo url('download',array('catid'=>$row['catid'],'id'=>$row['id']))?>" target="_blank">本地下载</a></li>
            </ul>
          </div>
          <div class="fl">
		   <?php $tag_parse = core::load_class('tag_parse');$data="";$data=$tag_parse->list_tag(array('catid'=>'77','order'=>'ordernum asc','num'=>'3'));?> 
				   <?php if(is_array($data)): $i = -1; $__DATA__ = $data;if(count($__DATA__)==0 )  echo "" ;foreach($__DATA__ as $key=>$v): $mod = ($i % 2 );++$i;?>
						<?php if($key==0):?><div class="AD_400_190 mb20"><a href="<?php echo $v["url"] ;?>"><img src="<?php echo $v["pic"] ;?>"></a></div><?php endif;?>
						<?php if($key==1):?><div class="AD_190_90 fl"><a href="<?php echo $v["url"] ;?>"><img src="<?php echo $v["pic"] ;?>"></a></div><?php endif;?>
						<?php if($key==2):?><div class="AD_190_90 fl ml20"><a href="<?php echo $v["url"] ;?>"><img src="<?php echo $v["pic"] ;?>"></a></div><?php endif;?>
				   <?php  endforeach;  endif; ?>
			  			  
          </div>
        </div>
        <div class="m-faq">
          <div class="g-title fix">
            <h2 class="title-txt">常见问题</h2>
          </div>
          <ul class="faq-list">
			<?php $tag_parse = core::load_class('tag_parse');$data="";$data=$tag_parse->list_tag(array('catid'=>'75','order'=>'addtime desc','num'=>'6'));?> 
				<?php if(is_array($data)): $i = -1; $__DATA__ = $data;if(count($__DATA__)==0 )  echo "" ;foreach($__DATA__ as $key=>$v): $mod = ($i % 2 );++$i;?>
				  <li class="item"> <span class="g-time fr"><?php echo date("Y-m-d",$v["addtime"]);?></span> <a  href="<?php echo $v["url"] ;?>" title="<?php echo $v["title"] ;?>" target="_blank" class="g-list-a" ><?php echo getsubstr($v["title"],0,26); ?> </a> </li>
				<?php  endforeach;  endif; ?>
			   
          </ul>
        </div>
        <div class="m-changyan" style="display: none;">
          <div class="g-title fix">
            <h2 class="title-txt">发表评论</h2>
          </div>
          <!-- //AJAX评论区 开始 --> 
          <script type='text/javascript' src='static/js/jquery.min.js'></script><!-- //如果你页面中已经加载过jquery库，请删除这一行，防止冲突 -->
          <link href="<?php echo TPL;?>static/css/comments.css" rel="stylesheet" type="text/css" />
          <div id="comments" class="entry-comments"> <div id="respond" class="comment-respond">
  <form id="commentform" action="" method="post">
    <input type="hidden" name="dopost" value="send" />
    <input type="hidden" name="aid" value="163" />
    <div class="comt-ctrl">
      <div class="comt-tips">
        <input type='hidden' name='comment_post_ID' value='163' id='comment_post_ID' />
        <input type='hidden' name='comment_parent' id='comment_parent' value='0' />
        <input type="hidden" id="_wp_unfiltered_html_comment_disabled" name="_wp_unfiltered_html_comment_disabled" value="caa5eb0098" />
        <script>(function(){if(window===window.parent){document.getElementById('_wp_unfiltered_html_comment_disabled').name='_wp_unfiltered_html_comment';}})();</script> 
      </div>
    </div>
    <div class="comment-form" id="_ajax_feedback">
      <div class="comment-form-comment">
        <textarea disabled="disabled" placeholder="需要登录才能发表评论" id="comment" name="msg" class="ipt" rows="4"></textarea>
      </div>
      <div class="form-submit">
        <div class="form-submit-text pull-left"><a title="注册" href="/member/index_do.php?fmdo=user&dopost=regnew">注册</a>&nbsp;&nbsp;<a title="登录" href="../member/login.php">登录</a></div>
        <input name="submit" type="submit" id="must-submit" class="submit" value="发表">
        <input type="button" id="cancel-comment-reply-link" class="submit" value="取消" style="display:none;">
      </div>
    </div>
  </form>
</div>
<script language='javascript'>
function CheckCommentsLogin(){
	var taget_obj = document.getElementById('_ajax_feedback');
	myajax = new DedeAjax(taget_obj,false,false,'','','');
	myajax.SendGet2("/19363/comments/checkcommentslogin.php?aid=163");
	DedeXHTTP = null;
}
CheckCommentsLogin();
</script>
<h3 class="comments-title"> 评论列表（<script type="text/javascript" src="<?php echo TPL;?>static/js/count.js"></script>条）</h3>
<div id="postcomments">
  <ul class="comments-list" id="commetcontent">
  </ul>
  <div class="pagination clearfix"> </div>
</div>
<script language='javascript'>
function LoadCommets(page)
{
	var taget_obj = document.getElementById('commetcontent');
	var waithtml = "<div style='line-height:50px'><img src='static/images/loadinglit.gif' />评论加载中...</div>";
	var myajax = new DedeAjax(taget_obj, true, true, '', 'x', waithtml);
	myajax.SendGet2("/19363/comments/comments.php?dopost=getlist&aid=163&page="+page);
	DedeXHTTP = null;
}
LoadCommets(1);
</script> </div>
          <script>
window.jsui = {
    uri: ''
};
</script> 
          <script type='text/javascript' src='static/js/loader.js'></script> 
          <!-- //AJAX评论区 结束 --> 
        </div>
      </div>
      <div class="m-side-col col-r-w sys_side_830">
        <div class="m-related-list mb50">
          <h2 class="list-title">相关下载</h2>
          <ul class="related-list">
			   <?php $tag_parse = core::load_class('tag_parse');$data=$tag_parse->relation_tag(array('catid'=>$catid,'num'=>'20','id'=>$id,'keywords'=>$row['seokeywords']));?> 
                               <?php if(is_array($data)): $i = -1; $__DATA__ = $data;if(count($__DATA__)==0 )  echo "" ;foreach($__DATA__ as $key=>$v): $mod = ($i % 2 );++$i;?>
                                     <li class="item"> <span class="g-time fr"><?php echo date("m-d",$v["addtime"]);?></span> <a href="<?php echo $v["url"] ;?>" title="<?php echo $v["title"] ;?>" class="g-list-a"> <?php echo strcut($v["title"],15); ?></a> </li>
			  
                                 <?php  endforeach;  endif; ?>
				      
          </ul>
        </div>
        <div class=" J_g_mouseover_tab">
          <div class="g-title border-blue fix mb25">
            <h2 class="title-txt"><?php echo $cat["title"] ;?>下载排行</h2>
          </div>
          <div class="J_tab_cont">
            <div class="tab-cont-item fix" style="display: block">
              <div class="m-rank u-dashed mb40">
                <ul>
					<?php $tag_parse = core::load_class('tag_parse');$data="";$data=$tag_parse->list_tag(array('catid'=>$cat['id'],'order'=>'hits desc','num'=>'10'));?> 
							<?php if(is_array($data)): $i = -1; $__DATA__ = $data;if(count($__DATA__)==0 )  echo "" ;foreach($__DATA__ as $key=>$v): $mod = ($i % 2 );++$i;?>
							   <li class="rank-item"> <a href="<?php echo $v["url"] ;?>" title="<?php echo $v["title"] ;?>" class="item-name ellipsis"> <span class="g-art-count fr"><?php echo $v["hits"] ;?>次</span> <span class="g-sort-num no<?php echo $key+1;?>"><?php echo $key+1;?></span> <?php echo getsubstr($v["title"],0,26); ?></a> </li>
							<?php  endforeach;  endif; ?>
					    
                </ul>
              </div>
            </div>
          </div>
        </div>
		  	<?php $tag_parse = core::load_class('tag_parse');$data="";$data=$tag_parse->list_tag(array('catid'=>'78','order'=>'ordernum asc','num'=>'3'));?> 
				   <?php if(is_array($data)): $i = -1; $__DATA__ = $data;if(count($__DATA__)==0 )  echo "" ;foreach($__DATA__ as $key=>$v): $mod = ($i % 2 );++$i;?>
		          <div class="AD_360_300 mb40" id="article_system_ad4"><a href="<?php echo $v["url"] ;?>"><img src="<?php echo $v["pic"] ;?>"></a></div>
				   <?php  endforeach;  endif; ?>
			  
		  
		  
		  
		  
        <div class="g-title fix border-blue"> <a href="<?php echo url('lists',array('catid'=>$rootcat['id']))?>" target="_blank" class="more fr" style="color:#358ff0">更多+</a>
          <h2 class="title-txt ">其他人正在下载</h2>
        </div>
        <ul class="sys_qt mb40">
			<?php $tag_parse = core::load_class('tag_parse');$data="";$data=$tag_parse->list_tag(array('catid'=>$rootcat['id'],'order'=>'hits desc','num'=>'10'));?> 
					<?php if(is_array($data)): $i = -1; $__DATA__ = $data;if(count($__DATA__)==0 )  echo "" ;foreach($__DATA__ as $key=>$v): $mod = ($i % 2 );++$i;?>
					  <li><a href="<?php echo $v["url"] ;?>" title="<?php echo $v["title"] ;?>"><?php echo getsubstr($v["title"],0,10); ?></a></li>
					<?php  endforeach;  endif; ?>
			    
        </ul>
        <div class="g-title fix">
          <h2 class="title-txt">系统教程排行</h2>
        </div>
        <div class="m-rank u-dashed mb40">
          <ul>
		   <?php $tag_parse = core::load_class('tag_parse');$data="";$data=$tag_parse->list_tag(array('catid'=>'3','order'=>'hits desc','num'=>'10'));?> 
		    <?php if(is_array($data)): $i = -1; $__DATA__ = $data;if(count($__DATA__)==0 )  echo "" ;foreach($__DATA__ as $key=>$v): $mod = ($i % 2 );++$i;?>
			   <li class="rank-item"> <a href="<?php echo $v["url"] ;?>" title="<?php echo $v["title"] ;?>" class="item-name ellipsis"> <span class="g-art-count fr"><?php echo $v["hits"] ;?>次</span> <span class="g-sort-num no1"><?php echo $key+1;?></span> <?php echo getsubstr($v["title"],0,26); ?></a> </li>
			<?php  endforeach;  endif; ?>
	           
          </ul>
        </div>
        <div class="g-title fix border-blue mb17">
          <h2 class="title-txt ">主题下载</h2>
        </div>
        <div class="banner1">
          <div class="focusBox" style="margin:0 auto">
            <div class="pic">
              <ul>
				  <?php $tag_parse = core::load_class('tag_parse');$data="";$data=$tag_parse->list_tag(array('catid'=>'66','order'=>'hits desc','pic'=>'1','num'=>'5'));?> 
					<?php if(is_array($data)): $i = -1; $__DATA__ = $data;if(count($__DATA__)==0 )  echo "" ;foreach($__DATA__ as $key=>$v): $mod = ($i % 2 );++$i;?>
						 <li> <a href="<?php echo $v["url"] ;?>" title="<?php echo $v["title"] ;?>" target="_blank"> <img src="<?php echo $v["pic"] ;?>" alt="<?php echo $v["title"] ;?>"> </a>
						  <div class="txt-bg">
							<p><?php echo getsubstr($v["title"],0,15); ?></p>
						  </div>
						</li>
					<?php  endforeach;  endif; ?>
				       				  
              </ul>
            </div>
            <div class="num">
              <ul>
				   <?php $tag_parse = core::load_class('tag_parse');$data="";$data=$tag_parse->list_tag(array('catid'=>'66','order'=>'hits desc','pic'=>'1','num'=>'5'));?> 
					<?php if(is_array($data)): $i = -1; $__DATA__ = $data;if(count($__DATA__)==0 )  echo "" ;foreach($__DATA__ as $key=>$v): $mod = ($i % 2 );++$i;?>
                      <li class="<?php if($key==0) { ?>on<?php } ?>"></li>
					<?php  endforeach;  endif; ?>
				       	 
               
              </ul>
            </div>
          </div>
        </div>
        <div class="special mb40">
          <ul class="special-list">
            <?php $tag_parse = core::load_class('tag_parse');$data="";$data=$tag_parse->list_tag(array('catid'=>'66','order'=>'hits desc','pic'=>'1','num'=>'5'));?> 
					<?php if(is_array($data)): $i = -1; $__DATA__ = $data;if(count($__DATA__)==0 )  echo "" ;foreach($__DATA__ as $key=>$v): $mod = ($i % 2 );++$i;?>
						<li> <a  href="<?php echo $v["url"] ;?>" title="<?php echo $v["title"] ;?>"> <span>主题</span>
              <p><?php echo getsubstr($v["title"],0,15); ?></p>
              </a> </li>
					<?php  endforeach;  endif; ?>
			     				  
                   
          </ul>
        </div>
      </div>
    </div>
    <!-- / 系统详情页 --> 
  </div>
</div>
<!-- 页尾 -->
<!-- sidebar -->


<div class="m-sidebar">
  <div class="go-top J_gotop"><i class="ico"></i></div>
</div>
<!-- / sidebar --> 
<!-- 页尾 -->
 <?php include tpl("foot"); ?>
<!-- / 页尾 --> 

<script src="<?php echo TPL;?>static/js/jquery-1.7.2.min.js"></script> 
<script src="<?php echo TPL;?>static/js/lazyload.js"></script> 
<script src="<?php echo TPL;?>static/js/jquery.superslide.2.1.1.js"></script> 
<script src="<?php echo TPL;?>static/js/main.js"></script> 
<script type="text/javascript">
$(document).ready(function(){
  $(".search-option").toggle(function(){ 
  $(this).next(".select").slideDown();
  },function(){ 
  $(this).next(".select").slideUp();
  }); 
$(".select dd").click(function(){ 
  $a=$(this).html();
  if($a=='XP系统'){
  $("#typeid").val('1');
  $(".search-option").html($a+'<i><em></em><span></span></i>'); 
  }
  else  if($a=='Win7系统'){
  $("#typeid").val('2');
  $(".search-option").html($a+'<i><em></em><span></span></i>'); 
  }
  else  if($a=='Win8系统'){
  $("#typeid").val('2');
  $(".search-option").html($a+'<i><em></em><span></span></i>'); 
  }
  else  if($a=='Win10系统'){
  $("#typeid").val('38');
  $(".search-option").html($a+'<i><em></em><span></span></i>'); 
  }
  else  if($a=='装机软件'){
  $("#typeid").val('5');
  $(".search-option").html($a+'<i><em></em><span></span></i>'); 
  }
  else  if($a=='主题下载'){
  $("#typeid").val('4');
  $(".search-option").html($a+'<i><em></em><span></span></i>'); 
  }
  else  if($a=='教程资讯'){
  $("#typeid").val('6');
  $(".search-option").html($a+'<i><em></em><span></span></i>'); 
  }
 $(".select").slideUp();
}); 
  
});
</script> 
<script>
        window._bd_share_config = {
            share : [{
                "bdSize" : 16
            }]
        }
        with(document)0[(getElementsByTagName('head')[0]||body).appendChild(createElement('script')).src='http://bdimg.share.baidu.com/static/api/js/share.js?cdnversion='+~(-new Date()/36e5)];
    </script>
</body>
</html>
