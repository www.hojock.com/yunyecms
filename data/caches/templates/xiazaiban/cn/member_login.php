<?php defined('IN_YUNYECMS') or exit('No permission.'); ?><!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta name="applicable-device" content="pc">
<title><?php echo $seo["title"] ;?></title>
<meta name="keywords" content="<?php echo $seo["keywords"] ;?>" />
<meta name="description" content="<?php echo $seo["description"] ;?>" />
    <link rel="stylesheet" href="<?php echo TPL;?>member/css/font-awesome.min.css">
    <!--==== Bootstrap css file ====-->
    <link rel="stylesheet" href="<?php echo TPL;?>member/css/bootstrap.min.css">
    <!--==== Swiper css file ====-->
    <link rel="stylesheet" href="<?php echo TPL;?>member/css/swiper.min.css">
    <!--==== Style css file ====-->
    <link rel="stylesheet" href="<?php echo TPL;?>member/css/style.css?201901">
    <!--==== Responsive css file ====-->
    <link rel="stylesheet" href="<?php echo TPL;?>member/css/responsive.css?201901">
    <!--==== Theme Color 1 css file ====-->
    <link rel="stylesheet" href="<?php echo TPL;?>member/css/theme-color-1.css">
    <!--==== Custom css file ====-->
    <link rel="stylesheet" href="<?php echo TPL;?>member/css/custom.css">	
	
<link href="<?php echo TPL;?>static/css/style.css" rel="stylesheet" />
<link rel="alternate" media="only screen and(max-width: 640px)" href="<?php echo ROOT;?>" >
<meta http-equiv="mobile-agent" content="format=xhtml;url=<?php echo ROOT;?>">
<script type="text/javascript">if(window.location.toString().indexOf('pref=padindex') != -1){}else{if(/AppleWebKit.*Mobile/i.test(navigator.userAgent) || (/MIDP|SymbianOS|NOKIA|SAMSUNG|LG|NEC|TCL|Alcatel|BIRD|DBTEL|Dopod|PHILIPS|HAIER|LENOVO|MOT-|Nokia|SonyEricsson|SIE-|Amoi|ZTE/.test(navigator.userAgent))){if(window.location.href.indexOf("?mobile")<0){try{if(/Android|Windows Phone|webOS|iPhone|iPod|BlackBerry/i.test(navigator.userAgent)){window.location.href="<?php echo ROOT;?>";}else if(/iPad/i.test(navigator.userAgent)){}else{}}catch(e){}}}}</script>
	
<!--IE8以下 圆角等补丁-->
<!--[if lt IE 9]>
<script type="text/javascript" src="<?php echo TPL;?>static/js/pie.js"></script>
<![endif]-->
</head>
<body>
 <?php include tpl("top",'content'); ?>
 <?php include tpl("head",'content'); ?>
<div class="page-tag">
  <div class="wrap auto">
	<?php if($rootcat["pic"]) { ?>
      <div class="AD_1200_108 ovh mb15" id="top_gg"><img src="<?php echo $rootcat["pic"] ;?>"></div>
    <?php } ?>   
	  <!-- 标签页 -->
<section class="pt-50 pb-120 default-bg">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <!-- gallery button inner-->
                    <div class="row">
                        <div class="col">
                            <div class="gallery--button-inner text-center" >
                                <div class="gallery-button">
                                    <!-- Start Gallery nav -->
                                    <nav>
                                        <ul class="gallery_filter">
											<li  ><a href="<?php echo url('member/member/register');?>"><span class="gallery-btn pt-sans">会员注册</span></a></li> 
											<li class="active"><a href="<?php echo url('member/member/login');?>"><span class="gallery-btn pt-sans">会员登录</span></a></li> 
										</ul>
                                    </nav>
                                    <!-- Start Gallery nav -->
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- End Of gallery button inner-->
                    <!-- gallery sorting area -->
                    <div class="row">    
                        <div class="col-md-12">
                             <div class="about-content" style="line-height: 2;font-size: 16px;">    
     <script language="javascript">
		function check() {
				var username=document.getElementById("username");
				var pwd=document.getElementById("pwd");
				if (username.value==""){
					window.alert("用户名不能为空!");
					username.focus();
					return false ;
				 }				
				if (pwd.value==""){
					window.alert("密码不能为空!");
					pwd.focus();
					return false ;
				}	
		}
       </script>                              
         	    <form name=userinfoform class="form-horizontal"  method="post"  action="<?php echo url('member/member/logincheck');?>"   enctype="multipart/form-data"  onsubmit="return check();" >
                         <div class="form-group form-row">
								<label class="col-sm-4 col-form-label  pc_right " for="keywords">用户名/手机：</label>
								 <div class="col-sm-5">
									<input type="text"   class="form-control"  name="username" id="username" placeholder="请输入用户名或手机号码"  />
								</div>
								<div class="col-sm-2">
                                 <span class="font-red"> * </span>
                               </div>
						 </div> 
                        <div class="form-group form-row">
								<label class="col-sm-4 col-form-label  pc_right " for="keywords">密码：</label>
								 <div class="col-sm-5">
									<input type="password"   class="form-control"  name="pwd" id="pwd" placeholder="请输入密码"  />
								</div>
								<div class="col-sm-2">
                                 <span class="font-red"> * </span>
                               </div>
						 </div>  
                        <div class="form-group form-row">
                          <label for="tel" class="col-sm-4 control-label"></label>
                          <div class="col-sm-offset-4 col-sm-5">
                            <input type="hidden" name="token" id="token" value="<?php echo $token;?>">
                            <button type="submit" name="button"  class="btn  btn-primary"> <i class="fa fa-sign-in"> </i> 登 录 </button>&nbsp;
                          </div>
                        </div>
                   </form>
                             </div>     
                        </div>
                    </div>
                    <!-- End of gallery sorting area -->
                </div>
            </div>
        </div>
    </section>
    <!-- / 标签页 --> 
  </div>
</div>
<!-- 页尾 -->
<!-- sidebar -->


<div class="m-sidebar">
  <div class="go-top J_gotop"><i class="ico"></i></div>
</div>
<!-- / sidebar --> 
<!-- 页尾 -->
 <?php include tpl("foot",'content'); ?>
<!-- / 页尾 --> 

<script src="<?php echo TPL;?>static/js/jquery-1.7.2.min.js"></script> 
<script src="<?php echo TPL;?>static/js/lazyload.js"></script> 
<script src="<?php echo TPL;?>static/js/jquery.superslide.2.1.1.js"></script> 
<script src="<?php echo TPL;?>static/js/main.js"></script> 
<script type="text/javascript">
$(document).ready(function(){
  $(".search-option").toggle(function(){ 
  $(this).next(".select").slideDown();
  },function(){ 
  $(this).next(".select").slideUp();
  }); 
$(".select dd").click(function(){ 
  $a=$(this).html();
  if($a=='XP系统'){
  $("#typeid").val('1');
  $(".search-option").html($a+'<i><em></em><span></span></i>'); 
  }
  else  if($a=='Win7系统'){
  $("#typeid").val('2');
  $(".search-option").html($a+'<i><em></em><span></span></i>'); 
  }
  else  if($a=='Win8系统'){
  $("#typeid").val('2');
  $(".search-option").html($a+'<i><em></em><span></span></i>'); 
  }
  else  if($a=='Win10系统'){
  $("#typeid").val('38');
  $(".search-option").html($a+'<i><em></em><span></span></i>'); 
  }
  else  if($a=='装机软件'){
  $("#typeid").val('5');
  $(".search-option").html($a+'<i><em></em><span></span></i>'); 
  }
  else  if($a=='主题下载'){
  $("#typeid").val('4');
  $(".search-option").html($a+'<i><em></em><span></span></i>'); 
  }
  else  if($a=='教程资讯'){
  $("#typeid").val('6');
  $(".search-option").html($a+'<i><em></em><span></span></i>'); 
  }
 $(".select").slideUp();
}); 
  
});
</script>
</body>
</html>
