<?php
$LANG['cart'] = '购物车';
$LANG['goodsid_is_notnum'] = '商品ID必须为数字，谢谢!';
$LANG['goods_parm_error'] = '商品参数错误，谢谢!';
$LANG['goods_price_require'] = '商品价格不能为空，谢谢!';
$LANG['cart_update_error'] = '更新购物车失败!';
$LANG['cart_insert_error'] = '添加购物车失败!';
$LANG['online_pay'] = '在线付款';
$LANG['improve_info_first'] = '请先完善您的资料，谢谢!';
$LANG['goods_least_one'] = '您至少应该选择一个商品!';
$LANG['error_goods_isdelete'] = '操作失败，您所选的商品已从购物车中删除！';
$LANG['error_checkout'] = '结算失败，请联系网站管理员!';
$LANG['orders_success_msg'] = '用户下单：订单号-%s，用户-%s-%s-%s';
$LANG['cart_goods_removeok'] = '您已成功将该商品移出购物车!';
$LANG['pay_success'] = '支付成功!';
$LANG['myorders'] = '我的订单';
$LANG['order_parm_error'] = '订单参数错误，请联系网站管理员!';
$LANG['order_not_exsit'] = '操作失败，该订单不存在！';


?>