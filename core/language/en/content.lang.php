<?php
$LANG['content_model_notexist'] = 'This information model does not exist';
$LANG['submit_daylimit'] = 'Sorry, you can only submit 10 times a day!';
$LANG['form_email_msg'] = 'You received a message from %s';
$LANG['required_fdtitle'] = '%s cannot be empty, please re-enter!';
$LANG['form_info_ok'] = '%s information submission success!';
$LANG['form_info_error'] = '%s information submission failed！';
?>