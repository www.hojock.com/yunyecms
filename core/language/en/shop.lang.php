<?php
$LANG['cart'] = 'Shopping Cart';
$LANG['goodsid_is_notnum'] = 'Commodity ID must be digital, thank you!';
$LANG['goods_parm_error'] = 'The commodity parameters are wrong. Thank you.';
$LANG['goods_price_require'] = 'Commodity prices can not be empty, thank you!';
$LANG['cart_update_error'] = 'Failed to update shopping cart!';
$LANG['cart_insert_error'] = 'Failure to add shopping cart!';
$LANG['online_pay'] = 'Online payment';
$LANG['improve_info_first'] = 'Please complete your information first. Thank you.';
$LANG['goods_least_one'] = 'You should choose at least one item!';
$LANG['error_goods_isdelete'] = 'The operation failed and the item you selected was deleted from the shopping cart！';
$LANG['error_checkout'] = 'Settlement failed, please contact the webmaster!';
$LANG['orders_success_msg'] = 'User orders:order number-%s,user-%s-%s-%s';
$LANG['cart_goods_removeok'] = 'You have successfully removed the item from the shopping cart.!';
$LANG['pay_success'] = 'Successful payment!';
$LANG['myorders'] = 'My order';
$LANG['order_parm_error'] = 'The order parameters are wrong. Please contact the webmaster.!';
$LANG['order_not_exsit'] = 'The operation failed and the order did not exist！';


?>