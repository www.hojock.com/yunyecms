<?php
define('InYUNYECMS',TRUE);
require '../config/const.php';
require YUNYECMS_CORE.'lib'.DIRECTORY_SEPARATOR.'captcha.class.php';
require YUNYECMS_CORE.'func'.DIRECTORY_SEPARATOR.'core.fun.php';
$identifying = new captcha();
$code = make_rand_letternum(4);
session_save_path(YUNYECMS_ADMSESSION);
session_start();
$_SESSION['admcode'] = strtolower($code);
$identifying->image_one($code);