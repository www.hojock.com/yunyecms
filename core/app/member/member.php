<?php
defined('IN_YUNYECMS') or exit('No permission.');
core::load_class('common',false);
core::load_fun('content');
core::load_fun('tree');
class member extends common {
    public $curmodel;
	function __construct() {
	 $this->db = core::load_model('content_model');
	 parent::__construct();
	 }
	//首页
	 public function index(){
        $seo['title']=$this->lang["seotitle"];
        $seo['keywords']=$this->lang["seokey"];
        $seo['description']=$this->lang["seodesc"];
		$seostr=Lan('member_center');
        $seo['title']="{$seostr}-{$seo['title']}";
        $seo['keywords']="{$seostr}-{$seo['keywords']}";
		$cfg=$this->cfg;
		$lang=$this->lang; 
	    $breadcumb=array('0'=>array('title'=>Lan('member_center'),'url'=>url("member/member/index")),
              '1'=>array('title'=>Lan('member_center'),'url'=>url("member/member/index"))
			);		
		$userid=usafestr(yunyecms_strdecode(ugetcookie("userid")));
		$member=$this->member;
        include tpl('index','member');
	}
	
	 public function register(){
        $seo['title']=$this->lang["seotitle"];
        $seo['keywords']=$this->lang["seokey"];
        $seo['description']=$this->lang["seodesc"];
		$seostr=Lan('member_register')."-".Lan('member_center');
        $seo['title']="{$seostr}-{$seo['title']}";
        $seo['keywords']="{$seostr}-{$seo['keywords']}";
        $seo['description']="{$seostr}-{$seo['description']}";
		$breadcumb=array('0'=>array('title'=>Lan('member_center'),'url'=>url("member/member/index")),
              '1'=>array('title'=>Lan('member_register'),'url'=>url("member/member/register"))
			);
		$token=make_rand_letternum(16);
		$_SESSION["token"]=$token;
		$cfg=$this->cfg;
		$lang=$this->lang; 
        include tpl('register','member');
	}
	
	
 function insert() {
	    $_POST=yytrim($_POST);
		 if(empty($_POST['username'])){
		     messagebox(Lan('required_username'));		
			}else{
	        $username=usafestr($_POST['username']);
		    $data['username'] = $username;
			 if(check_mobile($username)){
				$mobile=$username; 
			    $data['phone'] = $mobile;
			    $data['mobile'] = $mobile; 
			 }
		 }
	   if(empty($_POST['pwd'])){
		    messagebox(Lan('required_password'));		
		   }else{
		    $pwd=trim($_POST['pwd']);
	        $pwd=yypwd($_POST['pwd']);
		    $data['pwd'] = $pwd;
		 }
	   if(!empty($_POST['email'])){
		     $email=usafestr($_POST['email']);
			 if(!is_email($email)){
		       messagebox(Lan('email_error'));		
			 }	
		      $data['email'] = $email;
			}
	 
	 	  $token=trim($_POST["token"]);
		  if(empty($token)||$token!=$_SESSION['token']){
				messagebox(Lan('illegal_submit'));		
		  }	 
		  $cntcheckone=$this->db->GetCount("select count(*) from `#yunyecms_member` where  `username`='$username' or `mobile`='$username'");
		  if($cntcheckone>=1){
				 messagebox(Lan('username_exist'));	
		   }		 
	    $regtime=time();
		$data['score'] = 0;
		$data['status'] = 1;
		$data['addtime'] = $regtime;
		$data['rnd'] = make_rand_letternum(16);
	    $groupid_default=0;
		$groupdefault=$this->db->find("select * from `#yunyecms_membergroup`  where isdefault=1");
	    if(!empty($groupdefault)){
			$groupid_default=$groupdefault["id"];
		}
		$data['groupid']=$groupid_default;
        //保存当前数据对象
          $insertid = $this->db->insert($data,'member',true);
		  usetcookie("userid",$insertid);					  
		  usetcookie("username",$username);	
		  usetcookie("groupid",$groupid_default);					  
	      if($mobile)  usetcookie("mobile",$mobile);			
		  usetcookie("loginrnd",$data['rnd']);					  
		  usetcookie("logintruetime",$regtime);
		  usetcookie("logintime",$regtime);
        if ($insertid !== false){
				$msgcontent['mod']=ROUTE_M;
				$msgcontent['act']=ROUTE_A;
				$msgcontent['ctrl']=ROUTE_C;
				$msgcontent['table']="member";
				$msgcontent['id']=$insertid;
			    $msgcontent=serialize($msgcontent);
				$msgtitle=Lan('member_register')."：".Lan('username')."-{$username}，ID-{$insertid}";
			    $remsg=savemsg($msgtitle,2,$msgcontent,$insertid);
		        messagebox(Lan('register_success'),url('member/member/index'));		
          }else {
            //失败提示
		     messagebox(Lan('register_error'),$_SERVER['HTTP_REFERER'],'error');		
		    exit;
         }
      }
	
	 public function login(){
        $seo['title']=$this->lang["seotitle"];
        $seo['keywords']=$this->lang["seokey"];
        $seo['description']=$this->lang["seodesc"];
		$seostr=Lan('member_login')."-".Lan('member_center');
        $seo['title']="{$seostr}-{$seo['title']}";
        $seo['keywords']="{$seostr}-{$seo['keywords']}";
        $seo['description']="{$seostr}-{$seo['description']}";
		$breadcumb=array('0'=>array('title'=>Lan('member_center'),'url'=>url("member/member/index")),
              '1'=>array('title'=>Lan('member_login'),'url'=>url("member/member/login"))
			);
		$token=make_rand_letternum(16);
		$_SESSION["token"]=$token;
		$cfg=$this->cfg;
		$lang=$this->lang; 
        include tpl('login','member');
	}
	
 function logincheck() {
	     $_POST=yytrim($_POST);
		 if(empty($_POST['username'])){
		     messagebox(Lan('required_username'));		
			}else{
	        $username=usafestr($_POST['username']);
		 }
		if(empty($_POST['pwd'])){
		    messagebox(Lan('required_password'));		
		   }else{
	        $pwd=yypwd($_POST['pwd']);
		 }
	 	  $token=trim($_POST["token"]);
		  if(empty($token)||$token!=$_SESSION['token']){
				messagebox(Lan('token_error'));		
		  }	 
		  $cntcheckone=$this->db->GetCount("select count(*) from `#yunyecms_member` where  `username`='$username' or `mobile`='$username'");
		  if(empty($cntcheckone)){
			   messagebox(Lan('user_notexist'));
		   }	
		  $curmember=$this->db->find("select * from `#yunyecms_member` where  (`username`='$username' or `mobile`='$username') and pwd='$pwd'");
	      if(!empty($curmember)){
			  if($curmember["status"]){
					  $logintime=time();
					  $userid=$curmember["id"];
				      $loginrnd=make_rand_letternum(16);
			  		  $strsql2="update `#yunyecms_member` set rnd='$loginrnd'  where id=$userid ";
					  $this->db->query($strsql2);	  
					  usetcookie("userid",yunyecms_strencode($userid));					  
					  usetcookie("username",$curmember["username"]);					  
					  usetcookie("groupid",$curmember["groupid"]);
					  usetcookie("mobile",$curmember["mobile"]);					  
					  usetcookie("loginrnd",$loginrnd);					  
					  usetcookie("logintruetime",$logintime);
					  usetcookie("logintime",$logintime);
					  $strsql2="update `#yunyecms_member` set lastlogintime=".$logintime."  where id=".$userid." ";
					  $this->db->query($strsql2);	
					  messagebox(Lan('login_success'),url('member/member/index'));	 
			  }else{
                 messagebox(Lan('account_nocheck'));
			  }
		  }else{
              messagebox(Lan('password_error'));
		  }
      }
	
	function logout(){
	  usetcookie("userid",null);					  
	  usetcookie("username",null);					  
	  usetcookie("mobile",null);					  
	  usetcookie("loginrnd",null);					  
	  usetcookie("logintruetime",null);
	  usetcookie("logintime",null);
	  usetcookie("groupid",null);
	  messagebox(Lan('memner_logout'),url('content/index/index'));	 
	}
	
	
 public function myinfo(){
        $seo['title']=$this->lang["seotitle"];
        $seo['keywords']=$this->lang["seokey"];
        $seo['description']=$this->lang["seodesc"];
		$seostr=Lan('member_info')."-".Lan('member_center');
        $seo['title']="{$seostr}-{$seo['title']}";
        $seo['keywords']="{$seostr}-{$seo['keywords']}";
		$cfg=$this->cfg;
		$lang=$this->lang; 
	    $breadcumb=array('0'=>array('title'=>Lan('member_center'),'url'=>url("member/member/index")),
              '1'=>array('title'=>Lan('member_info'),'url'=>url("member/member/myinfo"))
			);	
	    islogin();
		$userid=usafestr(yunyecms_strdecode(ugetcookie("userid")));
		$member=$this->member;
	    if(isset($_POST["yyact"])){
					  $token=trim($_POST["token"]);
					  if(empty($token)||$token!=$_SESSION['token']){
							messagebox(Lan('illegal_submit')	);	
					  }	
			          if(!empty($_POST["id"])){
						     $id=$_POST["id"];
							 if($id!=$member['id']){
							   messagebox(Lan('illegal_submit')	);		
						     }
					  }else{
							messagebox(Lan('user_notexist'));		
					  }
				      //$pic=uhtmlspecialchars(trim($_POST["pic"]));
				      $_POST=yytrim($_POST);
					  $data["name"]=usafestr($_POST["name"]);
					  $data["email"]=usafestr($_POST["email"]);
					  $data["phone"]=usafestr($_POST["phone"]);
					  $data["mobile"]=usafestr($_POST["mobile"]);
					  $data["address"]=usafestr($_POST["address"]);
					  $data["sex"]=usafestr($_POST["sex"]);
					  $data["company"]=usafestr($_POST["company"]);
					  $data["position"]=usafestr($_POST["position"]);
					   if($_POST["yyact"]=="edit"){
							   $id=$_POST["id"];
							   if(!$this->check_exist($id)){
									messagebox(Lan('user_notexist'),url('member/member/index'),"warn");
							   }
							   $retres=$this->db->update($data,"member","id={$id}");
							   if($retres){
										messagebox(Lan('member_info_ok'),url('member/member/index'),"success");
								 }else{
										messagebox(Lan('member_info_error'),url('member/member/index'),"error");
								 }
					  }			  
		  }else{
			$token=ugetcookie("loginrnd");
			$_SESSION['token']=$token;
		}
		include tpl('myinfo','member');
	 }
	
	
 public function pwd(){
        $seo['title']=$this->lang["seotitle"];
        $seo['keywords']=$this->lang["seokey"];
        $seo['description']=$this->lang["seodesc"];
		$seostr=Lan('password_modify')."-".Lan('member_center');
        $seo['title']="{$seostr}-{$seo['title']}";
        $seo['keywords']="{$seostr}-{$seo['keywords']}";
		$cfg=$this->cfg;
		$lang=$this->lang; 
	    $breadcumb=array('0'=>array('title'=>Lan('member_center'),'url'=>url("member/member/index")),
              '1'=>array('title'=>Lan('password_modify'),'url'=>url("member/member/pwd"))
			);	
	    islogin();
		$userid=usafestr(yunyecms_strdecode(ugetcookie("userid")));
		$member=$this->member;
	    if(isset($_POST["yyact"])){
					  $token=trim($_POST["token"]);
					  if(empty($token)||$token!=ugetcookie("loginrnd")){
							messagebox(Lan('illegal_submit')	);		
					  }	
			          if(!empty($_POST["id"])){
						     $id=$_POST["id"];
							 if($id!=$member['id']){
							   messagebox(Lan('illegal_submit')	);	
						     }
					  }else{
							messagebox(Lan('member_notexist'));
					  }
				      $_POST=yytrim($_POST);
					 $pwd = $_POST['pwd'];
					 $oldpwd = $_POST['oldpwd'];
					 $oldpwd=yypwd($oldpwd);	 
					 if(empty($pwd)){
						   messagebox(Lan('newpassword_empty'));
						}
					 if(empty($oldpwd)){
						   messagebox(Lan('required_oldpassword'));
						  }	
					 if($oldpwd!=$member["pwd"]){
						   messagebox(Lan('error_oldpassword'));
						  }	
					 $pwd=yypwd($pwd);	 
					   $data["pwd"]=$pwd;
					   if($_POST["yyact"]=="edit"){
							   $id=$_POST["id"];
							   if(!$this->check_exist($id)){
									messagebox(Lan('member_notexist'),url('member/member/index'),"warn");
							   }
							   $retres=$this->db->update($data,"member","id={$id}");
							   if($retres){
										messagebox(Lan('ok_password_modify'),url('member/member/index'),"success");
								 }else{
										messagebox(Lan('error_password_modify'),url('member/member/index'),"error");
								 }
					  }			  
		  }else{
			$token=ugetcookie("loginrnd");
		}
		include tpl('pwd','member');
	 }
		
	 //自定义表单
	public function customform() {
		$seo['title']=$this->lang["seotitle"];
        $seo['keywords']=$this->lang["seokey"];
        $seo['description']=$this->lang["seodesc"];
		$cfg=$this->cfg;
		$lang=$this->lang; 
		$cat=$this->cat; 
		$seostr="{$cat['title']}-".Lan('member_center');
        $seo['title']="{$seostr}-{$seo['title']}";
        $seo['keywords']="{$seostr}-{$seo['keywords']}";
	    $breadcumb=array('0'=>array('title'=>Lan('member_center'),'url'=>url("member/member/index")),
              '1'=>array('title'=>$cat['title'],'url'=>url("member/member/customform",array("catid"=>$cat['id'])))
			);	
	    islogin();
		$userid=usafestr(yunyecms_strdecode(ugetcookie("userid")));
		$member=$this->member;
		if(!empty($_REQUEST['catid'])){
		    $catid = intval(usafestr($_REQUEST['catid']));
		    $modelid=getmodelid($catid);
		}
		if(empty($modelid)){
			 messagebox(Lan('model_notexist'),"back","warn");
		}else{
			$curmodel=getmodel($modelid);
		    $tablename="m_".$curmodel['tablename'];
		 }	
		 $modelfields=$this->db->select("select * from `#yunyecms_modelfields`  where modelid={$curmodel['modelid']}  and isdisplay=1 ");
		 $pagesize=20;
		 $sqlquery="select * from `#yunyecms_{$tablename}`  ";
		 $where=" where userid={$userid}  ";
		 $sqlcnt=" select count(*) from `#yunyecms_{$tablename}` ";
		 $order=" order by `addtime` desc ";
		  if(isset($_REQUEST)){
		   if(!empty($_REQUEST["searchkey"])){
		        $searchkey=usafestr(trim($_REQUEST["searchkey"]));
		        $where=$where." and ( `title`  like '%{$searchkey}%' or  `title_en`  like '%{$searchkey}%' )";
			  }
		 }
		 $pagearr=$this->db->pagelist($sqlcnt,$sqlquery,$where,$order,$pagesize);
		 if($pagearr["count"]!=0){
			 $list=$pagearr["query"];
			 foreach($list as $key=>$var){
				  $modelarr=$this->db->find("select modelid  from `#yunyecms_category` where `id`= {$var['catid']}");
				  if($modelarr){
				     $list[$key]["modelid"]=$modelarr["modelid"];
					 if(!empty($var["userid"])) {
				         $list[$key]["user"]=$this->db->getbyid($var["userid"],"member");
					 }
				  }
			 }
			 $page=$pagearr["page"];
		 }
		include tpl('customform','member');
	  }	
	
  public function customformdelete() {
	    $cfg=$this->cfg;
		$lang=$this->lang;
		$cat=$this->cat;
	    islogin();
		$userid=usafestr(yunyecms_strdecode(ugetcookie("userid")));
		$member=$this->member;
		if(!empty($_REQUEST['catid'])){
		    $catid = intval(usafestr($_REQUEST['catid']));
		    $modelid=getmodelid($catid);
		}
		if(empty($modelid)){
			 messagebox(Lan('model_notexist'),"back","warn");
		}else{
			$curmodel=getmodel($modelid);
		    $tablename="m_".$curmodel['tablename'];
		 }	
            $id = $_REQUEST["id"];
		    $id = intval(usafestr($_REQUEST['id']));
	         if(!is_numeric($id)){
				    messagebox(Lan('delete_error'),$_SERVER['HTTP_REFERER'],"warn");
			 }
		    if(!is_array($id)){
			  $id=compact('id');
			}
			$idarray=$id;
            if (isset($idarray)){
					   foreach($idarray as $key=>$var){
				 		if(!is_numeric($var)){
					        messagebox("错误的参数！",'back',"warn");			
					    }
				        $idarray[$key]=usafestr($var);
			           }
				 $idarray=implode(",",$idarray);
				 $retres =$this->db->delete($tablename,"id in ({$idarray}) and userid={$userid} ");
                if ($retres !== false) {
				    messagebox(Lan('delete_success'),$_SERVER['HTTP_REFERER'],"success");
                } else {
				    messagebox(Lan('delete_error'),$_SERVER['HTTP_REFERER'],"warn");
                }
            } else {
				   messagebox(Lan('delete_error'),$_SERVER['HTTP_REFERER'],"warn");
            }
    }		
	
	
	
	private function check_exist($id) {
			 $id = trim($id);
			 if(empty($id)){
				 return false;
				 }else{
					if(!is_numeric($id)){
						  return false;
					 }
				  if ($this->db->find("select count(*) as cnt from `#yunyecms_member` where `id`= {$id}")){
					  return true;
				  }				 
			  }
		  }		
}

?>