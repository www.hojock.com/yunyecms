<?php
defined('IN_YUNYECMS') or exit('No permission.');
core::load_class('common',false);
core::load_fun('content');
core::load_fun('tree');
core::load_fun('shop');
class notice extends common {
	function __construct() {
	 parent::__construct();
	 $this->db->tablename = 'msg';
	 }
	//首页
	 public function index(){
        $seo['title']=$this->lang["seotitle"];
        $seo['keywords']=$this->lang["seokey"];
        $seo['description']=$this->lang["seodesc"];
		$seostr=Lan('my_notice')."-".Lan('member_center');
        $seo['title']="{$seostr}-{$seo['title']}";
        $seo['keywords']="{$seostr}-{$seo['keywords']}";
		$cfg=$this->cfg;
		$lang=$this->lang; 
		islogin(); 
	    $breadcumb=array('0'=>array('title'=>Lan('member_center'),'url'=>url("member/member/index")),
              '1'=>array('title'=>Lan('member_notice'),'url'=>url("member/notice/index"))
			);		 
		$userid=usafestr(yunyecms_strdecode(ugetcookie("userid")));
		$member=$this->member;
		 $pagesize=20;
		 $sqlquery="select * from `#yunyecms_notice`  ";
		 $where=" where status=1  ";
		 $sqlcnt=" select count(*) from `#yunyecms_notice` ";
		 $order=" order by `addtime` desc,`id` desc ";
		  if(isset($_REQUEST)){
		   if(!empty($_REQUEST["searchkey"])){
		        $searchkey=usafestr(trim($_REQUEST["searchkey"]));
		        $where=$where." and ( `title`  like '%{$searchkey}%' )";
			  }
		 }
		 $pagearr=$this->db->pagelist($sqlcnt,$sqlquery,$where,$order,$pagesize);
		 if($pagearr["count"]!=0){
			 $list=$pagearr["query"];
			 foreach($list as $key=>$var){
				   $list[$key]["content"]=uhtmlspecialchars_decode($var["content"]);
				   $list[$key]["content_en"]=uhtmlspecialchars_decode($var["content_en"]);
						 if($var["exlink"]){
				               $list[$key]["url"]=$var["exlink"];
						 }else{
				               $list[$key]["url"]=url('member/notice/show',array('id'=>$var['id'])); 
						 }
			 }
			 $page=$pagearr["page"];
		 }
        include tpl('notice','member');
	}

	 public function show(){
        $seo['title']=$this->lang["seotitle"];
        $seo['keywords']=$this->lang["seokey"];
        $seo['description']=$this->lang["seodesc"];
		$cfg=$this->cfg;
		$lang=$this->lang;
		if($this->member){
			$token=ugetcookie("loginrnd");
		 }
	    $breadcumb=array('0'=>array('title'=>Lan('member_center'),'url'=>url("member/member/index")),
              '1'=>array('title'=>Lan('member_notice'),'url'=>url("member/notice/index"))
			);	
		if(!empty($_REQUEST["id"])){
		$id = intval(usafestr($_REQUEST['id']));
		if(!is_numeric($id)){
		  messagebox(Lan('error_parameter'));		
			 }
		$row=$this->db->find("select * from `#yunyecms_notice`  where id={$id}");
		if(empty($row)){
		      messagebox(Lan('error_parameter'));		
		}
		$row["content"]=uhtmlspecialchars_decode($row["content"]);
		$row["content_en"]=uhtmlspecialchars_decode($row["content_en"]);
		$prevsql='';
		$nextsql='';
		$prevsql=" addtime>=".$row['addtime']." and id<>".$id."  ";
		$nextsql=" addtime<=".$row['addtime']."  and id<>".$id."  ";	
		$prev=$this->db->find("select * from `#yunyecms_notice`  where {$prevsql} order by addtime asc limit 0,1");
	    if($prev){
			$prev["url"]=url("member/notice/show",array("id"=>$prev["id"]));
		}		 
		$next=$this->db->find("select * from `#yunyecms_notice`  where {$nextsql}  order by addtime desc limit 0,1");
		if($next){
			$next["url"]=url("member/notice/show",array("id"=>$next["id"]));
		}		 
		$this->db->query("update  `#yunyecms_notice` set hits=hits+1  where id={$id}");
		$row['time']=udate($row['addtime']);
			$seo['title']=$row["title"].'-'.$seo['title'];
			$seo["keywords"]=(empty($row["keywords"])?$row["title"]:$row["keywords"]).','.$seo["keywords"];
			$seo["description"]=(empty($row["description"])? strcut(trim(strip_tags($row["content"])),200):$cat["description"]);			 
		 }else{
		      messagebox(Lan('error_parameter'));		
		}
        include tpl('notice_show','member');
	}		
	
	private function check_exist($id) {
			  $id = trim($id);
			  if(empty($id)||empty($id)){
				 return false;
				 }else{
				  if(!is_numeric($id)){
						  return false;
					 }
				  $cnt=$this->db->GetCount("select count(*) from `#yunyecms_notice` where    `id`= {$id}");
				  if($cnt>=1){
					 return true;
				  }else{ return false;}			 
			  }
		  }		
}

?>