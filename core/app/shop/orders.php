<?php
defined('IN_YUNYECMS') or exit('No permission.');
core::load_class('common',false);
core::load_fun('content');
core::load_fun('tree');
core::load_fun('shop');
class orders extends common {
    public $curmodel;
	function __construct() {
	 parent::__construct();
	 $this->db->tablename = 'orders';
	 islogin(); 	
	 }
	//首页
	 public function index(){
        $seo['title']=$this->lang["seotitle"];
        $seo['keywords']=$this->lang["seokey"];
        $seo['description']=$this->lang["seodesc"];
		$seostr=Lan('myorders')."-".Lan('member_center');
        $seo['title']="{$seostr}-{$seo['title']}";
        $seo['keywords']="{$seostr}-{$seo['keywords']}";
		$cfg=$this->cfg;
		$lang=$this->lang; 
		islogin(); 
	    $breadcumb=array('0'=>array('title'=>Lan('member_center'),'url'=>url("member/member/index")),
              '1'=>array('title'=>Lan('myorders'),'url'=>url("shop/orders/index/"))
			);		 
		$userid=usafestr(yunyecms_strdecode(ugetcookie("userid")));
		$member=$this->member;
		 $pagesize=20;
		 $sqlquery="select * from `#yunyecms_orders`  ";
		 $where=" where  userid={$userid} ";
		 $sqlcnt=" select count(*) from `#yunyecms_orders` ";
		 $order=" order by `updatetime` desc,`addtime` desc,`id` desc ";
		  if(isset($_REQUEST)){
		   if(!empty($_REQUEST["searchkey"])){
		        $searchkey=usafestr(trim($_REQUEST["searchkey"]));
		        $where=$where." and ( `title`  like '%{$searchkey}%' )";
			  }
		 }
		 $pagearr=$this->db->pagelist($sqlcnt,$sqlquery,$where,$order,$pagesize);
		 if($pagearr["count"]!=0){
			 $list=$pagearr["query"];
			 foreach($list as $key=>$var){
				  $goods= unserialize($var["goods"]);
				  $list[$key]["goods"]=$goods;
				  foreach($list[$key]["goods"] as $k=>$v){
				     $list[$key]["goods"][$k]["url"]=url('content/index/show',array("catid"=>$v["catid"],"id"=>$v["gid"]));
				  }
			 }
			 $page=$pagearr["page"];
		 }
        include tpl('orders','shop');
	}
	

	
 function onlinepay() {
	    $seo['title']=$this->lang["seotitle"];
        $seo['keywords']=$this->lang["seokey"];
        $seo['description']=$this->lang["seodesc"];
		$seostr=Lan('online_pay')."-".Lan('member_center');
        $seo['title']="{$seostr}-{$seo['title']}";
        $seo['keywords']="{$seostr}-{$seo['keywords']}";
	    $breadcumb=array('0'=>array('title'=>Lan('member_center'),'url'=>url("member/member/index")),
              '1'=>array('title'=>Lan('online_pay'),'url'=>url("shop/cart/pay"))
			);	
		$cfg=$this->cfg;
		$lang=$this->lang; 
	    islogin(); 
		$userid=usafestr(yunyecms_strdecode(ugetcookie("userid")));
		$token=ugetcookie("loginrnd");
		$member=$this->member;
	    if(empty($member["address"])||empty($member["name"])||empty($member["mobile"])){
            messagebox(Lan('improve_info_first'),url('member/member/myinfo'));
			exit;
		 }	 
	    if(!empty($_REQUEST['orderid'])){
		    $orderid=$_REQUEST['orderid'];
			$orderid=usafestr(trim($orderid));
			if(!is_numeric($orderid)){
		       messagebox(Lan('order_parm_error'));		
             }
		 }else{
		       messagebox(Lan('order_parm_error'));		
		 }
	    $orderitem=$this->db->find("select * from `#yunyecms_orders` where userid={$member["id"]} and id=$orderid");
	    if(empty($orderitem)){
			messagebox(Lan('order_not_exsit'));
		}else{
			$orderitem['goods']=unserialize($orderitem['goods']);
			  foreach($orderitem['goods'] as $k=>$v){
				     $orderitem['goods'][$k]["url"]=url('content/index/show',array("catid"=>$v["catid"],"id"=>$v["gid"]));
				  }
		}
        include tpl('onlinepay','shop');
    }	
	
	private function check_exist($userid,$catid,$gid) {
			  $userid = trim($userid);
			  $gid = trim($gid);
			  $catid = trim($catid);
			  if(empty($userid)||empty($catid)||empty($gid)||!is_numeric($userid)||!is_numeric($catid)||!is_numeric($gid)){
				 return false;
				 }else{
				  $cnt=$this->db->GetCount("select count(*) from `#yunyecms_cart` where `userid`= {$userid} and  `catid`= {$catid} and  `gid`= {$gid}");
				  if($cnt>=1){
					 return true;
				  }else{ return false;}			 
			  }
		  }		
}

?>