<?php
/**
 * 支付宝接口类库   
 */
header("Content-Type: text/html;charset=utf-8");
defined('IN_YUNYECMS') or exit('No permission.');
core::load_class('common',false);
core::load_fun('content');
core::load_fun('tree');
core::load_fun('shop');
class alipay extends common {
	    public $db;
       //在类初始化方法中，引入相关类库    
		function __construct() {
		 $this->db = core::load_model('content_model');
		 parent::__construct();
		 islogin(); 
			include_once YUNYECMS_CORE.'extend'.DIRECTORY_SEPARATOR.'alipay'.DIRECTORY_SEPARATOR.'lib'.DIRECTORY_SEPARATOR.'core.php'; 
			include_once YUNYECMS_CORE.'extend'.DIRECTORY_SEPARATOR.'alipay'.DIRECTORY_SEPARATOR.'lib'.DIRECTORY_SEPARATOR.'md5.php'; 
			include_once YUNYECMS_CORE.'extend'.DIRECTORY_SEPARATOR.'alipay'.DIRECTORY_SEPARATOR.'lib'.DIRECTORY_SEPARATOR.'notify.php'; 
			include_once YUNYECMS_CORE.'extend'.DIRECTORY_SEPARATOR.'alipay'.DIRECTORY_SEPARATOR.'lib'.DIRECTORY_SEPARATOR.'submit.php'; 
		 }
    //doalipay方法
        /*该方法其实就是将接口文件包下alipayapi.php的内容复制过来  然后进行相关处理
        */
	
    public function doalipay(){
            /*********************************************************
            把alipayapi.php中复制过来的如下两段代码去掉，
            第一段是引入配置项，
            第二段是引入submit.class.php这个类。
           为什么要去掉？？
            第一，配置项的内容已经在项目的Config.php文件中进行了配置，我们只需用C函数进行调用即可；
            第二，这里调用的submit.class.php类库我们已经在PayAction的_initialize()中已经引入；所以这里不再需要；
            *****************************************************/
       // require_once("alipay.config.php");
       // require_once("lib/alipay_submit.class.php");
       
       //这里我们通过TP的C函数把配置项参数读出，赋给$alipay_config；
		include_once YUNYECMS_CORE.'extend'.DIRECTORY_SEPARATOR.'alipay'.DIRECTORY_SEPARATOR.'alipay.config.php'; 
		$alipaycfg=$this->cfg;
		$alipaycfg=$alipaycfg['alipay'];
		$alipay_config['partner']=$alipaycfg['partner'];
		$alipay_config['key']=$alipaycfg['key'];
		$alipay_config['seller_id']=$alipaycfg['partner'];
        /**************************请求参数**************************/
        $notify_url = "http://".SITE_URL.url("shop/alipay/notifyurl"); //服务器异步通知页面路径
        $return_url = "http://".SITE_URL.url("shop/alipay/returnurl"); //页面跳转同步通知页面路径
        //商户订单号，商户网站订单系统中唯一订单号，必填
        $out_trade_no = $_POST['WIDout_trade_no'];
        //订单名称，必填
        $subject = $_POST['WIDsubject'];
        //付款金额，必填
        $total_fee = $_POST['WIDtotal_fee'];
        //商品描述，可空
        $body = $_POST['WIDbody'];	
		if(!empty($out_trade_no)){
	      $strsql="select * from `#yunyecms_orders` where sn='{$out_trade_no}'";
          $cur= $this->db->find($strsql);
		}
		$money=$cur["money"];
		if($total_fee!=$money){
		   messagebox(Lan('goods_parm_error'),$_SERVER['HTTP_REFERER']);
		}
		
	/************************************************************/

//构造要请求的参数数组，无需改动
$parameter = array(
		"service"       => $alipay_config['service'],
		"partner"       => $alipay_config['partner'],
		"seller_id"  => $alipay_config['seller_id'],
		"payment_type"	=> $alipay_config['payment_type'],
		"notify_url"	=> $notify_url,
		"return_url"	=> $return_url,
		"anti_phishing_key"=>$alipay_config['anti_phishing_key'],
		"exter_invoke_ip"=>$alipay_config['exter_invoke_ip'],
		"out_trade_no"	=> $out_trade_no,
		"subject"	=> $subject,
		"total_fee"	=> $total_fee,
		"body"	=> $body,
		"_input_charset"	=> trim(strtolower($alipay_config['input_charset']))
		//其他业务参数根据在线开发文档，添加参数.文档地址:https://doc.open.alipay.com/doc2/detail.htm?spm=a219a.7629140.0.0.kiX33I&treeId=62&articleId=103740&docType=1
        //如"参数名"=>"参数值"
);
        //建立请求
		
			$alipaySubmit = new AlipaySubmit($alipay_config);
			$html_text = $alipaySubmit->buildRequestForm($parameter,"get", "确认");
			echo $html_text;
    }	
	
	
    
        /******************************
        服务器异步通知页面方法
        其实这里就是将notify_url.php文件中的代码复制过来进行处理
        
        *******************************/
	
	function notifyurl(){
		    include_once YUNYECMS_CORE.'extend'.DIRECTORY_SEPARATOR.'alipay'.DIRECTORY_SEPARATOR.'alipay.config.php'; 
		//计算得出通知验证结果
			$alipayNotify = new AlipayNotify($alipay_config);
			$verify_result = $alipayNotify->verifyNotify();

			if($verify_result) {//验证成功
				/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
				//请在这里加上商户的业务逻辑程序代
				//——请根据您的业务逻辑来编写程序（以下代码仅作参考）——
				//获取支付宝的通知返回参数，可参考技术文档中服务器异步通知参数列表
				//商户订单号
				$out_trade_no = $_POST['out_trade_no'];
				//支付宝交易号
				$trade_no = $_POST['trade_no'];
				//交易状态
				$trade_status = $_POST['trade_status'];
				if($_POST['trade_status'] == 'TRADE_FINISHED') {
				
					//判断该笔订单是否在商户网站中已经做过处理
						//如果没有做过处理，根据订单号（out_trade_no）在商户网站的订单系统中查到该笔订单的详细，并执行商户的业务程序
						//请务必判断请求时的total_fee、seller_id与通知时获取的total_fee、seller_id为一致的
						//如果有做过处理，不执行商户的业务程序

					//注意：
					//退款日期超过可退款期限后（如三个月可退款），支付宝系统发送该交易状态通知

					//调试用，写文本函数记录程序运行情况是否正常
					//logResult("这里写入想要调试的代码变量值，或其他运行的结果记录");
				}
				else if ($_POST['trade_status'] == 'TRADE_SUCCESS') {
	            $cur=$this->db->find("select * from `#yunyecms_orders` where sn='{$out_trade_no}'");
				  if($cur['status']==0){
						$strsql="update `#yunyecms_orders`  set  status=1,trade_no='".$trade_no."'  where  sn='".$out_trade_no."'";
						$this->db->query($strsql);
				   }
				   $phone=$cur['phone'];
				   $signName="云业汇";   
				   $templateCode="SMS_78745166"; 
				   $phoneNumbers=$phone;
				   $templateParam=Array(  // 短信模板中字段的值
				   "sn"=>$out_trade_no
					 );	
				   sendsms($signName,$templateCode, $phoneNumbers,$templateParam); 
					//判断该笔订单是否在商户网站中已经做过处理
						//如果没有做过处理，根据订单号（out_trade_no）在商户网站的订单系统中查到该笔订单的详细，并执行商户的业务程序
						//请务必判断请求时的total_fee、seller_id与通知时获取的total_fee、seller_id为一致的
						//如果有做过处理，不执行商户的业务程序
					//注意：
					//付款完成后，支付宝系统发送该交易状态通知
					//调试用，写文本函数记录程序运行情况是否正常
					//logResult("这里写入想要调试的代码变量值，或其他运行的结果记录");
				}
				//——请根据您的业务逻辑来编写程序（以上代码仅作参考）——
				echo "success";		//请不要修改或删除

	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
			}
			else {
				//验证失败
				echo "fail";

				//调试用，写文本函数记录程序运行情况是否正常
				//logResult("这里写入想要调试的代码变量值，或其他运行的结果记录");
			}
	}
	
	function returnurl(){
		$cfg=$this->cfg;
		$lang=$this->lang; 
	    islogin();
		$userid=usafestr(yunyecms_strdecode(ugetcookie("userid")));
        $breadcumb=array('0'=>array('title'=>Lan('member_center'),'url'=>url("member/member/index")),
              '1'=>array('title'=>Lan('pay_success'),'url'=>url("shop/alipay/returnurl"))
			);			
		include_once YUNYECMS_CORE.'extend'.DIRECTORY_SEPARATOR.'alipay'.DIRECTORY_SEPARATOR.'alipay.config.php'; 
		$alipayNotify = new AlipayNotify($alipay_config);
		$verify_result = $alipayNotify->verifyReturn();
		if($verify_result) {//验证成功
			//请在这里加上商户的业务逻辑程序代码
			//——请根据您的业务逻辑来编写程序（以下代码仅作参考）——
			//获取支付宝的通知返回参数，可参考技术文档中页面跳转同步通知参数列表
			//商户订单号
			$out_trade_no = $_GET['out_trade_no'];
			//支付宝交易号
			$trade_no = $_GET['trade_no'];
			//交易状态
			$trade_status = $_GET['trade_status'];
			if($_GET['trade_status'] == 'TRADE_FINISHED' || $_GET['trade_status'] == 'TRADE_SUCCESS') {
				//判断该笔订单是否在商户网站中已经做过处理
					//如果没有做过处理，根据订单号（out_trade_no）在商户网站的订单系统中查到该笔订单的详细，并执行商户的业务程序
					//如果有做过处理，不执行商户的业务程序
			}
			else {
			  echo "trade_status=".$_GET['trade_status'];
			}
			 include tpl('returnurl','shop');
			//——请根据您的业务逻辑来编写程序（以上代码仅作参考）——
			/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		}
		else {
			//验证失败
			//如要调试，请看alipay_notify.php页面的verifyReturn函数
			 include tpl('returnurl','shop');

		}
		
		
		
	}
	

	
	function olreturnurl(){
		$alipay_config=C('alipay_config');  
		$alipayNotify = new AlipayNotify($alipay_config);
		$verify_result = $alipayNotify->verifyReturn();
		if($verify_result) {//验证成功
			//请在这里加上商户的业务逻辑程序代码
			//——请根据您的业务逻辑来编写程序（以下代码仅作参考）——
			//获取支付宝的通知返回参数，可参考技术文档中页面跳转同步通知参数列表
			//商户订单号
			$out_trade_no = $_GET['out_trade_no'];
			//支付宝交易号
			$trade_no = $_GET['trade_no'];
			//交易状态
			$trade_status = $_GET['trade_status'];

			if($_GET['trade_status'] == 'TRADE_FINISHED' || $_GET['trade_status'] == 'TRADE_SUCCESS') {
				//判断该笔订单是否在商户网站中已经做过处理
					//如果没有做过处理，根据订单号（out_trade_no）在商户网站的订单系统中查到该笔订单的详细，并执行商户的业务程序
					//如果有做过处理，不执行商户的业务程序
			       $cur=M("onlinepay")->where("sn='".$out_trade_no."'")->find();
				   if(!empty($cur)){
					    if($cur['status']==0){
						$strsql="update ".C('DB_PREFIX')."onlinepay  set  status=1,trade_no='".$trade_no."'  where  sn='".$out_trade_no."'";
						$returnrs=M("onlinepay")->execute($strsql);
				       }
				    }
			}
			else {
			  echo "trade_status=".$_GET['trade_status'];
			}
			$rest=1;
			//——请根据您的业务逻辑来编写程序（以上代码仅作参考）——
			/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		}
		else {
			//验证失败
			//如要调试，请看alipay_notify.php页面的verifyReturn函数
			$rest=0;
		}
             $this->assign($rest,$rest);
			 $this->display();	

	}
	
	
   
}
?>