<?php
defined('IN_YUNYECMS') or exit('No permission.');
core::load_class('common',false);
core::load_fun('content');
core::load_fun('tree');
class index extends common {
	private $isen;
    public $curmodel;
	function __construct() {
	 parent::__construct();
	 $this->isen=!empty($_REQUEST['lang'])&&is_numeric($_REQUEST['lang'])?isenglish($_REQUEST['lang']):false;
		 if($this->isen){
			if(!empty($this->cat)){
				$this->cat["title"]=$this->cat["title_en"];
				$this->cat["keywords"]=$this->cat["keywords_en"];
				$this->cat["description"]=$this->cat["description_en"];
				$this->rootcat["title"]=$this->rootcat["title_en"];
				$this->rootcat["keywords"]=$this->rootcat["keywords_en"];
				$this->rootcat["description"]=$this->rootcat["description_en"];	
			} 
		  }
	 }
	//首页
	 public function index(){
        $seo['title']=$this->lang["seotitle"];
        $seo['keywords']=$this->lang["seokey"];
        $seo['description']=$this->lang["seodesc"];
		$cfg=$this->cfg;
		$lang=$this->lang; 
		$rootcat=$this->rootcat;
		$cat=$this->cat;
		if($cat)$catid=$cat["id"];
		if($rootcat)$rootcatid=$cat["id"];
        include tpl();		 
	}
	
	 public function lists(){
        $seo['title']=$this->lang["seotitle"];
        $seo['keywords']=$this->lang["seokey"];
        $seo['description']=$this->lang["seodesc"];
		$cfg=$this->cfg;
		$lang=$this->lang;
		$rootcat=$this->rootcat;
		$cat=$this->cat;
		$rootcatid=$rootcat["id"];
		if($cat){
			$catid=$cat["id"];
			$ispower=$cat["ispower"];
			$this->curmodel=getmodel($cat["modelid"]);
			$seo['title']=empty($cat["title"])?$seo["title"]:$cat["title"].'-'.$seo['title'];
			$seotitle_pre=empty($cat["title"])?'':$cat["title"].',';	
			$seo["keywords"]=(empty($cat["keywords"])?$seotitle_pre:$cat["keywords"].',').$seo["keywords"];
			$seo["description"]=(empty($cat["description"])?$seotitle_pre:strip_tags($cat["description"])).$seo["description"];	
		 }
		 
		 $tablename="m_".$this->curmodel['tablename'];
		 $pagesize=empty($rootcat["pages"])?20:$rootcat["pages"];
		 $sqlquery="select * from `#yunyecms_{$tablename}`  ";
		 $where=" where status=1  ";
		 $sqlcnt=" select count(*) from `#yunyecms_{$tablename}` ";
		 $order=" order by `addtime` desc ";
		 if(isset($_REQUEST)){
		   if(!empty($_REQUEST["searchkey"])){
		        $searchkey=usafestr(trim($_REQUEST["searchkey"]));
		        $where=$where." and ( `title`  like '%{$searchkey}%'  or  `title_en`  like '%{$searchkey}%' )";
			  }
			 $catid=trim($_REQUEST['catid']);
			 if(!is_numeric($catid)){
					messagebox("错误的参数","back",'warn');		
			  }
			 $catid=usafestr($catid);
			 if(!empty($catid)){
				   $subcat=get_cat_child($catid);
	               $catidstr=implode(',', $subcat);
		           $where=$where." and catid in($catidstr)";
			 }
		  }
	    $haspower=0;		 
        if($ispower){
				$userid=usafestr(yunyecms_strdecode(ugetcookie("userid")));
			    if($this->member){
					    $groupid=$this->member["groupid"];
					    if(empty($groupid)){
						      if($this->cfg["content"]["list"])  messagebox("{$cat['title']}栏目需认证会员才能访问");		
						   }else{
							 $mygroup=$this->db->find("select * from `#yunyecms_membergroup`  where id=$groupid");
							 if(empty($mygroup)){
						          if($this->cfg["content"]["list"])  messagebox("会员组不存在");		
							 }
							 if(!get_member_power('info','view')){
								  if($this->cfg["content"]["list"])	messagebox("您所在的会员组没有信息查看权限",'back',"info");	
							  }else{
								 //取得栏目权限
								 $catpower=fetch_mem_cat_power();
								 if(!empty($catpower)){
									 //取得拥有查看权限的分类ID
									  $catpwerid=get_mem_power_catid($catpower);
								 }
							 }
						   $subcat=get_cat_child($catid);
						   $catidstr=implode(',', $subcat);
						   if(!empty($catpwerid)){
							   $newcat=array_intersect($subcat,$catpwerid);
						       $messagetip="暂时没有{$this->curcat["title"]}信息";
								 if(empty($newcat)){
									 if($this->cfg["content"]["list"]) messagebox("您所在的会员组没有访问{$this->cat["title"]}信息的权限",'back',"info");			
									}else{
										$haspower=1;		 
										$catidstr=implode(',', $newcat);
										$where=$where." and catid in($catidstr)";
								    }
						   }else{
							   if($this->cfg["content"]["list"]) messagebox("您所在的会员组没有访问{$this->cat["title"]}信息的权限",'back',"info");			
						   }							
						 }
					 }else{
						$haspower=0;		 
						 if(empty($userid)){
							 if($this->cfg["content"]["list"]) messagebox("您没有{$cat['title']}栏目的访问权限",'back',"info");		
							}
				  } 
		   }else{
	       $haspower=1;		 
		  }		 
		 
		 
	    $modelfields=$this->db->select("select * from `#yunyecms_modelfields`  where  issys=0 and issearch=1 ");				
		foreach($modelfields as $key=>$var){
			$formctrl=$var[formctrl];
			$fdname=$var[fdname];
			if($formctrl=='checkbox'){
				$postfdname=usafestr(trim($_REQUEST["searchkey"]));
				if(!empty($postfdname)){
					$where=$where." FIND_IN_SET($postfdname,$fdname) "; 
				}
			}else{
				$postfdname=usafestr(trim($_REQUEST["searchkey"]));
				if(!empty($postfdname)){
				   $where=$where." $fdname ='$postfdname' "; 
				 }
			}
		 }	
		  $list=array();
		  $page="";
		 $pagearr=$this->db->pagelist($sqlcnt,$sqlquery,$where,$order,$pagesize);
		 if($pagearr["count"]!=0){
			 $list=$pagearr["query"];
		     if(isset($list)){
			    foreach($list as $key=>$var){
					   if(strlen(RD)>0&&stripos($list[$key]["pic"],RD)===FALSE){
						    $list[$key]["pic"]=RD.$list[$key]["pic"];
					    }
					  if($this->isen){
							 if(!empty($var['title_en']))$list[$key]["title"]=$var['title_en'];
							 if(!empty($var['content_en'])){
								 $list[$key]["content"]=uhtmlspecialchars_decode($var["content_en"]);
							 }
						      else{
								 $list[$key]["content"]=uhtmlspecialchars_decode($var["content"]); 
							  }
							 if(!empty($var['summary_en']))$list[$key]["summary"]=$var['summary_en'];
							 $list[$key]["url"]=!empty($var['exlink_en'])?$var['exlink_en']:url('show',array('catid'=>$var['catid'],'id'=>$var['id']));
						 }else{
							 $list[$key]["url"]=!empty($var['exlink'])?$var['exlink']:url('show',array('catid'=>$var['catid'],'id'=>$var['id']));
							 if(!empty($var['content']))$list[$key]["content"]=uhtmlspecialchars_decode($var["content"]);
						}
					  $modelarr=$this->db->find("select modelid  from `#yunyecms_category` where `id`= {$var['catid']}");
					  if($modelarr){
						 $list[$key]["modelid"]=$modelarr["modelid"];
					  }
				   }
				}			 
			 $page=$pagearr["page"];
		 }
		$tplfile=gettpl($cat['tpllist'],$rootcat['tpllist'],$this->curmodel['tpllist']);
        include tpl($tplfile);
	 }
	
	 public function singlepage(){
        $seo['title']=$this->lang["seotitle"];
        $seo['keywords']=$this->lang["seokey"];
        $seo['description']=$this->lang["seodesc"];
		$cfg=$this->cfg;
		$lang=$this->lang;
		$rootcat=$this->rootcat;
		$cat=$this->cat;
		$rootcatid=$rootcat["id"];
		if($cat){
				$catid=$cat["id"];
			    $this->curmodel=getmodel($cat["modelid"]);
		        $tablename="m_".$this->curmodel['tablename'];
				  $seo['title']=empty($cat["title"])?$seo["title"]:$cat["title"].'-'.$seo['title'];
				  $seotitle_pre=empty($cat["title"])?'':$cat["title"].',';	
				  $seo["keywords"]=(empty($cat["keywords"])?$seotitle_pre:$cat["keywords"].',').$seo["keywords"];
				  $seo["description"]=(empty($cat["description"])?$seotitle_pre:strip_tags($cat["description"])).$seo["description"];		
		 }
		 
		$row=$this->db->find("select * from `#yunyecms_{$tablename}`  where id={$catid}");
		if(empty($row)){
		     messagebox(Lan('error_parameter'));		
		}	
		$tplfile=gettpl($cat['tplcontent'],$rootcat['tplcontent'],$this->curmodel['tplcontent'],'',3);
        include tpl($tplfile);
	}	
	
	public function show(){
        $seo['title']=$this->lang["seotitle"];
        $seo['keywords']=$this->lang["seokey"];
        $seo['description']=$this->lang["seodesc"];
		$cfg=$this->cfg;
		$lang=$this->lang;
		$rootcat=$this->rootcat;
		$cat=$this->cat;
		$rootcatid=$rootcat["id"];
		if($this->member){
			 $token=ugetcookie("loginrnd");
		 }
		if($cat){
			$catid=$cat["id"];
		    $modelid=$cat["modelid"];
		 }
		if(empty($modelid)){
			 messagebox(Lan('content_model_notexist'));
		}else{
			$this->curmodel=getmodel($modelid);
		    $tablename="m_".$this->curmodel['tablename'];
		 }
		 $modelfields_all=$this->db->select("select * from `#yunyecms_modelfields`  where modelid={$modelid}  and issys=0 ");
		 if(!empty($_REQUEST["id"])){
			 $id=trim($_REQUEST["id"]);
			  if(!is_numeric($id)){
					messagebox(Lan('error_parameter'),"back",'warn');		
			  }
		     $id=usafestr($id);
		$row=$this->db->find("select * from `#yunyecms_{$tablename}`  where id={$id}");
		if(empty($row)){
		      messagebox(Lan('error_parameter'));		
		}	
	  $ispower=$row["ispower"];
	  if(empty( $ispower)) $ispower=$cat["ispower"];	 
	  $haspower=0;		 
        if($ispower){
				$userid=usafestr(yunyecms_strdecode(ugetcookie("userid")));
			    if($this->member){
					    $groupid=$this->member["groupid"];
					    if(empty($groupid)){
						     if($this->cfg["content"]["show"])   messagebox("{$cat['title']}栏目需认证会员才能访问");		
						   }else{
							 $mygroup=$this->db->find("select * from `#yunyecms_membergroup`  where id=$groupid");
							 if(empty($mygroup)){
						         if($this->cfg["content"]["show"]) messagebox("会员组不存在");		
							 }
							 if(!get_member_power('info','view')){
								if($this->cfg["content"]["show"])	messagebox("您所在的会员组没有信息查看权限",'back',"info");			
							  }else{
								 //取得栏目权限
								 $catpower=fetch_mem_cat_power();
								 if(!empty($catpower)){
									 //取得拥有查看权限的分类ID
									  $catpwerid=get_mem_power_catid($catpower);
								   }
							 }
						   $subcat=get_cat_child($catid);
						   $catidstr=implode(',', $subcat);
						   if(!empty($catpwerid)){
							   $newcat=array_intersect($subcat,$catpwerid);
						       $messagetip="暂时没有{$this->curcat["title"]}信息";
								 if(empty($newcat)){
									if($this->cfg["content"]["show"]) messagebox("您所在的会员组没有访问{$this->cat["title"]}信息的权限",'back',"info");			
									}else{
										$haspower=1;		 
								   }
						   }else{
							  if($this->cfg["content"]["show"]) messagebox("您所在的会员组没有访问{$this->cat["title"]}信息的权限",'back',"info");			
						   }							
						 }
					 }else{
						$haspower=0;		 
						 if(empty($userid)){
							 if($this->cfg["content"]["show"])  messagebox("您没有{$cat['title']}栏目的访问权限",'back',"info");		
							}
				  } 
		   }else{
	       $haspower=1;		 
		  }		 
		
			 
		$prevsql='';
		$nextsql='';
		if($catid!=''){
		$prevsql=" addtime>=".$row['addtime']." and id<>".$id."   and catid=".$catid."";
		$nextsql=" addtime<=".$row['addtime']."  and id<>".$id."   and catid=".$catid."";
			}else{
		$prevsql=" addtime>=".$row['addtime']." and id<>".$id."  ";
		$nextsql=" addtime<=".$row['addtime']."  and id<>".$id."  ";	
				}
		$prev=$this->db->find("select * from `#yunyecms_{$tablename}`  where {$prevsql} order by addtime asc limit 0,1");
	    if($prev){
			$prev["url"]=url("show",array("catid"=>$prev["catid"],"id"=>$prev["id"]));
			 if($this->isen){
				  $prev["title"]=$prev["title_en"];
				  $prev["content"]=$prev["content_en"];
			 }
		}		 
		$next=$this->db->find("select * from `#yunyecms_{$tablename}`  where {$nextsql}  order by addtime desc limit 0,1");
		if($next){
			$next["url"]=url("show",array("catid"=>$next["catid"],"id"=>$next["id"]));
			if($this->isen){
				$next["title"]=$next["title_en"];
			    $next["content"]=$next["content_en"];
				}
		}		 
		$this->db->query("update  `#yunyecms_{$tablename}` set hits=hits+1  where id={$id}");
		$row['time']=udate($row['addtime']);

			 foreach($modelfields_all as $key=>$var){
				 $fdname=$var['fdname'];
				  if(isset($row[$fdname])){
					 $row[$fdname]=uhtmlspecialchars_decode($row[$fdname]);
				  }
			   }
			$row["downurl"]=url("download",array("catid"=>$row["catid"],"id"=>$id));
			if(!empty($row["morepic"])){
			   $row["morepic"]=unserialize($row["morepic"]);
			    foreach($row["morepic"] as $k=>$var){
					if(strlen(RD)>0&&stripos($var,RD)===FALSE){
						$row["morepic"][$k]=RD.$var;
					}
			   }	
			}				 
			if(stripos(strlen(RD)>0&&$row["pic"],RD)===FALSE){
				 $row["pic"]=RD.$row["pic"];
				 }
			if($this->isen){
				$row["title"]=$row["title_en"];
			    $row["content"]=$row["content_en"];
				$row["seotitle"]=$row["seotitle_en"];
				$row["seokeywords"]=$row["seokeywords_en"];
				$row["seodesc"]=$row["seodesc_en"];
			    $seo["description"]=(empty($row["seodesc"])? strcut(trim(strip_tags($row["content"])),200):$row["seodesc"].','.$cat["description"]);		 
				}else{
			    $seo["description"]=(empty($row["seodesc"])? strcut(trim(strip_tags($row["content"])),200):$row["seodesc"].','.$cat["description"]);		 
				}
			 	$seo['title']=(empty($row["seotitle"])?$row["title"]:$row["seotitle"]).'-'.$cat["title"].'-'.$seo['title'];
			    $seo["keywords"]=(empty($row["seokeywords"])?$row["title"]:$row["seokeywords"]).','.$seo["keywords"];
		 }
		$tplfile=gettpl($cat['tplcontent'],$rootcat['tplcontent'],$this->curmodel['tplcontent'],$row['template'],2);
        include tpl($tplfile); 
	}	
	
public function download(){
        $seo['title']=$this->lang["seotitle"];
        $seo['keywords']=$this->lang["seokey"];
        $seo['description']=$this->lang["seodesc"];
		$cfg=$this->cfg;
		$lang=$this->lang;
		$rootcat=$this->rootcat;
		$cat=$this->cat;
		$rootcatid=$rootcat["id"];
		if($this->member){
			$token=ugetcookie("loginrnd");
		 }
		if($cat){
			$catid=$cat["id"];
		    $modelid=$cat["modelid"];
		 }
		if(empty($modelid)){
			 messagebox(Lan('content_model_notexist'));
		}else{
			$this->curmodel=getmodel($modelid);
		    $tablename="m_".$this->curmodel['tablename'];
		 }
		 $modelfields_all=$this->db->select("select * from `#yunyecms_modelfields`  where modelid={$modelid}  and issys=0 ");
		 if(!empty($_REQUEST["id"])){
			 $id=trim($_REQUEST["id"]);
			  if(!is_numeric($id)){
					messagebox(Lan('error_parameter'),"back",'warn');		
			  }
		     $id=usafestr($id);
		$row=$this->db->find("select * from `#yunyecms_{$tablename}`  where id={$id}");
		if(empty($row)){
		      messagebox(Lan('error_parameter'));		
		}			 
		$prevsql='';
		$nextsql='';
		if($catid!=''){
		$prevsql=" addtime>=".$row['addtime']." and id<>".$id."   and catid=".$catid."";
		$nextsql=" addtime<=".$row['addtime']."  and id<>".$id."   and catid=".$catid."";
			}else{
		$prevsql=" addtime>=".$row['addtime']." and id<>".$id."  ";
		$nextsql=" addtime<=".$row['addtime']."  and id<>".$id."  ";	
				}
		$prev=$this->db->find("select * from `#yunyecms_{$tablename}`  where {$prevsql} order by addtime asc limit 0,1");
	    if($prev){
			$prev["url"]=url("show",array("catid"=>$prev["catid"],"id"=>$prev["id"]));
			 if($this->isen){
				  $prev["title"]=$prev["title_en"];
				  $prev["content"]=$prev["content_en"];
			 }
		}		 
		$next=$this->db->find("select * from `#yunyecms_{$tablename}`  where {$nextsql}  order by addtime desc limit 0,1");
		if($next){
			$next["url"]=url("show",array("catid"=>$next["catid"],"id"=>$next["id"]));
			if($this->isen){
				$next["title"]=$next["title_en"];
			    $next["content"]=$next["content_en"];
				}
		}		 
		$this->db->query("update  `#yunyecms_{$tablename}` set hits=hits+1  where id={$id}");
		$row['time']=udate($row['addtime']);
			 foreach($modelfields_all as $key=>$var){
				 $fdname=$var['fdname'];
				  if(isset($row[$fdname])){
					 $row[$fdname]=uhtmlspecialchars_decode($row[$fdname]);
				  }
			   }
			 $ispower=$row["ispower"];
			 $strtip="";
			  if(empty( $ispower)) $ispower=$cat["ispower"];	 
			  $haspower=0;	
			 $picfile=$row["picfile"];
			 $ziyuanurl=ROOT.$picfile;
			 if(empty($picfile)){
					messagebox("该资源文件不存在");	
			 }
	    $haspower=0;
		$infourl=url("show",array('catid'=>$catid,'id'=>$id));
        if($ispower){
				$userid=usafestr(yunyecms_strdecode(ugetcookie("userid")));
			    if($this->member){
					    $groupid=$this->member["groupid"];
					    if(empty($groupid)){
								messagebox("{$cat['title']}栏目需认证会员才能访问",$infourl,"info");			
						   }else{
							 $mygroup=$this->db->find("select * from `#yunyecms_membergroup`  where id=$groupid");
							 if(empty($mygroup)){
						          messagebox("会员组不存在");		
							 }
							 if(!get_member_power('info','view')){
									 messagebox("您所在的会员组没有下载权限",$infourl,"info");			
							  }else{
								 //取得栏目权限
								 $catpower=fetch_mem_cat_power();
								 if(!empty($catpower)){
									 //取得拥有查看权限的分类ID
									  $catpwerid=get_mem_power_catid($catpower);
								 }
							 }
						   $subcat=get_cat_child($catid);
						   $catidstr=implode(',', $subcat);
						   if(!empty($catpwerid)){
							   $newcat=array_intersect($subcat,$catpwerid);
						       $messagetip="暂时没有{$this->curcat["title"]}信息";
								 if(empty($newcat)){
									 messagebox("您所在的会员组没有下载权限",$infourl,"info");			
									}else{
										$haspower=1;
								if(empty($row['downnum'])){
									$data["downnum"]=0;
									$where["id"]=$id;
									$retres=$this->db->update($data,$tablename,$where);
									$this->db->query("update  `#yunyecms_{$tablename}` set downnum=downnum+1  where id={$id}");
								    }else{
								    $this->db->query("update  `#yunyecms_{$tablename}` set downnum=downnum+1  where id={$id}");
								    }	
									 	header("Location: $ziyuanurl");
									    exit;
										
								   }
						   }else{
							   messagebox("您所在的会员组没有下载权限",$infourl,"info");			
						   }							
						 }
					 }else{
						$haspower=0;		 
						 if(empty($userid)){
						       messagebox("该资源只有会员才能下载，请先登录！",url('member/member/login'),"info");		
							}
				  } 
		   }else{
	           $haspower=1;	
			    if(empty($row['downnum'])){
						$data["downnum"]=0;
						$where["id"]=$id;
						$retres=$this->db->update($data,$tablename,$where);
		                $this->db->query("update  `#yunyecms_{$tablename}` set downnum=downnum+1  where id={$id}");
					    }else{
		                $this->db->query("update  `#yunyecms_{$tablename}` set downnum=downnum+1  where id={$id}");
					   }
			   header("Location: $ziyuanurl");
			   exit;
		   }		 
			 
			if($this->isen){
				$row["title"]=$row["title_en"];
			    $row["content"]=$row["content_en"];
				$row["seotitle"]=$row["seotitle_en"];
				$row["seokeywords"]=$row["seokeywords_en"];
				$row["seodesc"]=$row["seodesc_en"];
			    $seo["description"]=(empty($row["seodesc"])? strcut(trim(strip_tags($row["content"])),200):$row["seodesc"].','.$cat["description"]);		 
				}else{
			    $seo["description"]=(empty($row["seodesc"])? strcut(trim(strip_tags($row["content"])),200):$row["seodesc"].','.$cat["description"]);		 
				}
			 	$seo['title']=(empty($row["seotitle"])?$row["title"]:$row["seotitle"]).'-'.$cat["title"].'-'.$seo['title'];
			    $seo["keywords"]=(empty($row["seokeywords"])?$row["title"]:$row["seokeywords"]).','.$seo["keywords"];
		 }
		  $tplfile=gettpl($cat['tplcontent'],$rootcat['tplcontent'],$this->curmodel['tplcontent'],$row['template'],2);
          include tpl($tplfile);
	}		
		
	
	
	 public function customform(){
        $seo['title']=$this->lang["seotitle"];
        $seo['keywords']=$this->lang["seokey"];
        $seo['description']=$this->lang["seodesc"];
		$cfg=$this->cfg;
		$lang=$this->lang;
		$rootcat=$this->rootcat;
		$cat=$this->cat;
		$rootcatid=$rootcat["id"];
		if($cat){
				$catid=$cat["id"];
			    $this->curmodel=getmodel($cat["modelid"]);
			    $seo['title']=empty($cat["title"])?$seo["title"]:$cat["title"].'-'.$seo['title'];
				$seotitle_pre=empty($cat["title"])?'':$cat["title"].',';	
				$seo["keywords"]=(empty($cat["keywords"])?$seotitle_pre:$cat["keywords"].',').$seo["keywords"];
				$seo["description"]=(empty($cat["description"])?$seotitle_pre:strip_tags($cat["description"])).$seo["description"];	
		 }
		$tplfile=gettpl($cat['tplcontent'],$rootcat['tplcontent'],$this->curmodel['tplcontent'],'',3);
        include tpl($tplfile);
	}	
	
public function formadd(){
		$cfg=$this->cfg;
		$lang=$this->lang;
		$rootcat=$this->rootcat;
		$cat=$this->cat;
		$rootcatid=$rootcat["id"];
		if($cat){
			$catid=$cat["id"];
		    $modelid=$cat["modelid"];
		 }
		if(empty($modelid)){
			 messagebox(Lan('content_model_notexist'));
		}else{
			$this->curmodel=getmodel($modelid);
		    $tablename="m_".$this->curmodel['tablename'];
		 }
		 $modelfields=$this->db->select("select * from `#yunyecms_modelfields`  where modelid={$modelid}  and isadd=1 ");	
		 $modelfields_required=$this->db->select("select * from `#yunyecms_modelfields`  where modelid={$modelid}  and isrequired=1 ");	
				      $_POST=ustripslashes($_POST);
				 	  $token=trim($_POST["token"]);
	                  if(empty($token)||$token!=$_SESSION['token']){
							 messagebox(Lan('illegal_submit'));
					  }
		              $ip=getip();
				 	  $data["ip"]=$ip;
					  $todaytime=getToday();
					  $cntcheckone=$this->db->GetCount("select count(*) from `#yunyecms_{$tablename}` where  addtime>={$todaytime['start']} and addtime<{$todaytime['end']}  and ip='$ip'");
					  if($cntcheckone>=10){
							 messagebox(Lan('submit_daylimit'));
					   }	
				 	  $data["catid"]=usafestr(trim($_POST["catid"]));
				 	  $data["addtime"]=time();
				 	  $data["status"]=1;
	                  if(!empty($this->member)){
						 $data["userid"]=$this->member["id"];
					  }
					  $body="";
					  foreach($modelfields as $key=>$var){
						 $fdname=$var['fdname'];
						 $fdtitle=$var['fdtitle'];
						  if(isset($_POST[$fdname])){
						     $data[$fdname]=usafestr(trim($_POST[$fdname]));
						      $emailcfg= $this->cfg;
							  if($emailcfg['isfeedbackmail']){
							   $body=$body."{$var['fdtitle']}:{$data[$fdname]}<br/>";
						       }	 
						  }
					   }
	                  if(!empty($_POST['name'])){
						   $title=usafestr($_POST['name']);
					  }elseif(!empty($_POST['mobile'])){
						   $title=usafestr($_POST['mobile']);
					  }elseif(!empty($_POST['phone'])){
						   $title=usafestr($_POST['phone']);
					  }
					  if($emailcfg['isfeedbackmail']){
							 $to=$emailcfg['email']['toemail'];
							 $name= $title;
							 $subject=sprintf(lan('form_email_msg'), $title, $cat['title']);
							 send_mail($emailcfg['email'],$to, $name, $subject, $body);	
					  }
	                 foreach($modelfields_required as $key=>$var){
						 $fdname=$var['fdname'];
						 $fdtitle=$var['fdtitle'];
						   if(empty($data[$fdname])){
								 messagebox(sprintf(lan('required_fdtitle'),$data[$fdtitle]));		
						   }						  
					   }
					   if(empty($data["catid"])||!is_numeric($data["catid"])){
							messagebox(lan('error_parameter'));		
					   }
					 $retres=$this->db->insert($data,$tablename);
					 if($retres){
							messagebox(sprintf(lan('form_info_ok'),$cat['title']),url('customform',array('catid'=>$catid)),"success");
					 }else{
							messagebox(sprintf(lan('form_info_error'),$cat['title']),url('customform',array('catid'=>$catid)),"error");
					 }
  }	
	
   public function close(){
        $seo['title']=$this->lang["seotitle"];
        $seo['keywords']=$this->lang["seokey"];
        $seo['description']=$this->lang["seodesc"];
		$cfg=$this->cfg;
		$lang=$this->lang; 
		$rootcat=$this->rootcat;
		$cat=$this->cat;
		if($cat)$catid=$cat["id"];
		if($rootcat)$rootcatid=$cat["id"];
        include tpl('closesite');		 
	}
	
 public function test(){
	    header("Content-Type: text/html;charset=utf-8"); 
        $seo['title']=$this->lang["seotitle"];
        $seo['keywords']=$this->lang["seokey"];
        $seo['description']=$this->lang["seodesc"];
	    //echo url('',array('a'=>1,'b'=>2));
	    //$catlist=getcat(0,3);
	    //$parent=getbreadcumb(13);
        //include tpl();
	}			
	
}

?>