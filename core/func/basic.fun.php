<?php
function getsubstr($string, $start = 0, $sublen, $append = true) {
	$pa = "/[\x01-\x7f]|[\xc2-\xdf][\x80-\xbf]|\xe0[\xa0-\xbf][\x80-\xbf]|[\xe1-\xef][\x80-\xbf][\x80-\xbf]|\xf0[\x90-\xbf][\x80-\xbf][\x80-\xbf]|[\xf1-\xf7][\x80-\xbf][\x80-\xbf][\x80-\xbf]/";
	preg_match_all ( $pa, $string, $t_string );
	if (count ( $t_string [0] ) - $start > $sublen && $append == true) {
		return join ( '', array_slice ( $t_string [0], $start, $sublen ) ) . "...";
	} else {
		return join ( '', array_slice ( $t_string [0], $start, $sublen ) );
	}
}
//字符串截取函数
function strcut($sourcestr,$cutlength,$suffix='...')
{
    $str_length = strlen($sourcestr);
    if($str_length <= $cutlength) {
        return $sourcestr;
    }
    $returnstr='';
    $n = $i = $noc = 0;
    while($n < $str_length) {
        $t = ord($sourcestr[$n]);
        if($t == 9 || $t == 10 || (32 <= $t && $t <= 126)) {
            $i = 1; $n++; $noc++;
        } elseif(194 <= $t && $t <= 223) {
            $i = 2; $n += 2; $noc += 2;
        } elseif(224 <= $t && $t <= 239) {
            $i = 3; $n += 3; $noc += 2;
        } elseif(240 <= $t && $t <= 247) {
            $i = 4; $n += 4; $noc += 2;
        } elseif(248 <= $t && $t <= 251) {
            $i = 5; $n += 5; $noc += 2;
        } elseif($t == 252 || $t == 253) {
            $i = 6; $n += 6; $noc += 2;
        } else {
            $n++;
        }
        if($noc >= $cutlength) {
            break;
        }
    }
    if($noc > $cutlength) {
        $n -= $i;
    }
    $returnstr = substr($sourcestr, 0, $n);


    if ( substr($sourcestr, $n, 6)){
        $returnstr = $returnstr . $suffix;//超过长度时在尾处加上省略号
    }
    return $returnstr;
}


function is_letternum($string) 
{ 
	if (!preg_match("/^[a-zA-Z][a-zA-Z0-9_]+$/",$string)) 
	{ 
		return false;
	} else{
		return true;
	}
}


function savelocalfile($filearr, $savepath='', $thumb='', $arrext='',  $save_rule='', $objfile='', $havethumb=1) {
	if(empty($filearr) || empty($savepath)){
		return array('error'=>'请选择要上传的文件！');
	}
	$patharr = $deault = array();
	//debug 传入参数
	$filename = strip_tags($filearr['name']);
	$tmpname = str_replace('\\', '\\\\', $filearr['tmp_name']);
	//debug 文件后缀
	$ext = fileext($filename) == 'gif' ? 'jpg': fileext($filename);
	$patharr['name'] = addslashes($filename);
	$patharr['type'] = $ext;
	$patharr['size'] = $filearr['size'];
	if($patharr['size']/1024 > 20480){
		return array('error'=>'您上传的图片大小是：'.round($patharr['size']/1024).'文件不超过20480KB');
	}
	//debug 文件名
	if($objfile) {
		$newfilename = $objfile;
		$isimage = 0;
		$patharr['file'] = $patharr['thumb'] = $objfile;
	} else {
		if(empty($arrext)) $arrext = array('jpg', 'jpeg', 'gif', 'png', 'rar', 'doc', 'pdf');
			if(in_array($ext, $arrext)) {
		   /*	$imageinfo = @getimagesize($tmpname);
			list($width, $height, $type) = !empty($imageinfo) ? $imageinfo : array('', '', '');
			if(!in_array($type, array(1,2,3,6,13))) {
				return $deault;
			}*/
			$isimage = 1;
		} else {
			//$isimage = 0;
			//$ext = 'attach';
			return array('error'=>'您上传的文件类型不对！');
		}
		//文件名称
		if(empty($save_rule)){
			//$filemain = date('YmdHis').random(4);
		      $filemain=str_replace(".".$ext,'',$filename);
		}else{
			$filemain = $save_rule;
		}
		//得到存储目录
		$dirpath = getattachdir($savepath);
		$patharr['filename'] = $filemain.'.'.$ext;
		$patharr['file'] = $dirpath.'/'.$filemain.'.'.$ext;
		$patharr['path'] = $dirpath.'/';
		//上传
		$newfilename = $patharr['file'];
	}
	if(@copy($tmpname, $newfilename)) {
	} elseif((function_exists('move_uploaded_file') && @move_uploaded_file($tmpname, $newfilename))) {
	} elseif(@rename($tmpname, $newfilename)) {
	} else {
		return $deault;
	}
	@unlink($tmpname);
	//debug 缩略图水印
/*	if($isimage && empty($objfile)) {
		//缩略图
		if($havethumb == 1) {
			// 如果$thumb是字符串			
			if(is_array($thumb)){
				$arrThumbWidth = explode(',',$thumb['width']);
				$arrThumbHeight = explode(',',$thumb['height']);
				foreach($arrThumbWidth as $key => $item){
					 $patharr['img_'.$item.'_'.$arrThumbHeight[$key]] = makethumb($patharr['file'],array($item,$arrThumbHeight[$key]));
				}
			}
		}
	}*/
	return $patharr;
}
/*获取客户端ip*/
function getip(){
		if (isset($_SERVER['HTTP_CLIENT_IP']) && strcasecmp($_SERVER['HTTP_CLIENT_IP'], "unknown"))
			$ip = $_SERVER['HTTP_CLIENT_IP'];
		else if (isset($_SERVER['HTTP_X_FORWARDED_FOR']) && strcasecmp($_SERVER['HTTP_X_FORWARDED_FOR'], "unknown"))
			$ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
		else if (isset($_SERVER['REMOTE_ADDR']) && strcasecmp($_SERVER['REMOTE_ADDR'], "unknown"))
			$ip = $_SERVER['REMOTE_ADDR'];
		else if (isset($_SERVER['REMOTE_ADDR']) && isset($_SERVER['REMOTE_ADDR']) && strcasecmp($_SERVER['REMOTE_ADDR'], "unknown"))
			$ip = $_SERVER['REMOTE_ADDR'];
		else $ip = "";
	    if(!filter_var($ip, FILTER_VALIDATE_IP)) $ip = "";
		return ($ip);
}
function getipport(){
	$ipport=(int)$_SERVER['REMOTE_PORT'];
	return $ipport;
}

function url_admin($a=ROUTE_A,$c=ROUTE_C,$parm=array(),$hashstr=''){
	global $yunyecms_admlogin;
	if($a=='')$a=ROUTE_A;
	if($c=='')$c=ROUTE_C;
	$a=='init'?$returnurl= "admin.php?c=".$c:$returnurl= "admin.php?c=".$c."&a=".$a;
   if(!empty($parm)){
	   foreach($parm as $key=>$var){
		     $returnurl=$returnurl."&".$key."=".$var;
		   }
	   }
	if(empty($hashstr)){
		$hashstr="&".$yunyecms_admlogin["ehashname"]."=".$yunyecms_admlogin["ehash"];
	}
	return $returnurl.$hashstr;
}
/**
 * URL组装 支持不同URL模式
 * @param string $url URL表达式，格式：'[分组/模块/操作#锚点@域名]?参数1=值1&参数2=值2...'
 * @param string|array $vars 传入的参数，支持数组和字符串
 * @param string $suffix 伪静态后缀，默认为true表示获取配置值
 * @param boolean $redirect 是否跳转，如果设置为true则表示跳转到该URL地址
 * @param boolean $domain 是否显示域名
 * @return string
 */
function url($url='',$vars='',$suffix=true,$redirect=false,$domain=false) {
    // 解析URL
    $info   =  parse_url($url);
    $url    =  !empty($info['path'])?$info['path']:ROUTE_A;
    if(isset($info['fragment'])) { // 解析锚点
        $anchor =   $info['fragment'];
        if(false !== strpos($anchor,'?')) { // 解析参数
            list($anchor,$info['query']) = explode('?',$anchor,2);
        }        
        if(false !== strpos($anchor,'@')) { // 解析域名
            list($anchor,$host)    =   explode('@',$anchor, 2);
        }
    }elseif(false !== strpos($url,'@')) { // 解析域名
        list($url,$host)    =   explode('@',$info['path'], 2);
    }
    // 解析子域名
    if(isset($host)) {
        $domain = $host.(strpos($host,'.')?'':strstr($_SERVER['HTTP_HOST'],'.'));
    }elseif($domain===true){
        $domain = $_SERVER['HTTP_HOST'];
    }
    // 解析参数
    if(is_string($vars)) { // aaa=1&bbb=2 转换成数组
        parse_str($vars,$vars);
    }elseif(!is_array($vars)){
        $vars = array();
    }
    if(isset($info['query'])) { // 解析地址里面参数 合并到vars
        parse_str($info['query'],$params);
        $vars = array_merge($params,$vars);
    }
    // URL组装
    $depr = URL_PATHINFO_DEPR;
    if($url) {
        if(0=== strpos($url,'/')) {// 定义路由
            $route      =   true;
            $url        =   substr($url,1);
            if('/' != $depr) {
                $url    =   str_replace('/',$depr,$url);
            }
        }else{
            if('/' != $depr) { // 安全替换
                $url    =   str_replace('/',$depr,$url);
            }
            // 解析分组、模块和操作
            $url        =   trim($url,$depr);
            $path       =   explode($depr,$url);
            $var        =   array();
			if(empty($path)){
				$var["m"] = ROUTE_M;
				$var["c"] = ROUTE_C;
                $var["a"] = ROUTE_A;
			}else{
				$pathsize=count($path);
				switch($pathsize){
					case 1: 
					$var["m"] = ROUTE_M;
					$var["c"] = ROUTE_C;
					$var["a"] = strtolower(array_pop($path));
					break;
					case 2:
					$var["a"] = strtolower(array_pop($path));
					$var["c"] = strtolower(array_pop($path));
					$var["m"] = ROUTE_M;
					break;
					case 3:
					$var["a"] = strtolower(array_pop($path));
					$var["c"] = strtolower(array_pop($path));
					$var["m"] = strtolower(array_pop($path));
					break;
				}
			}
        }
    }
   if($var["m"]=="content"){
	   if($var["c"]=="index"&&$var["a"]=='index'){
	      $var=array();
	   }elseif($var["c"]=="index"&&$var["a"]!='index'){
	     unset($var["c"]);
	     unset($var["m"]);
	   }else{
	     unset($var["m"]);
	   }
    }

    if(URL_MODEL == 0) { // 普通模式URL转换
        $url        =   ROOT.'index.php?'.http_build_query($var);
        if(!empty($vars)) {
            $vars   =   urldecode(http_build_query($vars));
            $url   .=   '&'.$vars;
        }
		if(!empty($_REQUEST['lang'])&&is_numeric($_REQUEST['lang'])){
              $url   .=   '&lang='.$_REQUEST['lang'];
			}
    }else{ // PATHINFO模式或者兼容URL模式
        if(isset($route)) {
            $url    =   ROOT.'index.php/'.rtrim($url,$depr);
        }else{
            $url    =   ROOT.'index.php/'.implode($depr,array_reverse($var));
        }
        if(!empty($vars)) { // 添加参数
            foreach ($vars as $var => $val){
                if('' !== trim($val))   $url .= $depr . $var . $depr . urlencode($val);
            }                
        }
        if($suffix) {
            $suffix   =  $suffix===true?URL_HTML_SUFFIX:$suffix;
            if($pos = strpos($suffix, '|')){
                $suffix = substr($suffix, 0, $pos);
            }
            if($suffix && '/' != substr($url,-1)){
                $url  .=  '.'.ltrim($suffix,'.');
            }
        }
    }
    if(isset($anchor)){
        $url  .= '#'.$anchor;
    }
    if($domain) {
        $url   =  (is_ssl()?'https://':'http://').$domain.$url;
    }
    if($redirect) // 直接跳转URL
        redirect($url);
    else
        return $url;
}

/**
 * 判断是否SSL协议
 * @return boolean
 */
function is_ssl() {
    if(isset($_SERVER['HTTPS']) && ('1' == $_SERVER['HTTPS'] || 'on' == strtolower($_SERVER['HTTPS']))){
        return true;
    }elseif(isset($_SERVER['SERVER_PORT']) && ('443' == $_SERVER['SERVER_PORT'] )) {
        return true;
    }
    return false;
}

/**
 * URL重定向
 * @param string $url 重定向的URL地址
 * @param integer $time 重定向的等待时间（秒）
 * @param string $msg 重定向前的提示信息
 * @return void
 */
function redirect($url, $time=0, $msg='') {
    //多行URL地址支持
    $url        = str_replace(array("\n", "\r"), '', $url);
    if (empty($msg))
        $msg    = "系统将在{$time}秒之后自动跳转到{$url}！";
    if (!headers_sent()) {
        // redirect
        if (0 === $time) {
            header('Location: ' . $url);
        } else {
            header("refresh:{$time};url={$url}");
            echo($msg);
        }
        exit();
    } else {
        $str    = "<meta http-equiv='Refresh' content='{$time};URL={$url}'>";
        if ($time != 0)
            $str .= $msg;
        exit($str);
    }
}

function messagebox($msg, $reurl = 'back', $type="info",$target="",$msg2 ='',$time = 3000){
		switch($type)
		{
		case "error":
			$strico="ban";
			$strcss="danger";
			break;  
		case "success":
		  $strico="check";
		  $strcss=$type;
		  break;
		case "warn":
		  $strico="warning";
		  $strcss="warning";
		  break;
		case "info":
		  $strico="info";
		  $strcss="info";
		  break;
	    default:
		  $strico=$type;
		  $strcss=$type;
		}
		if (defined('IN_YUNYECMSAdmin')) {
			require YUNYECMS_CORE . 'admin/tpl/message.tpl.php';
		} else {
			if(!defined('LAN')){ define('LAN','cn');} 
			require YUNYECMS_CORE . 'app/inc/message_'.LAN.'.tpl.php';
		}
	exit;
}
function arrnull($array){
	 if(count($array)>0){
		 return false;
		 }else{
		   return true;
		 }
	}

function ToChinaseNum($num) {
    $char = array("零", "一", "二", "三", "四", "五", "六", "七", "八", "九");
    $dw = array("", "十", "百", "千", "万", "亿", "兆");
    $retval = "";
    $proZero = false;
    for ($i = 0; $i < strlen($num); $i++) {
        if ($i > 0) $temp = (int)(($num % pow(10, $i + 1)) / pow(10, $i));
        else $temp = (int)($num % pow(10, 1));
        if ($proZero == true && $temp == 0) continue;
        if ($temp == 0) $proZero = true;
        else $proZero = false;
        if ($proZero)

        {
            if ($retval == "") continue;
            $retval = $char[$temp].$retval;
        }
        else $retval = $char[$temp].$dw[$i].$retval;
    }
    if ($retval == "一十") $retval = "十";
    return $retval;
}


function  udecode($jsonstr){
  return json_decode($jsonstr,true);
  }
function usetcookie($name,$value,$isadmin=0,$expire=COOKIE_EXPIRE){
	$cookiepre=empty($isadmin)?COOKIE_PRE:COOKIE_ADMINPRE;
	return setcookie($cookiepre.$name,$value,$expire,COOKIE_PATH,COOKIE_DOMAIN);
}
function ugetcookie($name,$isadmin=0){
	$cookiename=empty($isadmin)?COOKIE_PRE.$name:COOKIE_ADMINPRE.$name;
	if(!empty($_COOKIE[$cookiename])){
		return $_COOKIE[$cookiename];
		}else{
		return 0;
		}
}	 
//文件处理函数
function fwritetext($filepath,$string){
	$fp=fopen($filepath,"w") or die("Unable to open file!");
	@fwrite($fp,$string);
	@fclose($fp);
	@chmod($filepath,0777);
}
function udate($datetime,$flag=0){
	 switch($flag)
		{
		case 0:
		    return date("Y-m-d H:i:s",$datetime);
			break;  
	    default:
		    return date("Y-m-d H:i:s",$datetime);
		}
	}

function dir_list($path, $exts = '', $list= array()) {
    $path = dir_path($path);
    $files = glob($path.'*');
    foreach($files as $v) {
        $fileext = fileext($v);
        if (!$exts || preg_match("/\.($exts)/i", $v)) {
            $list[] = $v;
            if (is_dir($v)) {
                $list = dir_list($v, $exts, $list);
            }
        }
    }
    return $list;
}
function fileext($filename) {
    return strtolower(trim(substr(strrchr($filename, '.'), 1, 10)));
}
function dir_path($path) {
    $path = str_replace('\\', '/', $path);
    if(substr($path, -1) != '/') $path = $path.'/';
    return $path;
}
function dir_delete($dir) {
    $dir = dir_path($dir);
    if (!is_dir($dir)) return FALSE;
    $list = glob($dir.'*');
    foreach((array)$list as $v) {
        is_dir($v) ? dir_delete($v) : @unlink($v);
    }
    return @rmdir($dir);
}

function file_delete($path) {
	$path='.'.$path;
    $dir = dirname($path);
	if (file_exists($path)) {  
           @unlink($path);  
		   $fileempty = glob($dir.'/*');
		   if(empty($fileempty)){
			  return @rmdir($dir);
		   }
    }else{
		 return FALSE;
	}
}
//文件处理函数end
//获取指定内容中的图片路径
 function  getcontentimg($content){
	      $pattern="/<[img|IMG].*?src=[\'|\"](.*?(?:[\.gif|\.jpg|\.png]))[\'|\"].*?[\/]?>/"; 
          //$pattern2="/<[img|IMG].+src=\"?(.+\.(jpg|gif|bmp|bnp|png))\"?.+>/i"; 
          preg_match_all($pattern,$content,$match); 
	      return $match[1];
 }

//去除数组字符串两端空格
function TrimArray($Input){
    if (!is_array($Input))
        return trim($Input);
    return array_map('TrimArray', $Input);
}

function check_mobile($text) {
   $search = "/^1[345789]{1}\d{9}$/";
   if(preg_match($search,$text)){
     return true;
   }else{
     return false;
   }
}

function is_email($text) {
   $search = "/^([a-z0-9_\.-]+)@([\da-z\.-]+)\.([a-z\.]{2,6})$/";
   ///^[a-z\d]+(\.[a-z\d]+)*@([\da-z](-[\da-z])?)+(\.{1,2}[a-z]+)+$/
   if(preg_match($search,$text)){
     return true;
   }else{
     return false;
   }
}

 function exportExcel($expTitle,$expCellName,$expTableData){
		//$xlsTitle = iconv('utf-8', 'gb2312', $expTitle);//文件名称
		$xlsTitle=$expTitle;
        $fileName = date('_YmdHis');//or $xlsTitle 文件名称可根据自己情况设定
        $fileName = $xlsTitle.date('_YmdHis');//or $xlsTitle 文件名称可根据自己情况设定
        $cellNum = count($expCellName);
        $dataNum = count($expTableData);
		include_once YUNYECMS_CORE.'extend'.DIRECTORY_SEPARATOR.'classes'.DIRECTORY_SEPARATOR.'PHPExcel.php'; 
        $objPHPExcel = new PHPExcel();
        $cellName = array('A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z','AA','AB','AC','AD','AE','AF','AG','AH','AI','AJ','AK','AL','AM','AN','AO','AP','AQ','AR','AS','AT','AU','AV','AW','AX','AY','AZ');
	    //自动换行
        $objPHPExcel->getActiveSheet()->getStyle('A:M')->getAlignment()->setWrapText(true);
	 
		$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(30);
		$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(15);
		$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(30);
		$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(20);
		$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(20);
		$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(30);
		$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(20);
		$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(20);
		$objPHPExcel->getActiveSheet()->getColumnDimension('M')->setWidth(15);
		$objPHPExcel->getActiveSheet()->getColumnDimension('N')->setWidth(15);
	    //$objPHPExcel->getActiveSheet(0)->mergeCells('A1:'.$cellName[$cellNum-1].'1');//合并单元格
       // $objPHPExcel->setActiveSheetIndex(0)->setCellValue('A1', $expTitle.'  Export time:'.date('Y-m-d H:i:s'));  
        for($i=0;$i<$cellNum;$i++){
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue($cellName[$i].'1', $expCellName[$i][1]); 
        } 
          // Miscellaneous glyphs, UTF-8   
        for($i=0;$i<$dataNum;$i++){
          for($j=0;$j<$cellNum;$j++){
            $objPHPExcel->getActiveSheet(0)->setCellValue($cellName[$j].($i+2), $expTableData[$i][$expCellName[$j][0]]);
          }             
        }  
        header('pragma:public');
        header('Content-type:application/vnd.ms-excel;charset=utf-8;name="'.$xlsTitle.'.xls"');
        header("Content-Disposition:attachment;filename=$fileName.xls");//attachment新窗口打印inline本窗口打印
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');  
        $objWriter->save('php://output'); 
        exit;   
    }

  function readexcel($filename,$encode='utf-8'){
		include_once YUNYECMS_CORE.'extend'.DIRECTORY_SEPARATOR.'classes'.DIRECTORY_SEPARATOR.'PHPExcel.php'; 
		$objReader = PHPExcel_IOFactory::createReader('Excel5'); 
		$objReader->setReadDataOnly(true); 
		$objPHPExcel = $objReader->load($filename); 
		$objWorksheet = $objPHPExcel->getActiveSheet(); 
		 $highestRow = $objWorksheet->getHighestRow(); 
		 $highestColumn=$objWorksheet->getHighestColumn();
		 $highestColumnIndex= PHPExcel_Cell::columnIndexFromString($highestColumn);
		  $excelData = array(); 
		  for ($row = 1; $row <= $highestRow; $row++) { 
			  for ($col = 0; $col < $highestColumnIndex; $col++) { 
			 $excelData[$row][] =(string)$objWorksheet->getCellByColumnAndRow($col, $row)->getValue();
			 } 
		} 
	return $excelData; 
   }  

//时间处理函数
function yytime($time) {
$rtime = date("m-d H:i",$time);
$htime = date("H:i",$time);
$time = time() - $time;
if ($time < 60) {
$str = '刚刚';
}
elseif ($time < 60 * 60) {
$min = floor($time/60);
$str = $min.'分钟前';
}
elseif ($time < 60 * 60 * 24) {
$h = floor($time/(60*60));
$str = $h.'小时前 '.$htime;
}
elseif ($time < 60 * 60 * 24 * 3) {
$d = floor($time/(60*60*24));
if($d==1)
$str = '昨天 '.$rtime;
else
$str = '前天 '.$rtime;
}
else {
$str = $rtime;
}
return $str;
}
//返回两个数组的交集
function yarray_intersect($subcat,$catpwerid){
	 $newcat=array();
	   foreach($subcat as $k=>$v){
		if(!empty($catpwerid)){
			  foreach($catpwerid as $kp=>$vp){
				  if($subcat[$k]==$catpwerid[$kp]){
					  $newcat[]=$subcat[$k];
				  }
			   }
		  }
      }
	return $newcat;
}

function isenglish($lang){
	 if(!empty($lang)&&is_numeric($lang)){
	      $curlang=getcurlang(trim($lang));
		  $title_lang=trim($curlang["title"]);
		  if(stripos($title_lang,"english")!==false||stripos($title_lang,"英文")!==false||stripos($title_lang,"英语")!==false){
			 return true;
		  }else{
			 return false;
		  }
	  }else{
			 return false;
	 }
}
function getToday(){
	$curtime=time();
	$today_start=strtotime(date('Y-m-d', $curtime));
	$today_end=strtotime(date('Y-m-d', $curtime))+86399;  
	$rearr['start'] = $today_start;
	$rearr['end'] = $today_end;
	return $rearr;
}

function getYesterday(){
	$beginYesterday=mktime(0,0,0,date('m'),date('d')-1,date('Y'));
    $endYesterday=mktime(0,0,0,date('m'),date('d'),date('Y'))-1; 
	$rearr['start']=$beginYesterday;
	$rearr['end']=$endYesterday;
	return $rearr;
}

function getThisweek(){
	$curtime=time();
	$w = date("w", $curtime); //获取当前周的第几天 周日是 0 周一 到周六是 1 -6
	$d = $w ? $w - 1 : 6; //如果是周日 -6天
	$start_week = strtotime("".date("Y-m-d")." -".$d." days"); //本周开始时间
	$end_week = strtotime("".date("Y-m-d",$start_week)." +7 days")-1; //本周结束时间
	$rearr['start']=$start_week;
	$rearr['end']=$end_week;
	return $rearr;
}

function getLastweek(){
	 $lastweek_start = mktime(0,0,0,date('m'),date('d')-date('w')+1-7,date('Y'));
	 $lastweek_end = mktime(23,59,59,date('m'),date('d')-date('w')+7-7,date('Y'));
	 $rearr['start']=$lastweek_start;
	 $rearr['end']=$lastweek_end;
	 return $rearr;
}

function yexplode($separator=',',$string){
	if(strpos($string,$separator)!==false){
		return explode($separator,$string);
	}else{
		return $string;
	}
}
//处理网站语言包
function dolang($lang,$cfg){
	if(empty($lang["tel"])) $lang["tel"]=$cfg["tel"];
	if(empty($lang["mobile"])) $lang["mobile"]=$cfg["mobile"];
	if(empty($lang["qq"])) $lang["qq"]=$cfg["qq"];
	if(empty($lang["sitename"])) $lang["sitename"]=$cfg["sitename"];
	if(empty($lang["mail"])) $lang["mail"]=$cfg["mail"];
	if(!empty($lang['copyright'])) $lang['copyright']=uhtmlspecialchars_decode($lang['copyright']);	
	if(!empty($lang['tel'])) $lang['tel']=yexplode(',',$lang['tel']);	
	if(!empty($lang['mobile'])) $lang['mobile']=yexplode(',',$lang['mobile']);	
	if(!empty($lang['qq'])) $lang['qq']=yexplode(',',$lang['qq']);	
	if(!empty($lang['mail'])) $lang['mail']=yexplode(',',$lang['mail']);	
	return $lang;
}

function br2nl($text){
    $text=preg_replace('/<br\\s*?\/??>/i',chr(13),$text);
 return preg_replace('/ /i',' ',$text);
}

function getSiteUrl() {
$currentdir=$_SERVER['REQUEST_URI']?$_SERVER['REQUEST_URI']:($_SERVER['PHP_SELF']?$_SERVER['PHP_SELF']:$_SERVER['SCRIPT_NAME']);
$currentdir=str_ireplace('\\','/',$currentdir);
return substr($currentdir, 0, strrpos($currentdir, '/')+1);
}

function getserver_domain() {
    $client_domaindomain=$_SERVER['HTTP_HOST'];
    if(stripos($_SERVER['HTTP_HOST'],":")){
		$client_domaindomain=substr($client_domaindomain, 0, strrpos($client_domaindomain, ':'));
	}
	return $client_domaindomain;
}


function getsofttype($type){
	 switch($type){
         case 1:
			$returnstr="Release";
			break;
		 case 2:
			$returnstr="Build ";
			break;
		 default:
			$returnstr="Release";
	 }
	return $returnstr;
}
function getsofttime($type,$version_time,$build_time){
	 switch($type){
         case 1:
			$returnstr=empty($version_time)?"":date("Y-m-d",$version_time);
			break;
		 case 2:
			$returnstr=empty($build_time)?"":date("Y-m-d",$build_time);
			break;
		 default:
			$returnstr=empty($version_time)?"":date("Y-m-d",$version_time);
	 }
	return $returnstr;
}
function getsoftname($type,$version,$version_time,$build_time){
	 switch($type){
         case 1:
			$returnstr="Yunyecms_$version"." ( ".date("Y-m-d",$version_time).")";
			break;
		 case 2:
			$returnstr=empty($build_time)?"":"Yunyecms_{$version}_Build".date("md",$build_time);
			break;
		 default:
			$returnstr="Yunyecms_$version"." ( ".date("Y-m-d",$version_time).")";
	 }
	return $returnstr;
 }
function getversion($type,$version,$version_time,$build_time){
	 switch($type){
         case 1:
			$returnstr="$version"." ( ".date("Y-m-d",$version_time).")";
			break;
		 case 2:
			$returnstr=empty($build_time)?"{$version}":"{$version} (Build_".date("md",$build_time).")";
			break;
		 default:
			$returnstr="$version"." ( ".date("Y-m-d",$version_time).")";
	 }
	return $returnstr;
 }

 
?>