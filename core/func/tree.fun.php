<?php
//
function generateTree($array){
    //第一步 构造数据
    $items = array();
    foreach($array as $value){
        $items[$value['id']] = $value;
    }
    //第二部 遍历数据 生成树状结构
    $tree = array();
    foreach($items as $key => $value){
        if(isset($items[$value['pid']])){
            $items[$value['pid']]['son'][] = &$items[$key];
        }else{
            $tree[] = &$items[$key];
        }
    }
    return $tree;
}
//递归算法生成分类树
function getTree($data, $pId=0)
{
    $tree = array();
    foreach($data as $k => $v)
    {		
        if($v['pid'] == $pId)
        {		
            $v['son'] = getTree($data, $v['id']);
            $tree[] = $v;
            unset($data[$k]);
        }
    }
    return $tree;
}

 



?>