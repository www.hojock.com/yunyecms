<?php
defined('IN_YUNYECMSAdmin') or exit('No permission.');
core::load_admin_class('admin');
class adminlogs extends YUNYE_admin {
	private $db;
	private $admuser;
	function __construct() {
		$this->db = core::load_model('adminlogs_model');
		$this->admuser=IsAdmLogin($this->db);
		parent::__construct();
	}
	 //加载首页
	  public function init() {
		  $parnav='<li><a href=\"'.url_admin('init','user','',$this->hashurl['usvg']).'\" target=\"maincontent\">管理员</a></li><li><a href=\"'.url_admin('init','adminlogs','',$this->hashurl['usvg']).'\" target=\"maincontent\">操作日志</a></li><li class=\"active\">操作日志管理</li>';
		   if(!getroot('users','dologs')){
			  messagebox(Lan('no_permission'),'back',"warn");			
		   }
		 $pagesize=20;
		 $sqlquery="select * from `#yunyecms_adminlogs`  ";
		 $where=" where `userid`<>'' ";
		 $sqlcnt=" select count(*) from `#yunyecms_adminlogs` ";
		 $order=" order by `logid` desc ";
		  if(isset($_REQUEST)){
		   if(!empty($_REQUEST["logsdate"])){
			    $logsdate=trim($_REQUEST["logsdate"]);
				$logsdate=explode("-",$logsdate);
				$sdate=strtotime($logsdate[0]." 00:00:00");
				$edate=strtotime($logsdate[1]." 23:59:59");
				$sdatestr=date("Y/m/d",$sdate);
				$edatestr=date("Y/m/d",$edate);
				if($sdate>=$edate){
			        messagebox(Lan('startdate_gt_enddate'),'back',"warn");
					}
					$where=$where." and ( addtime >={$sdate} and addtime <={$edate} ) ";
			   }
		   if(!empty($_REQUEST["searchkey"])){
		        $searchkey=usafestr(trim($_REQUEST["searchkey"]));
		        $where=$where." and ( `username`  like '%{$searchkey}%' or  `loginip`  like '%{$searchkey}%' )";
			  }
		   
		 }
		 
		 $pagearr=$this->db->pagelist($sqlcnt,$sqlquery,$where,$order,$pagesize);
		 if($pagearr["count"]!=0){
		 $list=$pagearr["query"];
		 $pages=$pagearr["page"];
		 }
		 require tpl_adm('adminlogs_list');
	  }
	  

	  public function adminlogs_delete(){
		   if(!getroot('users','dologs')){
			  messagebox(Lan('no_permission'),'back',"warn");			
		   }
		 if(!empty($_GET["id"])){
			 $id=trim($_GET["id"]);
			 if(!is_numeric($id)){
			   messagebox(Lan('admin_id_notnumber'),url_admin('init','','',$this->hashurl['usvg']),"warn");
			 }
			 $cadminlogs=$this->db->find("select * from `#yunyecms_adminlogs` where `logid`= {$id}");
			 if(empty($cadminlogs)){
				 messagebox(Lan('adminlogs_not_exist'),url_admin('init','','',$this->hashurl['usvg']),"warn");	
			  }
			$query=$this->db->query("delete from `#yunyecms_adminlogs` where logid='$id'");
			if($query){
				    messagebox(Lan('admin_delete_success'),url_admin('init','','',$this->hashurl['usvg']),"success");
				}else{
				    messagebox(Lan('admin_delete_error'),url_admin('init','','',$this->hashurl['usvg']),"warn");
					}
			}
	 }	
	 
	  public function deleteall(){
		   if(!getroot('users','dologs')){
			  messagebox(Lan('no_permission'),'back',"warn");			
		   }
		 if(!empty($_POST["adminlogsid"])){
			 $ids=$_POST["adminlogsid"];
			 $idstr="";
			    foreach($ids as $key=>$var){
				  if(!is_numeric($var)){
					   messagebox(Lan('adminlogs_id_notnumber'),url_admin('init','','',$this->hashurl['usvg']),"warn");
				   }
				  $ids[$key]=usafestr($var);
				 }
			 $idstr=implode(",",$ids);
			 $query=$this->db->query("delete from `#yunyecms_adminlogs` where logid in({$idstr})");
			  if($query){
				    messagebox(Lan('admin_deleteall_success'),url_admin('init','','',$this->hashurl['usvg']),"success");
				}else{
				    messagebox(Lan('admin_deleteall_error'),url_admin('init','','',$this->hashurl['usvg']),"warn");
					}
			}else{
			   messagebox(Lan('admin_delall_lessone'),url_admin('init','','',$this->hashurl['usvg']),"warn");
		    }
	 }	 	 
	  
	 
	 
	 
	private function checkadminlogs_exist($logid) {
		$logid =  trim($logid);
		 if(empty($logid)){
		     return false;
			 }else{
			  if ($this->db->find("select `adminlogsname` from `#yunyecms_adminlogs` where `logid`= {$logid}")){
				  return true;
			  }				 
		  }
	  }	

	 
	 
	 
	 
}
?>
