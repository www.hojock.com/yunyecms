<?php
 function insert_admlogs($doing,$yyact,$content="",$userid=0){
	global $yunyecms_dbconfig;
	if(!SAFE_ISOPLOG)
	{
		return "";
	}
	if($userid){
		$userid=(int)$userid;
		}else{
			$userid=ugetcookie('admuserid',1);
			}
	if(!$userid)
	{
		 messagebox(Lan('adminlogin_notlogin'),url_admin('init','login'),"warn");	
	}
	if(empty($doing)){$doing="";}
	$doing=addslashes(stripslashes($doing));
	$logip=getip();
	$ipport=getipport();
	$addtime=time();
	if(empty($yyact)){
		messagebox(Lan('admin_logs_emptyact'),url_admin(),"info");	
		}
	$yyact=usafestr($yyact);
	$strsql="insert into `#yunyecms_adminlogs`(userid,yyact,doing,content,logip,ipport,addtime) values('$userid','$yyact','$doing','$content','$logip','$ipport',$addtime)";
	//$strsql=str_ireplace('#yunyecms_',$yunyecms_dbconfig['tablepre'],trim($strsql));
	$sql=query($strsql);
}

 function getroot($key,$subkey=""){
	 global $yunyecms_dbconfig;
	 $roleid=ugetcookie('admroleid',1);
	  if(!$roleid)
	  {
		  messagebox(Lan('adminlogin_notlogin'),url_admin('init','login'),"warn");
		  exit;	
	  }
	  if(!is_numeric($roleid)){
			 messagebox(Lan('id_notnumber'),url_admin('init','login'),"warn");
		}
	 $strsql="select * from `#yunyecms_admin_role` where `roleid`= {$roleid}";
	 $crole=getone($strsql);
	 if(empty($crole)){
		 $ret=0;	
	  }else{
		 $powersarr=array();
		 $powers=yunyecms_strrange_decode($crole["powers"],$crole["salt"],$crole["range"]);
		 if(!empty($powers)){
			 $powersarr=json_decode($powers,true);
			 }
	      $ret=yunyecms_getarrayvalue($powersarr,$key,$subkey);  
		  }
		  return $ret;
}


 function get_cat_power($catid,$type){
	 $roleid=ugetcookie('admroleid',1);
	  if(!$roleid)
	  {
		  messagebox(Lan('adminlogin_notlogin'),url_admin('init','login'),"warn");
		  exit;	
	  }
	  if(!is_numeric($roleid)){
			 messagebox(Lan('id_notnumber'),url_admin('init','login'),"warn");
		}
	 $strsql="select * from `#yunyecms_admin_role` where `roleid`= {$roleid}";
	 $crole=getone($strsql);
	 if(empty($crole)){
		 $ret=0;	
	  }else{
			 $powercatarr=array();
			 if(!empty($crole["powercat"])){
			   $powercat=yunyecms_strdecode($crole["powercat"]);
			   $powercatarr=unserialize($powercat);
			 }
	         $ret=yunyecms_getarrayvalue($powercatarr,$catid,$type);  
		  }
		  return $ret;
  }

 function fetch_cat_power(){
	  $roleid=ugetcookie('admroleid',1);
	  if(!$roleid)
	  {
		  messagebox(Lan('adminlogin_notlogin'),url_admin('init','login'),"warn");
		  exit;	
	  }
	  if(!is_numeric($roleid)){
			 messagebox(Lan('id_notnumber'),url_admin('init','login'),"warn");
		}
	 $strsql="select * from `#yunyecms_admin_role` where `roleid`= {$roleid}";
	 $crole=getone($strsql);
	 if(empty($crole)){
		 $ret=0;	
	  }else{
			 $powercatarr=array();
			 if(!empty($crole["powercat"])){
			   $powercat=yunyecms_strdecode($crole["powercat"]);
			   $powercatarr=unserialize($powercat);
			 }
	       $ret=$powercatarr;  
		  }
		  return $ret;
  }

 function get_power_catid($powercat,$type='view'){
	     if(!empty($powercat)){
			 $catpwerid=array();
			 foreach($powercat as $key=>$var){
					 if(!empty($var[$type])){
				        $catpwerid[]= $key;
					 }
			      }
			 $return=!empty($catpwerid)?$catpwerid:false;
			 return $return;
		 }else{
			 return false;
		 }
   }

  function IsAdmLogin($db,$uid=0,$username='',$rnd=''){
	if(ROUTE_C != 'login'){
	$userid=$uid?$uid:ugetcookie('admuserid',1);
	$username=$username?$username:ugetcookie('admusername',1);
	$rnd=$rnd?$rnd:ugetcookie('admloginrnd',1);
	$userid=(int)$userid;
	$username=usafestr($username);
	if(!$userid||!$username||!$rnd)
	{
	    if(ROUTE_C=='main'&&ROUTE_A=="init"&&!stripos(urldecode($_SERVER['REQUEST_URI']),"?")){
			   header("Location: ".url_admin('init','login').""); 
			   exit;
			 }else{
		   messagebox(Lan('adminlogin_notlogin'),url_admin('init','login'),"warn","top");	
			 }
	}
	$roleid=(int)ugetcookie('admroleid',1);
	$truelogintime=(int)ugetcookie('admlogintruetime',1);
	//COOKIE验证
	YUNYECMS_CHECKCookieRnd($userid,$username,$rnd,$roleid,$truelogintime);
	//db
	$cuser=$db->find("select userid,roleid,catid,rnd,realname from `#yunyecms_user` where userid='$userid' and username='".$username."' and rnd='".$rnd."' and status=1 limit 1");
	if(!$cuser['userid'])
	{
		 messagebox(Lan('adminlogin_SingleUser'),url_admin('init','login'),"warn","top");
	}
	//登陆超时
	$logintime=ugetcookie('admlogintime',1);
	if($logintime)
	{
		if(time()-$logintime>ADMLOGIN_OVERTIME*60)
		{
			usetcookie("admloginrnd","",1);
		    messagebox(Lan('adminlogin_overtime'),url_admin('init','login'),"info","top");	
	    }
		usetcookie("admlogintime",time(),1);
	}
	if(ugetcookie('admloginlicense',1)!="yunyecmslicense")
	{
		  messagebox(Lan('adminlogin_notlogin'),url_admin('init','login'),"warn","top");	
	}
	$admuser['userid']=$userid;
	$admuser['username']=$username;
	$admuser['rnd']=$rnd;
	$admuser['roleid']=$cuser['roleid'];
	$admuser['catid']=$cuser['catid'];
	return $admuser;
	}else{
	 return 0;
		}
    }

  function GetMsgType($flag){
	  $retstr="";
	    if(empty($flag)){
			return false;
		}else{
			switch($flag){
				case 1:
	            $retstr="客户表单";
				break;
				case 2:
	            $retstr="会员注册";
				break;
				case 3:
	            $retstr="用户下单";
				break;	
				default:
	            $retstr="客户表单";
			}
		}
	    return $retstr;
  }

  


?>