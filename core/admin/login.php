<?php
defined('IN_YUNYECMSAdmin') or exit('No permission.');
core::load_admin_class('admin');
class login extends YUNYE_admin {
	private $db;
	function __construct() {
		$this->db = core::load_model('user_model');
	}
	 //加载首页
	  public function init(){
		   if(!empty($_POST)){
			    if(array_key_exists("yyact",$_POST)){
				   $yunyecmsact=usafestr(trim($_POST["yyact"]));
				    if($yunyecmsact=='admlogin'){
					   $username=usafestr(trim($_POST["uname"]));
					   $password=usafestr(trim($_POST["pwd"]));
					   $captcha=usafestr(trim($_POST["captcha"]));
					   $captcha=strtolower($captcha);
					   if(strlen($username)>30||empty($username)||strlen($username)<5)
						{
					        messagebox(Lan('adminlogin_username_error'),url_admin(),"warn");
						}
					   if(strlen($password)>30||empty($password)||strlen($password)<6)
						{
					        messagebox(Lan('adminlogin_pwd_error'),url_admin(),"warn");
						}
					   if(empty($captcha)||$captcha!=$_SESSION['admcode']||strlen($captcha)!=4)
						{
					        messagebox(Lan('adminlogin_captcha_error'),url_admin(),"warn");
						}						
	                    $this->adminlogin($username,$password);
					 }
					 else{
					  messagebox(Lan('adminlogin_Illegallogging'),url_admin(),"warn");
					  exit;		
						 }
					 
				  }else{
				     messagebox(Lan('adminlogin_Illegallogging'),url_admin(),"warn");			
					  }
			  }
		 $yyact="admlogin";
		 require tpl_adm('admin_login');
	  }
	  
	  
	     private function adminlogin ($username,$password){
			$logiparr["ipaddr"]=getip();
			$logiparr["ipport"]=getipport();
			$logipstr=yunyecms_strencode(json_encode($logiparr)); 
			$logintime=time();		  
		    $this->CheckLoginTimes($logiparr["ipaddr"],$logintime);
		    $cuser=$this->db->find("select `userid`,`username`,`password`,`salt`,`realname`,`roleid`,lastlogintime,lastip  from `#yunyecms_user` where `username`= '{$username}' and status=1");
				 if(empty($cuser)){
		               $this->InsertErrorLoginTimes($username,$password,$logiparr["ipaddr"],$logintime,Lan('admin_user_notexist'));
					   messagebox(Lan('adminlogin_usernotexist'),url_admin(),"warn");	
				  }else{
					 $salt=$cuser["salt"];
			         $passwordencode=YUNYECMSadmPwd($password,$salt);	
					  if($cuser["password"]!=$passwordencode){
		                $this->InsertErrorLoginTimes($username,$password,$logiparr["ipaddr"],$logintime,Lan('adminlogin_pwderror'));
					    messagebox(Lan('adminlogin_pwderror'),url_admin(),"warn");	
						}
		  $rnd=make_rand(20);
		  $sql=$this->db->query("update `#yunyecms_user`  set rnd='$rnd',loginnum=loginnum+1,lastip='$logipstr',lastlogintime='$logintime',prelogintime='$cuser[lastlogintime]',preip='$cuser[lastip]' where username='$username' limit 1");
		  //unset($_SESSION['admcode']);		  
		  usetcookie("admusername",$username,1);					  
		  usetcookie("admuserid",$cuser['userid'],1);					  
		  usetcookie("admroleid",$cuser['roleid'],1);					  
		  usetcookie("admloginrnd",$rnd,1);					  
	      usetcookie("admlogintruetime",$logintime,1);
	      usetcookie("admlogintime",$logintime,1);
	      usetcookie("admloginlicense",'yunyecmslicense',1);
		  //COOKIE加密验证
		  DelADMFileRnd($cuser['userid']);
		  CreateCookieRnd($cuser['userid'],$username,$rnd,$cuser['roleid'],$logintime);
	      $this->insert_loginlog($username,'',$logiparr["ipaddr"]);
		  messagebox(Lan('adminlogin_success'),url_admin('init','main').GetYunyecmsHashStrDef(0,'ehref'),"success");	
			 }
		 }
		  
   
   
   
   private function CheckLoginTimes($ip,$time){
	$checktime=$time-60*ADMLOGIN_MINUTES;
	$cnt=$this->db->GetCount("select count(*) as total from `#yunyecms_adminloginfail`  where ip='$ip' and failtimes>=".ADMLOGIN_MINUTES." and lastlogintime>$checktime limit 1");
	if($cnt)
	{
	   messagebox(Lan('adminlogin_fails_time'),url_admin(),"warn");			
	}
   }		  
		  



		  
  private function InsertErrorLoginTimes($username,$password,$ip,$time,$errresult=''){
	  $checktime=$time-60*ADMLOGIN_MINUTES;
	  $this->db->query("delete from `#yunyecms_adminloginfail` where lastlogintime<$checktime");
	  $row=$this->db->find("select `ip` from `#yunyecms_adminloginfail` where ip='$ip' limit 1");
	  if($row['ip'])
	  {
		  $this->db->query("update `#yunyecms_adminloginfail` set failtimes=failtimes+1,lastlogintime='$time' where ip='$ip' limit 1");
	  }
	  else
	  {
		  $this->db->query("insert into `#yunyecms_adminloginfail`(ip,failtimes,lastlogintime)values('$ip',1,'$time');");
	  }
	  $this->insert_loginlog($username,$password,$ip,$errresult,0);
  }
		 



		  
 private  function insert_loginlog($username,$password,$loginip,$errresult='',$status=1){
	if(SAFE_LOGINLOG)
	{
	$password=usafestr($password);
	$username=usafestr($username);
	$loginip=usafestr($loginip);
	$ipport=getipport();
	$status=usafestr($status);
	$errresult=usafestr($errresult);
	$logintime=time();
	$sql=$this->db->query("insert into `#yunyecms_adminloginlog` (username,loginip,logintime,status,errresult,password,ipport) values('$username','$loginip','$logintime','$status','$errresult','$password','$ipport');");
	}else{
		return "";
		}

}	  
	  
	  
	  
}
?>