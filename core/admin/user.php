<?php
defined('IN_YUNYECMSAdmin') or exit('No permission.');
core::load_admin_class('admin');
class user extends YUNYE_admin {
	private $db;
	private $admuser;
	function __construct() {
		$this->db = core::load_model('user_model');
		$this->admuser=IsAdmLogin($this->db);
		parent::__construct();
	}
	 //加载首页
	  public function init() {
		$parnav='<li><a href=\"'.url_admin('init','user','',$this->hashurl['usvg']).'\" target=\"maincontent\">管理员</a></li><li class=\"active\">管理员管理</li>';
		 $pagesize=20;
		 $sqlquery="select `userid`, `username`, `roleid`, `catid`, `realname`, `email`,`allowclass`,`createtime`,`addip`,`status`,`lastip` from `#yunyecms_user`  ";
		 $where=" where `username`<>'' ";
		 if(!getroot('users','admin')){
		    $where=$where." and `userid`={$this->admuser['userid']} ";
		 } 
		 $sqlcnt=" select count(*) from `#yunyecms_user` ";
		 $order=" order by `createtime` desc ";
		  if(isset($_POST["searchkey"])){
		   $searchkey=usafestr(trim($_POST["searchkey"]));
		   if(empty($searchkey)){
			   messagebox(Lan('search_key_empty'),url_admin('init','','',$this->hashurl['usvg']),"warn");
			   exit;		
			   }else{
		        $where=" where `username`  like'%{$searchkey}%' or  `realname`  like'%{$searchkey}%' ";
				   }
		 }
		  
		 $pagearr=$this->db->pagelist($sqlcnt,$sqlquery,$where,$order,$pagesize);

		 if($pagearr['count']){
			 $userlist=$pagearr["query"];
			 $pages=$pagearr["page"];
			 }

		 require tpl_adm('user_list');
	  }
	  
	 public function user_add(){
		 if(!empty($_GET["userid"])){
		  $parnav='<li><a href=\"'.url_admin('init','user','',$this->hashurl['usvg']).'\" target=\"maincontent\">管理员</a></li><li class=\"active\">修改用户</li>';
			$userid=trim($_GET["userid"]);
			 if(!is_numeric($userid)){
			   messagebox(Lan('user_userid_notnumber'),url_admin('init','','',$this->hashurl['usvg']),"warn");
			 }
			  if($userid!=$this->admuser['userid']){
				   if(!getroot('users','admin')){
					  messagebox(Lan('no_permission_adminuser'),'back',"warn");			
				    }
				 }			 
			$cuser=$this->db->find("select `userid`,`username`, `roleid`, `catid`, `realname`, `email`,`allowclass`,`status` from `#yunyecms_user` where `userid`= {$userid}");
			 if(empty($cuser)){
				   messagebox(Lan('user_not_exist'),url_admin('user_add','user',array('userid'=>$userid),$this->hashurl['usvg']),"warn");			
			  }else{
				   if(!empty($userid)){
					  if(!empty($cuser['allowclass'])) {
						  $allowclassarr=json_decode(yunyecms_strdecode($cuser['allowclass']),true);
						  }
					  }
				  }
		    $yyact="edit";
			}else{
			 
		 if(!getroot('users','admin')){
				messagebox(Lan('no_permission_adminuser'),'back',"warn");			
		 }	 			 
		    $parnav='<li><a href=\"'.url_admin('init','user','',$this->hashurl['usvg']).'\" target=\"maincontent\">管理员</a></li><li class=\"active\">添加用户</li>';
		    $yyact=yyact_get("add");
				}
		   $departmentlist=$this->db->select("select `departmentid`,`departmentname` from `#yunyecms_department` where `status`= 1 order by departmentid asc");
		   $rolelist=$this->db->select("select `roleid`,`rolename` from `#yunyecms_admin_role` order by roleid asc ");	
				    if(isset($_POST["yyact"])){
			       $username=$_POST["username"];
			       $email=$_POST["email"];
				   $roleid=$_POST["roleid"];
				   $catid=$_POST["catid"];
			       $realname=$_POST["realname"];
				   if(array_key_exists("status",$_POST)){$status=$_POST["status"];}else{
					   $status=0;
					   }
				   $allowclass=array();
				   if(array_key_exists("allowclass",$_POST)){
			          $allowclass=$_POST["allowclass"];
					   }
			if($_POST["yyact"]=="add"){
				   $password=$_POST["password"];
				   $this->add_admin_user($username,$email,$password,$roleid,$catid,$realname,$allowclass,$status);
			  }
		    if($_POST["yyact"]=="edit"){
			       $userid=$_POST["userid"];
			       $oldusername=$_POST["oldusername"];
				   $this->edit_admin_user($userid,$username,$oldusername,$email,$roleid,$catid,$realname,$allowclass,$status);
			  }			  
		  }
		require tpl_adm('user_add');
	 }
	 
	 
	  public function user_pwd(){
		 $parnav='<li><a href=\"'.url_admin('init','user','',$this->hashurl['usvg']).'\" target=\"maincontent\">管理员</a></li><li class=\"active\">修改密码</li>';
		 if(!empty($_GET["userid"])){
			$userid=trim($_GET["userid"]);
			 if(!is_numeric($userid)){
			   messagebox(Lan('user_userid_notnumber'),url_admin('init','','',$this->hashurl['usvg']),"warn");
			 }
			  if($userid!=$this->admuser['userid']){
				   if(!getroot('users','admin')){
					  messagebox(Lan('no_permission_adminuser'),'back',"warn");			
				    }
				 }
			$cuser=$this->db->find("select `userid`,`username`,  `realname`,`status` from `#yunyecms_user` where `userid`= {$userid}");
			 if(empty($cuser)){
				   messagebox(Lan('user_not_exist'),url_admin('user_pwd','user',array('userid'=>$userid),$this->hashurl['usvg']),"warn");			
			  }
		    $yyact="editpwd";
			}
	    if(isset($_POST["yyact"])){
				   $password=$_POST["password"];
				   $oldpassword=$_POST["oldpassword"];
				   if(array_key_exists("status",$_POST)){$status=$_POST["status"];}else{
					   $status=0;
					}
		    if($_POST["yyact"]=="editpwd"){
			       $userid=$_POST["userid"];
				   $username=$_POST["username"];
				   $this->edit_admin_user_pwd($userid,$username,$oldpassword,$password,$status);
			  }			  
		  }
		 require tpl_adm('user_pwd');
	 }
	 
	  public function user_delete(){
		  if(!getroot('users','admin')){
			  messagebox(Lan('no_permission_adminuser'),'back',"warn");			
		   } 
		 if(!empty($_GET["userid"])){
			 $userid=trim($_GET["userid"]);
			 if(!is_numeric($userid)){
			   messagebox(Lan('user_userid_notnumber'),url_admin('init','','',$this->hashurl['usvg']),"warn");
			 }
			 $cuser=$this->db->find("select `userid`,`username`,  `realname`,`status` from `#yunyecms_user` where `userid`= {$userid}");
			 if(empty($cuser)){
				 messagebox(Lan('user_not_exist'),url_admin('init','','',$this->hashurl['usvg']),"warn");	
			  }
			  $usernum=$this->db->GetCount("select count(*) as total from `#yunyecms_user` ");
			  if($usernum<=1){messagebox(Lan('user_islastuser'),url_admin('init','','',$this->hashurl['usvg']),"warn");
			 }
			 $query=$this->db->query("delete from `#yunyecms_user` where userid='$userid'");
			if($query){
				    messagebox(Lan('user_delete_success'),url_admin('init','','',$this->hashurl['usvg']),"success");
				}else{
				    messagebox(Lan('user_delete_error'),url_admin('init','','',$this->hashurl['usvg']),"warn");
					}
			}
	 }	 
	 
	private function checkuser_exist($username) {
		$username =  trim($username);
		 if(empty($username)){
		     return false;
			 }else{
			  if ($this->db->find("select `username` from `#yunyecms_user` where `username`= '{$username}'")){
				  return true;
			  }				 
		  }
	  }	
	private function add_admin_user($username,$email,$password,$roleid,$catid,$realname,$allowclass,$status=1) {
		 $username=usafestr(trim($username),1,1);
		 if(empty($username)){
			   messagebox(Lan('user_username_empty'),url_admin('user_add','','',$this->hashurl['usvg']),"error");			
			 }
		 if(empty($password)){
			   messagebox(Lan('user_pwd_empty'),url_admin('user_add','','',$this->hashurl['usvg']),"error");			
			 }
		 if(empty($roleid)){
			   messagebox(Lan('user_role_empty'),url_admin('user_add','','',$this->hashurl['usvg']),"warn");			
			 }	
		 if(empty($catid)){
			   messagebox(Lan('user_catid_empty'),url_admin('user_add','','',$this->hashurl['usvg']),"warn");			
			 }				 
		 $rnd=make_rand(20);
		 $salt=make_rand(16);
		 $email=usafestr(trim($email));
		 $password=usafestr(trim($password));
		 $password=YUNYECMSadmPwd($password,$salt);
		 $roleid=(int)trim($roleid);
		 $catid=(int)trim($catid);
		 $addip["ipaddr"]=getip();
		 $addip["ipport"]=getipport();
		 $addiparr=yunyecms_strencode(json_encode($addip)); 
		 $createtime=time();
		 $realname=usafestr(trim($realname));
		 $status=usafestr(trim($status));
		 if(empty($status)){
			 $status=0;
			 }else{
		     $status=(int)trim($status);
				 }
		 $allowclassarr=array();
		  if(!empty($allowclass)){
			 $allowclassarr=yunyecms_strencode(json_encode($_POST["allowclass"])); 
		  }else{
			  $allowclassarr=NULL;
			  }
		  if($this->checkuser_exist($username)){
				         messagebox(Lan('user_already_exist'),url_admin('user_add','','',$this->hashurl['usvg']),"warn");			
					}else{
				    $strsql="insert into `#yunyecms_user`(`username`, `password`, `roleid`, `catid`, `realname`, `salt`, `rnd`, `email`,`allowclass`,`createtime`,`addip`,`status`) values('$username','$password',$roleid,$catid,'$realname','$salt','$rnd','$email','$allowclassarr',$createtime,'$addiparr',$status)";
					$query=$this->db->query($strsql);
	                $userid=$this->db->insert_id();
					if($userid){
				         messagebox(Lan('user_add_success'),url_admin('init','','',$this->hashurl['usvg']),"success");			
						}
		    }
	 }
	 
	 
	private function edit_admin_user($userid,$username,$oldusername,$email,$roleid,$catid,$realname,$allowclass,$status=1) {
		 $username=usafestr(trim($username));
		 $oldusername=usafestr(trim($oldusername));
		 $userid=usafestr(intval(trim($userid)));
		 if(!is_numeric($userid)){
			   messagebox(Lan('user_userid_notnumber'),url_admin('init','','',$this->hashurl['usvg']),"warn");
		  }
		 if(empty($username)||empty($userid)){
			   messagebox(Lan('user_username_empty'),url_admin('user_add','user',array('userid'=>$userid),$this->hashurl['usvg']),"warn");			
			 }
		  if($username!=$oldusername){
			   $num=$this->db->GetCount("select count(*) as total from `#yunyecms_user` where username='$username' and userid<>$userid limit 1");
			   if($num){messagebox(Lan('user_already_exist'),url_admin('user_add','','',$this->hashurl['usvg']),"warn");	}
				   }

		 $roleid=(int)trim($roleid);
		 $catid=(int)trim($catid);	 
		 if(empty($roleid)){
			   messagebox(Lan('user_role_empty'),url_admin('user_add','user',array('userid'=>$userid),$this->hashurl['usvg']),"warn");			
			 }	
		 if(empty($catid)){
			   messagebox(Lan('user_catid_empty'),url_admin('user_add','user',array('userid'=>$userid),$this->hashurl['usvg']),"warn");			
			 }				 
		 $email=usafestr(trim($email));
		 $realname=usafestr(trim($realname));
		 $status=usafestr(trim($status));
		 if(empty($status)){
			 $status=0;
			 }else{
		     $status=(int)$status;
				 }
		 $allowclassarr=array();
		  if(!empty($allowclass)){
			 $allowclassarr=yunyecms_strencode(json_encode($_POST["allowclass"])); 
		  }else{
			  $allowclassarr=NULL;
			  }

				    $strsql="update `#yunyecms_user`  set `username`='$username',`roleid`=$roleid,`catid`=$catid,`realname`='$realname',`email`='$email',`allowclass`='$allowclassarr',`status`=$status where userid='$userid'";
					$query=$this->db->query($strsql);
					 if($query){
				         messagebox(Lan('user_edit_success'),url_admin('init','','',$this->hashurl['usvg']),"success");			
						}
	   }		 
	 
	 
	private function edit_admin_user_pwd($userid,$username,$oldpassword,$password,$status=1){
		 $username=usafestr(trim($username));
		 $oldpassword=usafestr(trim($oldpassword));
		 $password=usafestr(trim($password));
		 if(empty($username)||empty($userid)){
			   messagebox(Lan('user_username_empty'),url_admin('user_pwd','user',array('userid'=>$userid),$this->hashurl['usvg']),"warn");				
			 }
		 else{
				 $cuser=$this->db->find("select `userid`,`username`,`password`,`salt`  from `#yunyecms_user` where `userid`= {$userid}");
				 if(empty($cuser)){
					   messagebox(Lan('user_not_exist'),url_admin('user_pwd','user',array('userid'=>$userid),$this->hashurl['usvg']),"warn");			
				  }else{
					 $oldpassword_indb=$cuser["password"];
					 $salt=$cuser["salt"];
			         $oldpassword=YUNYECMSadmPwd($oldpassword,$salt);				  
					 if($oldpassword_indb!=$oldpassword){
			            messagebox(Lan('user_oldpwd_error'),url_admin('user_pwd','user',array('userid'=>$userid),$this->hashurl['usvg']),"warn");
						exit;				
						 }
					    else{
							 	 if(!empty($password)){
								   if(strlen($password)<6)
										 {
									     messagebox(Lan('user_pwd_less'),url_admin('user_add','user',array('userid'=>$userid),$this->hashurl['usvg']),"warn");			
										 }
								   $rnd=make_rand(20);
								   $salt_new=make_rand(16);
								   $password=YUNYECMSadmPwd($password,$salt_new);				  
								   $strpwd="update `#yunyecms_user`  set `password`='$password',`salt`='$salt_new',`rnd`='$rnd' where userid='$userid'";
								   $query=$this->db->query($strpwd);
									if($query){
										usetcookie("admuserid","",1);
										usetcookie("admusername","",1);
										usetcookie("admloginrnd","",1);
										usetcookie("admroleid","",1);
										usetcookie("loginyunyecmsckpass",'',1);
										usetcookie("loginyunyecmsfilernd",'',1);
										usetcookie("admlogintruetime",'',1);
										usetcookie("admlogintime",'',1);
										usetcookie("admloginlicense",'',1);
										DelADMFileRnd($userid);
										messagebox(Lan('user_edit_success'),url_admin('init','','',$this->hashurl['usvg']),"success");		
										}
								   }
					     }
				 }
			 }
	   }		 
	 
	 
	 	 
	 
	
	 
	 
}
?>
