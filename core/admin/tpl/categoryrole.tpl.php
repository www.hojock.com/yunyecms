<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>YUNYECMS <?php echo YUNYECMS_VERSION;?></title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="<?php echo YUNYECMS_UI;?>bootstrap/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?php echo YUNYECMS_UI;?>plugins/font-awesome-4.7.0/css/font-awesome.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo YUNYECMS_UI;?>plugins/iCheck/all.css">
  <link rel="stylesheet" href="<?php echo YUNYECMS_UI;?>dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="<?php echo YUNYECMS_UI;?>dist/css/skins/_all-skins.min.css">
  <link rel="stylesheet" href="<?php echo YUNYECMS_UI;?>dist/css/admin.css">
<link rel="stylesheet" href="<?php echo YUNYECMS_UI;?>validator/dist/css/bootstrapValidator.css"/>
  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
<script src="<?php echo YUNYECMS_UI;?>plugins/jQuery/jquery-2.2.3.min.js"></script>
<script src="<?php echo YUNYECMS_UI;?>plugins/iCheck/icheck.min.js"></script>
  
</head>
<body class="hold-transition rightbgcolor"  onload="changefrmHeight()">
  <!-- Content Wrapper. Contains page content -->
  <div class="container-fluid" id="mainwrap">
    <!-- Main content -->
    <section class="content">
      <div class="row">
        <!-- left column -->
        <div class="col-md-12">
          <div class="box box-primary">
            <div class="box-header with-border">
               <h3 class="box-title"><?php if($yyact=="add"):?> 增加权限 <?php elseif($yyact=="edit"):?> 修改权限 <?php  endif?></h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form class="form-horizontal" id="defaultForm"  method="post"  action="<?php echo YUNYECMS_URLADM;?>">
              <div class="box-body">
                <div class="form-group">
                  <label for="inputrolename" class="col-sm-1 control-label">权限名</label>
                     <div class="col-sm-5">
                      <div class="input-group">
                      <span class="input-group-addon"><i class="fa fa-columns"></i></span>
                      <input type="text" class="form-control" name="rolename" id="rolename"  <?php if(!empty($roleid)):?> value="<?php echo $crole['rolename'];?>" <?php endif ?> placeholder="请输入权限名" required>
                      </div>
                  </div>
                    <div class="col-sm-5">
                      <span class="help-block"><span class="font_red14"> * </span></span>
                    </div>
                </div>
                <div class="form-group">
                  <label for="inputrolename" class="col-sm-1 control-label">栏目权限</label>
                  <div class="col-sm-6" id="powerlist">
    <table class="table table-bordered table-striped ">
        <thead>
        <tr>
            <th  width="30" title="全选">
			<input type="checkbox" title="全选" name="selectall" id="selectall"  class="minimal-blue" >
            </th>
            <th width="50">ID</th>
            <th width="30%">栏目</th>
            <th width="20">查看</th>
            <th width="20">添加</th>
            <th width="20">编辑</th>
            <th width="20">删除</th>
        </tr>
        </thead>
        <tbody id="catlist">
        <?php  function catlist($list,$powercatarr,$level=0){  
	         $strspace="";
			 if($level){
				 for($i=0;$i<$level;$i++){
					$strspace=$strspace."&nbsp;&nbsp;&nbsp;&nbsp;";
					 }
				 $strspace.="|--";
				 }	
				if($level==0){ $cssstrong=" font-weight: bold;";$levelstr="";}else{
					$levelstr="&nbsp;<span style='color:#999'>(".ToChinaseNum($level+1)."级)</span>";
				}
			?> 
			  <?php foreach($list as $key=>$vo):?>
             <tr>
                <td><input type="checkbox" name="id_<?php echo $vo["id"];?>" id="id_<?php echo $vo["id"];?>" value="<?php echo $vo["id"];?>"  class="minimal-blue"  ><script language="javascript">      
										$(document).ready(function(){
										 $('#id_<?php echo $vo["id"];?>').on('ifChecked ifUnchecked', function(event) {
											  linecheck(event,<?php echo $vo["id"];?>);
										  });
									   });</script>
                    </td>
                <td><?php echo $vo["id"];?></td>
                <td align="left" style="text-align:left; <?php if(isset($cssstrong)) echo $cssstrong;?>">
                 <span class="font-blue-chambray"> <?php echo $strspace.$vo["title"].$levelstr;?> </span>
                </td>
                <td><input type="checkbox" class="minimal-blue" id="view_<?php echo $vo["id"];?>" name="powercat[<?php echo $vo['id']?>][view]" value="1" <?php if(yunyecms_getarrayvalue($powercatarr,$vo["id"],'view')=="1") echo "checked"; ?>> </td>
                <td> <input type="checkbox" class="minimal-blue" id="add_<?php echo $vo["id"];?>" name="powercat[<?php echo $vo['id']?>][add]" value="1" <?php if(yunyecms_getarrayvalue($powercatarr,$vo["id"],'add')==1) echo "checked";?>> </td>
                <td> <input type="checkbox" class="minimal-blue" id="edit_<?php echo $vo["id"];?>" name="powercat[<?php echo $vo['id']?>][edit]" value="1" <?php if(yunyecms_getarrayvalue($powercatarr,$vo["id"],'edit')==1) echo "checked";?>> </td>
                <td> <input type="checkbox" class="minimal-blue" id="del_<?php echo $vo["id"];?>" name="powercat[<?php echo $vo['id']?>][del]" value="1" <?php if(yunyecms_getarrayvalue($powercatarr,$vo["id"],'del')==1) echo "checked";?>> </td>
            </tr>
            <?php 
		       $curid=$vo["id"];
			   $sublist=getlist("select * from `#yunyecms_category`  where `pid`=$curid order by ordernum asc ");
			   if(!empty($sublist)){
				     catlist($sublist,$powercatarr,$level+1);
			   }					   
		    ?>
        <?php endforeach; ?>
		 <?php  }?>
        <?php catlist($list,$powercatarr); ?>
        </tbody>
    </table>
                     </div>
                   <div class="col-sm-offset-1 col-sm-10">
                     <div class="row">
                     <div class="col-sm-2 pb5">  <input type="checkbox" class="minimal-blue" name="selectallfoot"  id="selectallfoot"  value="1">&nbsp; &nbsp;选中全部
                         </div>
                     </div>
                   </div>
                     
                </div>
                
              </div>
              <!-- /.box-body -->
              <div class="box-footer">
              <div class="form-group">
                  <div class="col-sm-offset-1 col-sm-10">
                     <input name="yyact" type="hidden" value="<?php echo $yyact;?>">
                     <input name="c" type="hidden" value="<?php echo ROUTE_C;?>">
                     <input name="a" type="hidden" value="<?php echo ROUTE_A;?>">
					 <?php echo $this->hashurl['svp'];?>
                     <?php if(!empty($roleid)):?>
                     <input name="id" type="hidden" value="<?php echo $roleid;?>">
                     <input name="oldrolename" type="hidden" id="oldrolename" value="<?php echo $crole['rolename'];?>"> 
                     <?php  endif?>
                     <button type="submit" class="btn btn-primary"><?php  if($yyact=="add"):?> <i class="icon fa fa-plus"></i> 添 加 <?php elseif($yyact=="edit"):?> <i class="icon fa fa-edit"></i> 修 改 <?php  endif?></button> &nbsp; &nbsp;
                     <button type="reset" class="btn btn-default">  重 置 </button>
                  </div>
              </div>
              </div>
              <!-- /.box-footer -->
            </form>
          </div>
              <div class="alert alert-info alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <h4><i class="icon fa fa-info"></i> 备注：</h4>
                   全部不选则默认所有栏目<br/>
              </div>
              <div class="example-modal">
        <div class="modal modal-info" id="alertmodal">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">提示信息：</h4>
              </div>
              <div class="modal-body">
                <p></p>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">取消</button>
                <button type="button" class="btn btn-outline" onClick="javsacript:CloseModal('infoselfmodal','#alertmodal');">确认</button>
              </div>
            </div>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->
      </div>
              
              
        </div>
        <!--/.col (left) -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
<?php require tpl_adm('foot');?>
<!-- jQuery 2.2.3 -->
<!-- Bootstrap 3.3.6 -->
<script src="<?php echo YUNYECMS_UI;?>bootstrap/js/bootstrap.min.js"></script>
<!-- FastClick -->
<!-- AdminLTE for demo purposes -->
<script src="<?php echo YUNYECMS_UI;?>dist/js/admin.js"></script>
<script type="text/javascript" src="<?php echo YUNYECMS_UI;?>validator/dist/js/bootstrapValidator.js"></script>
<script>
  $(function () {
    //iCheck for checkbox and radio inputs
    $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
      checkboxClass: 'icheckbox_minimal-blue',
      radioClass: 'iradio_minimal-blue'
    });
    //Red color scheme for iCheck
    $('input[type="checkbox"].minimal-red, input[type="radio"].minimal-red').iCheck({
      checkboxClass: 'icheckbox_minimal-red',
      radioClass: 'iradio_minimal-red'
    });
	$('input[type="checkbox"].minimal-blue, input[type="radio"].minimal-blue').iCheck({
      checkboxClass: 'icheckbox_minimal-blue',
      radioClass: 'iradio_minimal-blue'
    });
    //Flat red color scheme for iCheck
      $('input[type="checkbox"].flat-blue, input[type="radio"].flat-blue').iCheck({
      checkboxClass: 'icheckbox_flat-blue',
      radioClass: 'iradio_flat-blue'
    });
  });
</script>
<script type="text/javascript">
$(document).ready(function() {
    $('#defaultForm').bootstrapValidator({
        message: 'This value is not valid',
//        live: 'disabled',
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            rolename: {
                message: '权限名是无效的',
                validators: {
                    notEmpty: {
                        message: '权限名必需填写'
                    },
                   /* remote: {
                        url: 'user_add.php',
                        message: '权限名是不可用的'
                    },*/
                }
            },
        }
    });
});
	

$(document).ready(function(){
//使用ON绑定事件
    var checkAll = $('#selectall');
	var checkboxes = $('#powerlist input');
	checkAll.on('ifChecked ifUnchecked', function(event) {
		if (event.type == 'ifChecked') {
			checkboxes.iCheck('check');
		} else {
			checkboxes.iCheck('uncheck');
		}
	});
	
    var checkAllfoot = $('#selectallfoot');
	var checkboxes = $('#powerlist input');
	checkAllfoot.on('ifChecked ifUnchecked', function(event) {
		if (event.type == 'ifChecked') {
			checkboxes.iCheck('check');
		} else {
			checkboxes.iCheck('uncheck');
		}
	});	

});
	
function linecheck(event,id){
	if (event.type == 'ifChecked') {
			$('#view_'+id).iCheck('check');
			$('#add_'+id).iCheck('check');
			$('#edit_'+id).iCheck('check');
			$('#delete_'+id).iCheck('check');
		  } else {
			$('#view_'+id).iCheck('uncheck');
			$('#add_'+id).iCheck('uncheck');
			$('#edit_'+id).iCheck('uncheck');
			$('#delete_'+id).iCheck('uncheck');  
		}
}	
	
	
	
</script>

<script language="javascript" type="text/javascript">
        $(function() {
			var navudinfo="<?php echo $parnav;?>";
			$('.breadcrumb',window.parent.document).children('#homeitem').nextAll().remove();
			$('.breadcrumb',window.parent.document).children('#homeitem').after(navudinfo);
        });
</script>

</body>
</html>