<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>YUNYECMS <?php echo YUNYECMS_VERSION;?></title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="<?php echo YUNYECMS_UI;?>bootstrap/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?php echo YUNYECMS_UI;?>plugins/font-awesome-4.7.0/css/font-awesome.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo YUNYECMS_UI;?>plugins/iCheck/all.css">
  <link rel="stylesheet" href="<?php echo YUNYECMS_UI;?>dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="<?php echo YUNYECMS_UI;?>dist/css/skins/_all-skins.min.css">
  <link rel="stylesheet" href="<?php echo YUNYECMS_UI;?>dist/css/admin.css">
<link rel="stylesheet" href="<?php echo YUNYECMS_UI;?>validator/dist/css/bootstrapValidator.css"/>
<script type="text/javascript" charset="utf-8" src="<?php echo YUNYECMS_PUBLIC;?>ueditor/ueditor.config.js"></script>
<script type="text/javascript" charset="utf-8" src="<?php echo YUNYECMS_PUBLIC;?>ueditor/ueditor.all.js"> </script> 
<script src="<?php echo YUNYECMS_UI;?>plugins/jQuery/jquery-2.2.3.min.js"></script>
 
  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
</head>
<body class="hold-transition rightbgcolor"  onload="changefrmHeight()">
  <!-- Content Wrapper. Contains page content -->
  <div class="container-fluid" id="mainwrap">
   
   <h1 class="page-title"> 系统配置信息   <small></small>
   </h1>
    <!-- Main content -->
    <section class="content">
      <div class="row">
        <!-- left column -->
        <div class="col-md-12">
					<form id="form1" class="form-horizontal" method="post"  enctype="multipart/form-data"     action="<?php echo YUNYECMS_URLADM;?>"   >
										   <!--BEGIN TABS-->
											<div class="nav-tabs-custom">
                                                <ul class="nav nav-tabs ">
												<li class="active"><a href="#tab_1_1" data-toggle="tab"> <i class="fa fa-cog"></i>  系统基本设置</a></li>
												<li><a href="#tab_1_7" data-toggle="tab"> <i class="fa fa-newspaper-o"></i> 内容</a></li>		
												<li><a href="#tab_1_2" data-toggle="tab"> <i class="fa fa-envelope-o"></i> 邮件系统</a></li>
											
												<li><a href="#tab_1_3" data-toggle="tab"> <i class="fa fa-mobile"></i>  短信接口</a></li>
												<li><a href="#tab_1_4" data-toggle="tab"> <i class="fa fa-weixin"></i>  微信公众平台</a></li>
												<li><a href="#tab_1_5" data-toggle="tab"> <i class="fa fa-money"></i> 支付宝</a></li>
												<li><a href="#tab_1_6" data-toggle="tab"> <i class="fa  fa-cloud-upload"></i>上传设置</a></li>
												<li><a href="#tab_1_8" data-toggle="tab"> <i class="fa  fa-mobile"></i> 手机端</a></li>
												<li><a href="#tab_1_9" data-toggle="tab"> <i class="fa  fa-mobile"></i> 商业授权</a></li>
											</ul>
											<div class="tab-content">
												<div class="tab-pane active" id="tab_1_1">
												      <div class="box-body">
		 <div class="form-group">
            <label class="col-md-2 control-label" for="title">站点名称</label>
            <div class="col-md-6">
                <input type="text"  class="form-control " id="sitename" name="sitename" value="<?php if(!empty($rs)){ echo $rs["sitename"]; } ?>" />
            </div>
        </div>	
         <div class="form-group">
            <label class="col-md-2 control-label" for="keywords">SEO标题</label>
            <div class="col-md-6">
                <textarea name="seotitle" id="seotitle" class="form-control"><?php if(!empty($rs)):?><?php echo $rs["seotitle"];?> <?php endif; ?></textarea>
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-2 control-label" for="keywords">SEO关键字</label>
            <div class="col-md-6">
                <textarea name="seokey" id="seokey" class="form-control"><?php if(!empty($rs)):?><?php echo $rs["seokey"];?> <?php endif; ?> </textarea>
            </div>
        </div>
      <div class="form-group">
            <label class="col-md-2 control-label" for="description">SEO简介</label>
            <div class="col-md-6">
                <textarea name="seodesc" id="seodesc" class="form-control"><?php if(!empty($rs)):?><?php echo $rs["seodesc"];?> <?php endif; ?> </textarea>
            </div>
        </div>    
        <div class="form-group">
            <label class="col-md-2 control-label" for="title">网站LOGO</label>
            <div class="col-md-6">
              <script type="text/plain" id="upload_ue"></script>
            <div class="col-md-12" id="imglogobox" style="padding-left: 0">
            <input type="hidden" id="logo" name="logo" <?php if(!empty($rs)):?>  value="<?php echo $rs["logo"];?>" <?php endif; ?> />
    	      <?php if(empty($rs['logo'])){ 
			  $strimgcss='style="display: none;"'; 
			  $picurl=YUNYECMS_PUBLIC.'img/addpic.png'; 
			  } else{ 
			  $strimgcss=''; 
			  $picurl=$rs['logo']; 
			  } ?>
    	        <img src="<?php echo $picurl;?>" id="img_logo" style="width: 150px;border: solid 1px #ccc;padding: 2px; cursor: pointer;  " onclick="upImage();" /><br>
				<div style="margin-top:8px;">
					<a href="javascript:void(0);" onclick="upImage();" class="btn btn-primary btn-flat"><i class="fa fa-upload"></i> 点击上传图片 </a>
  <?php if(!empty($rs["logo"])){ $strcanclecss="style=''";} else{ $strcanclecss="style='display:none;'" ;} ?>			
 <button type="button" class="btn bg-red btn-flat" id="cancle" <?php echo $strcanclecss; ?>><i class="fa fa-remove"></i> 取消图片</button>
				</div> 
              </div>
		<script type="text/javascript">
		var _editor;
		$(function() {
		//重新实例化一个编辑器，防止在上面的editor编辑器中显示上传的图片或者文件
		_editor = UE.getEditor('upload_ue');
		_editor.ready(function () {
		//设置编辑器不可用
		//_editor.setDisabled();
		//隐藏编辑器，因为不会用到这个编辑器实例，所以要隐藏
		_editor.hide();
		//侦听图片上传
		_editor.addListener('beforeInsertImage', function(t, arg) {
		//将地址赋值给相应的input,只去第一张图片的路径
		$("#logo").attr("value", arg[0].src);
		$("#img_logo").attr("src", arg[0].src);
		$("#imglogobox").show();
		//图片预览
		//$("#preview").attr("src", arg[0].src);
		})
	
		});
		}); 
		//弹出图片上传的对话框
		function upImage() {
		var myImage = _editor.getDialog("insertimage");
		myImage.open();
		}
		$("#cancle").click(function(){
					$("#logo").val('');
					$("#img_pic").attr("src","<?php echo YUNYECMS_PUBLIC;?>img/noimg.jpg");
				});	
			</script>
             
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-2 control-label" for="title">二维码</label>
            <div class="col-md-6">
              <script type="text/plain" id="upload_qrcode"></script>
            <div class="col-md-12" id="imgqrcodebox" style="padding-left: 0">
            <input type="hidden" id="qrcode" name="qrcode" <?php if(!empty($rs)):?>  value="<?php echo $rs["qrcode"];?>" <?php endif; ?> />
    	      <?php if(empty($rs['qrcode'])){ 
			  $strimgcss='style="display: none;"'; 
			  $picurl=YUNYECMS_PUBLIC.'img/addpic.png'; 
			  } else{ 
			  $strimgcss=''; 
			  $picurl=$rs['qrcode']; 
			  } ?>
    	        <img src="<?php echo $picurl;?>" id="img_qrcode" style="width: 150px;border: solid 1px #ccc;padding: 2px; cursor: pointer;  " onclick="upqrcode();" /><br>
				<div style="margin-top:8px;">
					<a href="javascript:void(0);" onclick="upqrcode();" class="btn btn-primary btn-flat"><i class="fa fa-upload"></i> 点击上传图片 </a>
 <button type="button" class="btn bg-red btn-flat" id="cancle_qrcode"  <?php if(!empty($rs["qrcode"])){ echo "style=''";} else{ echo "style='display:none;'" ;} ?> ><i class="fa fa-remove"></i> 取消图片</button>
				</div> 
              </div>
		<script type="text/javascript">
		var _editor;
		$(function() {
		//重新实例化一个编辑器，防止在上面的editor编辑器中显示上传的图片或者文件
		_editor_qrcode = UE.getEditor('upload_qrcode');
		_editor_qrcode.ready(function () {
		//设置编辑器不可用
		//_editor.setDisabled();
		//隐藏编辑器，因为不会用到这个编辑器实例，所以要隐藏
		_editor_qrcode.hide();
		//侦听图片上传
		_editor_qrcode.addListener('beforeInsertImage', function(t, arg) {
		//将地址赋值给相应的input,只去第一张图片的路径
		$("#qrcode").attr("value", arg[0].src);
		$("#img_qrcode").attr("src", arg[0].src);
		$("#imgqrcodebox").show();
		//图片预览
		//$("#preview").attr("src", arg[0].src);
		})
		});
		});
		//弹出图片上传的对话框
		function upqrcode() {
		var myImage = _editor_qrcode.getDialog("insertimage");
		myImage.open();
		}
		$("#cancle_qrcode").click(function(){
					$("#qrcode").val('');
					$("#img_qrcode").attr("src","<?php echo YUNYECMS_PUBLIC;?>img/noimg.jpg");
				});	
			</script>
             
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-2 control-label" for="title">客服电话</label>
            <div class="col-md-6">
                <input type="text"  class="form-control " id="tel" name="tel" value="<?php if(!empty($rs)){ echo $rs["tel"]; } ?>" />
            </div>
            <div class="col-md-4">
				 多个客服电话请用逗号隔开
		    </div>
        </div> 
        <div class="form-group">
            <label class="col-md-2 control-label" for="title">手机</label>
            <div class="col-md-6">
                <input type="text"  class="form-control " id="mobile" name="mobile" value="<?php if(!empty($rs)){ echo $rs["mobile"]; } ?>" />
            </div>
             <div class="col-md-4">
				 多个手机请用逗号隔开
		     </div>
        </div>	
        <div class="form-group">
            <label class="col-md-2 control-label" for="title">客服邮箱</label>
            <div class="col-md-6">
                <input type="text"  class="form-control " id="mail" name="mail" value="<?php if(!empty($rs)){ echo $rs["mail"]; } ?>" />
            </div>
              <div class="col-md-4">
				 多个邮箱请用逗号隔开
				</div>
        </div>
          <div class="form-group">
            <label class="col-md-2 control-label" for="title">客服QQ</label>
            <div class="col-md-6">
                <input type="text"  class="form-control " id="qq" name="qq" value="<?php if(!empty($rs)){ echo $rs["qq"]; } ?>" />
            </div>
            <div class="col-md-4">
				 多个QQ请用逗号隔开
				</div>
        </div>			            										       
  	    <div class="form-group">
				<label class="col-md-2 control-label" for="title">关闭网站</label>
				<div class="col-md-6">
				  <input type="radio" name="isclose" value="1" class="minimal-blue"  <?php if(!empty($rs)){ if($rs["isclose"]==1) echo  "checked"; } ?>>     是 &nbsp;
				  <input type="radio" name="isclose" value="2" class="minimal-blue"   <?php if(empty($rs)){ echo "checked";}else{ if($rs["isclose"]==2) echo  "checked"; } ?> >   否 &nbsp;
				</div>  
		</div>
  	    <div class="form-group">
				<label class="col-md-2 control-label" for="title">在线客服</label>
				<div class="col-md-6">
				  <input type="radio" name="iskefu" value="1" class="minimal-blue" <?php if(empty($rs)){ echo "checked";}else{ if($rs["iskefu"]==1) echo  "checked"; } ?> >     显示 &nbsp;
				  <input type="radio" name="iskefu" value="2" class="minimal-blue"  <?php if(!empty($rs)){ if($rs["iskefu"]==2) echo  "checked"; } ?>>   不显示 &nbsp;
				</div> 
		</div>
  	    <div class="form-group">
				<label class="col-md-2 control-label" for="title">商城</label>
				<div class="col-md-6">
				  <input type="radio" name="ismall" value="1" class="minimal-blue"  <?php if(empty($rs)){ echo "checked";}else{ if($rs["ismall"]==1) echo  "checked"; } ?> >     开启 &nbsp;
				  <input type="radio" name="ismall" value="2" class="minimal-blue"  <?php if(!empty($rs)){ if($rs["ismall"]==2) echo  "checked"; } ?>>   关闭 &nbsp;
					</div>  
		</div>
  	   <div class="form-group">
				<label class="col-md-2 control-label" for="title">快速导航浮动图标</label>
				<div class="col-md-6">
				  <input type="radio" name="isquicknav" value="1" class="minimal-blue"  <?php if(empty($rs)){ echo "checked";}else{ if($rs["isquicknav"]==1) echo  "checked"; } ?> >     开启 &nbsp;
				  <input type="radio" name="isquicknav" value="2" class="minimal-blue"  <?php if(!empty($rs)){ if($rs["isquicknav"]==2) echo  "checked"; } ?>>   关闭 &nbsp;
					</div>  
		</div>
  	   <div class="form-group">
				<label class="col-md-2 control-label" for="title">云邮件</label>
				<div class="col-md-6">
				  <input type="radio" name="ismailsub" value="1" class="minimal-blue"  <?php if(empty($rs)){ echo "checked";}else{ if($rs["ismailsub"]==1) echo  "checked"; } ?> >     开启 &nbsp;
				  <input type="radio" name="ismailsub" value="2" class="minimal-blue"  <?php if(!empty($rs)){ if($rs["ismailsub"]==2) echo  "checked"; } ?>>   关闭 &nbsp;
					</div>  
		</div>
  	   <div class="form-group">
				<label class="col-md-2 control-label" for="title">注册会员邮件认证</label>
				<div class="col-md-6">
				  <input type="radio" name="ismailreg" value="1" class="minimal-blue" <?php if(empty($rs)){ echo "checked";}else{ if($rs["ismailreg"]==1) echo  "checked"; } ?>>     开启 &nbsp;
				  <input type="radio" name="ismailreg" value="2" class="minimal-blue" <?php if(!empty($rs)){ if($rs["ismailreg"]==2) echo  "checked"; } ?> >   关闭 &nbsp;
				</div>  
		</div> 
				
  	   <div class="form-group">
				<label class="col-md-2 control-label" for="title">留言反馈发送到邮箱</label>
				<div class="col-md-6">
				  <input type="radio" name="isfeedbackmail" value="1" class="minimal-blue" <?php if(empty($rs)){ echo "checked";}else{ if($rs["isfeedbackmail"]==1) echo  "checked"; } ?>>     开启 &nbsp;
				  <input type="radio" name="isfeedbackmail" value="2" class="minimal-blue" <?php if(!empty($rs)){ if($rs["isfeedbackmail"]==2) echo  "checked"; } ?> >   关闭 &nbsp;
				</div>  
	  </div> 
  	   <div class="form-group">
				<label class="col-md-2 control-label" for="title">生成静态HTML</label>
				<div class="col-md-6">
				  <input type="radio" name="ishtml" value="1" class="minimal-blue"  <?php if(empty($rs)){ echo "checked";}else{ if($rs["ishtml"]==1) echo  "checked"; } ?>>     开启 &nbsp;
				  <input type="radio" name="ishtml" value="2" class="minimal-blue" <?php if(!empty($rs)){ if($rs["ishtml"]==2) echo  "checked"; } ?>>   关闭 &nbsp;
					</div>  
		</div>
  	   <div class="form-group">
				<label class="col-md-2 control-label" for="title">验证方式</label>
				<div class="col-md-10">
				  <input type="radio" name="verifymode" value="1" class="minimal-blue" <?php if(empty($rs)){ echo "checked";}else{ if($rs["verifymode"]==1) echo  "checked"; } ?>>   邮箱验证 &nbsp;
				  <input type="radio" name="verifymode" value="2" class="minimal-blue" <?php if(!empty($rs)){ if($rs["verifymode"]==2) echo  "checked"; } ?>>   手机验证 &nbsp;                 

					</div>  
		</div>	
		<div class="note note-info">
        <h4 class="block text-danger">备注：</h4>
                 <p class="text-danger">
                  1、 网站SEO配置、网站名称、LOGO,二维码，客服和联系方式等信息优先显示语言版里设置的信息，如果语言版里面没有进行设置，则显示系统配置信息里面的配置。<br/>
                  2、 比如选择邮箱验证，那么注册验证码，订单通知验证码，发货通知等都将使用邮箱验证<br/></p>
          </div>																																																														
<!-- /.box-body -->
              <div class="box-footer">
              <div class="form-group">
                 <div class="col-md-offset-3 col-md-9">
					 <input name="yyact" type="hidden" value="<?php echo $yyact;?>">
                     <input name="c" type="hidden" value="<?php echo ROUTE_C;?>">
                     <input name="a" type="hidden" value="<?php echo ROUTE_A;?>">
                     <?php echo $this->hashurl['svp'];?>
                     <?php if(!empty($rs)):?>
                     <input name="id" type="hidden" value="<?php echo $rs["id"];?>">
                     <?php  endif?>
                     <button type="submit" class="btn bg-blue  btn-flat"><?php if($yyact=="edit"):?> <i class="icon fa fa-edit"></i> 提 交 <?php  endif?></button> &nbsp; &nbsp;
                     <button type="reset" class="btn btn-default btn-flat"> <i class="fa fa-undo"></i> 重 置 </button>                                         
            </div>	
              </div>
              </div>
              <!-- /.box-footer -->               
    </div>

	</div>
<div class="tab-pane" id="tab_1_7">
		 <div class="box-body">
 	    <div class="form-group">
				<label class="col-md-2 control-label" for="title">无权限访问内容时列表页是否显示</label>
				<div class="col-md-6">
				  <input type="radio" name="content[list]" value="0" class="minimal-blue" <?php if(empty($rs["content"])){ echo "checked";}else{ if($rs["content"]["list"]==0) echo  "checked"; } ?>>     显示 &nbsp;
				  <input type="radio" name="content[list]" value="1" class="minimal-blue" <?php if(!empty($rs["content"])){ if($rs["content"]["list"]==1) echo  "checked"; } ?>>   弹窗提示需登录会员或会员级别不够 &nbsp;
				</div>  
		</div>
		<div class="form-group">
				<label class="col-md-2 control-label" for="title">无权限访问内容时内容页是否显示</label>
				<div class="col-md-6">
				  <input type="radio" name="content[show]" value="0" class="minimal-blue" <?php if(empty($rs["content"])){ echo "checked";}else{ if($rs["content"]["show"]==0) echo  "checked"; } ?>> 部分显示并提示查看全部需登录会员或会员权限不足 &nbsp;
				  <input type="radio" name="content[show]" value="1" class="minimal-blue" <?php if(!empty($rs["content"])){ if($rs["content"]["show"]==1) echo  "checked"; } ?>>  不显示并弹窗提示需登录会员或会员级别不够 &nbsp;
				</div>  
		</div>	
		<div class="form-group">
				<label class="col-md-2 control-label" for="title">是否禁止复制</label>
				<div class="col-md-6">
				  <input type="radio" name="content[copy]" value="0" class="minimal-blue" <?php if(empty($rs["content"])){ echo "checked";}else{ if($rs["content"]["copy"]==0) echo  "checked"; } ?>> 否 &nbsp;
				  <input type="radio" name="content[copy]" value="1" class="minimal-blue" <?php if(!empty($rs["content"])){ if($rs["content"]["copy"]==1) echo  "checked"; } ?>>  是 &nbsp;
				</div>  
		</div>		 
<!-- /.box-body -->
              <div class="box-footer">
              <div class="form-group">
                 <div class="col-md-offset-3 col-md-9">
					 <input name="yyact" type="hidden" value="<?php echo $yyact;?>">
                     <input name="c" type="hidden" value="<?php echo ROUTE_C;?>">
                     <input name="a" type="hidden" value="<?php echo ROUTE_A;?>">
                     <?php echo $this->hashurl['svp'];?>
                     <?php if(!empty($rs)):?>
                     <input name="id" type="hidden" value="<?php echo $rs["id"];?>">
                     <?php  endif?>
                     <button type="submit" class="btn bg-blue  btn-flat"><?php if($yyact=="edit"):?> <i class="icon fa fa-edit"></i> 提 交 <?php  endif?></button> &nbsp; &nbsp;
                     <button type="reset" class="btn btn-default btn-flat"> <i class="fa fa-undo"></i> 重 置 </button>                                         
            </div>	
              </div>
              </div>
              <!-- /.box-footer -->             
        
        
        
    </div>	

												
												</div>
	<div class="tab-pane" id="tab_1_2">
		 <div class="box-body">
 	    <div class="form-group">
				<label class="col-md-2 control-label" for="title">邮件服务器是否要求加密连接(SSL)</label>
				<div class="col-md-6">
				  <input type="radio" name="email[ssl]" value="1" class="minimal-blue" <?php if(empty($rs["email"])){ echo "checked";}else{ if($rs["email"]["ssl"]==1) echo  "checked"; } ?>>     是 &nbsp;
				  <input type="radio" name="email[ssl]" value="2" class="minimal-blue" <?php if(!empty($rs["email"])){ if($rs["email"]["ssl"]==2) echo  "checked"; } ?>>   否 &nbsp;
				</div>  
		</div>
       <div class="form-group">
            <label class="col-md-2 control-label" for="title"> 发送邮件服务器地址(SMTP):</label>
            <div class="col-md-6">
                <input type="text"  class="form-control " id="email[host]" name="email[host]" value="<?php if(!empty($rs)){ echo $rs["email"]["host"]; } ?>" />
            </div>
        </div> 
       <div class="form-group">
            <label class="col-md-2 control-label" for="title">端口号</label>
            <div class="col-md-6">
                <input type="text"  class="form-control " id="email[port]" name="email[port]" value="<?php if(!empty($rs)){ echo $rs["email"]["port"]; } ?>" />
            </div>
        </div>                     
          <div class="form-group">
            <label class="col-md-2 control-label" for="title">邮件发送帐号</label>
            <div class="col-md-6">
                <input type="text"  class="form-control " id="email[user]" name="email[user]" value="<?php if(!empty($rs)){ echo $rs["email"]["user"]; } ?>" />
            </div>
        </div> 
        <div class="form-group">
            <label class="col-md-2 control-label" for="title">账号密码</label>
            <div class="col-md-6">
                <input type="password"  class="form-control " id="email[pwd]" name="email[pwd]"/>
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-2 control-label" for="title">发件人邮箱</label>
            <div class="col-md-6">
                <input type="text"  class="form-control " id="email[fromemail]" name="email[fromemail]" value="<?php if(!empty($rs)){ echo $rs["email"]["fromemail"]; } ?>" />
            </div>
        </div>         
        <div class="form-group">
            <label class="col-md-2 control-label" for="title">发件人名称</label>
            <div class="col-md-6">
                <input type="text"  class="form-control " id="email[fromname]" name="email[fromname]" value="<?php if(!empty($rs)){ echo $rs["email"]["fromname"]; } ?>" />
            </div>
        </div>  
        <div class="form-group">
            <label class="col-md-2 control-label" for="title">收件箱</label>
            <div class="col-md-6">
                <input type="text"  class="form-control " id="email[toemail]" name="email[toemail]" value="<?php if(!empty($rs)){ echo $rs["email"]["toemail"]; } ?>" />
            </div>
        </div>                                                        
<!-- /.box-body -->
              <div class="box-footer">
              <div class="form-group">
                 <div class="col-md-offset-3 col-md-9">
					 <input name="yyact" type="hidden" value="<?php echo $yyact;?>">
                     <input name="c" type="hidden" value="<?php echo ROUTE_C;?>">
                     <input name="a" type="hidden" value="<?php echo ROUTE_A;?>">
                     <?php echo $this->hashurl['svp'];?>
                     <?php if(!empty($rs)):?>
                     <input name="id" type="hidden" value="<?php echo $rs["id"];?>">
                     <?php  endif?>
                     <button type="submit" class="btn bg-blue  btn-flat"><?php if($yyact=="edit"):?> <i class="icon fa fa-edit"></i> 提 交 <?php  endif?></button> &nbsp; &nbsp;
                     <button type="reset" class="btn btn-default btn-flat"> <i class="fa fa-undo"></i> 重 置 </button>                                         
            </div>	
              </div>
              </div>
              <!-- /.box-footer -->             
        
        
        
    </div>	

												
												</div>
	<div class="tab-pane" id="tab_1_3">
		      <div class="box-body">
          <div class="form-group">
            <label class="col-md-2 control-label" for="title">阿里短信用户名</label>
            <div class="col-md-6">
                <input type="text"  class="form-control " id="sms[user]" name="sms[user]" value="<?php if(!empty($rs)){ echo $rs["sms"]["user"]; } ?>" />
            </div>
        </div> 
        <div class="form-group">
            <label class="col-md-2 control-label" for="title">阿里短信密码</label>
            <div class="col-md-6">
                <input type="password"  class="form-control " id="sms[pwd]" name="sms[pwd]" value="<?php if(!empty($rs)){ echo $rs["sms"]["pwd"]; } ?>" />
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-2 control-label" for="title">商家的手机号</label>
            <div class="col-md-6">
                <input type="text"  class="form-control " id="sms[mobile]" name="sms[mobile]" value="<?php if(!empty($rs)){ echo $rs["sms"]["mobile"]; } ?>" />
            </div>
        </div>         
               	<div class="note note-info">
        <h4 class="block">备注：</h4>
                 <p> 短信接口采用阿里大于接口，其中商家的手机号用于接收下单通知，付款通知等短信</p>
          </div>                        						           
			           <!-- /.box-body -->
              <div class="box-footer">
              <div class="form-group">
                 <div class="col-md-offset-3 col-md-9">
					 <input name="yyact" type="hidden" value="<?php echo $yyact;?>">
                     <input name="c" type="hidden" value="<?php echo ROUTE_C;?>">
                     <input name="a" type="hidden" value="<?php echo ROUTE_A;?>">
                     <?php echo $this->hashurl['svp'];?>
                     <?php if(!empty($rs)):?>
                     <input name="id" type="hidden" value="<?php echo $rs["id"];?>">
                     <?php  endif?>
                     <button type="submit" class="btn bg-blue  btn-flat"><?php if($yyact=="edit"):?> <i class="icon fa fa-edit"></i> 提 交 <?php  endif?></button> &nbsp; &nbsp;
                     <button type="reset" class="btn btn-default btn-flat"> <i class="fa fa-undo"></i> 重 置 </button>                                         
            </div>	
              </div>
              </div>
              <!-- /.box-footer -->        
        
    </div>	

												
												</div>
												<div class="tab-pane" id="tab_1_4">

												      <div class="box-body">
       <div class="form-group">
            <label class="col-md-2 control-label" for="title"> 公众号名称:</label>
            <div class="col-md-6">
                <input type="text"  class="form-control " id="weixin[title]" name="weixin[title]" value="<?php if(!empty($rs)){ echo $rs["weixin"]["title"]; } ?>" />
            </div>
        </div> 
        <div class="form-group">
            <label class="col-md-2 control-label" for="title"> 公众号原始id:</label>
            <div class="col-md-6">
                <input type="text"  class="form-control" id="weixin[orgid]" placeholder="请认真填写，如：gh_642358162359" name="weixin[orgid]" value="<?php if(!empty($rs)){ echo $rs["weixin"]["orgid"]; } ?>" />
            </div>
        </div> 
      <div class="form-group">
            <label class="col-md-2 control-label" for="title"> AppID:</label>
            <div class="col-md-6">
                <input type="text"  class="form-control" id="weixin[appid]"  placeholder="用于自定义菜单等高级功能" name="weixin[appid]" value="<?php if(!empty($rs)){ echo $rs["weixin"]["appid"]; } ?>" />
            </div>
        </div> 
      <div class="form-group">
            <label class="col-md-2 control-label" for="title"> AppSecret:</label>
            <div class="col-md-6">
                <input type="text"  class="form-control" id="weixin[appsecret]"   name="weixin[appsecret]" value="<?php if(!empty($rs)){ echo $rs["weixin"]["appsecret"]; } ?>" />
            </div>
        </div>   
      <div class="form-group">
            <label class="col-md-2 control-label" for="title"> Token:</label>
            <div class="col-md-6">
                <input type="text"  class="form-control" id="weixin[token]"  placeholder="自定义的Token值"  name="weixin[token]" value="<?php if(!empty($rs)){ echo $rs["weixin"]["token"]; } ?>" />
            </div>
        </div>   
      <div class="form-group">
            <label class="col-md-2 control-label" for="title"> 微信授权回调域名:</label>
            <div class="col-md-6">
                <input type="text"  class="form-control" id="weixin[oauth_redirecturi]"  placeholder="微信授权回调域名"  name="weixin[oauth_redirecturi]" value="<?php if(!empty($rs)){ echo $rs["weixin"]["oauth_redirecturi"]; } ?>" />
            </div>
        </div>  
        
      <div class="form-group">
            <label class="col-md-2 control-label" for="title"> 接口地址:</label>
            <div class="col-md-6">
                <span class="label label-primary"> http://<?php echo $_SERVER['HTTP_HOST'];?>/weixin </span>
            </div>
        </div>        
<!-- /.box-body -->
              <div class="box-footer">
              <div class="form-group">
                 <div class="col-md-offset-3 col-md-9">
					 <input name="yyact" type="hidden" value="<?php echo $yyact;?>">
                     <input name="c" type="hidden" value="<?php echo ROUTE_C;?>">
                     <input name="a" type="hidden" value="<?php echo ROUTE_A;?>">
                     <?php echo $this->hashurl['svp'];?>
                     <?php if(!empty($rs)):?>
                     <input name="id" type="hidden" value="<?php echo $rs["id"];?>">
                     <?php  endif?>
                     <button type="submit" class="btn bg-blue  btn-flat"><?php if($yyact=="edit"):?> <i class="icon fa fa-edit"></i> 提 交 <?php  endif?></button> &nbsp; &nbsp;
                     <button type="reset" class="btn btn-default btn-flat"> <i class="fa fa-undo"></i> 重 置 </button>                                         
            </div>	
              </div>
              </div>
              <!-- /.box-footer -->                 
        
        
        
    </div>	

												
												</div>
												
	<div class="tab-pane" id="tab_1_5">

									      <div class="box-body">
       <div class="form-group">
            <label class="col-md-2 control-label" for="title"> 支付方式名称:</label>
            <div class="col-md-6">
                <input type="text"  class="form-control " id="alipay[title]" name="alipay[title]" value="<?php if(!empty($rs)){ echo $rs["alipay"]["title"]; } ?>" />
            </div>
        </div> 
       <div class="form-group">
            <label class="col-md-2 control-label" for="title"> 支付方式名称:</label>
            <div class="col-md-6">
                 <textarea  id="alipay[desc]" name="alipay[desc]"  class="form-control " rows="4"><?php if(!empty($rs)){ echo $rs["alipay"]["desc"]; } ?></textarea>
            </div>
        </div>         
        <div class="form-group">
            <label class="col-md-2 control-label" for="title"> 支付宝帐户:</label>
            <div class="col-md-6">
                <input type="text"  class="form-control " id="alipay[acount]" name="alipay[acount]" value="<?php if(!empty($rs)){ echo $rs["alipay"]["acount"]; } ?>" />
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-2 control-label" for="title"> 合作者身份ID:</label>
            <div class="col-md-6">
                <input type="text"  class="form-control " id="alipay[partner]" name="alipay[partner]" value="<?php if(!empty($rs)){ echo $rs["alipay"]["partner"]; } ?>" />
            </div>
        </div>  
       <div class="form-group">
            <label class="col-md-2 control-label" for="title">交易安全校验码</label>
            <div class="col-md-6">
                <input type="password"  class="form-control " id="alipay[key]" name="alipay[key]" value="<?php if(!empty($rs)){ echo $rs["alipay"]["key"]; } ?>" />

            </div>
        </div>
<!-- /.box-body -->
              <div class="box-footer">
              <div class="form-group">
                 <div class="col-md-offset-3 col-md-9">
					 <input name="yyact" type="hidden" value="<?php echo $yyact;?>">
                     <input name="c" type="hidden" value="<?php echo ROUTE_C;?>">
                     <input name="a" type="hidden" value="<?php echo ROUTE_A;?>">
                     <?php echo $this->hashurl['svp'];?>
                     <?php if(!empty($rs)):?>
                     <input name="id" type="hidden" value="<?php echo $rs["id"];?>">
                     <?php  endif?>
                     <button type="submit" class="btn bg-blue  btn-flat"><?php if($yyact=="edit"):?> <i class="icon fa fa-edit"></i> 提 交 <?php  endif?></button> &nbsp; &nbsp;
                     <button type="reset" class="btn btn-default btn-flat"> <i class="fa fa-undo"></i> 重 置 </button>                                         
            </div>	
              </div>
              </div>
              <!-- /.box-footer -->                
        
        
        
    </div>	

												
												</div>																							
																																																<div class="tab-pane" id="tab_1_6">

									      <div class="box-body">
       <div class="form-group">
            <label class="col-md-2 control-label" for="title"> 最大允许上传文件大小:</label>
            <div class="col-md-6">
                <div class="input-group">
                      <input type="text" id="upload[maxsize]" name="upload[maxsize]" value="<?php if(!empty($rs)){ echo $rs["upload"]["maxsize"]; } ?>"  class="form-control" placeholder="单位:M"><span class="input-group-addon">                                                                               M
                                                                            </span>
                  </div>
                
            </div> 
        </div> 
        
 	   <div class="form-group">
				<label class="col-md-2 control-label" for="title">是否生成缩略图</label>
				<div class="col-md-6">
				  <input type="radio" name="upload[isthumb]" value="1" class="minimal-blue"  <?php if(empty($rs)){ echo "checked";}else{ if($rs["upload"]["isthumb"]==1) echo  "checked"; } ?>>     是 &nbsp;
				  <input type="radio" name="upload[isthumb]" value="2" class="minimal-blue"  <?php if(!empty($rs)){ if($rs["upload"]["isthumb"]==2) echo  "checked"; } ?>>   否 &nbsp;
					</div>  
	   </div>
      
      <div class="form-group">
            <label class="col-md-2 control-label" for="title"> 缩略图宽:</label>
            <div class="col-md-6">
                <div class="input-group">
                                    <input type="text" id="upload[width]" name="upload[width]" value="<?php if(!empty($rs)){ echo $rs["upload"]["width"]; } ?>"  class="form-control" placeholder="单位:像素(px)">
                                                                            <span class="input-group-addon">
                                                                               像素(px)
                                                                            </span>
                  </div>
                
            </div> 
        </div>
    <div class="form-group">
            <label class="col-md-2 control-label" for="title"> 缩略图高:</label>
            <div class="col-md-6">
                <div class="input-group">
                                    <input type="text" id="upload[height]" name="upload[height]" value="<?php if(!empty($rs)){ echo $rs["upload"]["height"]; } ?>"  class="form-control" placeholder="单位:像素(px)">
                                                                            <span class="input-group-addon">
                                                                               像素(px)
                                                                            </span>
                  </div>
                
            </div> 
        </div>                	        	        	        
               	        	        	               	        	        	               	        	        	        
<!-- /.box-body -->
              <div class="box-footer">
              <div class="form-group">
                 <div class="col-md-offset-3 col-md-9">
					 <input name="yyact" type="hidden" value="<?php echo $yyact;?>">
                     <input name="c" type="hidden" value="<?php echo ROUTE_C;?>">
                     <input name="a" type="hidden" value="<?php echo ROUTE_A;?>">
                     <?php echo $this->hashurl['svp'];?>
                     <?php if(!empty($rs)):?>
                     <input name="id" type="hidden" value="<?php echo $rs["id"];?>">
                     <?php  endif?>
                     <button type="submit" class="btn bg-blue  btn-flat"><?php if($yyact=="edit"):?> <i class="icon fa fa-edit"></i> 提 交 <?php  endif?></button> &nbsp; &nbsp;
                     <button type="reset" class="btn btn-default btn-flat"> <i class="fa fa-undo"></i> 重 置 </button>                                         
            </div>	
              </div>
              </div>
              <!-- /.box-footer -->                 
    </div>	
												
									          </div>
												
<div class="tab-pane" id="tab_1_8">
		 <div class="box-body">
		<div class="form-group">
				<label class="col-md-2 control-label" for="title">手机端显示方式</label>
				<div class="col-md-6">
				  <input type="radio" name="mobilecfg[displaymode]" value="0" class="minimal-blue" <?php if(empty($rs["mobilecfg"])){ echo "checked";}else{ if($rs["mobilecfg"]["displaymode"]==0) echo  "checked"; } ?>> 响应式自适应手机端（适合内容简单的网站） &nbsp;
				  <input type="radio" name="mobilecfg[displaymode]" value="1" class="minimal-blue" <?php if(!empty($rs["mobilecfg"])){ if($rs["mobilecfg"]["displaymode"]==1) echo  "checked"; } ?>>  独立手机端（适合商城或功能复杂的网站） &nbsp;
				</div>  
		</div>		 
<!-- /.box-body -->
              <div class="box-footer">
              <div class="form-group">
                 <div class="col-md-offset-3 col-md-9">
					 <input name="yyact" type="hidden" value="<?php echo $yyact;?>">
                     <input name="c" type="hidden" value="<?php echo ROUTE_C;?>">
                     <input name="a" type="hidden" value="<?php echo ROUTE_A;?>">
                     <?php echo $this->hashurl['svp'];?>
                     <?php if(!empty($rs)):?>
                     <input name="id" type="hidden" value="<?php echo $rs["id"];?>">
                     <?php  endif?>
                     <button type="submit" class="btn bg-blue  btn-flat"><?php if($yyact=="edit"):?> <i class="icon fa fa-edit"></i> 提 交 <?php  endif?></button> &nbsp; &nbsp;
                     <button type="reset" class="btn btn-default btn-flat"> <i class="fa fa-undo"></i> 重 置 </button>                                         
            </div>	
              </div>
              </div>
              <!-- /.box-footer -->             
        
        
        
    </div>	

												
												</div>	
												
<div class="tab-pane" id="tab_1_9">
		 <div class="box-body">
		<div class="form-group">
				<label class="col-md-2 control-label" for="title">授权码</label>
				<div class="col-md-6">
			                 <textarea  id="grantcode" name="grantcode"  class="form-control " rows="15"><?php if(!empty($rs)){ echo $rs["grantcode"]; } ?></textarea>
				</div>
		</div>		 
<!-- /.box-body -->
              <div class="box-footer">
              <div class="form-group">
                 <div class="col-md-offset-3 col-md-9">
					 <input name="yyact" type="hidden" value="<?php echo $yyact;?>">
                     <input name="c" type="hidden" value="<?php echo ROUTE_C;?>">
                     <input name="a" type="hidden" value="<?php echo ROUTE_A;?>">
                     <?php echo $this->hashurl['svp'];?>
                     <?php if(!empty($rs)):?>
                     <input name="id" type="hidden" value="<?php echo $rs["id"];?>">
                     <?php  endif?>
                     <button type="submit" class="btn bg-blue  btn-flat"><?php if($yyact=="edit"):?> <i class="icon fa fa-edit"></i> 提 交 <?php  endif?></button> &nbsp; &nbsp;
                     <button type="reset" class="btn btn-default btn-flat"> <i class="fa fa-undo"></i> 重 置 </button>                                         
            </div>	
              </div>
              </div>
              <!-- /.box-footer -->             
        
        
        
    </div>	

												
												</div>												
												
												
												
												
											</div>
										</div>

										<!--END TABS-->
                            </form>		                 
              <div class="callout callout-info">
                <h4><i class="icon fa fa-info"></i> 温馨提示：</h4>
                 1.网站SEO配置信息请到语言版管理里面进行设置。<br/>
                 2.网站LOGO,二维码，客服和联系方式等信息优先显示语言版里设置的信息，如果语言版里面没有进行设置，则显示系统配置信息里面的配置。
              </div>
        </div>
        <!--/.col (left) -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
<?php require tpl_adm('foot');?>

<!-- jQuery 2.2.3 -->
<script src="<?php echo YUNYECMS_UI;?>plugins/jQuery/jquery-2.2.3.min.js"></script>
<!-- Bootstrap 3.3.6 -->
<script src="<?php echo YUNYECMS_UI;?>bootstrap/js/bootstrap.min.js"></script>
<!-- FastClick -->
<!-- AdminLTE for demo purposes -->
<script src="<?php echo YUNYECMS_UI;?>dist/js/admin.js"></script>
<script src="<?php echo YUNYECMS_UI;?>plugins/iCheck/icheck.min.js"></script>

<script type="text/javascript" src="<?php echo YUNYECMS_UI;?>validator/dist/js/bootstrapValidator.js"></script>
<script>
  $(function () {
    //iCheck for checkbox and radio inputs
    $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
      checkboxClass: 'icheckbox_minimal-blue',
      radioClass: 'iradio_minimal-blue'
    });
    //Red color scheme for iCheck
    $('input[type="checkbox"].minimal-red, input[type="radio"].minimal-red').iCheck({
      checkboxClass: 'icheckbox_minimal-red',
      radioClass: 'iradio_minimal-red'
    });
    //Flat red color scheme for iCheck
    $('input[type="checkbox"].flat-blue, input[type="radio"].flat-blue').iCheck({
      checkboxClass: 'icheckbox_flat-blue',
      radioClass: 'iradio_flat-blue'
    });
  });
</script>
<script type="text/javascript">
$(document).ready(function() {
    $('#defaultForm').bootstrapValidator({
        message: 'This value is not valid',
//        live: 'disabled',
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            title: {
                message: '栏目名称是无效的',
                validators: {
                    notEmpty: {
                        message: '栏目名称必需填写'
                    },
                   /* remote: {
                        url: 'user_add.php',
                        message: '部门名是不可用的'
                    },*/
                }
            },
        }
    });
});
</script>
<script language="javascript" type="text/javascript">
        $(function() {
			var navudinfo="<?php echo $parnav;?>";
			$('.breadcrumb',window.parent.document).children('#homeitem').nextAll().remove();
			$('.breadcrumb',window.parent.document).children('#homeitem').after(navudinfo);
        });
</script>

    <script>
			  $(function () {
				//iCheck for checkbox and radio inputs
				$('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
				  checkboxClass: 'icheckbox_minimal-blue',
				  radioClass: 'iradio_minimal-blue'
				});
				//Red color scheme for iCheck
				$('input[type="checkbox"].minimal-red, input[type="radio"].minimal-red').iCheck({
				  checkboxClass: 'icheckbox_minimal-red',
				  radioClass: 'iradio_minimal-red'
				});
				$('input[type="checkbox"].minimal-blue, input[type="radio"].minimal-blue').iCheck({
				  checkboxClass: 'icheckbox_minimal-blue',
				  radioClass: 'iradio_minimal-blue'
				});
				//Flat red color scheme for iCheck
				  $('input[type="checkbox"].flat-blue, input[type="radio"].flat-blue').iCheck({
				  checkboxClass: 'icheckbox_flat-blue',
				  radioClass: 'iradio_flat-blue'
				});
			  });
     </script>


</body>
</html>