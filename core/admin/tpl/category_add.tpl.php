<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>YUNYECMS <?php echo YUNYECMS_VERSION;?></title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="<?php echo YUNYECMS_UI;?>bootstrap/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?php echo YUNYECMS_UI;?>plugins/font-awesome-4.7.0/css/font-awesome.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo YUNYECMS_UI;?>plugins/iCheck/all.css">
  <link rel="stylesheet" href="<?php echo YUNYECMS_UI;?>dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="<?php echo YUNYECMS_UI;?>dist/css/skins/_all-skins.min.css">
  <link rel="stylesheet" href="<?php echo YUNYECMS_UI;?>dist/css/admin.css">
<link rel="stylesheet" href="<?php echo YUNYECMS_UI;?>validator/dist/css/bootstrapValidator.css"/>
<script type="text/javascript" charset="utf-8" src="<?php echo YUNYECMS_PUBLIC;?>ueditor/ueditor.config.js"></script>
 <script type="text/javascript" charset="utf-8" src="<?php echo YUNYECMS_PUBLIC;?>ueditor/ueditor.all.js"> </script> 
<script src="<?php echo YUNYECMS_UI;?>plugins/jQuery/jquery-2.2.3.min.js"></script>
 
  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
</head>
<body class="hold-transition rightbgcolor"  onload="changefrmHeight()">
  <!-- Content Wrapper. Contains page content -->
  <div class="container-fluid" id="mainwrap">
   
   <h1 class="page-title"><?php if($yyact=="add"):?> 增加栏目 <?php elseif($yyact=="edit"):?> 修改栏目 <?php  endif?>                            <small></small>
   </h1>
    <!-- Main content -->
    <section class="content">
      <div class="row">
        <!-- left column -->
        <div class="col-md-12">
					<form id="form1" class="form-horizontal" method="post" action="<?php echo YUNYECMS_URLADM;?>"   >
									<input type="hidden" name="id" value="{$row.id}">
										<!--BEGIN TABS-->
										<div class="nav-tabs-custom">
											<ul class="nav nav-tabs">
												<li class="active"><a href="#tab_1_1" data-toggle="tab"><span class="lancnbox"><span class="lan-cn"></span></span> 中文版</a></li>
												<li><a href="#tab_1_2" data-toggle="tab"><span class="lancnbox"><span class="lan-en"></span></span> English</a></li>
											</ul>
	   <div class="tab-content">
		<div class="tab-pane active" id="tab_1_1">
         <div class="box-body">
           <div class="form-group">
            <label class="col-md-2 control-label" for="title">所属父栏目</label>
            <div class="col-md-6">
               <select id="pid" name="pid" class="form-control" >
                   <?php if(!empty($strpid)):?>
                        <?php foreach($pcate as $key=>$vcat):?>
                         <option value="<?php echo $vcat["id"];?>"  <?php if(!empty($row)) { if($row["pid"]==$vcat["id"]){ echo "selected=\"selected\""; } }?>  ><?php echo $vcat["title"];?></option>
                      <?php endforeach; ?>
                   <?php else:?>
                       <?php if(empty($row)):?>
                            <option value="0" selected="selected">顶级栏目</option>
                            <?php elseif($row['pid']==0):?>
                            <option value="0" selected="selected">顶级栏目</option>
                            <?php else:?>
                            <?php foreach($pcate1 as $key=>$vcat):?>
                              <option value="<?php echo $vcat["id"];?>" <?php if(!empty($row)) { if($row["pid"]==$vcat["id"]){ echo "selected=\"selected\""; } }?> ><?php echo $vcat["title"];?></option>
                            <?php endforeach; ?>
                       <?php endif;?>
                   <?php endif;?>
               </select>
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-2 control-label" for="title">数据模型</label>
            <div class="col-md-6">
                <select id="modelid" name="modelid"  class="form-control">
                    <?php foreach($this->modellist as $key=>$vm):?>
				        <?php if(empty($pcate)):?>
                         <option value="<?php echo $vm["modelid"];?>" <?php if(!empty($row)) { if($row["modelid"]==$vm["modelid"]){ echo "selected=\"selected\""; } }?>  ><?php echo $vm["modelname"];?></option>
					   <?php else:?>
					        <?php if(empty($row)):?>
					        <option value="<?php echo $vm["modelid"];?>" <?php if($pcate[0]["modelid"]== $vm["modelid"]):?> selected="selected" <?php endif;?> ><?php echo $vm["modelname"];?></option>
					            <?php else:?>
									 <option value="<?php echo $vm["modelid"];?>" <?php if($row["modelid"]== $vm["modelid"]):?> selected="selected" <?php endif;?> ><?php echo $vm["modelname"];?></option>
					          <?php endif;?>
					   <?php endif;?>
                     <?php endforeach; ?>
                </select>
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-2 control-label" for="title">名称</label>
            <div class="col-md-6">
                 <input type="text"  class="form-control " id="title" name="title" <?php if(!empty($id)):?>  value="<?php echo $row["title"];?>" <?php endif; ?>   />
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-2 control-label" for="title">附标题</label>
            <div class="col-md-6">
                  <input type="text"  class="form-control" id="ftitle" name="ftitle" <?php if(!empty($id)):?>  value="<?php echo $row["ftitle"];?>" <?php endif; ?> />
            </div>
        </div>
<div class="form-group">
            <label class="col-md-2 control-label" for="title">栏目图片</label>
            <div class="col-md-6">
              <script type="text/plain" id="upload_ue"></script>
                  <div class="col-md-3" style="padding-left: 0px;">
<input type="hidden" id="pic" name="pic"  <?php if(!empty($id)):?>  value="<?php echo $row["pic"];?>" <?php endif; ?> />
<a href="javascript:void(0);" onclick="upImage();" class="btn btn-primary btn-flat"><i class="fa fa-upload"></i> 点击上传图片 </a>
    </div>  
   	     <?php if(empty($row['pic'])){ 
			  $strimgcss='style="display: none;"'; 
			  $picurl=YUNYECMS_PUBLIC.'img/noimg.jpg'; 
			  } else{ 
			  $strimgcss=''; 
			  $picurl=$row['pic']; 
			  } ?>
   	    <div class="col-md-9" id="imgpicbox" <?php echo $strimgcss;?> >
    	    <img src="<?php echo $picurl;?>" id="img_pic" style="width: 135px;  border: solid 1px #ccc;padding: 2px; cursor: pointer;  " onclick="upImage();" /><br>
                    <div style="margin-top:8px;">
                        <button type="button" class="btn bg-red btn-flat" id="cancle" ><i class="fa fa-remove"></i> 取消图片</button>
                    </div>
        </div>
		<script type="text/javascript">
		var _editor;
		$(function() {
		_editor = UE.getEditor('upload_ue');
		_editor.ready(function () {
		_editor.hide();
		_editor.addListener('beforeInsertImage', function(t, arg) {
		$("#pic").attr("value", arg[0].src);
		$("#img_pic").attr("src", arg[0].src);
		$("#imgpicbox").show();
		})
		});
		}); 
		//弹出图片上传的对话框
		function upImage() {
		var myImage = _editor.getDialog("insertimage");
		myImage.open();
		}
			$("#cancle").click(function(){
				$("#pic").val('');
				$("#img_pic").attr("src","<?php echo YUNYECMS_PUBLIC;?>img/noimg.jpg");
			});	
		</script>
             
            </div>
        </div>    
        <div class="form-group">
            <label class="col-md-2 control-label" for="title">跳转链接</label>
            <div class="col-md-6">
                  <input type="text"  class="form-control" id="exlink" name="exlink" <?php if(!empty($id)):?>  value="<?php echo $row["exlink"];?>" <?php endif; ?>  />
            </div>
        </div>  
       <div class="form-group">
				<label class="col-md-2 control-label" for="title">访问级别</label>
				<div class="col-md-6">
				   <input type="radio" name="ispower" value="1" class="minimal-blue" <?php if(!empty($id)) { if($rsinfo['ispower']==1) echo "checked";} ?> >  会员 &nbsp;
				   <input type="radio" name="ispower" value="0" class="minimal-blue"  <?php if(empty($id)){ echo "checked";}else{ if($rsinfo["ispower"]==0) echo  "checked"; } ?>  >  游客 
				</div>
	  </div>					 
        <div class="form-group">
            <label class="col-md-2 control-label" for="title">每页显示数量</label>
            <div class="col-md-6">
                <?php if(empty($id)):?>
                <input type="text" class="form-control" id="pages" name="pages" value="20" />
                <?php else: ?>
                <input type="text" class="form-control" id="pages" name="pages" value="<?php echo $row["pages"];?>"  />
                <?php endif; ?>
            </div>
        </div>        
        <div class="form-group" >
            <label class="col-md-2 control-label" for="copyfrom">排序</label>
            <div class="col-md-6">
			  <?php if(empty($id)):?>
                <input type="text"  class="form-control " id="ordernum" name="ordernum" value="<?php echo $ordermax;?>" />
                <?php else: ?>         
                <input type="text"  class="form-control " id="ordernum" name="ordernum" value="<?php echo $row["ordernum"];?>" />
              <?php endif; ?>                   
            </div>
        </div>
	    <div class="form-group">
				<label class="col-md-2 control-label" for="title">导航栏显示</label>
				<div class="col-md-6">
			     <input type="radio" name="isdisplay" value="1" class="minimal-blue"  <?php if(empty($id)){ echo "checked";}else{ if($row["isdisplay"]==1) echo  "checked"; } ?>  } >  是   
				   <input type="radio" name="isdisplay" value="0" class="minimal-blue"   <?php if(!empty($id)) { if($row['isdisplay']==0) echo "checked";} ?> >      否 &nbsp;
				</div>
		 </div> 
	     <div class="form-group">
				<label class="col-md-2 control-label" for="title">推荐</label>
				<div class="col-md-6">
			     <input type="radio" name="isgood" value="1" class="minimal-blue"  <?php if(empty($id)){ echo "checked";}else{ if($row["isgood"]==1) echo  "checked"; } ?> >  是   
				   <input type="radio" name="isgood" value="0" class="minimal-blue" <?php if(!empty($id)) { if($row['isgood']==0) echo "checked";} ?> >    否 &nbsp;
				</div>
		 </div>        
         <div class="form-group">
            <label class="col-md-2 control-label" for="keywords">SEO标题</label>
            <div class="col-md-6">
                <textarea name="seotitle" id="seotitle" class="form-control"><?php if(!empty($id)):?><?php echo $row["seotitle"];?> <?php endif; ?></textarea>
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-2 control-label" for="keywords">SEO关键字</label>
            <div class="col-md-6">
                <textarea name="keywords" id="keywords" class="form-control"><?php if(!empty($id)):?><?php echo $row["keywords"];?> <?php endif; ?> </textarea>
            </div>
        </div>
      <div class="form-group">
            <label class="col-md-2 control-label" for="description">SEO简介</label>
            <div class="col-md-6">
                <textarea name="description" id="description" class="form-control"><?php if(!empty($id)):?><?php echo $row["description"];?> <?php endif; ?> </textarea>
            </div>
        </div>                                          
                                                                       
        <div class="form-group">
            <label class="col-md-2 control-label" for="title">封面页模板</label>
            <div class="col-md-6">
                <input type="text"  class="form-control" id="tplhome" name="tplhome" <?php if(!empty($id)):?>  value="<?php echo $row["tplhome"];?>" <?php endif; ?>  />
            </div>
        </div>        
       <div class="form-group">
            <label class="col-md-2 control-label" for="title">列表页模板</label>
            <div class="col-md-6">
                <input type="text"  class="form-control" id="tpllist" name="tpllist" <?php if(!empty($id)):?>  value="<?php echo $row["tpllist"];?>" <?php endif; ?>  />
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-2 control-label" for="title">内容页模板</label>
            <div class="col-md-6">
                <input type="text"  class="form-control" id="tplcontent" name="tplcontent" <?php if(!empty($id)):?>  value="<?php echo $row["tplcontent"];?>" <?php endif; ?>   />
            </div>
        </div>
          </div>
              <!-- /.box-body -->
              <div class="box-footer">
				   <div class="row">
					<div class="col-md-offset-3 col-md-9">
						 <button type="submit" class="btn bg-blue  btn-flat"><?php  if($yyact=="add"):?> <i class="icon fa fa-plus"></i> 添 加 <?php elseif($yyact=="edit"):?> <i class="icon fa fa-edit"></i> 修 改 <?php  endif?></button> &nbsp; &nbsp;
                     <button type="reset" class="btn btn-default btn-flat"> <i class="fa fa-undo"></i> 重 置 </button>      
					</div>
				</div>
               </div>
              <!-- /.box-footer -->
		</div>
		<div class="tab-pane" id="tab_1_2">
	    <div class="box-body">
        <div class="form-group">
            <label class="col-md-2 control-label" for="title">名称（En）</label>
            <div class="col-md-6">
                  <input type="text"  class="form-control" id="title_en" name="title_en" <?php if(!empty($id)):?>  value="<?php echo $row["title_en"];?>" <?php endif; ?>  />
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-2 control-label" for="title">附标题（En）</label>
            <div class="col-md-6">
                  <input type="text"  class="form-control" id="ftitle_en" name="ftitle_en" <?php if(!empty($id)):?>  value="<?php echo $row["ftitle_en"];?>" <?php endif; ?>  />
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-2 control-label" for="title">跳转链接（En）</label>
            <div class="col-md-6">
                  <input type="text"  class="form-control" id="exlink_en" name="exlink_en" <?php if(!empty($id)):?>  value="<?php echo $row["exlink_en"];?>" <?php endif; ?>   />
            </div>
        </div>        
        <div class="form-group">
            <label class="col-md-2 control-label" for="keywords">SEO标题（En）</label>
            <div class="col-md-6">
                <textarea name="seotitle_en" id="seotitle_en" class="form-control"><?php if(!empty($id)) echo $row["seotitle_en"];?></textarea>
            </div>
        </div>        
        <div class="form-group">
            <label class="col-md-2 control-label" for="keywords">SEO关键字(En)</label>
            <div class="col-md-6">
                <textarea name="keywords_en" id="keywords_en" class="form-control"><?php if(!empty($id)) echo $row["keywords_en"];?> </textarea>
            </div>
        </div>
         
        <div class="form-group">
            <label class="col-md-2 control-label" for="description">SEO简介(En)</label>
            <div class="col-md-6">
                <textarea name="description_en" id="description_en" class="form-control"><?php if(!empty($id)) echo $row["description_en"];?></textarea>
            </div>
        </div> 
        </div>
         <div class="box-footer">
                                            <div class="row">
                                                   <div class="col-md-offset-3 col-md-9">
					 <input name="yyact" type="hidden" value="<?php echo $yyact;?>">
                     <input name="c" type="hidden" value="<?php echo ROUTE_C;?>">
                     <input name="a" type="hidden" value="<?php echo ROUTE_A;?>">
                     <?php echo $this->hashurl['svp'];?>
                     <?php if(!empty($id)):?>
                     <input name="id" type="hidden" value="<?php echo $id;?>">
                     <?php  endif?>
                     <button type="submit" class="btn bg-blue  btn-flat"><?php  if($yyact=="add"):?> <i class="icon fa fa-plus"></i> 添 加 <?php elseif($yyact=="edit"):?> <i class="icon fa fa-edit"></i> 修 改 <?php  endif?></button> &nbsp; &nbsp;
                     <button type="reset" class="btn btn-default btn-flat"> <i class="fa fa-undo"></i> 重 置 </button>                                               
                                                    </div>
                                                </div>
                                            </div>        
												</div>

											</div>

										</div>

										<!--END TABS-->
                            </form>		                 
              <div class="callout callout-info">
                <h4><i class="icon fa fa-info"></i> 温馨提示：</h4>
                 1.您可以设置当前栏目每页显示的数量。<br/>
                 2.您可以单独为每个栏目设置不同的模板，如果没有设置，将默认显示数据模型里设置的默认模板。
              </div>
        </div>
        <!--/.col (left) -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
<?php require tpl_adm('foot');?>

<!-- jQuery 2.2.3 -->
<script src="<?php echo YUNYECMS_UI;?>plugins/jQuery/jquery-2.2.3.min.js"></script>
<!-- Bootstrap 3.3.6 -->
<script src="<?php echo YUNYECMS_UI;?>bootstrap/js/bootstrap.min.js"></script>
<!-- FastClick -->
<!-- AdminLTE for demo purposes -->
<script src="<?php echo YUNYECMS_UI;?>dist/js/admin.js"></script>
<script src="<?php echo YUNYECMS_UI;?>plugins/iCheck/icheck.min.js"></script>

<script type="text/javascript" src="<?php echo YUNYECMS_UI;?>validator/dist/js/bootstrapValidator.js"></script>
<script>
  $(function () {
    //iCheck for checkbox and radio inputs
    $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
      checkboxClass: 'icheckbox_minimal-blue',
      radioClass: 'iradio_minimal-blue'
    });
    //Red color scheme for iCheck
    $('input[type="checkbox"].minimal-red, input[type="radio"].minimal-red').iCheck({
      checkboxClass: 'icheckbox_minimal-red',
      radioClass: 'iradio_minimal-red'
    });
    //Flat red color scheme for iCheck
    $('input[type="checkbox"].flat-blue, input[type="radio"].flat-blue').iCheck({
      checkboxClass: 'icheckbox_flat-blue',
      radioClass: 'iradio_flat-blue'
    });
  });
</script>
<script type="text/javascript">
$(document).ready(function() {
    $('#defaultForm').bootstrapValidator({
        message: 'This value is not valid',
//        live: 'disabled',
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            title: {
                message: '栏目名称是无效的',
                validators: {
                    notEmpty: {
                        message: '栏目名称必需填写'
                    },
                   /* remote: {
                        url: 'user_add.php',
                        message: '部门名是不可用的'
                    },*/
                }
            },
        }
    });
});
</script>
<script language="javascript" type="text/javascript">
        $(function() {
			var navudinfo="<?php echo $parnav;?>";
			$('.breadcrumb',window.parent.document).children('#homeitem').nextAll().remove();
			$('.breadcrumb',window.parent.document).children('#homeitem').after(navudinfo);
        });
</script>

    <script>
			  $(function () {
				//iCheck for checkbox and radio inputs
				$('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
				  checkboxClass: 'icheckbox_minimal-blue',
				  radioClass: 'iradio_minimal-blue'
				});
				//Red color scheme for iCheck
				$('input[type="checkbox"].minimal-red, input[type="radio"].minimal-red').iCheck({
				  checkboxClass: 'icheckbox_minimal-red',
				  radioClass: 'iradio_minimal-red'
				});
				$('input[type="checkbox"].minimal-blue, input[type="radio"].minimal-blue').iCheck({
				  checkboxClass: 'icheckbox_minimal-blue',
				  radioClass: 'iradio_minimal-blue'
				});
				//Flat red color scheme for iCheck
				  $('input[type="checkbox"].flat-blue, input[type="radio"].flat-blue').iCheck({
				  checkboxClass: 'icheckbox_flat-blue',
				  radioClass: 'iradio_flat-blue'
				});
			  });
     </script>


</body>
</html>