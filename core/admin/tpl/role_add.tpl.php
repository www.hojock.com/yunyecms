<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>YUNYECMS <?php echo YUNYECMS_VERSION;?></title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="<?php echo YUNYECMS_UI;?>bootstrap/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?php echo YUNYECMS_UI;?>plugins/font-awesome-4.7.0/css/font-awesome.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo YUNYECMS_UI;?>plugins/iCheck/all.css">
  <link rel="stylesheet" href="<?php echo YUNYECMS_UI;?>dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="<?php echo YUNYECMS_UI;?>dist/css/skins/_all-skins.min.css">
  <link rel="stylesheet" href="<?php echo YUNYECMS_UI;?>dist/css/admin.css">
<link rel="stylesheet" href="<?php echo YUNYECMS_UI;?>validator/dist/css/bootstrapValidator.css"/>
  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
</head>
<body class="hold-transition rightbgcolor"  onload="changefrmHeight()">
  <!-- Content Wrapper. Contains page content -->
  <div class="container-fluid" id="mainwrap">
    <!-- Main content -->
    <section class="content">
      <div class="row">
        <!-- left column -->
        <div class="col-md-12">
          <div class="box box-primary">
            <div class="box-header with-border">
               <h3 class="box-title"><?php if($yyact=="add"):?> 增加权限 <?php elseif($yyact=="edit"):?> 修改权限 <?php  endif?></h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form class="form-horizontal" id="defaultForm"  method="post"  action="<?php echo YUNYECMS_URLADM;?>">
              <div class="box-body">
                <div class="form-group">
                  <label for="inputrolename" class="col-sm-1 control-label">权限名</label>
                     <div class="col-sm-5">
                      <div class="input-group">
                      <span class="input-group-addon"><i class="fa fa-columns"></i></span>
                      <input type="text" class="form-control" name="rolename" id="rolename"  <?php if(!empty($roleid)):?> value="<?php echo $crole['rolename'];?>" <?php endif ?> placeholder="请输入权限名" required>
                      </div>
                  </div>
                    <div class="col-sm-5">
                      <span class="help-block"><span class="font_red14"> * </span></span>
                    </div>
                </div>
                <div class="form-group">
                  <label for="inputrolename" class="col-sm-1 control-label">权限</label>
         <div class="col-sm-11" id="powerlist">
          <div class="box box-info box-solid">
            <div class="box-header with-border">
              <i class="fa fa-gears"></i>
              <h3 class="box-title">系统设置</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                 <div class="row">
                    <div class="col-sm-2 pb5"> <input type="checkbox" class="minimal-blue" name="powers[system][setting]" value="1" <?php if(!empty($roleid)){if(yunyecms_getarrayvalue($powersarr,'system','setting')==1) echo "checked";}?>>&nbsp; &nbsp;系统参数设置
                     </div>
                      <div class="col-sm-2 pb5">  
                       <input type="checkbox" class="minimal-blue" name="powers[system][data]" value="1" <?php if(!empty($roleid)){if(yunyecms_getarrayvalue($powersarr,'system','data')==1) echo "checked";}?>>&nbsp; &nbsp;数据备份
                     </div>
                       <div class="col-sm-2 pb5">  <input type="checkbox" class="minimal-blue" name="powers[system][update]" value="1" <?php if(!empty($roleid)){if(yunyecms_getarrayvalue($powersarr,'system','update')==1) echo "checked";}?>>&nbsp; &nbsp;数据更新
                       </div>
                        <div class="col-sm-2 pb5">  <input type="checkbox" class="minimal-blue" name="powers[system][safe]" value="1" <?php if(!empty($roleid)){if(yunyecms_getarrayvalue($powersarr,'system','safe')==1) echo "checked";}?>>&nbsp; &nbsp;安全设置
                       </div>
                      <div class="col-sm-2 pb5">  <input type="checkbox" class="minimal-blue" name="powers[system][msgview]" value="1" <?php if(!empty($roleid)){if(yunyecms_getarrayvalue($powersarr,'system','msgview')==1) echo "checked";}?>>&nbsp; &nbsp;系统消息查看
                       </div>
                      <div class="col-sm-2 pb5">  <input type="checkbox" class="minimal-blue" name="powers[system][msgdel]" value="1" <?php if(!empty($roleid)){if(yunyecms_getarrayvalue($powersarr,'system','msgdel')==1) echo "checked";}?>>&nbsp; &nbsp;系统消息删除
                      </div>
                   </div>
                    <div class="row">
                    <div class="col-sm-2 pb5"> <input type="checkbox" class="minimal-blue" name="powers[lang][view]" value="1" <?php   if(!empty($roleid)){if(yunyecms_getarrayvalue($powersarr,'lang','view')==1) echo "checked";}?>>&nbsp; &nbsp;语言版列表
                     </div>
                     <div class="col-sm-2 pb5"> <input type="checkbox" class="minimal-blue" name="powers[lang][add]" value="1" <?php if(!empty($roleid)){if(yunyecms_getarrayvalue($powersarr,'lang','add')==1) echo "checked";}?>>&nbsp; &nbsp;语言版添加
                     </div>
                       <div class="col-sm-2 pb5"> <input type="checkbox" class="minimal-blue" name="powers[lang][edit]" value="1" <?php if(!empty($roleid)){if(yunyecms_getarrayvalue($powersarr,'lang','edit')==1) echo "checked";}?>>&nbsp; &nbsp;语言版编辑
                     </div>
                       <div class="col-sm-2 pb5"> <input type="checkbox" class="minimal-blue" name="powers[lang][del]" value="1" <?php if(!empty($roleid)){if(yunyecms_getarrayvalue($powersarr,'lang','del')==1) echo "checked";}?>>&nbsp; &nbsp;语言版删除
                     </div>
                   </div>
              </div>
            <!-- /.box-body -->
          </div>
          
          <div class="box box-info box-solid">
            <div class="box-header with-border">
              <i class="fa fa-info"></i>
              <h3 class="box-title">信息管理</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                     <div class="row"><div class="col-sm-4 pb5">  <input type="checkbox" class="minimal-red" name="powers[info][all]" value="1" id="infoall" <?php if(!empty($roleid)){if(yunyecms_getarrayvalue($powersarr,'info','all')==1) echo "checked";}?>>&nbsp; &nbsp;<span class="text-danger">可管理所选栏目所有信息</span>
                     </div>
                      <div class="col-sm-4 pb5">  
                       <input type="checkbox" class="minimal-red" name="powers[info][self]" id="infoself" value="1" <?php if(!empty($roleid)){if(yunyecms_getarrayvalue($powersarr,'info','self')==1) echo "checked";}?>>&nbsp; &nbsp;<span class="text-danger">只能管理所选栏目自己发布的信息</span>
                     </div>
                     </div>
                     <div class="row">
                       <div class="col-sm-2 pb5">  <input type="checkbox" class="minimal-blue" name="powers[info][view]" value="1" <?php if(!empty($roleid)){if(yunyecms_getarrayvalue($powersarr,'info','view')==1) echo "checked";}?>>&nbsp; &nbsp;信息查看
                           </div>
                         <div class="col-sm-2 pb5">  <input type="checkbox" class="minimal-blue" name="powers[info][add]" value="1" <?php if(!empty($roleid)){if(yunyecms_getarrayvalue($powersarr,'info','add')==1) echo "checked";}?>>&nbsp; &nbsp;增加信息
                           </div>
                           <div class="col-sm-2 pb5">  <input type="checkbox" class="minimal-blue" name="powers[info][edit]" value="1" <?php if(!empty($roleid)){if(yunyecms_getarrayvalue($powersarr,'info','edit')==1) echo "checked";}?>>&nbsp; &nbsp;修改信息
                           </div>
                            <div class="col-sm-2 pb5">  <input type="checkbox" class="minimal-blue" name="powers[info][del]" value="1" <?php if(!empty($roleid)){if(yunyecms_getarrayvalue($powersarr,'info','del')==1) echo "checked";}?>>&nbsp; &nbsp;删除信息
                           </div>
                            <div class="col-sm-2 pb5">  <input type="checkbox" class="minimal-blue" name="powers[info][recycle]" value="1" <?php if(!empty($roleid)){if(yunyecms_getarrayvalue($powersarr,'info','recycle')==1) echo "checked";}?>>&nbsp; &nbsp;回收站（信息）
                           </div>
                            <div class="col-sm-2 pb5">  <input type="checkbox" class="minimal-blue" name="powers[info][isrth]" value="1" <?php if(!empty($roleid)){if(yunyecms_getarrayvalue($powersarr,'info','isrth')==1) echo "checked";}?>>&nbsp; &nbsp;推荐/头条/置顶
                           </div>
                     </div>
                        <div class="row">
                         <div class="col-sm-2 pb5">  <input type="checkbox" class="minimal-blue" name="powers[info][verify]" value="1" <?php if(!empty($roleid)){if(yunyecms_getarrayvalue($powersarr,'info','verify')==1) echo "checked";}?>>&nbsp; &nbsp;审核
                           </div>
                           <div class="col-sm-2 pb5">  <input type="checkbox" class="minimal-blue" name="powers[info][movecopy]" value="1" <?php if(!empty($roleid)){if(yunyecms_getarrayvalue($powersarr,'info','movecopy')==1) echo "checked";}?>>&nbsp; &nbsp;移动/复制
                           </div>
                            <div class="col-sm-3 pb5">  <input type="checkbox" class="minimal-blue" name="powers[info][needverify]" value="1" id="infoneedverify" <?php if(!empty($roleid)){if(yunyecms_getarrayvalue($powersarr,'info','needverify')==1) echo "checked";}?>>&nbsp; &nbsp;信息发布后须审核
                           </div>
                            <div class="col-sm-3 pb5">  <input type="checkbox" class="minimal-blue" name="powers[info][auditednotedit]" value="1" id="infoauditednotedit" <?php if(!empty($roleid)){if(yunyecms_getarrayvalue($powersarr,'info','auditednotedit')==1) echo "checked";}?>>&nbsp; &nbsp;信息审核后不可修改
                           </div>
                       </div>
              </div>
            <!-- /.box-body -->
          </div>
          <div class="box box-info box-solid">
            <div class="box-header with-border">
              <i class="fa fa-columns"></i>
              <h3 class="box-title">栏目管理</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
             <div class="row">
               <div class="col-sm-2 pb5">  <input type="checkbox" class="minimal-blue" name="powers[column][view]" value="1" <?php if(!empty($roleid)){if(yunyecms_getarrayvalue($powersarr,'column','view')==1) echo "checked";}?>>&nbsp; &nbsp;查看
                     </div>
                    <div class="col-sm-2 pb5">  <input type="checkbox" class="minimal-blue" name="powers[column][add]" value="1" <?php if(!empty($roleid)){if(yunyecms_getarrayvalue($powersarr,'column','add')==1) echo "checked";}?>>&nbsp; &nbsp;增加
                     </div>
                      <div class="col-sm-2 pb5">  
                      <input type="checkbox" class="minimal-blue" name="powers[column][edit]" value="1" <?php if(!empty($roleid)){if(yunyecms_getarrayvalue($powersarr,'column','edit')==1) echo "checked";}?>>&nbsp; &nbsp;修改
                     </div>
                       <div class="col-sm-2 pb5">  <input type="checkbox" class="minimal-blue" name="powers[column][del]" value="1" <?php if(!empty($roleid)){if(yunyecms_getarrayvalue($powersarr,'column','del')==1) echo "checked";}?>>&nbsp; &nbsp;删除
                     </div>
                      <div class="col-sm-2 pb5">  <input type="checkbox" class="minimal-blue" name="powers[column][recycle]" value="1" <?php if(!empty($roleid)){if(yunyecms_getarrayvalue($powersarr,'column','recycle')==1) echo "checked";}?>>&nbsp; &nbsp;回收站（栏目）
                     </div>
                      <div class="col-sm-2 pb5">  <input type="checkbox" class="minimal-blue" name="powers[column][recommend]" value="1" <?php if(!empty($roleid)){if(yunyecms_getarrayvalue($powersarr,'column','recommend')==1) echo "checked";}?>>&nbsp; &nbsp;推荐
                     </div>
                  </div>
                  <div class="row">
                   <div class="col-sm-2 pb5">  <input type="checkbox" class="minimal-blue" name="powers[column][count]" value="1" <?php if(!empty($roleid)){if(yunyecms_getarrayvalue($powersarr,'column','count')==1) echo "checked";}?>>&nbsp; &nbsp;信息统计
                     </div>
                    <div class="col-sm-2 pb5">  <input type="checkbox" class="minimal-blue" name="powers[column][forms]" value="1" <?php if(!empty($roleid)){if(yunyecms_getarrayvalue($powersarr,'column','forms')==1) echo "checked";}?>>&nbsp; &nbsp;关联表单管理
                     </div>
                    <div class="col-sm-2 pb5">  
                      <input type="checkbox" class="minimal-blue" name="powers[column][field]" value="1" <?php if(!empty($roleid)){if(yunyecms_getarrayvalue($powersarr,'column','field')==1) echo "checked";}?>>&nbsp; &nbsp;栏目自定义字段
                     </div>
                       <div class="col-sm-2 pb5">  <input type="checkbox" class="minimal-blue" name="powers[column][special]" value="1" <?php if(!empty($roleid)){if(yunyecms_getarrayvalue($powersarr,'column','special')==1) echo "checked";}?>>&nbsp; &nbsp;专题管理
                     </div>
                     <div class="col-sm-2 pb5">  <input type="checkbox" class="minimal-blue" name="powers[column][chip]" value="1" <?php if(!empty($roleid)){if(yunyecms_getarrayvalue($powersarr,'column','chip')==1) echo "checked";}?>>&nbsp; &nbsp;碎片管理
                     </div>
                  </div>
              </div>
            <!-- /.box-body -->
          </div>
          <div class="box box-info box-solid">
            <div class="box-header with-border">
              <i class="fa fa-users"></i>
              <h3 class="box-title">管理员/会员</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                     <div class="row">
                       <div class="col-sm-2 pb5">  <input type="checkbox" class="minimal-blue" name="powers[users][admin]" value="1" <?php if(!empty($roleid)){if(yunyecms_getarrayvalue($powersarr,'users','admin')==1) echo "checked";}?>>&nbsp; &nbsp;管理员管理
                           </div>
                         <div class="col-sm-2 pb5">  <input type="checkbox" class="minimal-blue" name="powers[users][role]" value="1" <?php if(!empty($roleid)){if(yunyecms_getarrayvalue($powersarr,'users','role')==1) echo "checked";}?>>&nbsp; &nbsp;管理员权限组
                           </div>
                           <div class="col-sm-2 pb5">  <input type="checkbox" class="minimal-blue" name="powers[users][logs]" value="1" <?php if(!empty($roleid)){if(yunyecms_getarrayvalue($powersarr,'users','logs')==1) echo "checked";}?>>&nbsp; &nbsp;登录日志
                           </div>
                          <div class="col-sm-2 pb5">  <input type="checkbox" class="minimal-blue" name="powers[users][dologs]" value="1" <?php if(!empty($roleid)){if(yunyecms_getarrayvalue($powersarr,'users','dologs')==1) echo "checked";}?>>&nbsp; &nbsp;操作日志
                           </div>
                            <div class="col-sm-2 pb5">  <input type="checkbox" class="minimal-blue" name="powers[users][department]" value="1" <?php if(!empty($roleid)){if(yunyecms_getarrayvalue($powersarr,'users','department')==1) echo "checked";}?>>&nbsp; &nbsp;部门管理
                           </div>
                       </div>
                        <div class="row">
                          <div class="col-sm-2 pb5">  <input type="checkbox" class="minimal-blue" name="powers[member][view]" value="1" <?php if(!empty($roleid)){if(yunyecms_getarrayvalue($powersarr,'member','view')==1) echo "checked";}?>>&nbsp; &nbsp;会员列表
                           </div>
                           <div class="col-sm-2 pb5">  <input type="checkbox" class="minimal-blue" name="powers[member][add]" value="1" <?php if(!empty($roleid)){if(yunyecms_getarrayvalue($powersarr,'member','add')==1) echo "checked";}?>>&nbsp; &nbsp;会员添加
                           </div>
                            <div class="col-sm-2 pb5">  <input type="checkbox" class="minimal-blue" name="powers[member][edit]" value="1" <?php if(!empty($roleid)){if(yunyecms_getarrayvalue($powersarr,'member','edit')==1) echo "checked";}?>>&nbsp; &nbsp;会员编辑
                           </div>
                           <div class="col-sm-2 pb5">  <input type="checkbox" class="minimal-blue" name="powers[member][del]" value="1" <?php if(!empty($roleid)){if(yunyecms_getarrayvalue($powersarr,'member','del')==1) echo "checked";}?>>&nbsp; &nbsp;会员删除
                           </div>
                           <div class="col-sm-2 pb5">  <input type="checkbox" class="minimal-blue" name="powers[member][membergroup]" value="1" <?php if(!empty($roleid)){if(yunyecms_getarrayvalue($powersarr,'member','membergroup')==1) echo "checked";}?>>&nbsp; &nbsp;会员组
                           </div>
                           <div class="col-sm-2 pb5">  <input type="checkbox" class="minimal-blue" name="powers[member][notice]" value="1" <?php if(!empty($roleid)){if(yunyecms_getarrayvalue($powersarr,'member','notice')==1) echo "checked";}?>>&nbsp; &nbsp;通知消息
                           </div>
                            <div class="col-sm-2 pb5">  <input type="checkbox" class="minimal-blue" name="powers[member][memfield]" value="1" <?php if(!empty($roleid)){if(yunyecms_getarrayvalue($powersarr,'member','memfield')==1) echo "checked";}?>>&nbsp; &nbsp;会员自定义字段
                           </div>
                           <div class="col-sm-2 pb5">  <input type="checkbox" class="minimal-blue" name="powers[member][api]" value="1" <?php if(!empty($roleid)){if(yunyecms_getarrayvalue($powersarr,'member','api')==1) echo "checked";}?>>&nbsp; &nbsp;第三方API管理
                           </div>
                           <div class="col-sm-2 pb5">  <input type="checkbox" class="minimal-blue" name="powers[member][recycle]" value="1" <?php if(!empty($roleid)){if(yunyecms_getarrayvalue($powersarr,'member','recycle')==1) echo "checked";}?>>&nbsp; &nbsp;回收站(会员)
                           </div>
                       </div>
              </div>
            <!-- /.box-body -->
          </div>
          
<div class="box box-info box-solid">
            <div class="box-header with-border">
              <i class="fa  fa-pencil"></i>
              <h3 class="box-title">模板/模型 </h3> 
            </div>
            <!-- /.box-header -->
            <div class="box-body" id="tmf">
                      <div class="row">
                         <div class="col-sm-2 pb5">  <input type="checkbox" class="minimal-blue" name="powers[tpl][tplfiles]" value="1" <?php if(!empty($roleid)){if(yunyecms_getarrayvalue($powersarr,'tpl','tplfiles')==1) echo "checked";}?>>&nbsp; &nbsp;模板文件
                         </div>
                         <div class="col-sm-2 pb5">  <input type="checkbox" class="minimal-blue" name="powers[tpl][label]" value="1" <?php if(!empty($roleid)){if(yunyecms_getarrayvalue($powersarr,'tpl','label')==1) echo "checked";}?>>&nbsp; &nbsp;标签
                         </div>
                         <div class="col-sm-2 pb5">  <input type="checkbox" class="minimal-blue" name="powers[tpl][labelstyle]" value="1" <?php if(!empty($roleid)){if(yunyecms_getarrayvalue($powersarr,'tpl','labelstyle')==1) echo "checked";}?>>&nbsp; &nbsp;标签样式
                         </div>
                          <div class="col-sm-2 pb5"><input type="checkbox" class="minimal-blue" name="powers[tpl][tplvar]" value="1" <?php if(!empty($roleid)){if(yunyecms_getarrayvalue($powersarr,'tpl','tplvar')==1) echo "checked";}?>>&nbsp; &nbsp;模板变量
                         </div>
                         <div class="col-sm-2 pb5"><input type="checkbox" class="minimal-blue" name="powers[tpl][tplstyle]" value="1" <?php if(!empty($roleid)){if(yunyecms_getarrayvalue($powersarr,'tpl','tplstyle')==1) echo "checked";}?>>&nbsp; &nbsp;模板风格
                         </div>
                     </div>
                     <div class="row">
                         <div class="col-sm-2 pb5">  <input type="checkbox" class="minimal-blue" name="powers[model][modelview]" value="1" <?php if(!empty($roleid)){if(yunyecms_getarrayvalue($powersarr,'model','modelview')==1) echo "checked";}?>>&nbsp; &nbsp;模型查看
                         </div>
                         <div class="col-sm-2 pb5">  <input type="checkbox" class="minimal-blue" name="powers[model][modeladd]" value="1" <?php if(!empty($roleid)){if(yunyecms_getarrayvalue($powersarr,'model','modeladd')==1) echo "checked";}?>>&nbsp; &nbsp;模型添加
                         </div>
                         <div class="col-sm-2 pb5">  <input type="checkbox" class="minimal-blue" name="powers[model][modeledit]" value="1" <?php if(!empty($roleid)){if(yunyecms_getarrayvalue($powersarr,'model','modeledit')==1) echo "checked";}?>>&nbsp; &nbsp;模型编辑
                         </div>
                         <div class="col-sm-2 pb5">  <input type="checkbox" class="minimal-blue" name="powers[model][modeldel]" value="1" <?php if(!empty($roleid)){if(yunyecms_getarrayvalue($powersarr,'model','modeldel')==1) echo "checked";}?>>&nbsp; &nbsp;模型删除
                         </div>
                         <div class="col-sm-2 pb5">  <input type="checkbox" class="minimal-blue" name="powers[model][fieldsview]" value="1" <?php if(!empty($roleid)){if(yunyecms_getarrayvalue($powersarr,'model','fieldsview')==1) echo "checked";}?>>&nbsp; &nbsp;字段查看
                         </div>
                         <div class="col-sm-2 pb5">  <input type="checkbox" class="minimal-blue" name="powers[model][fieldsadd]" value="1" <?php if(!empty($roleid)){if(yunyecms_getarrayvalue($powersarr,'model','fieldsadd')==1) echo "checked";}?>>&nbsp; &nbsp;字段添加
                         </div>
                         <div class="col-sm-2 pb5">  <input type="checkbox" class="minimal-blue" name="powers[model][fieldsedit]" value="1" <?php if(!empty($roleid)){if(yunyecms_getarrayvalue($powersarr,'model','fieldsedit')==1) echo "checked";}?>>&nbsp; &nbsp;字段修改
                         </div>
                         <div class="col-sm-2 pb5">  <input type="checkbox" class="minimal-blue" name="powers[model][fieldsdel]" value="1" <?php if(!empty($roleid)){if(yunyecms_getarrayvalue($powersarr,'model','fieldsdel')==1) echo "checked";}?>>&nbsp; &nbsp;字段删除
                         </div>
                     </div>
              </div>
            <!-- /.box-body -->
          </div>          
          
<div class="box box-info box-solid">
            <div class="box-header with-border">
              <i class="fa fa-plug"></i>
              <h3 class="box-title">应用/其他</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                       <div class="row">
                         <div class="col-sm-2 pb5">  <input type="checkbox" class="minimal-blue" name="powers[other][weixin]" value="1" <?php if(!empty($roleid)){if(yunyecms_getarrayvalue($powersarr,'other','weixin')==1) echo "checked";}?>>&nbsp; &nbsp;微信
                         </div>
                         <div class="col-sm-2 pb5">  <input type="checkbox" class="minimal-blue" name="powers[other][links]" value="1" <?php if(!empty($roleid)){if(yunyecms_getarrayvalue($powersarr,'other','links')==1) echo "checked";}?>>&nbsp; &nbsp;友情链接
                         </div>
                         <div class="col-sm-2 pb5">  <input type="checkbox" class="minimal-blue" name="powers[other][ad]" value="1" <?php if(!empty($roleid)){if(yunyecms_getarrayvalue($powersarr,'other','ad')==1) echo "checked";}?>>&nbsp; &nbsp;广告管理
                         </div>
                          <div class="col-sm-2 pb5"><input type="checkbox" class="minimal-blue" name="powers[other][online]" value="1" <?php if(!empty($roleid)){if(yunyecms_getarrayvalue($powersarr,'other','online')==1) echo "checked";}?>>&nbsp; &nbsp;在线客服
                         </div>
                         <div class="col-sm-2 pb5"><input type="checkbox" class="minimal-blue" name="powers[other][pay]" value="1" <?php if(!empty($roleid)){if(yunyecms_getarrayvalue($powersarr,'other','pay')==1) echo "checked";}?>>&nbsp; &nbsp;在线支付
                         </div>
                         <div class="col-sm-2 pb5"><input type="checkbox" class="minimal-blue" name="powers[other][files]" value="1" <?php if(!empty($roleid)){if(yunyecms_getarrayvalue($powersarr,'other','files')==1) echo "checked";}?>>&nbsp; &nbsp;上传文件管理
                         </div>
                     </div>
              <div class="row">
                         <div class="col-sm-2 pb5">  <input type="checkbox" class="minimal-blue" name="powers[shop][ordersview]" value="1" <?php if(!empty($roleid)){if(yunyecms_getarrayvalue($powersarr,'shop','ordersview')==1) echo "checked";}?>>&nbsp; &nbsp;订单列表
                         </div>
                         <div class="col-sm-2 pb5">  <input type="checkbox" class="minimal-blue" name="powers[shop][ordersedit]" value="1" <?php if(!empty($roleid)){if(yunyecms_getarrayvalue($powersarr,'shop','ordersedit')==1) echo "checked";}?>>&nbsp; &nbsp;订单编辑
                         </div>
                          <div class="col-sm-2 pb5">  <input type="checkbox" class="minimal-blue" name="powers[shop][ordersdel]" value="1" <?php if(!empty($roleid)){if(yunyecms_getarrayvalue($powersarr,'shop','ordersdel')==1) echo "checked";}?>>&nbsp; &nbsp;订单删除
                         </div>
                     </div>
              </div>
            <!-- /.box-body -->
          </div>          
          
                     </div>
                   <div class="col-sm-offset-1 col-sm-10">
                     <div class="row">
                     <div class="col-sm-2 pb5">  <input type="checkbox" class="minimal-blue" name="selectall"  id="selectall"  value="1" >&nbsp; &nbsp;选中全部
                         </div>
                     </div>
                   </div>
                     
                </div>
                
              </div>
              <!-- /.box-body -->
              <div class="box-footer">
              <div class="form-group">
                  <div class="col-sm-offset-1 col-sm-10">
                     <input name="yyact" type="hidden" value="<?php echo $yyact;?>">
                     <input name="c" type="hidden" value="<?php echo ROUTE_C;?>">
                     <input name="a" type="hidden" value="<?php echo ROUTE_A;?>">
					 <?php echo $this->hashurl['svp'];?>
                     <?php if(!empty($roleid)):?>
                     <input name="id" type="hidden" value="<?php echo $roleid;?>">
                     <input name="oldrolename" type="hidden" id="oldrolename" value="<?php echo $crole['rolename'];?>"> 
                     <?php  endif?>
                     <button type="submit" class="btn btn-primary"><?php  if($yyact=="add"):?> <i class="icon fa fa-plus"></i> 添 加 <?php elseif($yyact=="edit"):?> <i class="icon fa fa-edit"></i> 修 改 <?php  endif?></button> &nbsp; &nbsp;
                     <button type="reset" class="btn btn-default">  重 置 </button>
                  </div>
              </div>
              </div>
              <!-- /.box-footer -->
            </form>
          </div>
              <div class="alert alert-info alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <h4><i class="icon fa fa-info"></i> 注：</h4>
                1.权限组：超级管理员拥有所有权限，开发人员权限拥有模板和模型板块的权限。<br/>
              </div>
              <div class="example-modal">
        <div class="modal modal-info" id="alertmodal">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">提示信息：</h4>
              </div>
              <div class="modal-body">
                <p></p>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">取消</button>
                <button type="button" class="btn btn-outline" onClick="javsacript:CloseModal('infoselfmodal','#alertmodal');">确认</button>
              </div>
            </div>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->
      </div>
              
              
        </div>
        <!--/.col (left) -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
<?php require tpl_adm('foot');?>
<!-- jQuery 2.2.3 -->
<script src="<?php echo YUNYECMS_UI;?>plugins/jQuery/jquery-2.2.3.min.js"></script>
<!-- Bootstrap 3.3.6 -->
<script src="<?php echo YUNYECMS_UI;?>bootstrap/js/bootstrap.min.js"></script>
<!-- FastClick -->
<!-- AdminLTE for demo purposes -->
<script src="<?php echo YUNYECMS_UI;?>dist/js/admin.js"></script>
<script src="<?php echo YUNYECMS_UI;?>plugins/iCheck/icheck.min.js"></script>
<script type="text/javascript" src="<?php echo YUNYECMS_UI;?>validator/dist/js/bootstrapValidator.js"></script>
<script>
  $(function () {
    //iCheck for checkbox and radio inputs
    $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
      checkboxClass: 'icheckbox_minimal-blue',
      radioClass: 'iradio_minimal-blue'
    });
    //Red color scheme for iCheck
    $('input[type="checkbox"].minimal-red, input[type="radio"].minimal-red').iCheck({
      checkboxClass: 'icheckbox_minimal-red',
      radioClass: 'iradio_minimal-red'
    });
	$('input[type="checkbox"].minimal-blue, input[type="radio"].minimal-blue').iCheck({
      checkboxClass: 'icheckbox_minimal-blue',
      radioClass: 'iradio_minimal-blue'
    });
    //Flat red color scheme for iCheck
      $('input[type="checkbox"].flat-blue, input[type="radio"].flat-blue').iCheck({
      checkboxClass: 'icheckbox_flat-blue',
      radioClass: 'iradio_flat-blue'
    });
  });
</script>
<script type="text/javascript">
$(document).ready(function() {
    $('#defaultForm').bootstrapValidator({
        message: 'This value is not valid',
//        live: 'disabled',
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            rolename: {
                message: '权限名是无效的',
                validators: {
                    notEmpty: {
                        message: '权限名必需填写'
                    },
                   /* remote: {
                        url: 'user_add.php',
                        message: '权限名是不可用的'
                    },*/
                }
            },
        }
    });
});

$(document).ready(function(){
//使用ON绑定事件
    var checkAll = $('#selectall');
	var checkboxes = $('#powerlist input');
	/*checkboxes.each(function(){
         if($(this).attr("name")=="powers[info][self']"){
		   $(this).remove();
		   }
		   if($(this).attr("name")=="powers[info][needverify']"){
		   $(this).remove();
		   }
		   if($(this).attr("name")=="powers[info][auditednotedit']"){
		   $(this).remove();
		   }
    });  */
	checkAll.on('ifChecked ifUnchecked', function(event) {
		if (event.type == 'ifChecked') {
			checkboxes.iCheck('check');
			$("#infoself").iCheck('uncheck');
			$("#infoneedverify").iCheck('uncheck');
			$("#infoauditednotedit").iCheck('uncheck');
		} else {
			checkboxes.iCheck('uncheck');
		}
	});
	
	/*$("#infoall").on('ifChecked', function(event) {
		if (event.type == 'ifChecked') {
			if($("#infoall").prop('checked')&&$("#infoself").prop('checked')){
				ModalPrompt('infoselfmodal','信息提示','不能同时选中'，'#alertmodal')
				}
		}
	});	*/
	
/*	$("#infoself").on('ifChecked', function(event) {
		if (event.type == 'ifChecked') {
			if($("#infoall").prop('checked')&&$("#infoself").prop('checked')){
				this.iCheck('uncheck');
				}
		}
	});		
	*/
	
	/*checkboxes.on('ifChanged', function(event){
		if(checkboxes.filter(':checked').length == checkboxes.length) {
			checkAll.prop('checked', 'checked');
		} else {
			checkAll.removeProp('checked');
		}
		checkAll.iCheck('update');
	});*/
	
});


</script>

<script language="javascript" type="text/javascript">
        $(function() {
			var navudinfo="<?php echo $parnav;?>";
			$('.breadcrumb',window.parent.document).children('#homeitem').nextAll().remove();
			$('.breadcrumb',window.parent.document).children('#homeitem').after(navudinfo);
        });
</script>

</body>
</html>