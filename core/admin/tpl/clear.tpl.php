<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title> 云业内容管理系统-YUNYECMS <?php echo YUNYECMS_VERSION;?></title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="<?php echo YUNYECMS_UI;?>bootstrap/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link href="<?php echo YUNYECMS_PUBLIC;?>plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" />
  <link rel="stylesheet" href="<?php echo YUNYECMS_UI;?>plugins/font-awesome-4.7.0/css/font-awesome.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo YUNYECMS_UI;?>dist/css/AdminLTE.min.css">
  <link rel="stylesheet" href="<?php echo YUNYECMS_UI;?>plugins/jvectormap/jquery-jvectormap-1.2.2.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="<?php echo YUNYECMS_UI;?>dist/css/skins/_all-skins.min.css">
  <link rel="stylesheet" href="<?php echo YUNYECMS_UI;?>dist/css/admin.css">
<link rel="stylesheet" href="<?php echo YUNYECMS_UI;?>validator/dist/css/bootstrapValidator.css"/>
  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
</head>
<body class="hold-transition rightbgcolor"  onload="changefrmHeight()">
  <!-- Content Wrapper. Contains page content -->
  <div class="container-fluid" id="mainwrap">
    <!-- Main content -->
    <section class="content">
      <!-- Info boxes -->
      <div class="row">
        <div class="col-md-12">
        	<div class="box box-info">
            <div class="box-header with-border">
              <h3 class="box-title">清理缓存</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
			    <?php echo $stroutput;?>
				
              <div  class="clearfix"> </div>              
              <!-- /.table-responsive -->
            </div>
            <!-- /.box-body -->
        
            <!-- /.box-footer -->
          </div>
        </div>       
        
        <!-- /.col -->
        
        <!-- /.col -->

        <!-- fix for small devices only -->
        <div class="clearfix visible-sm-block"></div>

        
        <!-- /.col -->
        
        <!-- /.col -->
      </div>
      <!-- /.row -->
       
      
      <!-- Main row -->
      
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
<?php require tpl_adm('foot');?>
<!-- jQuery 2.2.3 -->
<script src="<?php echo YUNYECMS_UI;?>plugins/jQuery/jquery-2.2.3.min.js"></script>
<!-- Bootstrap 3.3.6 -->
<script src="<?php echo YUNYECMS_UI;?>bootstrap/js/bootstrap.min.js"></script>
<!-- FastClick -->
<script src="<?php echo YUNYECMS_UI;?>plugins/fastclick/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="<?php echo YUNYECMS_UI;?>dist/js/app.min.js"></script>
<!-- Sparkline -->
<script src="<?php echo YUNYECMS_UI;?>plugins/sparkline/jquery.sparkline.min.js"></script>
<!-- jvectormap -->
<script src="<?php echo YUNYECMS_UI;?>plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
<script src="<?php echo YUNYECMS_UI;?>plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
<!-- SlimScroll 1.3.0 -->
<script src="<?php echo YUNYECMS_UI;?>plugins/slimScroll/jquery.slimscroll.min.js"></script>
<!-- ChartJS 1.0.1 -->
<script src="<?php echo YUNYECMS_UI;?>plugins/chartjs/Chart.min.js"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script src="<?php echo YUNYECMS_UI;?>dist/js/pages/dashboard2.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="<?php echo YUNYECMS_UI;?>dist/js/demo.js"></script>
<script language="javascript" type="text/javascript">
        $(function() {
			var navudinfo="<?php echo $parnav;?>";
			$('.breadcrumb',window.parent.document).children('#homeitem').nextAll().remove();
			$('.breadcrumb',window.parent.document).children('#homeitem').after(navudinfo);
        });
</script>
</body>
</html>