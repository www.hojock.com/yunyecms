<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>云业CMS管理员登录</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="<?php echo YUNYECMS_UI;?>bootstrap/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?php echo YUNYECMS_UI;?>plugins/font-awesome-4.7.0/css/font-awesome.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo YUNYECMS_UI;?>dist/css/AdminLTE.min.css">
  <!-- iCheck -->
  <link rel="stylesheet" href="<?php echo YUNYECMS_UI;?>plugins/iCheck/square/blue.css">
<link rel="stylesheet" href="<?php echo YUNYECMS_UI;?>validator/dist/css/bootstrapValidator.css"/>
   <link rel="stylesheet" href="<?php echo YUNYECMS_UI;?>dist/css/login.css">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
</head>
<body class="login">
<div class="login-box box box-login box-solid" style="background: none; border: none;">
<div class="box-header with-border" style="background: none;">
  <div class="login-logo">
   <a href="http://www.yunyecms.com" target="_blank" title="云业内容管理系统"><img src="<?php echo YUNYECMS_UI;?>dist/img/logo_login.png" alt="云业内容管理系统" width="80%;"></a>
  </div>
 </div>
  <!-- /.login-logo -->
  <div class="login-box-body ">
    <p class="login-box-msg">管理员登录</p>
  <form action="<?php echo YUNYECMS_URLADM;?>" id="defaultForm" method="post">
  <div class="form-group">
    <div class="input-group">
    <span class="input-group-addon"><i class="fa fa-user"></i></span>
    <input type="text" id="uname" name="uname" class="form-control"   placeholder="用户名" >
    </div>
  </div>
  <div class="form-group">
   <div class="input-group">
    <span class="input-group-addon"><i class="fa fa-lock"></i></span>
    <input type="password" name="pwd" id="pwd" class="form-control"  placeholder="密码">
    </div>
  </div>
  <div class="row">
    <div class="col-xs-7">
        <div class="form-group has-feedback">
            <div class="input-group">
            <span class="input-group-addon"><i class="fa fa-qrcode"></i></span>
                 <input type="text" class="form-control " name="captcha" placeholder="验证码">
            </div>
        </div>
    </div>
    <div class="col-xs-5" style="text-align:left; padding-left:0px;"><img width="130" src="<?php echo ROOT;?>core/extend/captcha.php" id="code_img" alt="点击刷新" onclick="javascript:this.src='<?php echo ROOT;?>core/extend/captcha.php?rnd='+Math.random();void(0);" style="cursor:pointer;">
    </div>
    </div>
     <div class="col-xs-8">
     </div>
      <div class="row">
        <!-- /.col -->
        <div class="col-xs-12">
          <input name="yyact" type="hidden" value="<?php echo $yyact;?>">
          <button type="submit" class="btn  bg-green btn-block btn-lg btn-flat " style="font-size: 18px;">登 录</button>
        </div>
        <!-- /.col -->
      </div>
    </form>
    <!-- /.social-auth-links -->
  </div>
  <div class="box-footer  login-footer" style="background-color: #6c7a8d; padding-top: 20px; padding-bottom: 20px;">
         Copyright <i class="fa fa-copyright"></i> 2019   <a href="http://www.yunyecms.com/" target="_blank">yunyecms.com 云业内容管理系统 </a>  版权所有
  </div>
  <!-- /.login-box-body -->
</div>
<!-- /.login-box -->
<!-- jQuery 2.2.3 -->
<script src="<?php echo YUNYECMS_UI;?>plugins/jQuery/jquery-2.2.3.min.js"></script>
<!-- Bootstrap 3.3.6 -->
<script src="<?php echo YUNYECMS_UI;?>bootstrap/js/bootstrap.min.js"></script>
<!-- iCheck -->
<script src="<?php echo YUNYECMS_UI;?>plugins/iCheck/icheck.min.js"></script>
<script type="text/javascript" src="<?php echo YUNYECMS_UI;?>validator/dist/js/bootstrapValidator.js"></script>
<script>
  $(function () {
    $('input').iCheck({
      checkboxClass: 'icheckbox_square-blue',
      radioClass: 'iradio_square-blue',
      increaseArea: '20%' // optional
    });
  });
</script>
<script type="text/javascript">
$(document).ready(function() {
    $('#defaultForm').bootstrapValidator({
        message: 'This value is not valid',
//        live: 'disabled',
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
			    uname: {
                message: '用户名是无效的',
                validators: {
                    notEmpty: {
                        message: '用户名必需填写'
                    },
                    stringLength: {
                        min: 5,
                        max: 30,
                        message: '用户名至少是5位，且小于30个字符'
                    },
                    different: {
                        field: 'pwd',
                        message: '用户名和密码不能相同'
                    }
                }
            },
            pwd: {
                validators: {
                    notEmpty: {
                        message: '密码必须填写'
                    },
                    different: {
                        field: 'uname',
                        message: '用户名和密码不能相同'
                    }
                }
            },
			 captcha: {
                validators: {
                    notEmpty: {
                        message: '验证码必须填写'
                    },
                }
            },


        }
    });
});
</script>
</body>
</html>
