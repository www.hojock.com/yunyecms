<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>YUNYECMS <?php echo YUNYECMS_VERSION;?></title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="<?php echo YUNYECMS_UI;?>bootstrap/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?php echo YUNYECMS_UI;?>plugins/font-awesome-4.7.0/css/font-awesome.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo YUNYECMS_UI;?>plugins/iCheck/all.css">
  <link rel="stylesheet" href="<?php echo YUNYECMS_UI;?>dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="<?php echo YUNYECMS_UI;?>dist/css/skins/_all-skins.min.css">
  <link rel="stylesheet" href="<?php echo YUNYECMS_UI;?>dist/css/admin.css">
<link rel="stylesheet" href="<?php echo YUNYECMS_UI;?>validator/dist/css/bootstrapValidator.css"/>
<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
</head>
<body class="hold-transition rightbgcolor"  onload="changefrmHeight()">
  <!-- Content Wrapper. Contains page content -->
  <div class="container-fluid" id="mainwrap">
    <!-- Main content -->
    <section class="content">
      <div class="row">
        <!-- left column -->
        <div class="col-md-12">
          <div class="box box-info">
            <div class="box-header with-border">
               <h3 class="box-title"><?php if($yyact=="add"):?> 添加字段 <?php elseif($yyact=="edit"):?> 修改字段 <?php  endif?></h3>
            </div>
            <!-- /.box-header -->
<form class="form-horizontal form" id="defaultForm"  method="post"  action="<?php echo YUNYECMS_URLADM;?>">
    <fieldset>
      <div class="box-body">
           <h3 class="form-section">基本设置</h3>
           <div class="form-group">
            <label class="col-md-2 control-label" for="title">所属数据表</label>
            <div class="col-md-6">
               <input type="text" name="tablename" class="form-control" disabled id="tablename" placeholder="数据表名，必须是英文或数字，表名提交后将不能修改" value=" <?php echo($dbpre."m_".$curmodel["tablename"]);?>">       
            </div>
        </div>   
         <div class="form-group">
            <label class="col-md-2 control-label" for="title">字段名</label>
            <div class="col-md-6">
                <input type="text" class="form-control " id="fdname" name="fdname" placeholder="由英文与数字组成，且不能以数字开头。比如：&#34;title&#34;"  <?php if(!empty($id)):?>  value="<?php echo $rs["fdname"];?>" <?php endif; ?>  />   
            </div>
        </div>  
        <div class="form-group">
            <label class="col-md-2 control-label" for="title">字段标识</label>
            <div class="col-md-6">
                <input type="text" class="form-control " id="fdtitle" name="fdtitle" placeholder="比如：&#34;标题&#34;"   <?php if(!empty($id)):?>  value="<?php echo $rs["fdtitle"];?>" <?php endif; ?>  />
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-2 control-label" for="title">字段标识(English)</label>
            <div class="col-md-6">
                <input type="text" class="form-control " id="fdtitle_en" name="fdtitle_en" placeholder="比如：&#34;标题&#34;(English)"   <?php if(!empty($id)):?>  value="<?php echo $rs["fdtitle_en"];?>" <?php endif; ?>  />
            </div>
        </div>
        <?php  if(empty($rs)): ?>
     	<div class="form-group">
				<label class="col-md-2 control-label" for="title">类型</label>
				<div class="col-md-6">
					<select name="fdtype" id="select" class="form-control " >
					  <option value="VARCHAR" <?php if(!empty($id)) { if($rs['fdtype']=="VARCHAR") echo "selected";} ?> >字符型0-65000字节(VARCHAR)</option>
					  <option value="CHAR" <?php if(!empty($id)) { if($rs['fdtype']=="CHAR") echo "selected";}?>>定长字符型0-255字节(CHAR)</option>
					  <option value="TEXT" <?php if(!empty($id)) { if($rs['fdtype']=="TEXT") echo "selected";}?>>小型字符型(TEXT)</option>
					  <option value="MEDIUMTEXT" <?php if(!empty($id)) { if($rs['fdtype']=="MEDIUMTEXT") echo "selected";}?> >中型字符型(MEDIUMTEXT)</option>
					  <option value="LONGTEXT" <?php if(!empty($id)) { if($rs['fdtype']=="LONGTEXT") echo "selected";}?>>大型字符型(LONGTEXT)</option>
					  <option value="TINYINT" <?php if(!empty($id)) { if($rs['fdtype']=="TINYINT") echo "selected";}?>>小数值型(TINYINT)</option>
					  <option value="SMALLINT" <?php if(!empty($id)) { if($rs['fdtype']=="SMALLINT") echo "selected";}?>>中数值型(SMALLINT)</option>
					  <option value="INT" <?php if(!empty($id)) { if($rs['fdtype']=="INT") echo "selected";}?>>大数值型(INT)</option>
					  <option value="BIGINT" <?php if(!empty($id)) { if($rs['fdtype']=="BIGINT") echo "selected";}?>>超大数值型(BIGINT)</option>
					  <option value="FLOAT" <?php if(!empty($id)) { if($rs['fdtype']=="FLOAT") echo "selected";}?>>数值浮点型(FLOAT)</option>
					  <option value="DOUBLE" <?php if(!empty($id)) { if($rs['fdtype']=="DOUBLE") echo "selected";}?>>数值双精度型(DOUBLE)</option>
					  <option value="DATE" <?php if(!empty($id)) { if($rs['fdtype']=="DATE") echo "selected";}?>>日期型(DATE)</option>
					  <option value="DATETIME" <?php if(!empty($id)) { if($rs['fdtype']=="DATETIME") echo "selected";}?>>日期时间型(DATETIME)</option>
					</select>
				</div>
		 </div>
         <?php else :?>
         <input type="hidden" name="fdtype" value="<?php echo $rs["fdtype"];?>">
         	<div class="form-group">
				<label class="col-md-2 control-label" for="title">类型</label>
				<div class="col-md-6"><div style="padding-top: 7px;"><?php echo $rs["fdtype"];?> </div></div>
		 </div>
		<?php endif;?>
         <div class="form-group">
            <label class="col-md-2 control-label" for="title">字段长度</label>
            <div class="col-md-6">
                <input type="text" class="form-control " id="fdlen" name="fdlen" placeholder="字段长度"  <?php if(!empty($id)):?>  value="<?php echo $rs["fdlen"];?>" <?php endif; ?>  />
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-2 control-label" for="title">字段默认值</label>
            <div class="col-md-6">
                <input type="text" class="form-control " id="defaultvalue" name="defaultvalue" placeholder="字段默认值"   <?php if(!empty($id)):?>  value="<?php echo $rs["defaultvalue"];?>"  <?php endif; ?>  />
            </div>
        </div>
          <div class="form-group">
				<label class="col-md-2 control-label" for="title">语言版</label>
				<div class="col-md-6">
			      <input type="radio" name="language" value="1" class="minimal-blue" <?php if(empty($id)){ echo "checked";}else{ if($rs["language"]==1) echo  "checked"; } ?> >  中文    &nbsp;
				   <input type="radio" name="language" value="2" class="minimal-blue" <?php if(!empty($id)) { if($rs["language"]==2) echo  "checked"; } ?> >  English 
				</div>
		 </div> 
	<h3 class="form-section">表单显示设置</h3>
        	<div class="form-group">
				<label class="col-md-2 control-label" for="title">输入表单显示元素</label>
				<div class="col-md-6">
					 <select name="formctrl" id="formctrl" class="form-control ">
					  <option value="text"  <?php if(!empty($id)) { if($rs['formctrl']=="text") echo "selected";} ?> >单行文本框(text)</option>
					  <option value="password" <?php if(!empty($id)) { if($rs['formctrl']=="password") echo "selected";} ?> >密码框(password)</option>
					  <option value="select" <?php if(!empty($id)) { if($rs['formctrl']=="select") echo "selected";} ?> >下拉框(select)</option>
					  <option value="radio" <?php if(!empty($id)) { if($rs['formctrl']=="radio") echo "selected";} ?> >单选框(radio)</option>
					  <option value="checkbox" <?php if(!empty($id)) { if($rs['formctrl']=="checkbox") echo "selected";} ?> >复选框(checkbox)</option>
					  <option value="textarea" <?php if(!empty($id)) { if($rs['formctrl']=="textarea") echo "selected";} ?> >多行文本框(textarea)</option>
					  <option value="editor" <?php if(!empty($id)) { if($rs['formctrl']=="editor") echo "selected";} ?> >编辑器(editor)</option>
					  <option value="img" <?php if(!empty($id)) { if($rs['formctrl']=="img") echo "selected";} ?> >图片(img)</option>
					  <option value="flash" <?php if(!empty($id)) { if($rs['formctrl']=="flash") echo "selected";} ?> >FLASH文件(flash)</option>
					  <option value="file" <?php if(!empty($id)) { if($rs['formctrl']=="file") echo "selected";} ?> >文件(file)</option>
					  <option value="date" <?php if(!empty($id)) { if($rs['formctrl']=="date") echo "selected";} ?> >日期(date)</option>
					  <option value="color" <?php if(!empty($id)) { if($rs['formctrl']=="color") echo "selected";} ?> >颜色(color)</option>
					  <option value="morevaluefield" <?php if(!empty($id)) { if($rs['formctrl']=="morevaluefield") echo "selected";} ?> >多值字段(morevaluefield)</option>
					</select>
				</div>
		 </div>	 
       <div class="form-group">
            <label class="col-md-2 control-label" for="title">元素长度</label>
            <div class="col-md-6">
                <input type="text" class="form-control " id="formsize" name="formsize" placeholder="元素长度，空为默认" <?php if(!empty($id)):?>  value="<?php echo $rs["formsize"];?>"  <?php endif; ?>  />
            </div>
        </div>  
          <div class="form-group">
            <label class="col-md-2 control-label" for="title">元素尺寸宽度</label>
            <div class="col-md-6">
              <input type="text" class="form-control " id="formwidth" name="formwidth" placeholder="元素尺寸宽度，空为默认"  <?php if(!empty($id)):?>  value="<?php echo $rs["formwidth"];?>"  <?php endif; ?>   />
            </div>
        </div>   
         <div class="form-group">
            <label class="col-md-2 control-label" for="title">元素尺寸高度</label>
            <div class="col-md-6">
            <input type="text" class="form-control " id="formheight" name="formheight" placeholder="元素尺寸宽度，空为默认"  <?php if(!empty($id)):?>  value="<?php echo $rs["formheight"];?>"  <?php endif; ?>  />
            </div>
        </div>   
       <div class="form-group">
            <label class="col-md-2 control-label" for="title">初始值</label>
            <div class="col-md-6">
                  <textarea name="formvalue" id="formvalue" placeholder="(多个值用'回车'格开；“下拉/单选/复选”格式用：值==名称；
当值等于名称时，名称可省略；
默认选项后面加：:default)
"  class="form-control" rows="5"><?php if(!empty($id)){ echo $rs["formvalue"]; }?></textarea>
            </div>
        </div>        		   
    <h3 class="form-section">特殊属性</h3>
          
     <div class="form-group">
				<label class="col-md-2 control-label" for="title">允许录入</label>
				<div class="col-md-6">
			      <input type="radio" name="isadd" value="1" class="minimal-blue" <?php if(empty($id)){ echo "checked";}else{ if($rs["isadd"]==1) echo  "checked"; } ?> >  是    &nbsp;
				   <input type="radio" name="isadd" value="0" class="minimal-blue" <?php if(!empty($id)) { if($rs["isadd"]==0) echo  "checked"; } ?> >  否 
				</div>
		 </div>           
           <div class="form-group">
				<label class="col-md-2 control-label" for="title">允许会员提交信息（投稿）</label>
				<div class="col-md-6">
				   <input type="radio" name="istougao" value="1" class="minimal-blue" <?php if(!empty($id)) { if($rs['istougao']==1) echo "checked";} ?> >  是 &nbsp;
				   <input type="radio" name="istougao" value="0" class="minimal-blue" <?php if(empty($id)){ echo "checked";}else{ if($rs["istougao"]==0) echo  "checked"; } ?> >  否 
				</div>
		 </div>
         <div class="form-group">
				<label class="col-md-2 control-label" for="title">是否必填</label>
				<div class="col-md-6">
			     <input type="radio" name="isrequired" value="1" class="minimal-blue"  <?php if(empty($id)){ echo "checked";}else{ if($rs["isrequired"]==1) echo  "checked"; } ?> >  是   
				 <input type="radio" name="isrequired" value="0" class="minimal-blue"  <?php if(!empty($id)) { if($rs['isrequired']==0) echo "checked";} ?> >      否 &nbsp;
				</div>
		 </div>
         <div class="form-group">
				<label class="col-md-2 control-label" for="title">在列表中显示</label>
				<div class="col-md-6">
			     <input type="radio" name="isdisplay" value="1" class="minimal-blue"  <?php if(empty($id)){ echo "checked";}else{ if($rs["isdisplay"]==1) echo  "checked"; } ?> >  是   
				 <input type="radio" name="isdisplay" value="0" class="minimal-blue"  <?php if(!empty($id)) { if($rs['isdisplay']==0) echo "checked";} ?> >      否 &nbsp;
				</div>
		 </div>
         <div class="form-group">
				<label class="col-md-2 control-label" for="title">是否允许删除</label>
				<div class="col-md-6">
			   	  <input type="radio" name="isallowdel" value="1" class="minimal-blue"  <?php if(empty($id)){ echo "checked";}else{ if($rs["isallowdel"]==1) echo  "checked"; } ?>  >  是   &nbsp;
				  <input type="radio" name="isallowdel" value="0" class="minimal-blue" <?php if(!empty($id)) { if($rs['isallowdel']==0) echo "checked";} ?> >  否 
				</div>
		 </div>
         <div class="form-group">
				<label class="col-md-2 control-label" for="title">加索引</label>
				<div class="col-md-6">
				   <input type="radio" name="isindex" value="1" class="minimal-blue"  <?php if(!empty($id)) { if($rs['isindex']==1) echo "checked";} ?> >  是 &nbsp;
				   <input type="radio" name="isindex" value="0" class="minimal-blue"   <?php if(empty($id)){ echo "checked";}else{ if($rs["isindex"]==0) echo  "checked"; } ?> >  否 
				</div>
		 </div>
         <div class="form-group">
				<label class="col-md-2 control-label" for="title">值唯一</label>
				<div class="col-md-6">
				   <input type="radio" name="isunique" value="1" class="minimal-blue" <?php if(!empty($id)) { if($rs['isunique']==1) echo "checked";} ?> >  是 &nbsp;
				   <input type="radio" name="isunique" value="0" class="minimal-blue"  <?php if(empty($id)){ echo "checked";}else{ if($rs["isunique"]==0) echo  "checked"; } ?>  >  否 
				</div>
		 </div>
         <div class="form-group">
				<label class="col-md-2 control-label" for="title">分页字段</label>
				<div class="col-md-6">
				   <input type="radio" name="ispage" value="1" class="minimal-blue"  <?php if(!empty($id)) { if($rs['ispage']==1) echo "checked";} ?> >  是 &nbsp;
				   <input type="radio" name="ispage" value="0" class="minimal-blue"   <?php if(empty($id)){ echo "checked";}else{ if($rs["ispage"]==0) echo  "checked"; } ?> >  否    &nbsp; &nbsp;<span class="font-blue"> 备注：表只可设置一个分页字段</span>
				</div>
		 </div>      
        <div class="form-group">
            <label class="col-md-2 control-label" for="url">显示顺序</label>
            <div class="col-md-6">
               <?php if(empty($id)):?>
				<if condition='$rs.ordernum eq ""'>
                <input type="text"  class="form-control " id="ordernum" name="ordernum" value="<?php echo $maxorder;?>" />
                <?php else: ?>         
                <input type="text"  class="form-control " id="ordernum" name="ordernum" value="<?php echo $rs["ordernum"];?>" />
                </if>
              <?php endif; ?>                  
            </div>
          </div>    
		</div>
              <!-- /.box-body -->
              <div class="box-footer">
              <div class="form-group">
				   <div class="row">
					<div class="col-md-offset-2 col-md-9">
					<input type="hidden" name="modelid" value="<?php echo $modelid?>">
					 <input name="yyact" type="hidden" value="<?php echo $yyact;?>">
                     <input name="c" type="hidden" value="<?php echo ROUTE_C;?>">
                     <input name="a" type="hidden" value="<?php echo ROUTE_A;?>">
                     <?php echo $this->hashurl['svp'];?>
                     <?php if(!empty($rs)):?>
                     <input name="id" type="hidden" value="<?php echo $rs['id'];?>">
                     <input name="oldfdname" type="hidden" id="oldfdname" value="<?php echo $rs['fdname'];?>"> 
                     <?php  endif?>
                     <button type="submit" class="btn btn-primary"><?php  if($yyact=="add"):?> <i class="icon fa fa-plus"></i> 添 加 <?php elseif($yyact=="edit"):?> <i class="icon fa fa-edit"></i> 修 改 <?php  endif?></button> &nbsp; &nbsp;
					 <button type="reset" class="btn default"> <i class="fa fa-undo"></i> 取消</button>
					</div>
				</div>
			</div> 
		 </div>
    </fieldset>
</form>
            
          </div>
              <div class="alert alert-info alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <h4><i class="icon fa fa-info"></i> 注：</h4>
                1.数据表名：数据表创建后将无法修改<br/>
                2.模型名：必须唯一<br/>
              </div>
        </div>
        <!--/.col (left) -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
<?php require tpl_adm('foot');?>

<!-- jQuery 2.2.3 -->
<script src="<?php echo YUNYECMS_UI;?>plugins/jQuery/jquery-2.2.3.min.js"></script>
<!-- Bootstrap 3.3.6 -->
<script src="<?php echo YUNYECMS_UI;?>bootstrap/js/bootstrap.min.js"></script>
<!-- FastClick -->
<!-- AdminLTE for demo purposes -->
<script src="<?php echo YUNYECMS_UI;?>dist/js/admin.js"></script>
<script src="<?php echo YUNYECMS_UI;?>plugins/iCheck/icheck.min.js"></script>

<script type="text/javascript" src="<?php echo YUNYECMS_UI;?>validator/dist/js/bootstrapValidator.js"></script>
<script>
  $(function () {
    //iCheck for checkbox and radio inputs
    $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
      checkboxClass: 'icheckbox_minimal-blue',
      radioClass: 'iradio_minimal-blue'
    });
    //Red color scheme for iCheck
    $('input[type="checkbox"].minimal-red, input[type="radio"].minimal-red').iCheck({
      checkboxClass: 'icheckbox_minimal-red',
      radioClass: 'iradio_minimal-red'
    });
    //Flat red color scheme for iCheck
    $('input[type="checkbox"].flat-blue, input[type="radio"].flat-blue').iCheck({
      checkboxClass: 'icheckbox_flat-blue',
      radioClass: 'iradio_flat-blue'
    });
  });
</script>
<script type="text/javascript">
$(document).ready(function() {
    $('#defaultForm').bootstrapValidator({
        message: 'This value is not valid',
//        live: 'disabled',
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            fdname: {
                message: '字段名是无效的',
                validators: {
                    notEmpty: {
                        message: '字段名必需填写'
                    },
                }
            },
			   fdtitle: {
                message: '字段标识是无效的',
                validators: {
                    notEmpty: {
                        message: '字段标识必需填写'
                    },
                }
            },
        }
    });
});
</script>
<script language="javascript" type="text/javascript">
        $(function() {
			var navudinfo="<?php echo $parnav;?>";
			$('.breadcrumb',window.parent.document).children('#homeitem').nextAll().remove();
			$('.breadcrumb',window.parent.document).children('#homeitem').after(navudinfo);
        });
</script>
</body>
</html>