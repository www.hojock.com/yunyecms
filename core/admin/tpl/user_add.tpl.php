<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>YUNYECMS <?php echo YUNYECMS_VERSION;?></title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="<?php echo YUNYECMS_UI;?>bootstrap/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?php echo YUNYECMS_UI;?>plugins/font-awesome-4.7.0/css/font-awesome.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo YUNYECMS_UI;?>plugins/iCheck/all.css">
  <link rel="stylesheet" href="<?php echo YUNYECMS_UI;?>dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="<?php echo YUNYECMS_UI;?>dist/css/skins/_all-skins.min.css">
  <link rel="stylesheet" href="<?php echo YUNYECMS_UI;?>dist/css/admin.css">
<link rel="stylesheet" href="<?php echo YUNYECMS_UI;?>validator/dist/css/bootstrapValidator.css"/>
  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
</head>
<body class="hold-transition rightbgcolor"  onload="changefrmHeight()">
  <!-- Content Wrapper. Contains page content -->
  <div class="container-fluid" id="mainwrap">
    <!-- Main content -->
    <section class="content">
      <div class="row">
        <!-- left column -->
        <div class="col-md-12">
          <div class="box box-info">
            <div class="box-header with-border">
               <h3 class="box-title"><?php if($yyact=="add"):?> 增加用户 <?php elseif($yyact=="edit"):?> 修改用户 <?php  endif?></h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form class="form-horizontal" id="defaultForm"  method="post"  action="<?php echo YUNYECMS_URLADM;?>">
              <div class="box-body">
                <div class="form-group">
                  <label for="inputusername" class="col-sm-2 control-label">用户名</label>
                     <div class="col-sm-5">
                      <div class="input-group">
                      <span class="input-group-addon"><i class="fa fa-user-o"></i></span>
                      <input type="text" class="form-control" name="username" id="username"  <?php if(!empty($userid)):?> value="<?php echo $cuser['username'];?>" <?php endif ?> placeholder="请输入用户名" required>
                      </div>
                  </div>
                    <div class="col-sm-5">
                      <span class="help-block"><span class="font_red14"> * </span></span>
                    </div>
                </div>
                <?php if($yyact=="add"):?>
                <div class="form-group">
                  <label for="inputpassword" class="col-sm-2 control-label">密码</label>
                  <div class="col-sm-5">
                  <div class="input-group">
                   <span class="input-group-addon"><i class="fa fa-lock"></i></span>
                   <input type="password" name="password" id="password" class="form-control" placeholder="请输入您的管理密码">
                   </div>
                  </div>
                    <div class="col-sm-5">
                      <span class="help-block"><span class="font_red14"> * </span> 密码设置6位以上，且密码不能包含：$ & * # < > ' " / \ % ; 空格 </span>
                    </div>

                </div>
                <?php endif?>
                <div class="form-group">
                  <label for="inputrealname" class="col-sm-2 control-label">姓名</label>
                  <div class="col-sm-5">
                   <div class="input-group">
                      <span class="input-group-addon"><i class="fa fa-user-o"></i></span>
                      <input type="text" name="realname" id="realname" class="form-control" <?php if(!empty($userid)):?> value="<?php echo $cuser['realname'];?>" <?php endif ?> placeholder="请输入您的姓名">
                      </div>
                  </div>
                </div>                
                <div class="form-group">
                  <label for="inputemail" class="col-sm-2 control-label">邮箱</label>
                  <div class="col-sm-5">
                      <div class="input-group">
                      <span class="input-group-addon"><i class="fa fa-envelope-o"></i></span>
                      <input type="text" class="form-control" name="email" id="email" <?php if(!empty($userid)):?> value="<?php echo $cuser['email'];?>" <?php endif ?> placeholder="请输入邮箱">
                      </div>
                  </div>
                </div>  
                <div class="form-group">
                  <label for="inputrole" class="col-sm-2 control-label">权限组</label>
                  <div class="col-sm-5">
                  <div class="input-group">
                  <span class="input-group-addon"><i class="fa fa-users"></i></span>
                   <select class="form-control" name="roleid">
                   <?php foreach($rolelist as $key=>$var): ?>
                    <option value="<?php echo $var['roleid'];?>" <?php if(!empty($userid)){if($cuser['roleid']==$var['roleid']) echo "selected";}?>><?php echo $var['rolename'];?></option>
                 <?php endforeach; ?>
                   </select>
                      </div>
                  </div>
                </div>  
                <div class="form-group">
                  <label for="inputrole" class="col-sm-2 control-label">部门</label>
                  <div class="col-sm-5">
                  <div class="input-group">
                   <span class="input-group-addon"><i class="fa fa-columns"></i></span>
                   <select class="form-control" name="catid">
                    <option value="0">请选择部门</option>
                 <?php foreach($departmentlist as $key=>$var): ?>
                    <option value="<?php echo $var['departmentid'];?>" <?php if(!empty($userid)){if($cuser['catid']==$var['departmentid']) echo "selected";}?>><?php echo $var['departmentname'];?></option>
                 <?php endforeach; ?>
                  </select>
                      </div>
                  </div>
                </div>                                
                <div class="form-group">
                  <div class="col-sm-offset-2 col-sm-10">
                    <label>
                      <input type="checkbox" class="flat-blue" name="status" value="1" <?php if(!empty($userid)){if($cuser['status']==1) echo "checked";}else{echo "checked";}?>>&nbsp; &nbsp;是否启用账户
                    </label>
                  </div>
                </div>
              </div>
              <!-- /.box-body -->
              <div class="box-footer">
              <div class="form-group">
                  <div class="col-sm-offset-2 col-sm-10">
                     <input name="yyact" type="hidden" value="<?php echo $yyact;?>">
                     <input name="c" type="hidden" value="<?php echo ROUTE_C;?>">
                     <input name="a" type="hidden" value="<?php echo ROUTE_A;?>">
                     <?php echo $this->hashurl['svp'];?>
                     <?php if(!empty($userid)):?>
                     <input name="userid" type="hidden" value="<?php echo $userid;?>">
                     <input name="oldusername" type="hidden" id="oldusername" value="<?php echo $cuser['username'];?>"> 
                     <?php  endif?>
                     <button type="submit" class="btn btn-primary"><?php  if($yyact=="add"):?> <i class="icon fa fa-plus"></i> 添 加 <?php elseif($yyact=="edit"):?> <i class="icon fa fa-edit"></i> 修 改 <?php  endif?></button> &nbsp; &nbsp;
                     <button type="reset" class="btn btn-default"> 重 置 </button>
                  </div>
              </div>
              </div>
              <!-- /.box-footer -->
            </form>
          </div>
              <div class="alert alert-info alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <h4><i class="icon fa fa-info"></i> 注：</h4>
                1.权限组：超级管理员拥有所有权限，开发人员用户拥有模板和模型板块的权限。<br/>
                2.允许管理的栏目：不选择时择默认全部。
              </div>
        </div>
        <!--/.col (left) -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
<?php require tpl_adm('foot');?>

<!-- jQuery 2.2.3 -->
<script src="<?php echo YUNYECMS_UI;?>plugins/jQuery/jquery-2.2.3.min.js"></script>
<!-- Bootstrap 3.3.6 -->
<script src="<?php echo YUNYECMS_UI;?>bootstrap/js/bootstrap.min.js"></script>
<!-- FastClick -->
<!-- AdminLTE for demo purposes -->
<script src="<?php echo YUNYECMS_UI;?>dist/js/admin.js"></script>
<script src="<?php echo YUNYECMS_UI;?>plugins/iCheck/icheck.min.js"></script>

<script language="javascript" type="text/javascript">
        $(function() {
			var navudinfo="<?php echo $parnav;?>";
			$('.breadcrumb',window.parent.document).children('#homeitem').nextAll().remove();
			$('.breadcrumb',window.parent.document).children('#homeitem').after(navudinfo);
        });
</script>


<script type="text/javascript" src="<?php echo YUNYECMS_UI;?>validator/dist/js/bootstrapValidator.js"></script>
<script>
  $(function () {
    //iCheck for checkbox and radio inputs
    $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
      checkboxClass: 'icheckbox_minimal-blue',
      radioClass: 'iradio_minimal-blue'
    });
    //Red color scheme for iCheck
    $('input[type="checkbox"].minimal-red, input[type="radio"].minimal-red').iCheck({
      checkboxClass: 'icheckbox_minimal-red',
      radioClass: 'iradio_minimal-red'
    });
    //Flat red color scheme for iCheck
    $('input[type="checkbox"].flat-blue, input[type="radio"].flat-blue').iCheck({
      checkboxClass: 'icheckbox_flat-blue',
      radioClass: 'iradio_flat-blue'
    });
  });
</script>
<script type="text/javascript">
$(document).ready(function() {
    $('#defaultForm').bootstrapValidator({
        message: 'This value is not valid',
//        live: 'disabled',
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            username: {
                message: '用户名是无效的',
                validators: {
                    notEmpty: {
                        message: '用户名必需填写'
                    },
                   /* remote: {
                        url: 'user_add.php',
                        message: '用户名是不可用的'
                    },*/
                 <?php if(empty($userid)):?>
                    different: {
                        field: 'password',
                        message: '用户名不能和密码一样'
                    }
				 <?php endif?>
                }
            },
            email: {
                validators: {
                    emailAddress: {
                        message: '您输入的不是一个有效的电子邮件地址！'
                    },
                }
            },
          <?php if(empty($userid)):?>
            password: {
                validators: {
                    notEmpty: {
                        message: '密码不能为空'
                    },
                    different: {
                        field: 'username',
                        message: '密码不能和用户名一样'
                    }
                }
            },
          <?php endif?>
        }
    });
});
</script>
</body>
</html>