<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>YUNYECMS <?php echo YUNYECMS_VERSION;?></title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="<?php echo YUNYECMS_UI;?>bootstrap/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?php echo YUNYECMS_UI;?>plugins/font-awesome-4.7.0/css/font-awesome.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo YUNYECMS_UI;?>plugins/iCheck/all.css">
  <link rel="stylesheet" href="<?php echo YUNYECMS_UI;?>dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="<?php echo YUNYECMS_UI;?>dist/css/skins/_all-skins.min.css">
  <link rel="stylesheet" href="<?php echo YUNYECMS_UI;?>dist/css/admin.css">
  <link rel="stylesheet" href="<?php echo YUNYECMS_UI;?>validator/dist/css/bootstrapValidator.css"/>

  <link rel="stylesheet" href="<?php echo YUNYECMS_UI;?>plugins/daterangepicker/daterangepicker.css">
  <!-- bootstrap datepicker -->
  <link rel="stylesheet" href="<?php echo YUNYECMS_UI;?>plugins/datepicker/datepicker3.css">
  <!-- Bootstrap time Picker -->
  <link rel="stylesheet" href="<?php echo YUNYECMS_UI;?>plugins/timepicker/bootstrap-timepicker.min.css">
  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
</head>
<body class="hold-transition rightbgcolor"  onload="changefrmHeight()">
  <!-- Content Wrapper. Contains page content -->
  <div class="container-fluid" id="mainwrap">
    <!-- Main content -->
    <section class="content">
      <div class="row">
        <!-- left column -->
        <div class="col-xs-12">
          <div class="box box-primary">
            <div class="box-header">
             <div class="col-xs-12 col-sm-12 col-md-8 pb10">
              <h3 class="box-title">通知消息列表</h3> &nbsp; 
             </div>
             <div class="col-xs-12 col-sm-12 col-md-4">
              <div class="box-tools">
               <form class="form-search" method="post" action="<?php echo YUNYECMS_URLADM;?>">
               <?php echo $this->hashurl['svp'];?>
               <input type="hidden" name="msgdate" id="msgdate">
                <div class="col-xs-12 col-md-6 pb10">
                 <div class="input-group input-group-sm" style="width:100%; ">
                  <button type="button" class="form-control btn btn-default  btn-sm" id="msgdaterange" name="msgdaterange" style="width:100%; text-align:left;">
                    <span>
                      <i class="fa fa-calendar"></i> 请选择登录时间
                    </span>
                    <i class="fa fa-caret-down"></i>
                  </button>
                </div>
                </div>               
                <div class="col-xs-12 col-md-6">
                  <div class="input-group input-group-sm" >
                   <input type="text" name="searchkey" class="form-control pull-right" value="<?php if(!empty($searchkey)) echo $searchkey;?>" placeholder="请输入关键词">
                  <div class="input-group-btn">
                   <button type="submit" class="btn btn-info"><i class="fa fa-search"></i></button>
                  </div>
                  </div>
                </div>
             </form>
              </div>
       </div>
 </div>
            <!-- /.box-header --> 
    <?php  if(!empty($list)):?>
    <form class="form-horizontal" method="post" id="form_infolist" name="form_infolist">
    <div class="box-body">
    <?php echo $this->hashurl['svp'];?>
	<table class="table table-bordered table-striped ">
        <thead>
        <tr>
            <th  width="30" title="全选">
			<input type="checkbox" title="全选" name="selectall" id="selectall"  class="minimal-blue" >
            </th>
            <th width="30">ID</th>
            <th width="320" >标题</th>
            <th  width="100">时间</th>
            <th  width="60">类型</th>
            <th  width="40">状态</th>
            <th  width="150">操作</th>
        </tr>
        </thead>
        <tbody id="catlist">
        <?php  function contentlist($list){ ?> 
			  <?php foreach($list as $key=>$vo):?>
             <tr>
                <td><input type="checkbox" name="id[]" value="<?php echo $vo["id"];?>"  class="minimal-blue"  >
                  </td>
                <td><?php echo $vo["id"];?></td>
                <td align="left"><?php echo $vo["title"];?></td>
                <td align="left" > <?php echo udate($vo['addtime']);?>   </td>  
                <td align="left" > <?php echo GetMsgType($vo['flag']);?>   </td>  
                <td>
				<?php if($vo['isview']==1):?><span class="label label-success"><i class="fa fa-check"></i>已读</span><?php else:?><span class="label label-danger"> 未读</span><?php endif;?>
				</td>
                <td>
                  <a href="<?php echo url_admin('view','msg',array("id"=>$vo["id"]));?>" class="btn btn-sm bg-green btn-flat"><i class="fa fa-pencil-square-o icon-white"></i> 查看详细</a>
                  <a href="javascript:void(0);" class="btn btn-danger btn-sm btn-flat"  onClick="javsacript:ConfirmDel('infoid','<?php echo url_admin('finaldelete','msg',array("id"=>$vo["id"]));?>','删除确认','您确定要删除通知消息<?php echo $vo["title"];?>吗?','delmodal');"><i class="fa fa-remove"></i> 删除</a>
                </td>
            </tr>
        <?php endforeach; ?>
		 <?php  }?>
        <?php contentlist($list); ?>
        </tbody>
    </table>
            </div>
            <!-- /.box-body -->
            <div class="box-footer clearfix">
			 <input type="checkbox" title="全选" name="selectall_foot" id="selectall_foot" class="minimal-blue" >
              &nbsp; &nbsp;选中全部
				<button type="button" class="btn bg-red btn-flat" id="batchdel" ><i class="fa fa-remove"></i> 批量删除 </button> &nbsp;
				<ul class="pagination pagination-sm no-margin pull-right">
                  <?php echo $page;?>
                </ul>
            </div>      
      </form>
        <div class="modal modal-info" id="delmodal">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">删除确认</h4>
              </div>
              <div class="modal-body">
                <p>您确定要删除该信息吗？</p>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">取消</button>
                <button type="button" class="btn btn-outline" onClick="javsacript:CloseAndJump('infoid');">确认</button>
              </div>
            </div>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->
        <div class="modal modal-warning" id="delallmodal">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">删除确认</h4>
              </div>
              <div class="modal-body">
                <p>您确定要删除该用户吗？</p>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">取消</button>
                <button type="button" class="btn btn-outline" onClick="javsacript:CloseAndSubmit('infoid','delallmodal','form_infolist');">确认</button>
              </div>
            </div>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->
        <div class="modal" id="alertmodal">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">×</span></button>
                <h4 class="modal-title">提示信息</h4>
              </div>
              <div class="modal-body">
                <p>您至少要选定一条记录！</p>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal">确定</button>
              </div>
            </div>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->
		    <?php else: ?>
               <div class="container">
               <div class="callout callout-info">
                <h4><i class="icon fa fa-info"></i> 提示信息：</h4>
                暂时没有任何通知消息信息<br/>
               </div>
               </div>
            <?php endif;?>
          </div>
          <!-- /.box -->
        </div>
        <!--/.col (left) -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
<?php require tpl_adm('foot');?>
<!-- jQuery 2.2.3 -->
<script src="<?php echo YUNYECMS_UI;?>plugins/jQuery/jquery-2.2.3.min.js"></script>
<!-- Bootstrap 3.3.6 -->
<script src="<?php echo YUNYECMS_UI;?>bootstrap/js/bootstrap.min.js"></script>
<!-- FastClick -->
<script src="<?php echo YUNYECMS_UI;?>plugins/fastclick/fastclick.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="<?php echo YUNYECMS_UI;?>dist/js/admin.js"></script>
<script src="<?php echo YUNYECMS_UI;?>dist/js/modal.js"></script>
<script src="<?php echo YUNYECMS_UI;?>plugins/iCheck/icheck.min.js"></script>
<!-- date-range-picker -->
<script src="<?php echo YUNYECMS_UI;?>plugins/moment/moment.min.js"></script>
<script src="<?php echo YUNYECMS_UI;?>plugins/daterangepicker/daterangepicker.js"></script>
<!-- bootstrap datepicker -->
<script src="<?php echo YUNYECMS_UI;?>plugins/datepicker/bootstrap-datepicker.js"></script>
<!-- bootstrap time picker -->
<script src="<?php echo YUNYECMS_UI;?>plugins/timepicker/bootstrap-timepicker.min.js"></script>
<script>

  $(function () {
    //iCheck for checkbox and radio inputs
    $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
      checkboxClass: 'icheckbox_minimal-blue',
      radioClass: 'iradio_minimal-blue'
    });
    //Red color scheme for iCheck
    $('input[type="checkbox"].minimal-red, input[type="radio"].minimal-red').iCheck({
      checkboxClass: 'icheckbox_minimal-red',
      radioClass: 'iradio_minimal-red'
    });
    //Flat red color scheme for iCheck
    $('input[type="checkbox"].flat-blue, input[type="radio"].flat-blue').iCheck({
      checkboxClass: 'icheckbox_flat-blue',
      radioClass: 'iradio_flat-blue'
    });
	$('input[type="checkbox"].minimal-blue, input[type="radio"].minimal-blue').iCheck({
      checkboxClass: 'icheckbox_minimal-blue',
      radioClass: 'iradio_minimal-blue'
    });
  });

/*$(document).ready(function(){
//使用ON绑定事件
    var checkAll = $('#selectall');
	var checkboxes = $('#loginlist input');
	checkAll.on('ifChecked ifUnchecked', function(event) {
		if (event.type == 'ifChecked') {
			checkboxes.iCheck('check');
		} else {
			checkboxes.iCheck('uncheck');
		}
	});
});
  */
</script>
<script type="text/javascript">
			  $(document).ready(function(){
			//使用ON绑定事件
				var checkAll = $('#selectall');
				var checkboxes = $('#catlist input');
				checkAll.on('ifChecked ifUnchecked', function(event) {
					if (event.type == 'ifChecked') {
						checkboxes.iCheck('check');
					} else {
						checkboxes.iCheck('uncheck');
					}
				});
				var checkAll_foot = $('#selectall_foot');
				checkAll_foot.on('ifChecked ifUnchecked', function(event) {
					if (event.type == 'ifChecked') {
						checkboxes.iCheck('check');
					} else {
						checkboxes.iCheck('uncheck');
					}
				});

			});
	
				 
			 $(document).ready(function()
             {
                $('#batchdel').click(function()
                {
					if($("input[name='id[]']").is(':checked')){
					  $("#form_infolist").attr("action","<?php echo url_admin('finaldelete');?>");
	                   ConfirmDel('infoid','','删除确认','您确定要删除所选通知消息吗,删除后该通知消息将不能恢复！','delallmodal');
					}else{
	                  AlertModal('提示信息','您至少应该选择一条记录！');
                      return false;
					}
					//alert($("input[name='cat_id']:checked").val());
                });
				 
			 <?php if(empty($sdatestr)&&empty($edatestr)) :?>
			 start=moment();
			 end=moment();
			 $('#msgdaterange span').html(start.format('YYYY/MM/DD') + ' - ' + end.format('YYYY/MM/DD'));
			 $('#msgdate').val(start.format('YYYY/MM/DD') + ' - ' + end.format('YYYY/MM/DD'));
			 <?php  else:?>
			 $('#msgdaterange span').html('<?php echo $sdatestr;?> - <?php echo $edatestr;?>');
			 $('#msgdate').val('<?php echo $sdatestr;?> - <?php echo $edatestr;?>');
			 <?php  endif; ?> 
				 
             });
	
 $('#msgdaterange').daterangepicker(
        {
          ranges: {
            '今天': [moment(), moment()],
            '昨天': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
            '最近7天': [moment().subtract(6, 'days'), moment()],
            '最近30天': [moment().subtract(29, 'days'), moment()],
            '本月': [moment().startOf('month'), moment().endOf('month')],
            '上个月': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')],
            '近半年': [moment().subtract(182, 'days'), moment()]
          },
          startDate: moment(),
          endDate: moment()
        },
        function (start, end) {
          $('#msgdaterange span').html(start.format('YYYY/MM/DD') + ' - ' + end.format('YYYY/MM/DD'));
          $('#msgdate').val(start.format('YYYY/MM/DD') + ' - ' + end.format('YYYY/MM/DD'));
        }
    );
			</script>


<script language="javascript" type="text/javascript">
        $(function() {
			var navudinfo="<?php echo $parnav;?>";
			$('.breadcrumb',window.parent.document).children('#homeitem').nextAll().remove();
			$('.breadcrumb',window.parent.document).children('#homeitem').after(navudinfo);
        });
</script>

</body>
</html>