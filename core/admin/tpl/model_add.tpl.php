<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>YUNYECMS <?php echo YUNYECMS_VERSION;?></title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="<?php echo YUNYECMS_UI;?>bootstrap/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?php echo YUNYECMS_UI;?>plugins/font-awesome-4.7.0/css/font-awesome.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo YUNYECMS_UI;?>plugins/iCheck/all.css">
  <link rel="stylesheet" href="<?php echo YUNYECMS_UI;?>dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="<?php echo YUNYECMS_UI;?>dist/css/skins/_all-skins.min.css">
  <link rel="stylesheet" href="<?php echo YUNYECMS_UI;?>dist/css/admin.css">
<link rel="stylesheet" href="<?php echo YUNYECMS_UI;?>validator/dist/css/bootstrapValidator.css"/>
<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
</head>
<body class="hold-transition rightbgcolor"  onload="changefrmHeight()">
  <!-- Content Wrapper. Contains page content -->
  <div class="container-fluid" id="mainwrap">
    <!-- Main content -->
    <section class="content">
      <div class="row">
        <!-- left column -->
        <div class="col-md-12">
          <div class="box box-info">
            <div class="box-header with-border">
               <h3 class="box-title"><?php if($yyact=="add"):?> 添加模型 <?php elseif($yyact=="edit"):?> 修改模型 <?php  endif?></h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form class="form-horizontal" id="defaultForm"  method="post"  action="<?php echo YUNYECMS_URLADM;?>">
              <div class="box-body">
                <div class="form-group">
                  <label for="inputmodelname" class="col-sm-2 control-label">模型名</label>
                     <div class="col-sm-5">
                      <div class="input-group">
                      <span class="input-group-addon"><i class="fa fa-cubes"></i></span>
                      <input type="text" class="form-control" name="modelname" id="modelname"  <?php if(!empty($modelid)):?> value="<?php echo $cmodel['modelname'];?>" <?php endif ?> placeholder="请输入模型名，例如：文章、图片..." required>
                      </div>
                  </div>
                    <div class="col-sm-5">
                      <span class="help-block"><span class="font_red14"> * </span></span>
                    </div>
                </div>
                <div class="form-group">
                  <label for="inputmodelname" class="col-sm-2 control-label">数据表名</label>
                     <div class="col-sm-5">
                      <div class="input-group">
                      <span class="input-group-addon"><?php echo $dbpre;?>m_</span>
                      <input type="text" class="form-control" name="tablename" id="tablename"  <?php if(!empty($modelid)):?> value="<?php echo $cmodel['tablename'];?>" disabled <?php endif ?> placeholder="数据表名，必须是英文或数字，表名提交后将不能修改" required>
                      </div>
                  </div>
                    <div class="col-sm-5">
                      <span class="help-block"><span class="font_red14"> * </span></span>
                    </div>
                </div>
			    <div class="form-group">
						<label class="col-md-2 control-label" for="title">类型</label>
						<div class="col-md-5">
						   <input type="radio" name="modeltype" value="1" class="minimal-blue"
							 <?php if(empty($modelid)):?> checked  <?php else: ?> <?php if($cmodel['modeltype']==1):?> checked  <?php endif ?> disabled <?php endif ?> >  信息列表 &nbsp;
							<input type="radio" name="modeltype" value="2" class="minimal-blue" <?php if(!empty($modelid)){ if($cmodel['modeltype']==2) { echo "checked"; }  echo " disabled"; } ?>   >  单页面
							<input type="radio" name="modeltype" value="3" class="minimal-blue" <?php if(!empty($modelid)){ if($cmodel['modeltype']==3) { echo "checked"; }   echo "disabled"; } ?> >  表单    &nbsp; &nbsp;  <span class="text-red">备注：类型一旦确定将不能修改</span>
						</div>
			   </div>         
                 <div class="form-group">
                  <label for="tpl_list_home" class="col-sm-2 control-label">默认模板（封面页）</label>
                     <div class="col-sm-5">
                      <div class="input-group">
                          <input type="text" id="tplhome" name="tplhome"  <?php if(!empty($modelid)):?> value="<?php echo $cmodel['tplhome'];?>" <?php endif ?> class="form-control">
                          <div class="input-group-btn">
                            <button type="button" class="btn btn-info">请选择</button>
                          </div>
                        </div>
                  </div>
                    <div class="col-sm-5">
                      <span class="help-block"><span class="font_red14"> * </span></span>
                    </div>
                </div>
                <div class="form-group">
                  <label for="inputmodelname" class="col-sm-2 control-label">默认模板（列表页）</label>
                     <div class="col-sm-5">
                      <div class="input-group">
                          <input type="text" id="tpllist" name="tpllist" <?php if(!empty($modelid)):?> value="<?php echo $cmodel['tpllist'];?>" <?php endif ?> class="form-control">
                          <div class="input-group-btn">
                            <button type="button" class="btn btn-info">请选择</button>
                          </div>
                        </div>
                  </div>
                    <div class="col-sm-5">
                      <span class="help-block"><span class="font_red14"> * </span></span>
                    </div>
                </div>
                
                <div class="form-group">
                  <label for="inputmodelname" class="col-sm-2 control-label">默认模板（内容页）</label>
                     <div class="col-sm-5">
                      <div class="input-group">
                          <input type="text" id="tplcontent" name="tplcontent" <?php if(!empty($modelid)):?> value="<?php echo $cmodel['tplcontent'];?>" <?php endif ?> class="form-control">
                          <div class="input-group-btn">
                            <button type="button" class="btn btn-info">请选择</button>
                          </div>
                        </div>
                  </div>
                    <div class="col-sm-5">
                      <span class="help-block"><span class="font_red14"> * </span></span>
                    </div>
                </div>
                 <div class="form-group">
                  <label for="inputmodelname" class="col-sm-2 control-label">排序</label>
                     <div class="col-sm-2">
                      <input type="text" class="form-control" name="ordernum" id="ordernum"   value="<?php echo $maxorder;?>" placeholder="排序" required>
                  </div>
                    <div class="col-sm-5">
                      <span class="help-block"><span class="font_red14"> * </span></span>
                    </div>
                </div>
                
              </div>
              <!-- /.box-body -->
              <div class="box-footer">
              <div class="form-group">
                  <div class="col-sm-offset-2 col-sm-10">
                     <input name="yyact" type="hidden" value="<?php echo $yyact;?>">
                     <input name="c" type="hidden" value="<?php echo ROUTE_C;?>">
                     <input name="a" type="hidden" value="<?php echo ROUTE_A;?>">
                     <?php echo $this->hashurl['svp'];?>
                     <?php if(!empty($modelid)):?>
                     <input name="id" type="hidden" value="<?php echo $modelid;?>">
                     <input name="oldmodelname" type="hidden" id="oldmodelname" value="<?php echo $cmodel['modelname'];?>"> 
                     <?php  endif?>
                     <button type="submit" class="btn btn-primary"><?php  if($yyact=="add"):?> <i class="icon fa fa-plus"></i> 添 加 <?php elseif($yyact=="edit"):?> <i class="icon fa fa-edit"></i> 修 改 <?php  endif?></button> &nbsp; &nbsp;
                     <button type="reset" class="btn btn-default"> 重 置 </button>
                  </div>
              </div>
              </div>
              <!-- /.box-footer -->
            </form>
          </div>
              <div class="alert alert-info alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <h4><i class="icon fa fa-info"></i> 注：</h4>
                1.数据表名：数据表创建后将无法修改<br/>
                2.模型名：必须唯一<br/>
              </div>
        </div>
        <!--/.col (left) -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
<?php require tpl_adm('foot');?>

<!-- jQuery 2.2.3 -->
<script src="<?php echo YUNYECMS_UI;?>plugins/jQuery/jquery-2.2.3.min.js"></script>
<!-- Bootstrap 3.3.6 -->
<script src="<?php echo YUNYECMS_UI;?>bootstrap/js/bootstrap.min.js"></script>
<!-- FastClick -->
<!-- AdminLTE for demo purposes -->
<script src="<?php echo YUNYECMS_UI;?>dist/js/admin.js"></script>
<script src="<?php echo YUNYECMS_UI;?>plugins/iCheck/icheck.min.js"></script>

<script type="text/javascript" src="<?php echo YUNYECMS_UI;?>validator/dist/js/bootstrapValidator.js"></script>
<script>
  $(function () {
    //iCheck for checkbox and radio inputs
    $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
      checkboxClass: 'icheckbox_minimal-blue',
      radioClass: 'iradio_minimal-blue'
    });
    //Red color scheme for iCheck
    $('input[type="checkbox"].minimal-red, input[type="radio"].minimal-red').iCheck({
      checkboxClass: 'icheckbox_minimal-red',
      radioClass: 'iradio_minimal-red'
    });
    //Flat red color scheme for iCheck
    $('input[type="checkbox"].flat-blue, input[type="radio"].flat-blue').iCheck({
      checkboxClass: 'icheckbox_flat-blue',
      radioClass: 'iradio_flat-blue'
    });
  });
</script>
<script type="text/javascript">
$(document).ready(function() {
    $('#defaultForm').bootstrapValidator({
        message: 'This value is not valid',
//        live: 'disabled',
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            modelname: {
                message: '模型名是无效的',
                validators: {
                    notEmpty: {
                        message: '模型名必需填写'
                    },
                   /* remote: {
                        url: 'user_add.php',
                        message: '模型名是不可用的'
                    },*/
                }
            },
        }
    });
});
</script>
<script language="javascript" type="text/javascript">
        $(function() {
			var navudinfo="<?php echo $parnav;?>";
			$('.breadcrumb',window.parent.document).children('#homeitem').nextAll().remove();
			$('.breadcrumb',window.parent.document).children('#homeitem').after(navudinfo);
        });
</script>
</body>
</html>