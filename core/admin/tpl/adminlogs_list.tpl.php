<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>YUNYECMS <?php echo YUNYECMS_VERSION;?></title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="<?php echo YUNYECMS_UI;?>bootstrap/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?php echo YUNYECMS_UI;?>plugins/font-awesome-4.7.0/css/font-awesome.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo YUNYECMS_UI;?>plugins/iCheck/all.css">
  <link rel="stylesheet" href="<?php echo YUNYECMS_UI;?>dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="<?php echo YUNYECMS_UI;?>dist/css/skins/_all-skins.min.css">
  <link rel="stylesheet" href="<?php echo YUNYECMS_UI;?>dist/css/admin.css">
<link rel="stylesheet" href="<?php echo YUNYECMS_UI;?>validator/dist/css/bootstrapValidator.css"/>


  <link rel="stylesheet" href="<?php echo YUNYECMS_UI;?>plugins/daterangepicker/daterangepicker.css">
  <!-- bootstrap datepicker -->
  <link rel="stylesheet" href="<?php echo YUNYECMS_UI;?>plugins/datepicker/datepicker3.css">
  <!-- Bootstrap time Picker -->
  <link rel="stylesheet" href="<?php echo YUNYECMS_UI;?>plugins/timepicker/bootstrap-timepicker.min.css">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
</head>
<body class="hold-transition rightbgcolor"  onload="changefrmHeight()">
  <!-- Content Wrapper. Contains page content -->
  <div class="container-fluid" id="mainwrap">
    <!-- Main content -->
    <section class="content">
      <div class="row">
        <!-- left column -->
        <div class="col-xs-12">
          <div class="box box-primary">
            <div class="box-header">
             <div class="col-xs-12 col-sm-12 col-md-4 pb10">
              <h3 class="box-title">操作日志管理</h3> &nbsp; 
             </div>
             <div class="col-xs-12 col-sm-12 col-md-8">
              <div class="box-tools">
               <form class="form-search" method="post" action="<?php echo YUNYECMS_URLADM;?>">
               <?php echo $this->hashurl['svp'];?>
               <input type="hidden" name="logsdate" id="logsdate">
                <div class="col-xs-12 col-md-6 pb10">
                 <div class="input-group input-group-sm" style="width:100%; ">
                  <button type="button" class="form-control btn btn-default  btn-sm" id="logsdaterange" name="logsdaterange" style="width:100%; text-align:left;">
                    <span>
                      <i class="fa fa-calendar"></i> 请选择登录时间
                    </span>
                    <i class="fa fa-caret-down"></i>
                  </button>

                </div>
                </div>
                <div class="col-xs-12 col-md-6">
                  <div class="input-group input-group-sm" >
                   <input type="text" name="searchkey" class="form-control pull-right" value="<?php if(!empty($searchkey)) echo $searchkey;?>" placeholder="请输入关键词">
                  <div class="input-group-btn">
                   <button type="submit" class="btn btn-info"><i class="fa fa-search"></i></button>
                  </div>
                  </div>
                </div>
             </form>
              </div>
              </div>
            </div>
          <form class="form-search" name="admlogslist" id="admlogslist" method="post" action="<?php echo url_admin('deleteall','adminlogs');?>">
             <?php echo $this->hashurl['svp'];?>
            <!-- /.box-header -->
            <div class="box-body table-responsive no-padding pb10">
            <?php  if(!empty($list)):?>
              <table class="table table-hover table-striped" id="loginlist">
                <tr>
                  <th>ID</th>
                  <th>用户名</th>
                  <th>动作</th>
                  <th>操作详细</th>
                  <th>操作时间</th>
                  <th>登录IP</th>
                  <th>操作</th>
                </tr>
				<?php foreach($list as $key=>$var):?>
                <?php  
				if($var["userid"]){
					$cuserid=$var["userid"];
					$cuser=$this->db->find('select `username` from `#yunyecms_user` where userid='.$cuserid.'');
					$cusername=$cuser['username'];
					}
				?>
                
                  <tr>
                    <td><input name="adminlogsid[]"  class="minimal-blue" type="checkbox" value="<?php echo $var["logid"]; ?>"> <?php echo $var["logid"]; ?>  </td>
                    <td><?php echo $cusername;?></td>
                    <td><?php echo $var["yyact"];?></td>
                    <td><?php echo $var["doing"];?></td>
                    <td><?php echo udate($var["addtime"]);?></td>
                    <td><?php echo $var["logip"].":".$var["ipport"];?></td>
                    <td>
                    <a href="javascript:void(0);" class="btn btn-danger btn-sm"  onClick="javsacript:ConfirmDel('logid','<?php echo url_admin("adminlogs_delete","adminlogs",array('id'=>$var["logid"]),$this->hashurl['usvg']);?>','删除确认','您确定要删除该操作日志<?php echo $var["logid"];?>吗?');"><i class="fa fa-remove"></i> 删除</a>
                    </td>
                  </tr>
                <?php endforeach; ?>
              </table>
            </div>
       <div class="example-modal">
        <div class="modal modal-info" id="delmodal">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">删除确认</h4>
              </div>
              <div class="modal-body">
                <p>您确定要删除该用户吗？</p>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">取消</button>
                <button type="button" class="btn btn-outline" onClick="javsacript:CloseAndJump('logid');">确认</button>
              </div>
            </div>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->
      </div>
      
<div class="example-modal">
        <div class="modal modal-warning" id="delallmodal">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">删除确认</h4>
              </div>
              <div class="modal-body">
                <p>您确定要删除该用户吗？</p>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">取消</button>
                <button type="button" class="btn btn-outline" onClick="javsacript:CloseAndSubmit('logid','delallmodal','admlogslist');">确认</button>
              </div>
            </div>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->
      </div>      
      
      
      
      
            <!-- /.box-body -->
            <div class="box-footer clearfix">
             <input type="checkbox" class="minimal-blue" name="selectall"  id="selectall"  value="1" >&nbsp; &nbsp;选中全部
             <button type="button" class="btn btn-primary"  onClick="javsacript:ConfirmDel('logid','','删除确认','您确定要删除所选操作日志吗?','delallmodal');">批量删除</button>

            
              <ul class="pagination pagination-sm no-margin pull-right">
                  <?php echo $pages;?>
              </ul>
            </div>
            </form>
		    <?php else: ?>
               <div class="container">
               <div class="alert alert-info alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <h4><i class="icon fa fa-info"></i> 提示信息：</h4>
                暂时没有任何登录信息<br/>
               </div>
               </div>
            <?php endif;?>
          </div>
          <!-- /.box -->
        </div>
        <!--/.col (left) -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
<?php require tpl_adm('foot');?>

<!-- jQuery 2.2.3 -->
<script src="<?php echo YUNYECMS_UI;?>plugins/jQuery/jquery-2.2.3.min.js"></script>
<!-- Bootstrap 3.3.6 -->
<script src="<?php echo YUNYECMS_UI;?>bootstrap/js/bootstrap.min.js"></script>
<!-- FastClick -->
<script src="<?php echo YUNYECMS_UI;?>plugins/fastclick/fastclick.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="<?php echo YUNYECMS_UI;?>dist/js/admin.js"></script>
<script src="<?php echo YUNYECMS_UI;?>dist/js/modal.js"></script>
<script src="<?php echo YUNYECMS_UI;?>plugins/iCheck/icheck.min.js"></script>

<!-- date-range-picker -->
<script src="<?php echo YUNYECMS_UI;?>plugins/moment/moment.min.js"></script>
<script src="<?php echo YUNYECMS_UI;?>plugins/daterangepicker/daterangepicker.js"></script>
<!-- bootstrap datepicker -->
<script src="<?php echo YUNYECMS_UI;?>plugins/datepicker/bootstrap-datepicker.js"></script>
<!-- bootstrap time picker -->
<script src="<?php echo YUNYECMS_UI;?>plugins/timepicker/bootstrap-timepicker.min.js"></script>

<script>
  $(function () {
    //iCheck for checkbox and radio inputs
    $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
      checkboxClass: 'icheckbox_minimal-blue',
      radioClass: 'iradio_minimal-blue'
    });
    //Red color scheme for iCheck
    $('input[type="checkbox"].minimal-red, input[type="radio"].minimal-red').iCheck({
      checkboxClass: 'icheckbox_minimal-red',
      radioClass: 'iradio_minimal-red'
    });
    //Flat red color scheme for iCheck
    $('input[type="checkbox"].flat-blue, input[type="radio"].flat-blue').iCheck({
      checkboxClass: 'icheckbox_flat-blue',
      radioClass: 'iradio_flat-blue'
    });
	$('input[type="checkbox"].minimal-blue, input[type="radio"].minimal-blue').iCheck({
      checkboxClass: 'icheckbox_minimal-blue',
      radioClass: 'iradio_minimal-blue'
    });
  });
  
$(document).ready(function(){
//使用ON绑定事件
    var checkAll = $('#selectall');
	var checkboxes = $('#loginlist input');
	checkAll.on('ifChecked ifUnchecked', function(event) {
		if (event.type == 'ifChecked') {
			checkboxes.iCheck('check');
		} else {
			checkboxes.iCheck('uncheck');
		}
	});
	
	
	 <?php if(empty($sdatestr)&&empty($edatestr)) :?>
	 start=moment();
	 end=moment();
	 $('#logsdaterange span').html(start.format('YYYY/MM/DD') + ' - ' + end.format('YYYY/MM/DD'));
	 $('#logsdate').val(start.format('YYYY/MM/DD') + ' - ' + end.format('YYYY/MM/DD'));
	 <?php  else:?>
	 $('#logsdaterange span').html('<?php echo $sdatestr;?> - <?php echo $edatestr;?>');
	 $('#logsdate').val('<?php echo $sdatestr;?> - <?php echo $edatestr;?>');
	 <?php  endif; ?>
});
 $('#logsdaterange').daterangepicker(
        {
          ranges: {
            '今天': [moment(), moment()],
            '昨天': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
            '最近7天': [moment().subtract(6, 'days'), moment()],
            '最近30天': [moment().subtract(29, 'days'), moment()],
            '本月': [moment().startOf('month'), moment().endOf('month')],
            '上个月': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')],
            '近半年': [moment().subtract(182, 'days'), moment()]
          },
          startDate: moment(),
          endDate: moment()
        },
        function (start, end) {
          $('#logsdaterange span').html(start.format('YYYY/MM/DD') + ' - ' + end.format('YYYY/MM/DD'));
          $('#logsdate').val(start.format('YYYY/MM/DD') + ' - ' + end.format('YYYY/MM/DD'));
        }
    );


  
</script>

<script language="javascript" type="text/javascript">
        $(function() {
			var navudinfo="<?php echo $parnav;?>";
			$('.breadcrumb',window.parent.document).children('#homeitem').nextAll().remove();
			$('.breadcrumb',window.parent.document).children('#homeitem').after(navudinfo);
        });
</script>

</body>
</html>