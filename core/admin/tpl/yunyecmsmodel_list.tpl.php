<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>YUNYECMS <?php echo YUNYECMS_VERSION;?></title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="<?php echo YUNYECMS_UI;?>bootstrap/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?php echo YUNYECMS_UI;?>plugins/font-awesome-4.7.0/css/font-awesome.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo YUNYECMS_UI;?>plugins/iCheck/all.css">
  <link rel="stylesheet" href="<?php echo YUNYECMS_UI;?>dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="<?php echo YUNYECMS_UI;?>dist/css/skins/_all-skins.min.css">
  <link rel="stylesheet" href="<?php echo YUNYECMS_UI;?>dist/css/admin.css">
<link rel="stylesheet" href="<?php echo YUNYECMS_UI;?>validator/dist/css/bootstrapValidator.css"/>
  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
</head>
<body class="hold-transition rightbgcolor"  onload="changefrmHeight()">
  <!-- Content Wrapper. Contains page content -->
  <div class="container-fluid" id="mainwrap">
    <!-- Main content -->
    <section class="content">
      <div class="row">
        <!-- left column -->
        <div class="col-xs-12">
          <div class="box box-primary">
            <div class="box-header">
             <div class="col-xs-12 col-sm-12 col-md-8 pb10">
              <h3 class="box-title">模型列表</h3> &nbsp; 
              <a href="<?php echo url_admin("model_add");?>" class="btn btn-primary btn-sm btn-flat"><i class="fa fa-user-plus" aria-hidden="true"> </i> 添加模型</a> &nbsp; 
              <a href="<?php echo url_admin();?>" class="btn btn-success btn-sm  btn-flat"><i class="fa fa-columns" aria-hidden="true"></i> 模型管理</a>
             </div>
             <div class="col-xs-12 col-sm-12 col-md-4">
              <div class="box-tools">
               <form class="form-search" method="post" action="<?php echo YUNYECMS_URLADM;?>">
               <?php echo $this->hashurl['svp'];?>
                <div class="input-group input-group-sm">
                   <input type="text" name="searchkey" class="form-control pull-right" placeholder="请输入模型名称">
                  <div class="input-group-btn">
                   <button type="submit" class="btn btn-info"><i class="fa fa-search"></i></button>
                  </div>
                </div>
             </form>
              </div>
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body table-responsive no-padding pb10">
            <?php  if(!empty($list)):?>
              <table class="table table-hover table-striped">
                <tr>
                  <th>模型ID</th>
                  <th>模型</th>
                  <th>模型类型</th>
                  <th>数据表</th>
                  <th style="width: 100px;">封面页模板</th>
                  <th >列表页模板</th>
                  <th >内容页模板</th>
                  <th>排序</th>
                  <th>操作</th>
                </tr>
             <?php foreach($list as $key=>$var):?>
                <tr>
                  <td><?php echo $var["modelid"]; ?>  </td>
                  <td><?php echo $var["modelname"];?></td>
                  <td><?php echo $var["modeltype"];?></td>
                  <td>
                     <?php echo "{$dbpre}m_{$var['tablename']}";?>
                  </td>
                  <td ><?php if($var["tplhome"]) echo $var['tplhome'].".html"; ?> </td>
				  <td ><?php if($var["tpllist"]) echo $var['tpllist'].".html"; ?></td>
				  <td ><?php if($var["tplcontent"]) echo $var['tplcontent'].".html"; ?></td>
                  <td>
                     <?php echo $var["ordernum"];?>
                  </td>
                  <td>
				<a href="<?php echo url_admin("fieldslist","yunyecmsmodel",array('modelid'=>$var["modelid"]),$this->hashurl['usvg']);?>" class="btn  btn-sm  bg-green btn-flat"><i class="fa fa-th-list"></i> 字段管理</a>
				<a href=" <?php echo url_admin("fieldsadd","yunyecmsmodel",array('modelid'=>$var["modelid"]),$this->hashurl['usvg']);?>" class="btn  btn-sm  bg-gray-dark btn-flat"><i class="fa fa-plus"></i> 添加字段</a>
                 
                  <a href="<?php echo url_admin("model_add","yunyecmsmodel",array('id'=>$var["modelid"]),$this->hashurl['usvg']);?>" class="btn btn-primary btn-sm btn-flat"><i class="fa fa-edit"></i> 编辑</a>
                  <a href="javascript:void(0);" class="btn btn-danger btn-sm btn-flat"  onClick="javsacript:ConfirmDel('modelid','<?php echo url_admin("model_delete","yunyecmsmodel",array('id'=>$var["modelid"]),$this->hashurl['usvg']);?>','重要信息删除确认！','您确定要删除<?php echo $var["modelname"];?>模型吗? 删除后，该模型相关的数据库表和资料将全部丢失，并且不能恢复，请谨慎操作！！！');"><i class="fa fa-remove"></i> 删除</a>
                  </td>
                </tr>
			  <?php endforeach; ?>
              </table>
            </div>
       <div class="example-modal">
        <div class="modal modal-danger" id="delmodal">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">警告！您确定要删除该模型吗</h4>
              </div>
              <div class="modal-body">
                <p>您确定要删除该用户吗？</p>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">取消</button>
                <button type="button" class="btn btn-outline" onClick="javsacript:CloseAndJump('modelid');">确认</button>
              </div>
            </div>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->
      </div>
            <!-- /.box-body -->
            <div class="box-footer clearfix">
              <ul class="pagination pagination-sm no-margin pull-right">
                  <?php echo $pages;?>
              </ul>
            </div>
		    <?php else: ?>
               <div class="container">
               <div class="alert alert-info alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <h4><i class="icon fa fa-info"></i> 提示信息：</h4>
                暂时没有任何模型信息<br/>
               </div>
               </div>
            <?php endif;?>
          </div>
          <!-- /.box -->
        </div>
        <!--/.col (left) -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
<?php require tpl_adm('foot');?>
<!-- jQuery 2.2.3 -->
<script src="<?php echo YUNYECMS_UI;?>plugins/jQuery/jquery-2.2.3.min.js"></script>
<!-- Bootstrap 3.3.6 -->
<script src="<?php echo YUNYECMS_UI;?>bootstrap/js/bootstrap.min.js"></script>
<!-- FastClick -->
<script src="<?php echo YUNYECMS_UI;?>plugins/fastclick/fastclick.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="<?php echo YUNYECMS_UI;?>dist/js/admin.js"></script>
<script src="<?php echo YUNYECMS_UI;?>dist/js/modal.js"></script>
<script src="<?php echo YUNYECMS_UI;?>plugins/iCheck/icheck.min.js"></script>
<script language="javascript" type="text/javascript">
        $(function() {
			var navudinfo="<?php echo $parnav;?>";
			$('.breadcrumb',window.parent.document).children('#homeitem').nextAll().remove();
			$('.breadcrumb',window.parent.document).children('#homeitem').after(navudinfo);
        });
</script>
<script>
  $(function () {
    //iCheck for checkbox and radio inputs
    $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
      checkboxClass: 'icheckbox_minimal-blue',
      radioClass: 'iradio_minimal-blue'
    });
    //Red color scheme for iCheck
    $('input[type="checkbox"].minimal-red, input[type="radio"].minimal-red').iCheck({
      checkboxClass: 'icheckbox_minimal-red',
      radioClass: 'iradio_minimal-red'
    });
    //Flat red color scheme for iCheck
    $('input[type="checkbox"].flat-blue, input[type="radio"].flat-blue').iCheck({
      checkboxClass: 'icheckbox_flat-blue',
      radioClass: 'iradio_flat-blue'
    });
  });
</script>

</body>
</html>