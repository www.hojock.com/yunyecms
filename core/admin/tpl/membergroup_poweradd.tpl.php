<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>YUNYECMS <?php echo YUNYECMS_VERSION;?></title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="<?php echo YUNYECMS_UI;?>bootstrap/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?php echo YUNYECMS_UI;?>plugins/font-awesome-4.7.0/css/font-awesome.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo YUNYECMS_UI;?>plugins/iCheck/all.css">
  <link rel="stylesheet" href="<?php echo YUNYECMS_UI;?>dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="<?php echo YUNYECMS_UI;?>dist/css/skins/_all-skins.min.css">
  <link rel="stylesheet" href="<?php echo YUNYECMS_UI;?>dist/css/admin.css">
<link rel="stylesheet" href="<?php echo YUNYECMS_UI;?>validator/dist/css/bootstrapValidator.css"/>
  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
</head>
<body class="hold-transition rightbgcolor"  onload="changefrmHeight()">
  <!-- Content Wrapper. Contains page content -->
  <div class="container-fluid" id="mainwrap">
    <!-- Main content -->
    <section class="content">
      <div class="row">
        <!-- left column -->
        <div class="col-md-12">
          <div class="box box-primary">
            <div class="box-header with-border">
               <h3 class="box-title"><?php if($yyact=="add"):?> 增加权限 <?php elseif($yyact=="edit"):?> 修改权限 <?php  endif?></h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form class="form-horizontal" id="defaultForm"  method="post"  action="<?php echo YUNYECMS_URLADM;?>">
              <div class="box-body">
                <div class="form-group">
                  <label for="inputgroupname" class="col-sm-1 control-label">权限名</label>
                     <div class="col-sm-5">
                      <div class="input-group">
                      <span class="input-group-addon"><i class="fa fa-columns"></i></span>
                      <input type="text" class="form-control" name="groupname" id="groupname"  <?php if(!empty($roleid)):?> value="<?php echo $crole['groupname'];?>" <?php endif ?> placeholder="请输入权限名" required>
                      </div>
                  </div>
                    <div class="col-sm-5">
                      <span class="help-block"><span class="font_red14"> * </span></span>
                    </div>
                </div>
                <div class="form-group">
                  <label for="inputgroupname" class="col-sm-1 control-label">权限</label>
         <div class="col-sm-11" id="powerlist">
          <div class="box box-info box-solid">
            <div class="box-header with-border">
              <i class="fa fa-info"></i>
              <h3 class="box-title">信息管理</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                     <div class="row">
                       <div class="col-sm-2 pb5">  <input type="checkbox" class="minimal-blue" name="powers[info][view]" value="1" <?php if(!empty($roleid)){if(yunyecms_getarrayvalue($powersarr,'info','view')==1) echo "checked";}?>>&nbsp; &nbsp;信息查看
                           </div>
                         <div class="col-sm-2 pb5">  <input type="checkbox" class="minimal-blue" name="powers[info][add]" value="1" <?php if(!empty($roleid)){if(yunyecms_getarrayvalue($powersarr,'info','add')==1) echo "checked";}?>>&nbsp; &nbsp;增加信息
                           </div>
                           <div class="col-sm-2 pb5">  <input type="checkbox" class="minimal-blue" name="powers[info][edit]" value="1" <?php if(!empty($roleid)){if(yunyecms_getarrayvalue($powersarr,'info','edit')==1) echo "checked";}?>>&nbsp; &nbsp;修改信息
                           </div>
                            <div class="col-sm-2 pb5">  <input type="checkbox" class="minimal-blue" name="powers[info][del]" value="1" <?php if(!empty($roleid)){if(yunyecms_getarrayvalue($powersarr,'info','del')==1) echo "checked";}?>>&nbsp; &nbsp;删除信息
                           </div>
                            <div class="col-sm-2 pb5">  <input type="checkbox" class="minimal-blue" name="powers[info][recycle]" value="1" <?php if(!empty($roleid)){if(yunyecms_getarrayvalue($powersarr,'info','recycle')==1) echo "checked";}?>>&nbsp; &nbsp;回收站（信息）
                           </div>
						    <div class="col-sm-2 pb5">  <input type="checkbox" class="minimal-blue" name="powers[info][verify]" value="1" <?php if(!empty($roleid)){if(yunyecms_getarrayvalue($powersarr,'info','verify')==1) echo "checked";}?>>&nbsp; &nbsp;审核
                           </div>
                     </div>
                        <div class="row">
                           <div class="col-sm-2 pb5">  <input type="checkbox" class="minimal-blue" name="powers[info][movecopy]" value="1" <?php if(!empty($roleid)){if(yunyecms_getarrayvalue($powersarr,'info','movecopy')==1) echo "checked";}?>>&nbsp; &nbsp;移动/复制
                           </div>
                            <div class="col-sm-2 pb5">  <input type="checkbox" class="minimal-blue" name="powers[info][needverify]" value="1" id="infoneedverify" <?php if(!empty($roleid)){if(yunyecms_getarrayvalue($powersarr,'info','needverify')==1) echo "checked";}?>>&nbsp; &nbsp;信息发布后须审核
                           </div>
                            <div class="col-sm-2 pb5">  <input type="checkbox" class="minimal-blue" name="powers[info][auditednotedit]" value="1" id="infoauditednotedit" <?php if(!empty($roleid)){if(yunyecms_getarrayvalue($powersarr,'info','auditednotedit')==1) echo "checked";}?>>&nbsp; &nbsp;信息审核后不可修改
                           </div>
                       </div>
              </div>
            <!-- /.box-body -->
          </div>
          <div class="box box-info box-solid">
            <div class="box-header with-border">
              <i class="fa fa-columns"></i>
              <h3 class="box-title">评论</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
             <div class="row">
               <div class="col-sm-2 pb5">  <input type="checkbox" class="minimal-blue" name="powers[comment][view]" value="1" <?php if(!empty($roleid)){if(yunyecms_getarrayvalue($powersarr,'comment','view')==1) echo "checked";}?>>&nbsp; &nbsp;查看
                     </div>
                    <div class="col-sm-2 pb5">  <input type="checkbox" class="minimal-blue" name="powers[comment][add]" value="1" <?php if(!empty($roleid)){if(yunyecms_getarrayvalue($powersarr,'comment','add')==1) echo "checked";}?>>&nbsp; &nbsp;发布评论
                     </div>
                      <div class="col-sm-2 pb5">  
                      <input type="checkbox" class="minimal-blue" name="powers[comment][edit]" value="1" <?php if(!empty($roleid)){if(yunyecms_getarrayvalue($powersarr,'comment','edit')==1) echo "checked";}?>>&nbsp; &nbsp;修改评论
                     </div>
                       <div class="col-sm-2 pb5">  <input type="checkbox" class="minimal-blue" name="powers[comment][del]" value="1" <?php if(!empty($roleid)){if(yunyecms_getarrayvalue($powersarr,'comment','del')==1) echo "checked";}?>>&nbsp; &nbsp;删除评论
                     </div>
                      <div class="col-sm-2 pb5">  <input type="checkbox" class="minimal-blue" name="powers[comment][recycle]" value="1" <?php if(!empty($roleid)){if(yunyecms_getarrayvalue($powersarr,'comment','recycle')==1) echo "checked";}?>>&nbsp; &nbsp;回收站（评论）
                     </div>
                      <div class="col-sm-2 pb5">  <input type="checkbox" class="minimal-blue" name="powers[comment][istop]" value="1" <?php if(!empty($roleid)){if(yunyecms_getarrayvalue($powersarr,'comment','istop')==1) echo "checked";}?>>&nbsp; &nbsp;推荐/置顶
                     </div>
                  </div>
                  <div class="row">
                   <div class="col-sm-2 pb5">  <input type="checkbox" class="minimal-blue" name="powers[reply][add]" value="1" <?php if(!empty($roleid)){if(yunyecms_getarrayvalue($powersarr,'reply','add')==1) echo "checked";}?>>&nbsp; &nbsp;回复
                     </div>
                    <div class="col-sm-2 pb5">  <input type="checkbox" class="minimal-blue" name="powers[reply][edit]" value="1" <?php if(!empty($roleid)){if(yunyecms_getarrayvalue($powersarr,'reply','edit')==1) echo "checked";}?>>&nbsp; &nbsp;修改回复
                     </div>
                    <div class="col-sm-2 pb5">  
                      <input type="checkbox" class="minimal-blue" name="powers[reply][del]" value="1" <?php if(!empty($roleid)){if(yunyecms_getarrayvalue($powersarr,'reply','del')==1) echo "checked";}?>>&nbsp; &nbsp;删除回复
                     </div>
                  </div>
              </div>
            <!-- /.box-body -->
          </div>
                     </div>
                   <div class="col-sm-offset-1 col-sm-10">
                     <div class="row">
                     <div class="col-sm-2 pb5">  <input type="checkbox" class="minimal-blue" name="selectall"  id="selectall"  value="1" >&nbsp; &nbsp;选中全部
                         </div>
                     </div>
                   </div>
                     
                </div>
                
              </div>
              <!-- /.box-body -->
              <div class="box-footer">
              <div class="form-group">
                  <div class="col-sm-offset-1 col-sm-10">
                     <input name="yyact" type="hidden" value="<?php echo $yyact;?>">
                     <input name="c" type="hidden" value="<?php echo ROUTE_C;?>">
                     <input name="a" type="hidden" value="<?php echo ROUTE_A;?>">
					 <?php echo $this->hashurl['svp'];?>
                     <?php if(!empty($roleid)):?>
                     <input name="id" type="hidden" value="<?php echo $roleid;?>">
                     <input name="oldgroupname" type="hidden" id="oldgroupname" value="<?php echo $crole['groupname'];?>"> 
                     <?php  endif?>
                     <button type="submit" class="btn btn-primary"><?php  if($yyact=="add"):?> <i class="icon fa fa-plus"></i> 添 加 <?php elseif($yyact=="edit"):?> <i class="icon fa fa-edit"></i> 修 改 <?php  endif?></button> &nbsp; &nbsp;
                     <button type="reset" class="btn btn-default">  重 置 </button>
                  </div>
              </div>
              </div>
              <!-- /.box-footer -->
            </form>
          </div>
              <div class="alert alert-info alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <h4><i class="icon fa fa-info"></i> 注：</h4>
                1.会员权限组：可自由定义前台会员的信息权限。<br/>
              </div>

              <div class="example-modal">
        <div class="modal modal-info" id="alertmodal">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">提示信息：</h4>
              </div>
              <div class="modal-body">
                <p></p>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">取消</button>
                <button type="button" class="btn btn-outline" onClick="javsacript:CloseModal('infoselfmodal','#alertmodal');">确认</button>
              </div>
            </div>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->
      </div>
              
              
        </div>
        <!--/.col (left) -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
<?php require tpl_adm('foot');?>
<!-- jQuery 2.2.3 -->
<script src="<?php echo YUNYECMS_UI;?>plugins/jQuery/jquery-2.2.3.min.js"></script>
<!-- Bootstrap 3.3.6 -->
<script src="<?php echo YUNYECMS_UI;?>bootstrap/js/bootstrap.min.js"></script>
<!-- FastClick -->
<!-- AdminLTE for demo purposes -->
<script src="<?php echo YUNYECMS_UI;?>dist/js/admin.js"></script>
<script src="<?php echo YUNYECMS_UI;?>plugins/iCheck/icheck.min.js"></script>
<script type="text/javascript" src="<?php echo YUNYECMS_UI;?>validator/dist/js/bootstrapValidator.js"></script>
<script>
  $(function () {
    //iCheck for checkbox and radio inputs
    $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
      checkboxClass: 'icheckbox_minimal-blue',
      radioClass: 'iradio_minimal-blue'
    });
    //Red color scheme for iCheck
    $('input[type="checkbox"].minimal-red, input[type="radio"].minimal-red').iCheck({
      checkboxClass: 'icheckbox_minimal-red',
      radioClass: 'iradio_minimal-red'
    });
	$('input[type="checkbox"].minimal-blue, input[type="radio"].minimal-blue').iCheck({
      checkboxClass: 'icheckbox_minimal-blue',
      radioClass: 'iradio_minimal-blue'
    });
    //Flat red color scheme for iCheck
      $('input[type="checkbox"].flat-blue, input[type="radio"].flat-blue').iCheck({
      checkboxClass: 'icheckbox_flat-blue',
      radioClass: 'iradio_flat-blue'
    });
  });
</script>
<script type="text/javascript">
$(document).ready(function() {
    $('#defaultForm').bootstrapValidator({
        message: 'This value is not valid',
//        live: 'disabled',
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            groupname: {
                message: '会员组名是无效的',
                validators: {
                    notEmpty: {
                        message: '会员组名必需填写'
                    },
                   /* remote: {
                        url: 'user_add.php',
                        message: '权限名是不可用的'
                    },*/
                }
            },
        }
    });
});

$(document).ready(function(){
//使用ON绑定事件
    var checkAll = $('#selectall');
	var checkboxes = $('#powerlist input');
	/*checkboxes.each(function(){
         if($(this).attr("name")=="powers[info][self']"){
		   $(this).remove();
		   }
		   if($(this).attr("name")=="powers[info][needverify']"){
		   $(this).remove();
		   }
		   if($(this).attr("name")=="powers[info][auditednotedit']"){
		   $(this).remove();
		   }
    });  */
	checkAll.on('ifChecked ifUnchecked', function(event) {
		if (event.type == 'ifChecked') {
			checkboxes.iCheck('check');
			$("#infoneedverify").iCheck('uncheck');
			$("#infoauditednotedit").iCheck('uncheck');
		} else {
			checkboxes.iCheck('uncheck');
		}
	});
	
	/*$("#infoall").on('ifChecked', function(event) {
		if (event.type == 'ifChecked') {
			if($("#infoall").prop('checked')&&$("#infoself").prop('checked')){
				ModalPrompt('infoselfmodal','信息提示','不能同时选中'，'#alertmodal')
				}
		}
	});	*/
	
/*	$("#infoself").on('ifChecked', function(event) {
		if (event.type == 'ifChecked') {
			if($("#infoall").prop('checked')&&$("#infoself").prop('checked')){
				this.iCheck('uncheck');
				}
		}
	});		
	*/
	
	/*checkboxes.on('ifChanged', function(event){
		if(checkboxes.filter(':checked').length == checkboxes.length) {
			checkAll.prop('checked', 'checked');
		} else {
			checkAll.removeProp('checked');
		}
		checkAll.iCheck('update');
	});*/
	
});


</script>

<script language="javascript" type="text/javascript">
        $(function() {
			var navudinfo="<?php echo $parnav;?>";
			$('.breadcrumb',window.parent.document).children('#homeitem').nextAll().remove();
			$('.breadcrumb',window.parent.document).children('#homeitem').after(navudinfo);
        });
</script>

</body>
</html>