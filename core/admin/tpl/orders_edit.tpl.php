<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>YUNYECMS <?php echo YUNYECMS_VERSION;?></title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="<?php echo YUNYECMS_UI;?>bootstrap/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?php echo YUNYECMS_UI;?>plugins/font-awesome-4.7.0/css/font-awesome.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo YUNYECMS_UI;?>plugins/iCheck/all.css">
  <link rel="stylesheet" href="<?php echo YUNYECMS_UI;?>dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="<?php echo YUNYECMS_UI;?>dist/css/skins/_all-skins.min.css">
  <link rel="stylesheet" href="<?php echo YUNYECMS_UI;?>dist/css/admin.css">
<link rel="stylesheet" href="<?php echo YUNYECMS_UI;?>validator/dist/css/bootstrapValidator.css"/>
<script type="text/javascript" charset="utf-8" src="<?php echo YUNYECMS_PUBLIC;?>ueditor/ueditor.config.js"></script>
 <script type="text/javascript" charset="utf-8" src="<?php echo YUNYECMS_PUBLIC;?>ueditor/ueditor.all.js"> </script> 
<script src="<?php echo YUNYECMS_UI;?>plugins/jQuery/jquery-2.2.3.min.js"></script>
 
  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
</head>
<body class="hold-transition rightbgcolor"  onload="changefrmHeight()">
  <!-- Content Wrapper. Contains page content -->
  <div class="container-fluid" id="mainwrap">
   
 
    <!-- Main content -->
    <section class="content">
      <div class="row">
        <!-- left column -->
        <div class="col-md-12">
        
        <div class="box box-info">
            <div class="box-header with-border">
               <h3 class="box-title"><?php if($yyact=="add"):?> 增加订单 <?php elseif($yyact=="edit"):?> 修改订单 <?php  endif?>   </h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
			  <form id="form1" class="form-horizontal" method="post" action="<?php echo YUNYECMS_URLADM;?>"   >
              <div class="box-body">
                            <div class="form-group">
            <label class="col-md-2 control-label" for="title">订单号</label>
            <div class="col-md-6">
                  <input type="text"  class="form-control" id="sn" disabled name="sn" <?php if(!empty($id)):?>  value="<?php echo $row["sn"];?>" <?php endif; ?>  />
            </div>
        </div>
                     <div class="form-group">
            <label class="col-md-2 control-label" for="title">标题</label>
            <div class="col-md-6">
                 <input type="text"  class="form-control " id="title" name="title" <?php if(!empty($id)):?>  value="<?php echo $row["title"];?>" <?php endif; ?>   />
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-2 control-label" for="title">姓名</label>
            <div class="col-md-6">
                  <input type="text"  class="form-control" id="name" name="name" <?php if(!empty($id)):?>  value="<?php echo $row["name"];?>" <?php endif; ?> />
            </div>
        </div>
                <div class="form-group">
            <label class="col-md-2 control-label" for="title">地址</label>
            <div class="col-md-6">
                  <input type="text"  class="form-control" id="address" name="address" <?php if(!empty($id)):?>  value="<?php echo $row["address"];?>" <?php endif; ?> />
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-2 control-label" for="title">手机</label>
            <div class="col-md-6">
                  <input type="text"  class="form-control" id="mobile" name="mobile" <?php if(!empty($id)):?>  value="<?php echo $row["mobile"];?>" <?php endif; ?> />
            </div>
        </div>
         <div class="form-group">
            <label class="col-md-2 control-label" for="keywords">备注</label>
            <div class="col-md-6">
                <textarea name="remark" id="remark" class="form-control"><?php if(!empty($id)):?><?php echo $row["remark"];?> <?php endif; ?></textarea>
            </div>
        </div>
            

       <div class="form-group">
				<label class="col-md-2 control-label" for="title">订单状态</label>
				<div class="col-md-6">
				   <input type="radio" name="status" value="0" class="minimal-blue"   <?php if(!empty($id)) { if($row['status']==0) echo "checked";} ?> >      未付款 &nbsp;
				    <input type="radio" name="status" value="1" class="minimal-blue"  <?php if(empty($id)){ echo "checked";}else{  if($row["status"]==1) echo  "checked"; } ?>  } >  已付款   
				    <input type="radio" name="status" value="2" class="minimal-blue"  <?php if(empty($id)){ echo "checked";}else{  if($row["status"]==2) echo  "checked"; } ?>  } >  已完成   
				</div>
		 </div>                                                                                                 
              </div>
              <!-- /.box-body -->
              <div class="box-footer">
              <div class="form-group">
                 <div class="col-md-offset-3 col-md-9">
					 <input name="yyact" type="hidden" value="<?php echo $yyact;?>">
                     <input name="c" type="hidden" value="<?php echo ROUTE_C;?>">
                     <input name="a" type="hidden" value="<?php echo ROUTE_A;?>">
                     <?php echo $this->hashurl['svp'];?>
                     <?php if(!empty($id)):?>
                     <input name="id" type="hidden" value="<?php echo $id;?>">
                     <?php  endif?>
                     <button type="submit" class="btn bg-blue  btn-flat"><?php  if($yyact=="add"):?> <i class="icon fa fa-plus"></i> 添 加 <?php elseif($yyact=="edit"):?> <i class="icon fa fa-edit"></i> 修 改 <?php  endif?></button> &nbsp; &nbsp;
                     <button type="reset" class="btn btn-default btn-flat"> <i class="fa fa-undo"></i> 重 置 </button>                                         
            </div>	
              </div>
              </div>
              <!-- /.box-footer -->
            </form>
          </div>
        
        
                         
            
        </div>
        <!--/.col (left) -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
<?php require tpl_adm('foot');?>

<!-- jQuery 2.2.3 -->
<script src="<?php echo YUNYECMS_UI;?>plugins/jQuery/jquery-2.2.3.min.js"></script>
<!-- Bootstrap 3.3.6 -->
<script src="<?php echo YUNYECMS_UI;?>bootstrap/js/bootstrap.min.js"></script>
<!-- FastClick -->
<!-- AdminLTE for demo purposes -->
<script src="<?php echo YUNYECMS_UI;?>dist/js/admin.js"></script>
<script src="<?php echo YUNYECMS_UI;?>plugins/iCheck/icheck.min.js"></script>

<script type="text/javascript" src="<?php echo YUNYECMS_UI;?>validator/dist/js/bootstrapValidator.js"></script>
<script>
  $(function () {
    //iCheck for checkbox and radio inputs
    $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
      checkboxClass: 'icheckbox_minimal-blue',
      radioClass: 'iradio_minimal-blue'
    });
    //Red color scheme for iCheck
    $('input[type="checkbox"].minimal-red, input[type="radio"].minimal-red').iCheck({
      checkboxClass: 'icheckbox_minimal-red',
      radioClass: 'iradio_minimal-red'
    });
    //Flat red color scheme for iCheck
    $('input[type="checkbox"].flat-blue, input[type="radio"].flat-blue').iCheck({
      checkboxClass: 'icheckbox_flat-blue',
      radioClass: 'iradio_flat-blue'
    });
  });
</script>
<script type="text/javascript">
$(document).ready(function() {
    $('#defaultForm').bootstrapValidator({
        message: 'This value is not valid',
//        live: 'disabled',
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            title: {
                message: '栏目名称是无效的',
                validators: {
                    notEmpty: {
                        message: '栏目名称必需填写'
                    },
                   /* remote: {
                        url: 'user_add.php',
                        message: '部门名是不可用的'
                    },*/
                }
            },
        }
    });
});
</script>
<script language="javascript" type="text/javascript">
        $(function() {
			var navudinfo="<?php echo $parnav;?>";
			$('.breadcrumb',window.parent.document).children('#homeitem').nextAll().remove();
			$('.breadcrumb',window.parent.document).children('#homeitem').after(navudinfo);
        });
</script>

    <script>
			  $(function () {
				//iCheck for checkbox and radio inputs
				$('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
				  checkboxClass: 'icheckbox_minimal-blue',
				  radioClass: 'iradio_minimal-blue'
				});
				//Red color scheme for iCheck
				$('input[type="checkbox"].minimal-red, input[type="radio"].minimal-red').iCheck({
				  checkboxClass: 'icheckbox_minimal-red',
				  radioClass: 'iradio_minimal-red'
				});
				$('input[type="checkbox"].minimal-blue, input[type="radio"].minimal-blue').iCheck({
				  checkboxClass: 'icheckbox_minimal-blue',
				  radioClass: 'iradio_minimal-blue'
				});
				//Flat red color scheme for iCheck
				  $('input[type="checkbox"].flat-blue, input[type="radio"].flat-blue').iCheck({
				  checkboxClass: 'icheckbox_flat-blue',
				  radioClass: 'iradio_flat-blue'
				});
			  });
     </script>


</body>
</html>