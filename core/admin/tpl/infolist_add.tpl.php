<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>YUNYECMS <?php echo YUNYECMS_VERSION;?></title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="<?php echo YUNYECMS_UI;?>bootstrap/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?php echo YUNYECMS_UI;?>plugins/font-awesome-4.7.0/css/font-awesome.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo YUNYECMS_UI;?>plugins/iCheck/all.css">
  <link rel="stylesheet" href="<?php echo YUNYECMS_UI;?>dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="<?php echo YUNYECMS_UI;?>dist/css/skins/_all-skins.min.css">
  <link rel="stylesheet" href="<?php echo YUNYECMS_UI;?>dist/css/admin.css">
<link rel="stylesheet" href="<?php echo YUNYECMS_UI;?>validator/dist/css/bootstrapValidator.css"/>
<script type="text/javascript" charset="utf-8" src="<?php echo YUNYECMS_PUBLIC;?>ueditor/ueditor.config.js"></script>
 <script type="text/javascript" charset="utf-8" src="<?php echo YUNYECMS_PUBLIC;?>ueditor/ueditor.all.js"> </script> 
<script src="<?php echo YUNYECMS_UI;?>plugins/jQuery/jquery-2.2.3.min.js"></script>
  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
</head>
<body class="hold-transition rightbgcolor"  onload="changefrmHeight()">
  <!-- Content Wrapper. Contains page content -->
  <div class="container-fluid" id="mainwrap">
   <h1 class="page-title"><?php if($yyact=="add"):?> 增加信息 <?php elseif($yyact=="edit"):?> 修改信息 <?php  endif?>                            <small></small>
   </h1>
    <!-- Main content -->
    <section class="content">
      <div class="row">
        <!-- left column -->
        <div class="col-md-12">
					<form id="form1" class="form-horizontal" method="post" action="<?php echo YUNYECMS_URLADM;?>"   >
										<!--BEGIN TABS-->
										<div class="nav-tabs-custom">
											<ul class="nav nav-tabs">
												<li class="active"><a href="#tab_1_1" data-toggle="tab"><span class="lancnbox"><span class="lan-cn"></span></span> 中文版</a></li>
												<li><a href="#tab_1_2" data-toggle="tab"><span class="lancnbox"><span class="lan-en"></span></span> English</a></li>
											</ul>
	   <div class="tab-content">
		<div class="tab-pane active" id="tab_1_1">
         <div class="box-body">
          <div class="form-group">
            <label class="col-md-2 control-label" for="title">栏目</label>
            <div class="col-md-6">
              <select id="catid" name="catid"  class="form-control "  onchange="reloadpage(this.options[this.options.selectedIndex].value)"  >
              <?php
                if(!empty($rsinfo)){
                $parentid=getparentid($catid);
                getsubcatsel(0,0,$rsinfo["catid"]);
                }else{
                $level=getlevel($catid);
                if($level==3){
                 $parentid=getparentid($catid);
                 getsubcatsel($parentid,0,$rsinfo["catid"]);
                  }else{
                 if(!empty($catsubarr)){
                  getsubcatsel($catid,0,0);
                 }else{
                  getsubcatsel(0,0,$catid);
                  }
                 }
                }
               ?>
                </select>
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-2 control-label" for="title">标题</label>
            <div class="col-md-6">
                <input type="text"  class="form-control " id="title" name="title" <?php if(!empty($id)):?>  value="<?php echo $rsinfo["title"];?>" <?php endif; ?>  />
            </div>
        </div>
       <div class="form-group">
            <label class="col-md-2 control-label" for="title">首页推荐</label>
            <div class="col-md-6">
                <input name="isgood" type="checkbox" class="minimal-blue" value="1" <?php if(!empty($rsinfo)){ if($rsinfo["isgood"]==1) echo  "checked"; }?> >
            </div>
        </div>  
        <div class="form-group">
            <label class="col-md-2 control-label" for="title">缩略图</label>
            <div class="col-md-6">
              <script type="text/plain" id="upload_ue"></script>
             <div class="col-md-12" id="imgpicbox" style="padding-left: 0">
            <input type="hidden" id="pic" name="pic" <?php if(!empty($id)):?>  value="<?php echo $rsinfo["pic"];?>" <?php endif; ?> />
    	      <?php if(empty($rsinfo['pic'])){ 
			  $strimgcss='style="display: none;"'; 
			  $picurl=YUNYECMS_PUBLIC.'img/addpic.png'; 
			  } else{ 
			  $strimgcss=''; 
			  $picurl=$rsinfo['pic']; 
			  } ?>
    	        <img src="<?php echo $picurl;?>" id="img_pic" style="width: 135px;height:135px;  border: solid 1px #ccc;padding: 2px; cursor: pointer;  " onclick="upImage();" /><br>
				<div style="margin-top:8px;">
					<a href="javascript:void(0);" onclick="upImage();" class="btn btn-primary btn-flat"><i class="fa fa-upload"></i> 点击上传图片 </a>
  <?php if(!empty($rsinfo["pic"])){ $strcanclecss="style=''";} else{ $strcanclecss="style='display:none;'" ;} ?>			
 <button type="button" class="btn bg-red btn-flat" id="cancle" <?php echo $strcanclecss; ?>><i class="fa fa-remove"></i> 取消图片</button>
				</div> 
              </div>
		<script type="text/javascript">
		var _editor;
		$(function() {
		//重新实例化一个编辑器，防止在上面的editor编辑器中显示上传的图片或者文件
		_editor = UE.getEditor('upload_ue');
		_editor.ready(function () {
		//设置编辑器不可用
		//_editor.setDisabled();
		//隐藏编辑器，因为不会用到这个编辑器实例，所以要隐藏
		_editor.hide();
		//侦听图片上传
		_editor.addListener('beforeInsertImage', function(t, arg) {
		//将地址赋值给相应的input,只去第一张图片的路径
		$("#pic").attr("value", arg[0].src);
		$("#img_pic").attr("src", arg[0].src);
		$("#imgpicbox").show();
		//图片预览
		//$("#preview").attr("src", arg[0].src);
		})
		//侦听文件上传，取上传文件列表中第一个上传的文件的路径
		_editor.addListener('afterUpfile', function (t, arg) {
		$("#upfile").attr("value", arg[0].url);
		})
		});
		}); 
		//弹出图片上传的对话框
		function upImage() {
		var myImage = _editor.getDialog("insertimage");
		myImage.open();
		}
		//弹出文件上传的对话框
		function upFiles() {
		var myFiles = _editor.getDialog("attachment");
		myFiles.open();
		}
				
		$("#cancle").click(function(){
					$("#pic").val('');
					$("#img_pic").attr("src","<?php echo YUNYECMS_PUBLIC;?>img/noimg.jpg");
				});	
			</script>
             
            </div>
        </div>        
<?php if($morepicfield['isadd']==1) : ?>
<div class="form-group">
            <label class="col-md-2 control-label" for="title">更多图片</label>
            <div class="col-md-10">
             <script type="text/plain" id="upload_ue_morepic"></script>
             <input type="hidden" id="morepic" name="morepic"  <?php if(!empty($morepicstr)):?>  value="<?php echo $morepicstr;?>" <?php endif; ?> />
             <div class="col-md-12" id="imgmorepicbox" style="padding-left: 0" >
			 <div style="float:left; margin-right: 15px;"><img src="<?php echo YUNYECMS_PUBLIC;?>img/addpic.png" id="img_addpic" style="width: 135px;  border: solid 1px #ccc;padding: 2px; cursor: pointer;  " onclick="upmoreImage();" /><br>
			 <div style="margin-top:8px;">
				  <a href="javascript:void(0);" onclick="upmoreImage();" class="btn btn-primary btn-flat"><i class="fa fa-upload"></i> 点击上传图片 </a> 
			 </div>
			 </div>
			   <?php 
				 if(!empty($id)&&!empty($morepicarr)): foreach($morepicarr as $key=>$var): ?>
				<div style="float:left;margin-right: 15px; margin-bottom: 10px;" id="morepicitem<?php echo $key;?>"><img src="<?php echo $var;?>" style="width:135px;height:135px;  border: solid 1px #ccc;padding: 2px; cursor: pointer;  " onclick="upmoreImage();" id="img_morepic<?php echo $key;?>" /><br><div style="margin-top:8px;"><button type="button" class="btn bg-red btn-flat"  onclick="canclepic('img_morepic<?php echo $key;?>','morepicitem<?php echo $key;?>');" ><i class="fa fa-remove"></i> 取消图片</button></div></div>
			   <?php endforeach; endif; ?>
           </div>
<script type="text/javascript">
	
Array.prototype.indexOf = function(val) { //prototype 给数组添加属性
  for (var i = 0; i < this.length; i++) { //this是指向数组，this.length指的数组类元素的数量
	if (this[i] == val) return i; //数组中元素等于传入的参数，i是下标，如果存在，就将i返回
  }
  return -1; 
};
Array.prototype.remove = function(val) {  //prototype 给数组添加属性
  var index = this.indexOf(val); //调用index()函数获取查找的返回值
  if (index > -1) {
	this.splice(index, 1); //利用splice()函数删除指定元素，splice() 方法用于插入、删除或替换数组的元素
  }
};	
	
var _editor_morepic;
$(function() {
//重新实例化一个编辑器，防止在上面的editor编辑器中显示上传的图片或者文件
_editor_morepic = UE.getEditor('upload_ue_morepic');
_editor_morepic.ready(function () {
//设置编辑器不可用
//_editor.setDisabled();
//隐藏编辑器，因为不会用到这个编辑器实例，所以要隐藏
_editor_morepic.hide();
//侦听图片上传
_editor_morepic.addListener('beforeInsertImage', function(t, arg) {
//将地址赋值给相应的input,只去第一张图片的路径
	var morepicstr="";
	for(i=0;i<arg.length;i++){
		var htmlstr='<div style="float:left;margin-right: 15px; margin-bottom: 10px; " id="morepicitem'+i+'"><img src="'+arg[i].src+'" style="width:135px;height:135px;  border: solid 1px #ccc;padding: 2px; cursor: pointer;  " onclick="upmoreImage();" id="img_morepic'+i+'" /><br><div style="margin-top:8px;"><button type="button" class="btn red"  onclick="canclepic(\'img_morepic'+i+'\',\'morepicitem'+i+'\');" ><i class="fa fa-remove"></i> 取消图片</button></div></div>';
		$("#imgmorepicbox").append(htmlstr);
		if(i==arg.length-1){
			morepicstr=morepicstr+arg[i].src;
		}else{
			morepicstr=morepicstr+arg[i].src+",";
		}
	}
	    var oldvalue=$("#morepic").val();
	    if(oldvalue.length>0){
		  $("#morepic").attr("value", oldvalue+","+morepicstr);
		}else{
		  $("#morepic").attr("value", morepicstr);
		}
 })

});
}); 
//弹出图片上传的对话框
function upmoreImage() {
var myImage_morepic = _editor_morepic.getDialog("insertimage");
myImage_morepic.open();
}

function canclepic(obj,obj2){
	var oldsrc= $("#"+obj).attr("src");;
    $("#"+obj2).remove();
	var oldvalue=$("#morepic").val();
	oldarr=oldvalue.split(",");
	oldarr.remove(oldsrc);
	newstr=oldarr.join(",")
	$("#morepic").attr("value", newstr);
}	
</script>
             
            </div>
        </div>    
<?php endif; ?>
    
     <div class="form-group">
            <label class="col-md-2 control-label" for="title">外部链接</label>
            <div class="col-md-6">
                <input type="text"  class="form-control " id="exlink" name="exlink"  <?php if(!empty($id)):?>  value="<?php echo $rsinfo["exlink"];?>" <?php endif; ?> />
            </div>
      </div>

	<div class="form-group">
		<label class="col-md-2 control-label">上传文件</label>
		 <div class="col-md-6">
			<div class="input-group">
				<input type="text" class="form-control" placeholder="上传文件"  id="upfile"  name="picfile" >
				<span class="input-group-btn">
					<button class="btn btn-primary btn-flat" type="button" onclick="upFiles();"> <i class="fa fa-upload"></i> 点击上传文件 </button>
				</span>
			</div>
		</div>
	</div> 
       <div class="form-group">
            <label class="col-md-2 control-label" for="description">简介</label>
            <div class="col-md-6">
                <textarea name="summary" id="summary"   class="form-control "  class="span5"><?php if(!empty($id)) echo $rsinfo["summary"];?> </textarea>
            </div>
        </div>  
	  <div class="form-group">
				<label class="col-md-2 control-label" for="title">访问级别</label>
				<div class="col-md-6">
				   <input type="radio" name="ispower" value="1" class="minimal-blue" <?php if(!empty($id)) { if($rsinfo['ispower']==1) echo "checked";} ?> >  会员 &nbsp;
				   <input type="radio" name="ispower" value="0" class="minimal-blue"  <?php if(empty($id)){ echo "checked";}else{ if($rsinfo["ispower"]==0) echo  "checked"; } ?>  >  游客 
				</div>
	  </div>	 
		<?php foreach($modelfields as $kf=>$vf): ?> 
			<?php 
				$formctrl=$vf["formctrl"];
		     ?>
			<?php if( $formctrl=='text'):  ?>
				 <div class="form-group">
						<label class="col-md-2 control-label" for="keywords"><?php echo $vf["fdtitle"];?></label>
						 <div class="col-md-6">
							<input type="text"   class="form-control"  name="<?php echo $vf["fdname"];?>" id="<?php echo $vf["fdname"];?>" value="<?php if(!empty($id)) echo $rsinfo[$vf["fdname"]];?>" />
						</div>
				  </div>
			 <?php elseif( $formctrl=='textarea'): ?>
			  <div class="form-group">
					<label class="col-md-2 control-label" for="description"><?php echo $vf["fdtitle"];?></label>
					<div class="col-md-6">
						<textarea name="<?php echo $vf["fdname"];?>" id="<?php echo $vf["fdname"];?>"   class="form-control "  class="span5"><?php if(!empty($id)) echo $rsinfo[$vf["fdname"]];?></textarea>
					</div>
				</div>  
			 <?php elseif( $formctrl=='editor'): ?>
				 <div class="form-group">
				<label class="col-md-2 control-label" for="content"><?php echo $vf["fdtitle"];?></label>
				<div class="col-md-10">
					<script id="<?php echo $vf["fdname"];?>_editor" type="text/plain" style="width:100%;height:400px;" name="<?php echo $vf["fdname"];?>"><?php if(!empty($id)) echo uhtmlspecialchars_decode($rsinfo[$vf["fdname"]]);?></script>
				   <script type="text/javascript">
					var ue = UE.getEditor('<?php echo $vf["fdname"];?>_editor');
				  </script>	
				</div>
			</div>
			 <?php elseif( $formctrl=='radio'): ?>
				 <div class="form-group">
						<label class="col-md-2 control-label" for="keywords"><?php echo $vf["fdtitle"];?></label>
						 <div class="col-md-6">
							<?php $formvalue=$vf["formvalue"]; $radiocheck=""; $formvalue=explode("<br/>", $formvalue); 
							  foreach($formvalue as $kvl=>$vvl) {
							     $radioarr=explode("=", $vvl);
							     if(!empty($rsinfo)){
							        if( $rsinfo[$vf["fdname"]]==$radioarr[1]){ $radiocheck="checked";}else{ $radiocheck="";}
							     }else{
							       if($kvl==0){ $radiocheck="checked"; }else { $radiocheck="";}
							     }
							     echo "<input type=\"radio\" class=\"minimal-blue\" name='".$vf["fdname"]."'  value='".$radioarr[1]."'  ".$radiocheck." > ".$radioarr[0]."  &nbsp; &nbsp;";
							 ?>
							<?php } ?> 
						</div>
				  </div>
			 <?php elseif( $formctrl=='checkbox'): ?>
				 <div class="form-group">
						<label class="col-md-2 control-label" for="keywords"><?php echo $vf["fdtitle"];?></label>
						 <div class="col-md-6">
							<?php $formvalue=$vf["formvalue"]; $defchecked=""; $formvalue=explode("<br/>", $formvalue); 
							  foreach($formvalue as $kvl=>$vvl) {
							     $radioarr=explode("=", $vvl);
							     if(!empty($rsinfo)){
						             $rsinfofdarr=explode(",",$rsinfo[$vf["fdname"]]); 
						             if(in_array($radioarr[1],$rsinfofdarr)){ 
						               $defchecked="checked";  }else{  $defchecked="";
						             }
							     }else{
							       if($kvl==0){ $defchecked="checked"; }else { $defchecked="";}
							     }
							     echo "<input type=\"checkbox\" class=\"minimal-blue\" name='".$vf["fdname"]."[]' value='".$radioarr[1]."' ".$defchecked." > ".$radioarr[0]."  &nbsp; &nbsp;";
							 ?>
							<?php } ?> 
						</div>
				  </div>
			 <?php else: ?>			 
			 <div class="form-group">
						<label class="col-md-2 control-label" for="keywords"><?php echo $vf["fdtitle"];?></label>
						 <div class="col-md-6">
							<input type="text"   class="form-control"  name="<?php echo $vf["fdname"];?>" id="<?php echo $vf["fdname"];?>" value="<?php if(!empty($id)) echo $rsinfo[$vf["fdname"]];?>" />
						</div>
				  </div>
			 
			 <?php endif; ?>
		<?php endforeach; ?> 
        <div class="form-group">
            <label class="col-md-2 control-label" for="keywords">SEO标题</label>
             <div class="col-md-6">
                <input type="text"   class="form-control"  name="seotitle" id="seotitle" value="<?php if(!empty($id)) echo $rsinfo["seotitle"];?>" />
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-2 control-label" for="keywords">SEO关键字</label>
             <div class="col-md-6">
                <input type="text"   class="form-control"  name="seokeywords" id="seokeywords" value="<?php if(!empty($id)) echo $rsinfo["seokeywords"];?>" />
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-2 control-label" for="description">SEO描述</label>
            <div class="col-md-6">
                <textarea name="seodesc" id="seodesc"   class="form-control "  class="span5"><?php if(!empty($id)) echo $rsinfo["seodesc"];?></textarea>
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-2 control-label" for="title">时间</label>
            <div class="col-md-6">
			    <?php if(!empty($rsinfo["addtime"])):?>
                <input type="text"  class="form-control " id="addtime" name="addtime" value="<?php echo date('Y-m-d H:i:s',$rsinfo['addtime']);?>" />
                <?php else: ?>         
                <input type="text"  class="form-control " id="addtime" name="addtime" value="<?php echo date('Y-m-d H:i:s',time());?>" />
                <?php endif; ?>                    
            </div>
        </div>      
       <div class="form-group">
            <label class="col-md-2 control-label" for="title">排序</label>
            <div class="col-md-6">
                <?php if(empty($id)):?>
                <input type="text"  class="form-control " id="ordernum" name="ordernum" value="<?php echo $strmax;?>" />
                <?php else: ?>         
                <input type="text"  class="form-control " id="ordernum" name="ordernum" value="<?php echo $rsinfo["ordernum"];?>" />
                <?php endif; ?>    
            </div>
        </div>      
          </div>
              <!-- /.box-body -->
              <div class="box-footer">
				   <div class="row">
					<div class="col-md-offset-3 col-md-9">
						 <button type="submit" class="btn bg-blue  btn-flat"><?php  if($yyact=="add"):?> <i class="icon fa fa-plus"></i> 添 加 <?php elseif($yyact=="edit"):?> <i class="icon fa fa-edit"></i> 修 改 <?php  endif?></button> &nbsp; &nbsp;
                     <button type="reset" class="btn btn-default btn-flat"> <i class="fa fa-undo"></i> 重 置 </button>      
					</div>
				</div>
               </div>
              <!-- /.box-footer -->
		</div>
		<div class="tab-pane" id="tab_1_2">
	    <div class="box-body">
         <div class="form-group">
            <label class="col-md-2 control-label" for="title">标题(English)</label>
            <div class="col-md-6">
                <input type="text"  class="form-control " id="title_en" name="title_en" <?php if(!empty($id)):?>  value="<?php echo $rsinfo["title_en"];?>" <?php endif; ?>  />
            </div>
        </div>
         <div class="form-group">
            <label class="col-md-2 control-label" for="title">外部链接(English)</label>
            <div class="col-md-6">
                <input type="text"  class="form-control " id="exlink_en" name="exlink_en"  <?php if(!empty($id)):?>  value="<?php echo $rsinfo["exlink_en"];?>" <?php endif; ?> />
            </div>
        </div> 
        
        <div class="form-group">
            <label class="col-md-2 control-label" for="description">简介(English)</label>
            <div class="col-md-6">
                <textarea name="summary_en" id="summary_en"   class="form-control "  class="span5"><?php if(!empty($id)) echo $rsinfo["summary_en"];?></textarea>
            </div>
        </div>   
        
  	<?php foreach($modelfields_en as $kf=>$vf): ?> 
			<?php 
				$formctrl=$vf["formctrl"];
		     ?>
			<?php if( $formctrl=='text'):  ?>
				 <div class="form-group">
						<label class="col-md-2 control-label" for="keywords"><?php echo $vf["fdtitle"];?></label>
						 <div class="col-md-6">
							<input type="text"   class="form-control"  name="<?php echo $vf["fdname"];?>" id="<?php echo $vf["fdname"];?>" value="<?php if(!empty($id)) echo $rsinfo[$vf["fdname"]];?>" />
						</div>
				  </div>
			 <?php elseif( $formctrl=='textarea'): ?>
			  <div class="form-group">
					<label class="col-md-2 control-label" for="description"><?php echo $vf["fdtitle"];?></label>
					<div class="col-md-6">
						<textarea name="<?php echo $vf["fdname"];?>" id="<?php echo $vf["fdname"];?>"   class="form-control "  class="span5"><?php if(!empty($id)) echo $rsinfo[$vf["fdname"]];?></textarea>
					</div>
				</div>  
			 <?php elseif( $formctrl=='editor'): ?>
				 <div class="form-group">
				<label class="col-md-2 control-label" for="content"><?php echo $vf["fdtitle"];?></label>
				<div class="col-md-10">
					<script id="<?php echo $vf["fdname"];?>_editor" type="text/plain" style="width:100%;height:400px;" name="<?php echo $vf["fdname"];?>"><?php if(!empty($id)) echo uhtmlspecialchars_decode($rsinfo[$vf["fdname"]]);?></script>
				   <script type="text/javascript">
					var ue = UE.getEditor('<?php echo $vf["fdname"];?>_editor');
				  </script>	
				</div>
			</div>
			 <?php else: ?>
			 <div class="form-group">
						<label class="col-md-2 control-label" for="keywords"><?php echo $vf["fdtitle"];?></label>
						 <div class="col-md-6">
							<input type="text"   class="form-control"  name="<?php echo $vf["fdname"];?>" id="<?php echo $vf["fdname"];?>" value="<?php if(!empty($id)) echo $rsinfo[$vf["fdname"]];?>" />
						</div>
				  </div>
			 
			 <?php endif; ?>
	<?php endforeach; ?>       
 
    <div class="form-group">
            <label class="col-md-2 control-label" for="keywords">SEO标题(English)</label>
             <div class="col-md-6">
                <input type="text"   class="form-control"  name="seotitle_en" id="seotitle_en" <?php if(!empty($id)):?>  value="<?php echo $rsinfo["seotitle_en"];?>" <?php endif; ?>  />
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-2 control-label" for="keywords">SEO关键字(English)</label>
             <div class="col-md-6">
                <input type="text"   class="form-control"  name="seokeywords_en" id="seokeywords_en" <?php if(!empty($id)):?>  value="<?php echo $rsinfo["seokeywords_en"];?>" <?php endif; ?> />
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-2 control-label" for="description">SEO描述(English)</label>
            <div class="col-md-6">
                <textarea name="seodesc_en" id="seodesc_en"   class="form-control "  class="span5"><?php if(!empty($id)) echo $rsinfo["seodesc_en"];?></textarea>
            </div>
        </div>        
        </div>
         <div class="box-footer">
                      <div class="row">
                      <div class="col-md-offset-3 col-md-9">
					 <input name="yyact" type="hidden" value="<?php echo $yyact;?>">
                     <input name="c" type="hidden" value="<?php echo ROUTE_C;?>">
                     <input name="a" type="hidden" value="<?php echo ROUTE_A;?>">
                     <?php echo $this->hashurl['svp'];?>
                     <?php if(!empty($id)):?>
                     <input name="id" type="hidden" value="<?php echo $id;?>">
                     <?php  endif?>
                     <button type="submit" class="btn bg-blue  btn-flat"><?php  if($yyact=="add"):?> <i class="icon fa fa-plus"></i> 添 加 <?php elseif($yyact=="edit"):?> <i class="icon fa fa-edit"></i> 修 改 <?php  endif?></button> &nbsp; &nbsp;
                     <button type="reset" class="btn btn-default btn-flat"> <i class="fa fa-undo"></i> 重 置 </button>                                               
                                                    </div>
                                                </div>
                                            </div>        
												</div>

											</div>

										</div>

										<!--END TABS-->
                            </form>		                 
              <div class="callout callout-info">
                <h4><i class="icon fa fa-info"></i> 温馨提示：</h4>
                 1、内容支持自定义属性，如果现有属性不够用的话，您还可以到系统模型里添加属性字段就可以了
				 2、如果栏目里设置了访问级别，则以栏目的访问级别为优先，比如如果栏目设置了会员才能访问，那么内容里设置游客访问是无效的。
  
              </div>
        </div>
        <!--/.col (left) -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
<?php require tpl_adm('foot');?>

<!-- jQuery 2.2.3 -->
<script src="<?php echo YUNYECMS_UI;?>plugins/jQuery/jquery-2.2.3.min.js"></script>
<!-- Bootstrap 3.3.6 -->
<script src="<?php echo YUNYECMS_UI;?>bootstrap/js/bootstrap.min.js"></script>
<!-- FastClick -->
<!-- AdminLTE for demo purposes -->
<script src="<?php echo YUNYECMS_UI;?>dist/js/admin.js"></script>
<script src="<?php echo YUNYECMS_UI;?>plugins/iCheck/icheck.min.js"></script>

<script type="text/javascript" src="<?php echo YUNYECMS_UI;?>validator/dist/js/bootstrapValidator.js"></script>
<script>
  $(function () {
    //iCheck for checkbox and radio inputs
    $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
      checkboxClass: 'icheckbox_minimal-blue',
      radioClass: 'iradio_minimal-blue'
    });
    //Red color scheme for iCheck
    $('input[type="checkbox"].minimal-red, input[type="radio"].minimal-red').iCheck({
      checkboxClass: 'icheckbox_minimal-red',
      radioClass: 'iradio_minimal-red'
    });
    //Flat red color scheme for iCheck
    $('input[type="checkbox"].flat-blue, input[type="radio"].flat-blue').iCheck({
      checkboxClass: 'icheckbox_flat-blue',
      radioClass: 'iradio_flat-blue'
    });
  });
</script>
<script type="text/javascript">
$(document).ready(function() {
    $('#defaultForm').bootstrapValidator({
        message: 'This value is not valid',
//        live: 'disabled',
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            title: {
                message: '栏目名称是无效的',
                validators: {
                    notEmpty: {
                        message: '栏目名称必需填写'
                    },
                   /* remote: {
                        url: 'user_add.php',
                        message: '部门名是不可用的'
                    },*/
                }
            },
        }
    });
});
</script>
<script language="javascript" type="text/javascript">
        $(function() {
			var navudinfo="<?php echo $parnav;?>";
			$('.breadcrumb',window.parent.document).children('#homeitem').nextAll().remove();
			$('.breadcrumb',window.parent.document).children('#homeitem').after(navudinfo);
        });
</script>

    <script>
			  $(function () {
				//iCheck for checkbox and radio inputs
				$('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
				  checkboxClass: 'icheckbox_minimal-blue',
				  radioClass: 'iradio_minimal-blue'
				});
				//Red color scheme for iCheck
				$('input[type="checkbox"].minimal-red, input[type="radio"].minimal-red').iCheck({
				  checkboxClass: 'icheckbox_minimal-red',
				  radioClass: 'iradio_minimal-red'
				});
				$('input[type="checkbox"].minimal-blue, input[type="radio"].minimal-blue').iCheck({
				  checkboxClass: 'icheckbox_minimal-blue',
				  radioClass: 'iradio_minimal-blue'
				});
				//Flat red color scheme for iCheck
				  $('input[type="checkbox"].flat-blue, input[type="radio"].flat-blue').iCheck({
				  checkboxClass: 'icheckbox_flat-blue',
				  radioClass: 'iradio_flat-blue'
				});
			  });
     </script>


</body>
</html>