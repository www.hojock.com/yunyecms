<!DOCTYPE html>
<html><head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title> 云业内容管理系统-YUNYECMS <?php echo YUNYECMS_VERSION;?></title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="<?php echo YUNYECMS_UI;?>bootstrap/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?php echo YUNYECMS_UI;?>plugins/font-awesome-4.7.0/css/font-awesome.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo YUNYECMS_UI;?>plugins/iCheck/all.css">
  <link rel="stylesheet" href="<?php echo YUNYECMS_UI;?>dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="<?php echo YUNYECMS_UI;?>dist/css/skins/_all-skins.min.css">
  <link href="<?php echo YUNYECMS_UI;?>plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css" />
  <link rel="stylesheet" href="<?php echo YUNYECMS_UI;?>dist/css/admin.css">
  <link rel="stylesheet" href="<?php echo YUNYECMS_UI;?>dist/css/plugins.css">
  <link rel="shortcut icon" href="<?php echo YUNYECMS_UI;?>dist/img/favicon.ico" /> 
   <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
</head>
<body class="hold-transition skin-blue sidebar-mini" onload="changeHeight()">
<div class="wrapper">
  <header class="main-header">
    <!-- Logo -->
    <a href="http://www.yunyecms.com" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><img src="<?php echo YUNYECMS_UI;?>dist/img/logo-mini.png" alt="云业内容管理系统"></span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><img src="<?php echo YUNYECMS_UI;?>dist/img/logo.png" alt="云业内容管理系统"></span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </a>
      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
          <li class="dropdown messages-menu">
            <a href="#" class="dropdown-toggle" id="yysystem"  onClick="showsidermenu(this);">
             <i class="fa fa-cog" aria-hidden="true"></i> 系统
            </a>
          </li>
           <li class="dropdown messages-menu">
            <a href="#" class="dropdown-toggle" id="yyinfo" onClick="showsidermenu(this);">
             <i class="fa fa-info" aria-hidden="true"></i> 内容
            </a>
          </li>
          <li class="dropdown messages-menu">
            <a href="#" class="dropdown-toggle" id="yymember"  onClick="showsidermenu(this);">
             <i class="fa fa-user-circle-o"></i> 用户
            </a>
          </li>
          <li class="dropdown messages-menu">
            <a href="#" class="dropdown-toggle" id="yyapps"  onClick="showsidermenu(this);">
             <i class="fa  fa-th-large"></i> 应用
            </a>
          </li>          
      
           <!-- Control Sidebar Toggle Button -->
          <li class="tipright">
            <a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>
          </li>
          <!-- User Account: style can be found in dropdown.less -->
          <li class="dropdown user user-menu tipright">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <img src="<?php echo YUNYECMS_UI;?>dist/img/user.png" class="user-image" alt="User Image">
              <span class="hidden-xs"><?php echo $this->admuser['username'];?></span>
            </a>
            <ul class="dropdown-menu">
              <!-- User image -->
              <li class="user-header">
                <img src="<?php echo YUNYECMS_UI;?>dist/img/user.png" class="img-circle" alt="User Image">
                <p>
                  <?php if(!empty($admrealname)){echo $admrealname.'-';}?> <?php echo $admrolename;?> <?php if(!empty($admpartment)){echo '-'.$admpartment;}?>
                  <small>上次登录：<?php echo $prelgtime;?></small>
                </p>
              </li>
              <!-- Menu Footer-->
              <li class="user-footer">
                   <div class="pull-left">
                    <a href="<?php echo url_admin('user_add','user',array('userid'=>$this->admuser['userid']),$this->hashurl['usvg']);?>" class="btn btn-info btn-flat" target="maincontent"><i class="fa fa-user"></i> 个人资料</a>
                   </div>
                   <div class="pull-right">
                   <a href="<?php echo url_admin('logout','main','',$this->hashurl['usvg']);?>" class="btn btn-danger btn-flat"><i class="fa fa-close"></i> 退 出</a>
                  </div>
              </li>
            </ul>
          </li>
<!-- Notifications: style can be found in dropdown.less -->
         <?php if($msgpower):?>
          <li class="dropdown notifications-menu tipright">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <i class="fa fa-bell-o"></i>
              <span class="label label-warning"><?php echo $msgnum;?></span>
            </a>
            <ul class="dropdown-menu">
              <li class="header">您有<span class="text-danger"> <?php echo $msgnum;?></span> 条待处理的通知消息</li>
              <li>
                <!-- inner menu: contains the actual data -->
                <ul class="menu">
                   <?php foreach($msgto as $key=>$vo): ?> 
					  <li>
						<a href="<?php echo $vo['url'];?>" target="maincontent">
							<span class="details pull-left "><?php echo $vo['icon'];?>
							<?php echo strcut($vo['title'],75);?>
							</span>
							<span class="time pull-right text-blue"><?php echo yytime($vo['addtime']);?></span>
							<div class="clearfix"></div>
						</a>
					  </li>
                  <?php endforeach ?> 
                </ul>
              </li>
              <li class="footer"><a href="<?php echo url_admin('init','msg');?>" target="maincontent">查看全部</a></li>
            </ul>
          </li>   
         <?php endif;?>
		 <?php if($formpower):?>
                 <!-- Messages: style can be found in dropdown.less-->
          <li class="dropdown messages-menu tipright">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <i class="fa fa-envelope-o"></i>
              <span class="label label-success"><?php echo $customfromnum;?></span>
            </a>
            <ul class="dropdown-menu">
              <li class="header">您有<span class="bold"><?php echo $customfromnum;?>条</span>新的消息</li>
              <li>
                <!-- inner menu: contains the actual data -->
                <ul class="menu">
                   <?php foreach($customfromto as $key=>$vo): ?> 
						   <li><!-- start message -->
							<a href="<?php echo url_admin('customform_view','content',array('catid'=>$vo["catid"],'id'=>$vo["id"]));?>"  target="maincontent">
							  <h4>
							   <?php echo $vo['name'];?>
								<small><i class="fa fa-clock-o"></i> <?php echo yytime($vo['addtime']);?></small>
							  </h4>
							  <p><?php echo strcut($vo['remark'],50);?></p>
							</a>
						  </li>	  
                  <?php endforeach ?> 
                  <!-- end message -->
                </ul>
              </li>
              <?php if($formurl):?>
              <li class="footer"><a href="<?php echo $formurl;?>" target="maincontent">查看全部</a></li>
              <?php endif;?>
            </ul>
          </li>
         <?php endif;?>
        </ul>
      </div>
    </nav>
  </header>
  <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="info">
           <p><?php echo $this->admuser['username'];?></p>
          <a href="<?php echo url_admin('user_add','user',array('userid'=>$this->admuser['userid']),$this->hashurl['usvg']);?>" target="maincontent"><i class="fa fa-circle text-success"></i> <?php echo $admrolename;?></a> &nbsp;
           <a href="<?php echo url_admin('logout','main','',$this->hashurl['usvg']);?>"><i class="fa fa-close text-danger"></i> 退出</a>
        </div>
      </div>
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu" id="yymenu">
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper" id="content-wapper">
      <!-- Content Header (Page header) -->
    <section class="content-header">
      <ol class="breadcrumb">
        <li id="homeitem"><a href="<?php echo url_admin('home','main');?>" target="maincontent"><i class="fa fa-home"></i> 首页</a></li>
        <li><a href="<?php echo url_admin('home','main');?>" target="maincontent">控制面板</a></li>
      </ol>
      <div class="fastmenu">
<!--      <a href="#" class="btn btn-info btn-xs btn-flat"><i class="fa fa-calculator" aria-hidden="true"></i> 信息统计</a>-->
      <a href="<?php echo url_admin('cacheclear','main');?>" target="maincontent" class="btn bg-olive btn-xs btn-flat"><i class="fa fa-refresh" aria-hidden="true"></i> 清理缓存 </a>
      <a href="<?php echo ROOT;?>" class="btn btn-primary btn-xs btn-flat" target="_blank"><i class="fa fa-home " aria-hidden="true"></i> 站点首页</a>
      </div>
    </section>
     <iframe src="<?php echo url_admin('home','main','',$this->hashurl['usvg']);?>" id="maincontent" name="maincontent" scrolling="auto" width="100%" height="1080" frameborder="0"></iframe>
  </div>
  <!-- /.content-wrapper -->
  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Create the tabs -->
    <ul class="nav nav-tabs nav-justified control-sidebar-tabs">
      <li class="active"><a href="#control-sidebar-settings-tab" data-toggle="tab"><i class="fa fa-gears"></i> 设置</a></li>
      <li <?php if(!$msgpower):?> style="display: none;"  <?php endif;?>><a href="#control-sidebar-home-tab" data-toggle="tab"><i class="fa fa-bell-o"></i> 消息</a></li>
    </ul>
    <!-- Tab panes -->
    <div class="tab-content">
           <!-- Settings tab content -->
      <div class="tab-pane active" id="control-sidebar-settings-tab">
          <h3 class="control-sidebar-heading">系统基本设置</h3>
                           <form id="form1" class="form-horizontal" method="post"  enctype="multipart/form-data" target="maincontent"     action="<?php echo url_admin('savetop','config');?>">
                                    <ul class="list-items borderless">
							                              
                                        <li>关闭网站
                                            <input type="checkbox" class="make-switch" value="1" name="isclose"  <?php if($config['isclose']==1): ?> checked <?php endif;?> data-size="small" data-on-1color="success" data-on-text="&nbsp;&nbsp;是&nbsp;&nbsp;"  data-off-color="default" data-off-text="&nbsp;&nbsp;否&nbsp;&nbsp;"> </li>
                                        <li> 在线客服
                                            <input type="checkbox"  class="make-switch" name="iskefu" value="1" <?php if($config['iskefu']==1): ?> checked <?php endif;?> data-size="small" data-on-color="info" data-on-text="显示" data-off-color="default" data-off-text="关闭"> </li>
                                        <li> 商城
                                            <input type="checkbox" class="make-switch" name="ismall" value="1" <?php if($config['ismall']==1): ?> checked <?php endif;?>  data-size="small" data-on-color="danger" data-on-text="开启" data-off-color="default" data-off-text="关闭"> </li>
                                        <li> 云邮件
                                            <input type="checkbox" class="make-switch" name="ismailsub" value="1" <?php if($config['ismailsub']==1): ?> checked <?php endif;?> data-size="small" data-on-color="success" data-on-text="开启" data-off-color="default" data-off-text="关闭"> </li>
                                        <li> 注册会员邮件认证
                                            <input type="checkbox" class="make-switch" name="ismailreg" value="1" <?php if($config['ismailreg']==1): ?> checked <?php endif;?> data-size="small" data-on-color="success" data-on-text="开启" data-off-color="default" data-off-text="关闭"> </li>
                                        <li> 表单发送到邮箱
                                            <input type="checkbox" class="make-switch" name="isfeedbackmail" value="1" <?php if($config['isfeedbackmail']==1): ?> checked <?php endif;?> data-size="small" data-on-color="success" data-on-text="开启" data-off-color="default" data-off-text="关闭"> </li>   
                                        <li> 生成静态HTML
                                            <input type="checkbox" class="make-switch" name="ishtml" value="1" <?php if($config['ishtml']==1): ?> checked <?php endif;?> data-size="small" data-on-color="danger" data-on-text="开启" data-off-color="default" data-off-text="关闭"> </li>
                                        <li> 验证方式
                                            <input type="checkbox" class="make-switch" name="verifymode" value="1" <?php if($config['verifymode']==1): ?> checked <?php endif;?> data-size="small" data-on-color="warning" data-on-text="邮箱" data-off-color="default" data-off-text="手机"> </li>
										  <li> 是否生成缩略图
                                            <input type="checkbox" class="make-switch" name="upload[isthumb]" value="1" <?php if($config['upload']['isthumb']==1): ?> checked <?php endif;?> data-size="small" data-on-color="warning" data-on-text="是" data-off-color="default" data-off-text="否"> </li>
                                        <li> 上传文件最大限制
                                            <input name="upload[maxsize]" class="form-control input-inline input-sm input-small" value="<?php echo $config['upload']['maxsize'];?>" /> </li>                                    </ul>
                                    <div class="inner-content">
                                        <button class="btn btn-success">
										  <?php if(!empty($config)): ?>
										   <input type="hidden" name="id" value="<?php echo $config['id'];?>">
										  <?php endif; ?>
				                        	<?php echo $this->hashurl['svp'];?>
					                        <input name="yyact" type="hidden" value="edit">
                                            <i class="icon-settings"></i> 保存 </button>
                                    </div>
                                    </form>
      </div>
      <!-- /.tab-pane -->
      <!-- Home tab content -->
      <div class="tab-pane" id="control-sidebar-home-tab">
        <h3 class="control-sidebar-heading">最新消息</h3>
        <ul class="control-sidebar-menu">
		 <?php foreach($msgto as $key=>$vo): ?> 
		  <li>
            <a href="?php echo $vo['url'];?>">
              <?php echo $vo['icon2'];?>

              <div class="menu-info">
                <h4 class="control-sidebar-subheading"><?php echo strcut($vo['title'],75);?></h4>
                <p><?php echo yytime($vo['addtime']);?></p>
              </div>
            </a>
          </li>
       <?php endforeach ?>           
        </ul>
        <!-- /.control-sidebar-menu -->
        <!-- /.control-sidebar-menu -->
      </div>
      <!-- /.tab-pane -->
      <!-- Stats tab content -->
      <div class="tab-pane" id="control-sidebar-stats-tab">Stats Tab Content</div>
      <!-- /.tab-pane -->

    </div>
  </aside>
  <!-- /.control-sidebar -->
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->
<!-- jQuery 2.2.3 -->
<script src="<?php echo YUNYECMS_UI;?>plugins/jQuery/jquery-2.2.3.min.js"></script>
<!-- Bootstrap 3.3.6 -->
<script src="<?php echo YUNYECMS_UI;?>bootstrap/js/bootstrap.min.js"></script>
<!-- FastClick -->
<script src="<?php echo YUNYECMS_UI;?>plugins/fastclick/fastclick.js"></script>
<script src="<?php echo YUNYECMS_UI;?>plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
<!-- AdminLTE App -->
<script src="<?php echo YUNYECMS_UI;?>dist/js/app.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="<?php echo YUNYECMS_UI;?>dist/js/demo.js"></script>
<script src="<?php echo YUNYECMS_UI;?>dist/js/admin.js"></script>
<script src="<?php echo url_admin('getmenu','menu');?>"></script>
<script language="javascript">
	$('.sidebar-menu').html(menu_yyinfo);
	  // menuclick();
	$("#yyinfo").parent().addClass("active");
    $('.make-switch').bootstrapSwitch();
</script>
</body>
</html>