<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>YUNYECMS <?php echo YUNYECMS_VERSION;?></title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="<?php echo YUNYECMS_UI;?>bootstrap/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?php echo YUNYECMS_UI;?>plugins/font-awesome-4.7.0/css/font-awesome.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo YUNYECMS_UI;?>plugins/iCheck/all.css">
  <link rel="stylesheet" href="<?php echo YUNYECMS_UI;?>dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="<?php echo YUNYECMS_UI;?>dist/css/skins/_all-skins.min.css">
  <link rel="stylesheet" href="<?php echo YUNYECMS_UI;?>dist/css/admin.css">
<link rel="stylesheet" href="<?php echo YUNYECMS_UI;?>validator/dist/css/bootstrapValidator.css"/>
<script type="text/javascript" charset="utf-8" src="<?php echo YUNYECMS_PUBLIC;?>ueditor/ueditor.config.js"></script>
 <script type="text/javascript" charset="utf-8" src="<?php echo YUNYECMS_PUBLIC;?>ueditor/ueditor.all.js"> </script> 
<script src="<?php echo YUNYECMS_UI;?>plugins/jQuery/jquery-2.2.3.min.js"></script>
 
  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
</head>
<body class="hold-transition rightbgcolor"  onload="changefrmHeight()">
  <!-- Content Wrapper. Contains page content -->
  <div class="container-fluid" id="mainwrap">
   
 
    <!-- Main content -->
    <section class="content">
      <div class="row">
        <!-- left column -->
        <div class="col-md-12">
        
        <div class="box box-info">
            <div class="box-header with-border">
               <h3 class="box-title"><?php if($yyact=="add"):?> 增加语言版 <?php elseif($yyact=="edit"):?> 修改语言版 <?php  endif?>   </h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
			  <form id="form1" class="form-horizontal" method="post" action="<?php echo YUNYECMS_URLADM;?>"   >
              <div class="box-body">
                     <div class="form-group">
            <label class="col-md-2 control-label" for="title">名称</label>
            <div class="col-md-6">
                 <input type="text"  class="form-control " id="title" name="title" <?php if(!empty($id)):?>  value="<?php echo $row["title"];?>" <?php endif; ?> placeholder="例如:中文版"  />
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-2 control-label" for="title">名称(English)</label>
            <div class="col-md-6">
                 <input type="text"  class="form-control " id="title_en" name="title_en" <?php if(!empty($id)):?>  value="<?php echo $row["title_en"];?>" <?php endif; ?> placeholder="例如:Chinese"  />
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-2 control-label" for="title">当前模板风格</label>
            <div class="col-md-6">
                  <input type="text"  class="form-control" id="theme" name="theme" <?php if(!empty($id)):?>  value="<?php echo $row["theme"];?>"   <?php endif; ?> placeholder="例如默认模板风格：输入 default"  />
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-2 control-label" for="title">模板文件或语言包目录</label>
            <div class="col-md-6">
                  <input type="text"  class="form-control" id="landir" name="landir" placeholder="例如中文版：输入 cn" <?php if(!empty($id)):?>  value="<?php echo $row["landir"];?>"  <?php endif; ?>  />
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-2 control-label" for="title">网站名称</label>
            <div class="col-md-6">
                  <input type="text"  class="form-control" id="sitename" name="sitename" <?php if(!empty($id)):?>  value="<?php echo $row["sitename"];?>" <?php endif; ?> />
            </div>
        </div>  
         <div class="form-group">
            <label class="col-md-2 control-label" for="keywords">SEO标题</label>
            <div class="col-md-6">
                <textarea name="seotitle" id="seotitle" class="form-control"><?php if(!empty($id)):?><?php echo $row["seotitle"];?> <?php endif; ?></textarea>
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-2 control-label" for="keywords">SEO关键字</label>
            <div class="col-md-6">
                <textarea name="seokey" id="seokey" class="form-control"><?php if(!empty($id)):?><?php echo $row["seokey"];?> <?php endif; ?> </textarea>
            </div>
        </div>
      <div class="form-group">
            <label class="col-md-2 control-label" for="description">SEO简介</label>
            <div class="col-md-6">
                <textarea name="seodesc" id="seodesc" class="form-control"><?php if(!empty($id)):?><?php echo $row["seodesc"];?> <?php endif; ?> </textarea>
            </div>
        </div>                 
        
       <div class="form-group">
            <label class="col-md-2 control-label" for="title">网站LOGO</label>
            <div class="col-md-6">
              <script type="text/plain" id="upload_ue"></script>
            <div class="col-md-12" id="imglogobox" style="padding-left: 0">
            <input type="hidden" id="logo" name="logo" <?php if(!empty($row)):?>  value="<?php echo $row["logo"];?>" <?php endif; ?> />
    	      <?php if(empty($row['logo'])){ 
			  $strimgcss='style="display: none;"'; 
			  $picurl=YUNYECMS_PUBLIC.'img/addpic.png'; 
			  } else{ 
			  $strimgcss=''; 
			  $picurl=$row['logo']; 
			  } ?>
    	        <img src="<?php echo $picurl;?>" id="img_logo" style="width: 150px;border: solid 1px #ccc;padding: 2px; cursor: pointer;  " onclick="upImage();" /><br>
				<div style="margin-top:8px;">
					<a href="javascript:void(0);" onclick="upImage();" class="btn btn-primary btn-flat"><i class="fa fa-upload"></i> 点击上传图片 </a>
  <?php if(!empty($row["logo"])){ $strcanclecss="style=''";} else{ $strcanclecss="style='display:none;'" ;} ?>			
 <button type="button" class="btn bg-red btn-flat" id="cancle" <?php echo $strcanclecss; ?>><i class="fa fa-remove"></i> 取消图片</button>
				</div> 
              </div>
		<script type="text/javascript">
		var _editor;
		$(function() {
		//重新实例化一个编辑器，防止在上面的editor编辑器中显示上传的图片或者文件
		_editor = UE.getEditor('upload_ue');
		_editor.ready(function () {
		//设置编辑器不可用
		//_editor.setDisabled();
		//隐藏编辑器，因为不会用到这个编辑器实例，所以要隐藏
		_editor.hide();
		//侦听图片上传
		_editor.addListener('beforeInsertImage', function(t, arg) {
		//将地址赋值给相应的input,只去第一张图片的路径
		$("#logo").attr("value", arg[0].src);
		$("#img_logo").attr("src", arg[0].src);
		$("#imglogobox").show();
		//图片预览
		//$("#preview").attr("src", arg[0].src);
		})
	
		});
		}); 
		//弹出图片上传的对话框
		function upImage() {
		var myImage = _editor.getDialog("insertimage");
		myImage.open();
		}
		$("#cancle").click(function(){
					$("#logo").val('');
					$("#img_pic").attr("src","<?php echo YUNYECMS_PUBLIC;?>img/noimg.jpg");
				});	
			</script>
             
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-2 control-label" for="title">二维码</label>
            <div class="col-md-6">
              <script type="text/plain" id="upload_qrcode"></script>
            <div class="col-md-12" id="imgqrcodebox" style="padding-left: 0">
            <input type="hidden" id="qrcode" name="qrcode" <?php if(!empty($row)):?>  value="<?php echo $row["qrcode"];?>" <?php endif; ?> />
    	      <?php if(empty($row['qrcode'])){ 
			  $strimgcss='style="display: none;"'; 
			  $picurl=YUNYECMS_PUBLIC.'img/addpic.png'; 
			  } else{ 
			  $strimgcss=''; 
			  $picurl=$row['qrcode']; 
			  } ?>
    	        <img src="<?php echo $picurl;?>" id="img_qrcode" style="width: 150px;border: solid 1px #ccc;padding: 2px; cursor: pointer;  " onclick="upqrcode();" /><br>
				<div style="margin-top:8px;">
					<a href="javascript:void(0);" onclick="upqrcode();" class="btn btn-primary btn-flat"><i class="fa fa-upload"></i> 点击上传图片 </a>
 <button type="button" class="btn bg-red btn-flat" id="cancle_qrcode"  <?php if(!empty($row["qrcode"])){ echo "style=''";} else{ echo "style='display:none;'" ;} ?> ><i class="fa fa-remove"></i> 取消图片</button>
				</div> 
              </div>
		<script type="text/javascript">
		var _editor;
		$(function() {
		//重新实例化一个编辑器，防止在上面的editor编辑器中显示上传的图片或者文件
		_editor_qrcode = UE.getEditor('upload_qrcode');
		_editor_qrcode.ready(function () {
		//设置编辑器不可用
		//_editor.setDisabled();
		//隐藏编辑器，因为不会用到这个编辑器实例，所以要隐藏
		_editor_qrcode.hide();
		//侦听图片上传
		_editor_qrcode.addListener('beforeInsertImage', function(t, arg) {
		//将地址赋值给相应的input,只去第一张图片的路径
		$("#qrcode").attr("value", arg[0].src);
		$("#img_qrcode").attr("src", arg[0].src);
		$("#imgqrcodebox").show();
		//图片预览
		//$("#preview").attr("src", arg[0].src);
		})
		});
		});
		//弹出图片上传的对话框
		function upqrcode() {
		var myImage = _editor_qrcode.getDialog("insertimage");
		myImage.open();
		}
		$("#cancle_qrcode").click(function(){
					$("#qrcode").val('');
					$("#img_qrcode").attr("src","<?php echo YUNYECMS_PUBLIC;?>img/noimg.jpg");
				});	
			</script>
             
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-2 control-label" for="title">客服电话</label>
            <div class="col-md-6">
                <input type="text"  class="form-control " id="tel" name="tel" value="<?php if(!empty($row)){ echo $row["tel"]; } ?>" />
            </div>
            <div class="col-md-4">
				 多个客服电话请用逗号隔开
		    </div>
        </div> 
        <div class="form-group">
            <label class="col-md-2 control-label" for="title">手机</label>
            <div class="col-md-6">
                <input type="text"  class="form-control " id="mobile" name="mobile" value="<?php if(!empty($row)){ echo $row["mobile"]; } ?>" />
            </div>
             <div class="col-md-4">
				 多个手机请用逗号隔开
		     </div>
        </div>	
        <div class="form-group">
            <label class="col-md-2 control-label" for="title">客服邮箱</label>
            <div class="col-md-6">
                <input type="text"  class="form-control " id="mail" name="mail" value="<?php if(!empty($row)){ echo $row["mail"]; } ?>" />
            </div>
              <div class="col-md-4">
				 多个邮箱请用逗号隔开
				</div>
        </div>
          <div class="form-group">
            <label class="col-md-2 control-label" for="title">客服QQ</label>
            <div class="col-md-6">
                <input type="text"  class="form-control " id="qq" name="qq" value="<?php if(!empty($row)){ echo $row["qq"]; } ?>" />
            </div>
            <div class="col-md-4">
				 多个QQ请用逗号隔开
				</div>
        </div>			            						
          <div class="form-group">
            <label class="col-md-2 control-label" for="title">地址</label>
            <div class="col-md-6">
                <input type="text"  class="form-control " id="address" name="address" value="<?php if(!empty($row)){ echo $row["address"]; } ?>" />
            </div>
        </div>	
        
    <div class="form-group">
            <label class="col-md-2 control-label" for="content">版权</label>
            <div class="col-md-10">
    <script type="text/javascript" charset="utf-8" src="<?php echo YUNYECMS_PUBLIC;?>ueditor/lang/zh-cn/zh-cn.js"></script>
    <script id="content_editor" type="text/plain" style="width:100%;height:350px;" name="copyright"><?php if(!empty($id)) echo uhtmlspecialchars_decode($row["copyright"]);?></script>
	   <script type="text/javascript">
		var ue = UE.getEditor('content_editor',{     
         enterTag : 'br'  
           });  
	   </script>
      </div>
   </div>   
        <div class="form-group" >
            <label class="col-md-2 control-label" for="copyfrom">排序</label>
            <div class="col-md-6">
			  <?php if(empty($id)):?>
                <input type="text"  class="form-control " id="ordernum" name="ordernum" value="<?php echo $ordermax;?>" />
                <?php else: ?>         
                <input type="text"  class="form-control " id="ordernum" name="ordernum" value="<?php echo $row["ordernum"];?>" />
              <?php endif; ?>                   
            </div>
        </div>
       <div class="form-group">
				<label class="col-md-2 control-label" for="title">默认语言</label>
				<div class="col-md-6">
			    <input type="radio" name="isdefault" value="1" class="minimal-blue"  <?php if(!empty($id)) { if($row['isdefault']==1) echo "checked";} ?>   >  是   
				   <input type="radio" name="isdefault" value="0" class="minimal-blue"  <?php if(empty($id)){ echo "checked";}else{ if($row["isdefault"]==0) echo  "checked"; } ?>  >      否 &nbsp;
				</div>
		 </div>                                                                                                 
              </div>
              <!-- /.box-body -->
              <div class="box-footer">
              <div class="form-group">
                 <div class="col-md-offset-3 col-md-9">
					 <input name="yyact" type="hidden" value="<?php echo $yyact;?>">
                     <input name="c" type="hidden" value="<?php echo ROUTE_C;?>">
                     <input name="a" type="hidden" value="<?php echo ROUTE_A;?>">
                     <?php echo $this->hashurl['svp'];?>
                     <?php if(!empty($id)):?>
                     <input name="id" type="hidden" value="<?php echo $id;?>">
                     <input name="oldtitle" type="hidden" id="oldtitle" value="<?php echo $row['title'];?>"> 
                     <input name="oldlandir" type="hidden" id="oldlandir" value="<?php echo $row['landir'];?>"> 
                     <?php  endif?>
                     <button type="submit" class="btn bg-blue  btn-flat"><?php  if($yyact=="add"):?> <i class="icon fa fa-plus"></i> 添 加 <?php elseif($yyact=="edit"):?> <i class="icon fa fa-edit"></i> 修 改 <?php  endif?></button> &nbsp; &nbsp;
                     <button type="reset" class="btn btn-default btn-flat"> <i class="fa fa-undo"></i> 重 置 </button>                                         
            </div>	
              </div>
              </div>
              <!-- /.box-footer -->
            </form>
          </div>
           <div class="callout callout-info">
                <h4><i class="icon fa fa-info"></i> 温馨提示：</h4>
                 1.当前模板风格对应主题目录theme目录下面的目录，比如default风格的对应目录为theme/default。<br/>
                 2.模板文件或语言包目录:该项设置对应两个地方。<br>
                 一、该语言的模板文件目录，比如中文版cn,则对应theme/default/cn<br/>
                 二、对应语言包目录，如：core/language/cn
           </div>
        </div>
        <!--/.col (left) -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
<?php require tpl_adm('foot');?>

<!-- jQuery 2.2.3 -->
<script src="<?php echo YUNYECMS_UI;?>plugins/jQuery/jquery-2.2.3.min.js"></script>
<!-- Bootstrap 3.3.6 -->
<script src="<?php echo YUNYECMS_UI;?>bootstrap/js/bootstrap.min.js"></script>
<!-- FastClick -->
<!-- AdminLTE for demo purposes -->
<script src="<?php echo YUNYECMS_UI;?>dist/js/admin.js"></script>
<script src="<?php echo YUNYECMS_UI;?>plugins/iCheck/icheck.min.js"></script>

<script type="text/javascript" src="<?php echo YUNYECMS_UI;?>validator/dist/js/bootstrapValidator.js"></script>
<script>
  $(function () {
    //iCheck for checkbox and radio inputs
    $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
      checkboxClass: 'icheckbox_minimal-blue',
      radioClass: 'iradio_minimal-blue'
    });
    //Red color scheme for iCheck
    $('input[type="checkbox"].minimal-red, input[type="radio"].minimal-red').iCheck({
      checkboxClass: 'icheckbox_minimal-red',
      radioClass: 'iradio_minimal-red'
    });
    //Flat red color scheme for iCheck
    $('input[type="checkbox"].flat-blue, input[type="radio"].flat-blue').iCheck({
      checkboxClass: 'icheckbox_flat-blue',
      radioClass: 'iradio_flat-blue'
    });
  });
</script>
<script type="text/javascript">
$(document).ready(function() {
    $('#defaultForm').bootstrapValidator({
        message: 'This value is not valid',
//        live: 'disabled',
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            title: {
                message: '栏目名称是无效的',
                validators: {
                    notEmpty: {
                        message: '栏目名称必需填写'
                    },
                   /* remote: {
                        url: 'user_add.php',
                        message: '部门名是不可用的'
                    },*/
                }
            },
        }
    });
});
</script>
<script language="javascript" type="text/javascript">
        $(function() {
			var navudinfo="<?php echo $parnav;?>";
			$('.breadcrumb',window.parent.document).children('#homeitem').nextAll().remove();
			$('.breadcrumb',window.parent.document).children('#homeitem').after(navudinfo);
        });
</script>

    <script>
			  $(function () {
				//iCheck for checkbox and radio inputs
				$('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
				  checkboxClass: 'icheckbox_minimal-blue',
				  radioClass: 'iradio_minimal-blue'
				});
				//Red color scheme for iCheck
				$('input[type="checkbox"].minimal-red, input[type="radio"].minimal-red').iCheck({
				  checkboxClass: 'icheckbox_minimal-red',
				  radioClass: 'iradio_minimal-red'
				});
				$('input[type="checkbox"].minimal-blue, input[type="radio"].minimal-blue').iCheck({
				  checkboxClass: 'icheckbox_minimal-blue',
				  radioClass: 'iradio_minimal-blue'
				});
				//Flat red color scheme for iCheck
				  $('input[type="checkbox"].flat-blue, input[type="radio"].flat-blue').iCheck({
				  checkboxClass: 'icheckbox_flat-blue',
				  radioClass: 'iradio_flat-blue'
				});
			  });
     </script>


</body>
</html>