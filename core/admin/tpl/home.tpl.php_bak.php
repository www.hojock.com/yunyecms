<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title> 云业内容管理系统-YUNYECMS <?php echo YUNYECMS_VERSION;?></title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="<?php echo YUNYECMS_UI;?>bootstrap/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link href="<?php echo YUNYECMS_PUBLIC;?>plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" />
  <link rel="stylesheet" href="<?php echo YUNYECMS_UI;?>plugins/font-awesome-4.7.0/css/font-awesome.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo YUNYECMS_UI;?>dist/css/AdminLTE.min.css">
  <link rel="stylesheet" href="<?php echo YUNYECMS_UI;?>plugins/jvectormap/jquery-jvectormap-1.2.2.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="<?php echo YUNYECMS_UI;?>dist/css/skins/_all-skins.min.css">
  <link rel="stylesheet" href="<?php echo YUNYECMS_UI;?>dist/css/admin.css">
<link rel="stylesheet" href="<?php echo YUNYECMS_UI;?>validator/dist/css/bootstrapValidator.css"/>
  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
</head>
<body class="hold-transition rightbgcolor"  onload="changefrmHeight()">
  <!-- Content Wrapper. Contains page content -->
  <div class="container-fluid" id="mainwrap">
    <!-- Main content -->
    <section class="content">
      <!-- Info boxes -->
      <div class="row">
        <div class="col-md-12">
        	<div class="box box-info">
            <div class="box-header with-border">
              <h3 class="box-title">内容</h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
			  <?php foreach($category as $key=>$var):?>
						   <div class="col-md-2 col-sm-2 col-xs-6" style="margin-bottom: 10px;">
							<a href="<?php echo $var['url']; ?>" class="btn btn-app btn-flat" style="width: 95%;">
											<i class="<?php echo $var['colcss']; ?>"></i>
											<div><?php echo $var['title']; ?></div>
											<span class="badge bg-aqua"><?php echo $var['cnt']; ?></span>
							</a>
							</div>
              <?php endforeach ?>           
                              <div  class="clearfix"> </div>              
       
              <!-- /.table-responsive -->
            </div>
            <!-- /.box-body -->
        
            <!-- /.box-footer -->
          </div>
        </div>       
        
        <!-- /.col -->
        
        <!-- /.col -->

        <!-- fix for small devices only -->
        <div class="clearfix visible-sm-block"></div>

        
        <!-- /.col -->
        
        <!-- /.col -->
      </div>
      <!-- /.row -->
      <div class="row">
      	 <?php if(!empty($msgto)):?>
      	 <div class="col-md-6">
      	 	<div class="box box-info">
            <div class="box-header with-border">
              <h3 class="box-title">最新系统消息</h3>
              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <div class="table-responsive">
                <table class="table no-margin">
                  <thead>
                  <tr>
                    <th>标题</th>
                    <th>时间</th>
                  </tr>
                  </thead>
                  <tbody>
                    <?php foreach($msgto as $key=>$vo): ?> 
					   <tr>
                     <td><?php echo $vo['icon'];?> <a href="<?php echo $vo['url'];?>"><?php echo strcut($vo['title'],66);?></a></td>
                     <td><?php echo yytime($vo['addtime']);?></td>
                     </tr>
                    <?php endforeach ?> 
                  </tbody>
                </table>
              </div>
              <!-- /.table-responsive -->
            </div>
            <!-- /.box-body -->
            <div class="box-footer clearfix">
              <a href="<?php echo url_admin('init','msg');?>" class="btn btn-sm btn-info btn-flat pull-right">查看全部</a>
            </div>
            <!-- /.box-footer -->
          </div>
      	 </div>
      	  <?php endif;?>
      	 <?php if(!empty($customfromto)):?>
      	 <div class="col-md-6">
      	 	<div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">最新<?php echo $formcat["title"];?></h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <div class="table-responsive">
                <table class="table no-margin">
                  <thead>
                  <tr>
                    <th>姓名</th>
                    <th>标题</th>
                    <th>内容</th>
                    <th>时间</th>
                  </tr>
                  </thead>
                  <tbody>
                    <?php foreach($customfromto as $key=>$vo): ?> 
					  <tr>
						<td><a href="<?php echo url_admin('customform_view','content',array('catid'=>$vo["catid"],'id'=>$vo["id"]));?>">  <?php echo $vo['name'];?></a></td>
						<td><?php echo $vo['title'];?></a></td>
						<td><?php echo strcut($vo['remark'],50);?></td>
						<td>
						<?php echo yytime($vo['addtime']);?>
						</td>
					  </tr>
                  <?php endforeach ?> 
                  </tbody>
                </table>
              </div>
              <!-- /.table-responsive -->
            </div>
            <!-- /.box-body -->
            <div class="box-footer clearfix">
              <a href="<?php echo $formurl;?>" class="btn btn-sm btn-info btn-flat pull-right">查看全部</a>
            </div>
            <!-- /.box-footer -->
          </div>
      	 </div>
      	 <?php endif;?>
      	 
      </div> 
         <div class="row">
      	 <?php if(!empty($newmember)):?>
      	 <div class="col-md-6">
      	 	<div class="box box-warning">
            <div class="box-header with-border">
              <h3 class="box-title">最新会员</h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <div class="table-responsive">
                <table class="table no-margin">
                  <thead>
                  <tr>
                    <th>用户名</th>
                    <th>手机号</th>
                    <th>姓名</th>
                    <th>E-mail</th>
                    <th>时间</th>
                  </tr>
                  </thead>
                  <tbody>
                    <?php foreach($newmember as $key=>$vo): ?> 
                  <tr>
                    <td><?php echo $vo['username'];?></td>
                    <td><?php echo $vo['mobile'];?></td>
                    <td><?php echo $vo['name'];?></td>
                    <td><?php echo $vo['email'];?></td>
                    <td><?php echo yytime($vo['addtime']);?></td>
                  </tr>
                  <?php endforeach ?> 
                  </tbody>
                </table>
              </div>
              <!-- /.table-responsive -->
            </div>
            <!-- /.box-body -->
            <div class="box-footer clearfix">
              <a href="<?php echo url_admin('init','member');?>" class="btn btn-sm btn-info btn-flat pull-right">查看全部</a>
            </div>
            <!-- /.box-footer -->
          </div>
      	 </div>
      	 <?php endif;?>
      	 
      	 
      	 <?php if(!empty($neworders)):?>
      	 <div class="col-md-6">
      	 	<div class="box box-danger">
            <div class="box-header with-border">
              <h3 class="box-title">最新订单</h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <div class="table-responsive">
                <table class="table no-margin">
                  <thead>
                  <tr>
                    <th >订单号</th>
					<th >商品</th>
					<th >金额</th>
					<th >会员</th>
                  </tr>
                  </thead>
                  <tbody>
                   <?php foreach($neworders as $key=>$vo): ?> 
                  <tr>
                   <td><?php echo  $vo["sn"];?></td>
                   <td align="left" style="text-align:left;">
                      <?php foreach($vo['goods'] as $k=>$v):?>                  
                                       <a href="<?php echo $v["url"];?>" target="_blank"> <?php echo $v["title"];?> <span><svg 
                                             xmlns="http://www.w3.org/2000/svg"
                                             xmlns:xlink="http://www.w3.org/1999/xlink"
                                             width="8px" height="8px">
                                            <path fill-rule="evenodd"  fill="rgb(130, 135, 167)"
                                             d="M0.070,7.929 C0.118,7.976 0.179,8.000 0.241,8.000 C0.302,8.000 0.364,7.976 0.411,7.929 L3.992,4.341 L7.573,7.929 C7.620,7.976 7.682,8.000 7.743,8.000 C7.805,8.000 7.867,7.976 7.914,7.929 C8.008,7.835 8.008,7.682 7.914,7.588 L4.333,4.000 L7.914,0.412 C8.008,0.318 8.008,0.165 7.914,0.070 C7.820,-0.024 7.667,-0.024 7.573,0.070 L3.992,3.658 L0.411,0.071 C0.317,-0.024 0.165,-0.024 0.070,0.071 C-0.024,0.165 -0.024,0.318 0.070,0.412 L3.651,4.000 L0.070,7.588 C-0.024,7.682 -0.024,7.835 0.070,7.929 Z"/>
                                            </svg></span>   <?php echo $v["num"];?>
                                            ￥<?php echo $v["price"]*$v["num"];?>元 </a ><br>
                   <?php endforeach; ?>
                </td> 
				 <td>￥<?php echo  $vo["money"];?></td>
				 <td><?php echo  $vo["member"]["username"];?></td>
                  </tr>
                  <?php endforeach ?> 
                
                  </tbody>
                </table>
              </div>
              <!-- /.table-responsive -->
            </div>
            <!-- /.box-body -->
            <div class="box-footer clearfix">
              <a href="<?php echo url_admin('init','orders');?>" class="btn btn-sm btn-info btn-flat pull-right">查看全部</a>
            </div>
            <!-- /.box-footer -->
          </div>
      	 </div>
      	 <?php endif;?>
      </div> 
      <div class="row">
      	 <div class="col-md-6">
      	 	<div class="box box-default">
            <div class="box-header with-border">
              <h3 class="box-title">系统信息</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <div class="table-responsive">
                <table class="table no-margin">
                  <thead>
                  <tr>
                    <th>系统名称：云业内容管理系统 &nbsp;&nbsp;&nbsp;&nbsp; [云业CMS或yunyecms]</th>
                  </tr>
                  </thead>
                  <tbody>
                  <tr>
                    <td>品牌名称：yunyecms <span class="text-red">(已注册)</span></td>
                  </tr>
                   <tr>
                    <td>著作权登记号：2018SR208761 </td>
                  </tr>
                  <tr>
                    <td>版权所有：洛阳云业信息科技有限公司 </td>
                  </tr>
                  <tr>
                    <td>版本：V1.0 </td>
				  </tr>
                  <tr>
                    <td>官方网站：<a href="http://www.yunyecms.com" target="_blank">http://www.yunyecms.com</a></td>
				  </tr>
                  <tr>
                    <td>手机/微信： 15937957328 &nbsp;&nbsp;  电话： 0379-62210683 &nbsp;&nbsp;  QQ： 527150978/15003219  </td>
				  </tr>
                  </tbody>
                </table>
              </div>
              <!-- /.table-responsive -->
            </div>
          </div>
      	 </div>
      	 <div class="col-md-6">
      	 	<div class="box box-success">
            <div class="box-header with-border">
              <h3 class="box-title">授权信息</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <div class="table-responsive">
                <table class="table no-margin">
                  <thead>
                  <tr>
                    <th>程序名称：YUNYECMS <?php echo YUNYECMS_VERSION;?> Release 20190219</th>
                  </tr>
                  </thead>
                  <tbody>
                  <tr>
                    <td>授权给：洛阳云业信息科技有限公司</td>
                  </tr>
                   <tr>
                    <td>授权域名：www.yunyecms.com </td>
                  </tr>
                  <tr>
                    <td>序列号：0000-0000-0000-0000 </td>
                  </tr>
                   <tr>
                    <td>授权期限：2018-01-01～永久 </td>
                  </tr>
                  </tbody>
                </table>
              </div>
              <!-- /.table-responsive -->
            </div>
            <!-- /.box-body -->
            <div class="box-footer clearfix">
              <a href="<?php echo url_admin('init','orders');?>" class="btn btn-sm btn-info btn-flat pull-right">查看全部</a>
            </div>
            <!-- /.box-footer -->
          </div>
      	 </div>
      </div>
      <!-- Main row -->
      
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
<?php require tpl_adm('foot');?>
<!-- jQuery 2.2.3 -->
<script src="<?php echo YUNYECMS_UI;?>plugins/jQuery/jquery-2.2.3.min.js"></script>
<!-- Bootstrap 3.3.6 -->
<script src="<?php echo YUNYECMS_UI;?>bootstrap/js/bootstrap.min.js"></script>
<!-- FastClick -->
<script src="<?php echo YUNYECMS_UI;?>plugins/fastclick/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="<?php echo YUNYECMS_UI;?>dist/js/app.min.js"></script>
<!-- Sparkline -->
<script src="<?php echo YUNYECMS_UI;?>plugins/sparkline/jquery.sparkline.min.js"></script>
<!-- jvectormap -->
<script src="<?php echo YUNYECMS_UI;?>plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
<script src="<?php echo YUNYECMS_UI;?>plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
<!-- SlimScroll 1.3.0 -->
<script src="<?php echo YUNYECMS_UI;?>plugins/slimScroll/jquery.slimscroll.min.js"></script>
<!-- ChartJS 1.0.1 -->
<script src="<?php echo YUNYECMS_UI;?>plugins/chartjs/Chart.min.js"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script src="<?php echo YUNYECMS_UI;?>dist/js/pages/dashboard2.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="<?php echo YUNYECMS_UI;?>dist/js/demo.js"></script>
<script language="javascript" type="text/javascript">
        $(function() {
			var navudinfo="<?php echo $parnav;?>";
			$('.breadcrumb',window.parent.document).children('#homeitem').nextAll().remove();
			$('.breadcrumb',window.parent.document).children('#homeitem').after(navudinfo);
        });
</script>
</body>
</html>