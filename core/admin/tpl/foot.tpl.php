<footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b><?php echo YUNYECMS_VERSION;?></b> 
    </div>
    <strong>Copyright &copy; 2019 洛阳云业信息科技有限公司 <a href="http://www.yunyecms.com" target="_blank">www.yunyecms.com</a>.</strong> All rights
    reserved.
</footer>