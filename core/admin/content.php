<?php
defined('IN_YUNYECMSAdmin') or exit('No permission.');
core::load_admin_class('admin');
class content extends YUNYE_admin {
	private $db;
	private $admuser;
	private $curmodel;  //当前模型
	private $curcat;   //当前栏目
	private $rootcat;  //当前根栏目
	function __construct() {
		$this->db = core::load_model('content_model');
		$this->admuser=IsAdmLogin($this->db);
		parent::__construct();
			if(!getroot('info','view')){
					messagebox(Lan('no_permission_infoview'),'back',"warn");			
			 } 			
		if(!empty($_REQUEST['catid'])){
				 $catid=trim($_REQUEST['catid']);
				 if(!is_numeric($catid)){
						messagebox("错误的参数","back",'warn');		
				  }
				 $catid=usafestr($catid);
			    $this->curcat=$this->db->find("select * from `#yunyecms_category` where id={$catid} ");
			    if(empty($this->curcat)){
						   messagebox("栏目不存在或参数错误",$_SERVER['HTTP_REFERER']);			
				}else{
			       $modelid=$this->curcat['modelid'];
				   if(empty($modelid)){
					   messagebox('您选择的栏目有误，请重新选择!',$_SERVER['HTTP_REFERER'],"warn");
					}else{
						$this->curmodel=$this->db->find("select * from `#yunyecms_model` where modelid={$modelid} ");
					}
				}
				if(empty($this->curmodel)){
					 messagebox('该分类所在的数据模型不存在，请重新选择!',$_SERVER['HTTP_REFERER'],"warn");
				}	
			    $ppid=getppid($catid);
			    if(!empty($ppid)){
				   $this->rootcat=$this->db->find("select * from `#yunyecms_category` where id={$ppid} ");
				}
		 }else{
			 messagebox("栏目不存在或参数错误",$_SERVER['HTTP_REFERER']);			
		 }
		
	}
	 //加载首页
	  public function lists() {
		  $aa=getroot('info','view');
		 if(!getroot('info','view')){
				messagebox(Lan('no_permission_infoview'),'back',"warn");			
		  }else{
			 //取得栏目权限
			 $catpower=fetch_cat_power();
			 if(!empty($catpower)){
				 //取得拥有查看权限的分类ID
				 $catpwerid=get_power_catid($catpower);
			 }
		 } 	
		 $tablename="m_".$this->curmodel['tablename'];
		 $parnav='<li><a href=\"'.url_admin('init',"category").'\" target=\"maincontent\">内容</a></li><li><a href=\"'.url_admin('lists','content',array("catid"=>$this->curcat['id'])).'\" target=\"maincontent\">'.$this->rootcat['title'].'</a></li><li class=\"active\">'.$this->curcat['title'].'</li>';
		  $pagesize=20;
		  $sqlquery="select * from `#yunyecms_{$tablename}`  ";
		  $where=" where 1=1  ";
		  $sqlcnt=" select count(*) from `#yunyecms_{$tablename}` ";
		  $order=" order by `addtime` desc ";
		  if(isset($_REQUEST)){
		   if(!empty($_REQUEST["searchkey"])){
		        $searchkey=usafestr(trim($_REQUEST["searchkey"]));
		        $where=$where." and ( `title`  like '%{$searchkey}%' or  `title_en`  like '%{$searchkey}%' )";
			  }
			 $catid=trim($_REQUEST['catid']);
				 if(!is_numeric($catid)){
						messagebox("错误的参数","back",'warn');		
				  }
				 $catid=usafestr($catid);
			 if(!empty($catid)){
				   $subcat=get_cat_child($catid);
	               $catidstr=implode(',', $subcat);
				   $newcat=array_intersect($subcat,$catpwerid);
				   $messagetip="暂时没有{$this->curcat["title"]}信息";
				   if(!empty($catpwerid)){
					     if(empty($newcat)){
							 messagebox("您没有管理{$this->curcat["title"]}信息的权限",'back',"warn");			
							}else{
							    $catidstr=implode(',', $newcat);
							    $where=$where." and catid in($catidstr)";
						   }
				   }else{
		              $where=$where." and catid in($catidstr)";
				   }
			  }
			 if(getroot('info','self')){
					 $where=$where." and admin_userid ={$this->admuser["userid"]}";
			 } 
		 }
		 $pagearr=$this->db->pagelist($sqlcnt,$sqlquery,$where,$order,$pagesize);
		 if($pagearr["count"]!=0){
			 $list=$pagearr["query"];
			 foreach($list as $key=>$var){
				  $modelarr=$this->db->find("select modelid  from `#yunyecms_category` where `id`= {$var['catid']}");
				  if($modelarr){
				     $list[$key]["modelid"]=$modelarr["modelid"];
				  }
			 }
			 $pages=$pagearr["page"];
		 }
		 require tpl_adm('infolist');
	  }
	
	
 public function infolist_add(){
				$tablename="m_".$this->curmodel['tablename'];
				$modelfields=$this->db->select("select * from `#yunyecms_modelfields`  where modelid={$this->curmodel['modelid']} and language=1 and issys=0 and isadd=1 order by ordernum ");
				$modelfields_en=$this->db->select("select * from `#yunyecms_modelfields`  where modelid={$this->curmodel['modelid']} and language=2 and issys=0 and isadd=1  order by ordernum ");
				$morepicfield=$this->db->find("select * from `#yunyecms_modelfields`  where modelid={$this->curmodel['modelid']} and  issys=1 and fdname='morepic'");
				$membergroup=$this->db->select("select * from `#yunyecms_membergroup` order by ordernum asc");
			   if(!empty($_REQUEST["id"])){
						 $parnav='<li><a href=\"'.url_admin('init',"category").'\" target=\"maincontent\">内容</a></li><li><a href=\"'.url_admin('lists','content',array("catid"=>$this->curcat['id'])).'\" target=\"maincontent\">'.$this->rootcat['title'].'</a></li><li class=\"active\">'.$this->curcat['title'].'</li>';
						 $id=trim($_REQUEST["id"]);
						 if(!is_numeric($id)){
						   messagebox("参数必须为数字，谢谢!",$_SERVER['HTTP_REFERER']);
						 }
			             $id=usafestr($id);
						 $rsinfo=$this->db->find("select * from `#yunyecms_{$tablename}` where `id`= {$id}");
						 if(empty($rsinfo)){
							   messagebox("信息不存在",$_SERVER['HTTP_REFERER']);			
						  }
						$morepic=$rsinfo["morepic"];
						if(!empty($morepic)){
						  $morepicarr=unserialize($morepic);
						  $morepicstr=implode(",",$morepicarr);
						 }				   
						$yyact="edit";
				}else{
				     $yyact=yyact_get("add");
					 $parnav='<li><a href=\"'.url_admin('init',"category").'\" target=\"maincontent\">内容</a></li><li><a href=\"'.url_admin('lists','content',array("catid"=>$this->curcat['id'])).'\" target=\"maincontent\">'.$this->rootcat['title'].'</a></li><li class=\"active\">'.$this->curcat['title'].'</li>';
					 $catid=$this->curcat['id'];
				   	  if(!getroot('info','add')){
							messagebox(Lan('no_permission_infoadd'),'back',"warn");			
					  }else{
					    if(!get_cat_power($catid,"add")){
						    messagebox("您没有添加{$this->curcat["title"]}信息的权限",'back',"warn");			
					      }
					  }
			   		$strmaxarray=$this->db->find("select max(ordernum) as maxorder  from `#yunyecms_{$tablename}` where `catid`= {$this->curcat['id']}");
						$strmax=$strmaxarray["maxorder"]+1;
		   }
	
	        if(isset($_POST["yyact"])){
					  $catpower=fetch_cat_power();
				      $_POST=ustripslashes($_POST);
				      $modelfields_all=$this->db->select("select * from `#yunyecms_modelfields`  where modelid={$this->curmodel['modelid']}  and issys=0 ");
					  if(!empty($_POST['morepic'])){
						   $morepic=$_POST['morepic'];
						   $morepicstr=explode(",",$morepic);
						   $morepicstr=serialize($morepicstr);
						   $data["morepic"]=$morepicstr;
					   }		
				 	  $data["ispower"]=usafestr(trim($_POST["ispower"]));
				 	  $data["catid"]=usafestr(trim($_POST["catid"]));
				      $data["isgood"]=empty($_POST["isgood"])?0:usafestr(trim($_POST["isgood"]));
				 	  $data["title"]=usafestr(trim($_POST["title"]));
				 	  $data["pic"]=uhtmlspecialchars(trim($_POST["pic"]));
				      $data["exlink"]=uhtmlspecialchars(trim($_POST["exlink"]));				 	 if(uhtmlspecialchars(trim($_POST["picfile"])))$data["picfile"]=uhtmlspecialchars(trim($_POST["picfile"]));
				 	  $data["summary"]=usafestr(trim($_POST["summary"]));
				 	  $data["seotitle"]=usafestr(trim($_POST["seotitle"]));
				 	  $data["seokeywords"]=usafestr(trim($_POST["seokeywords"]));
				 	  $data["seodesc"]=usafestr(trim($_POST["seodesc"]));
				 	  $data["ordernum"]=usafestr(trim($_POST["ordernum"]));
				 	  $data["title_en"]=usafestr(trim($_POST["title_en"]));
				 	  $data["exlink_en"]=uhtmlspecialchars(trim($_POST["exlink_en"]));
				 	  $data["summary_en"]=usafestr(trim($_POST["summary_en"]));
				 	  $data["seotitle_en"]=usafestr(trim($_POST["seotitle_en"]));
				 	  $data["seokeywords_en"]=usafestr(trim($_POST["seokeywords_en"]));
				 	  $data["seodesc_en"]=usafestr(trim($_POST["seodesc_en"]));
				 	  $data["status"]=1;
					  foreach($modelfields_all as $key=>$var){
						 $fdname=$var['fdname'];
						 $formctrl=$var['formctrl'];
						 $fdtitle=$var['fdtitle'];
						 $isunique=$var['isunique'];
						  if(isset($_POST[$fdname])&& !empty($_POST[$fdname])){
						     $data[$fdname]=uhtmlspecialchars($_POST[$fdname]);
						  }
						 if($formctrl=='checkbox'){
								$postfdname=$_POST[$fdname];
								if(!empty($postfdname)){
									$postfdname=implode(",",$postfdname);
						            $data[$fdname]=$postfdname;
								}
							}
					   }
					   if(empty($data["title"])){
							messagebox("标题不能为空，谢谢!",$_SERVER['HTTP_REFERER']);		
					   }
					   if(empty($data["catid"])){
							messagebox("栏目不能为空，请重新选择!",$_SERVER['HTTP_REFERER']);		
					   }
					   if(!is_numeric($data["catid"])){
							messagebox("栏目参数错误，请重新选择!",$_SERVER['HTTP_REFERER']);		
						 }
				     $catid=$data["catid"];
			      if($_POST["yyact"]=="add"){
				 	 $data["addtime"]=time();
				 	 $data["admin_userid"]=$this->admuser["userid"];
					 $retres=$this->db->insert($data,$tablename);
					 if($retres){
								$doing="添加{$this->curcat['title']}信息—".$data["title"];
								$yyact="addinfolist";
								insert_admlogs($doing,$yyact);
								messagebox("添加{$this->curcat['title']}信息成功！",url_admin('lists','content',array('catid'=>$catid)),"success");
					 }else{
								messagebox("添加{$this->curcat['title']}信息失败！",url_admin('lists','content',array('catid'=>$catid)),"error");
					 }
			  }
		      if($_POST["yyact"]=="edit"){
				   if(!getroot('info','edit')){
							messagebox(Lan('no_permission_infoedit'),'back',"warn");			
					  }else{
					    if(!get_cat_power($data["catid"],"edit")){
						     messagebox("您没有编辑{$this->curcat["title"]}信息的权限",'back',"warn");			
					      }
					  }					  
					   $id=$_POST["id"];
				 	   $data["addtime"]=strtotime(trim($_POST["addtime"]));
					   if(!$this->check_exist($id)){
							messagebox("该内容不存在！",url_admin('init'),"warn");
					   }
					   $retres=$this->db->update($data,$tablename,"id={$id}");
						if($retres){
								$doing="更新{$this->curcat['title']}信息—".$data["title"];
									$yyact="updateinfolist";
									insert_admlogs($doing,$yyact);
									messagebox("更新{$this->curcat['title']}信息成功！",url_admin('lists','content',array('catid'=>$catid)),"success");
						 }else{
									messagebox("更新{$this->curcat['title']}信息失败！",url_admin('lists','content',array('catid'=>$catid)),"error");
						 }
			  }			  
		  }
	 
		require tpl_adm('infolist_add');
	 }	
	
	
	
    public function singlepage() {
		if(!empty($_REQUEST["id"])||!empty($_REQUEST["catid"])){
			$tablename="m_".$this->curmodel['tablename'];
			$modelfields=$this->db->select("select * from `#yunyecms_modelfields`  where modelid={$this->curmodel['modelid']} and language=1 and issys=0 ");
			$modelfields_en=$this->db->select("select * from `#yunyecms_modelfields`  where modelid={$this->curmodel['modelid']} and language=2 and issys=0 ");		
				 $parnav='<li><a href=\"'.url_admin('init',"category").'\" target=\"maincontent\">内容</a></li><li><a href=\"'.url_admin('lists','content',array("catid"=>$this->curcat['id'])).'\" target=\"maincontent\">'.$this->rootcat['title'].'</a></li><li class=\"active\">'.$this->curcat['title'].'</li>';
				 $catid = $_REQUEST['catid'];
				 $id=trim($catid);
				 if(!is_numeric($id)){
				     messagebox("参数必须为数字！");
				  }
				 $id=usafestr(trim($id));
				 $catid=usafestr(trim($catid));
			     $check=$this->db->GetCount("select count(*) from `#yunyecms_{$tablename}`  where id=$catid");
					if($check<=0){
						$data['id']=$catid;
						$data['catid']=$catid;
						$data['title']=$this->curcat['title'];
						$data['seotitle']=$this->curcat['title'];
						$data['seokeywords']=$this->curcat['keywords'];
						$data['seodesc']=$this->curcat['description'];
						$data['seotitle_en']=$this->curcat['title_en'];
						$data['seokeywords_en']=$this->curcat['keywords_en'];
						$data['seodesc_en']=$this->curcat['description_en'];
						$retres=$this->db->insert($data,$tablename);
					}			
				 $rsinfo=$this->db->find("select * from `#yunyecms_{$tablename}` where `id`= {$id}");
				 if(empty($rsinfo)){
				     messagebox("栏目不存在！");
				  }
				   if(!getroot('info','view')){
							messagebox(Lan('no_permission_infoview'),'back',"warn");			
					  }else{
					    if(!get_cat_power($catid,"view")){
						    messagebox("您没有管理{$this->curcat["title"]}信息的权限",'back',"warn");			
					      }
					  }				
			
			
				$yyact="edit";
		}else{
			 messagebox("栏目参数错误，请重新选择!");
		}
		
     if(isset($_POST["yyact"])){
				
				      $modelfields_all=$this->db->select("select * from `#yunyecms_modelfields`  where modelid={$this->curmodel['modelid']}  and issys=0 ");	
				      $_POST=ustripslashes($_POST);
				 	  $data["catid"]=usafestr(trim($_POST["catid"]));
				 	  $data["title"]=usafestr(trim($_POST["title"]));
				 	  $data["pic"]=uhtmlspecialchars(trim($_POST["pic"]));
				 	  $data["summary"]=usafestr(trim($_POST["summary"]));
				 	  $data["seotitle"]=usafestr(trim($_POST["seotitle"]));
				 	  $data["seokeywords"]=usafestr(trim($_POST["seokeywords"]));
				 	  $data["seodesc"]=usafestr(trim($_POST["seodesc"]));
				 	  $data["title_en"]=usafestr(trim($_POST["title_en"]));
				 	  $data["summary_en"]=usafestr(trim($_POST["summary_en"]));
				 	  $data["seotitle_en"]=usafestr(trim($_POST["seotitle_en"]));
				 	  $data["seokeywords_en"]=usafestr(trim($_POST["seokeywords_en"]));
				 	  $data["seodesc_en"]=usafestr(trim($_POST["seodesc_en"]));
		              if(isset($_POST["content"])||isset($_POST["content_en"])){
				 	     $data["content"]=uhtmlspecialchars(trim($_POST["content"]));
					  }
		              if(isset($_POST["content_en"])){
				 	     $data["content_en"]=uhtmlspecialchars(trim($_POST["content_en"]));
					  }		 
					  foreach($modelfields_all as $key=>$var){
						 $fdname=$var['fdname'];
						  if(isset($_POST[$fdname])){
						     $data[$fdname]=uhtmlspecialchars(trim($_POST[$fdname]));
						  }
					   }
					   if(empty($data["title"])){
							messagebox("标题不能为空，谢谢!",$_SERVER['HTTP_REFERER']);		
					   }
					   if(empty($data["catid"])){
							messagebox("栏目不能为空，请重新选择!",$_SERVER['HTTP_REFERER']);		
					   }
					   if(!is_numeric($data["catid"])){
							messagebox("栏目参数错误，请重新选择!",$_SERVER['HTTP_REFERER']);		
						 }
			          $catid=$data["catid"];
				     if(!getroot('info','edit')){
							messagebox(Lan('no_permission_infoedit'),'back',"warn");			
					  }else{
					    if(!get_cat_power($catid,"edit")){
						    messagebox("您没有编辑{$this->curcat["title"]}信息的权限",'back',"warn");			
					      }
					  }			 
		 
		      if($_POST["yyact"]=="edit"){
					   $id=$_POST["id"];
					   if(!$this->check_exist($id)){
							messagebox("该内容不存在！");
					   }
					    $retres=$this->db->update($data,$tablename,"id={$id}");
						if($retres){
								$doing="更新{$this->curcat['title']}信息—".$data["title"];
								$yyact="updatesinglepage";
								insert_admlogs($doing,$yyact);
								messagebox("更新{$this->curcat['title']}信息成功！",url_admin('singlepage','content',array('catid'=>$catid)),"success");
						 }else{
								messagebox("更新{$this->curcat['title']}信息失败！",url_admin('singlepage','content',array('catid'=>$catid)),"error");
						 }
			  }			  
		  }		
		
		require tpl_adm('singlepage');
    }	
	
	 //加载首页
	public function customform() {
		 if(!getroot('info','view')){
				messagebox(Lan('no_permission_infoview'),'back',"warn");			
		  }else{
			 $catpower=fetch_cat_power();
			 if(!empty($catpower)){
				 $catpwerid=get_power_catid($catpower);
			 }
		 } 		
		  $tablename="m_".$this->curmodel['tablename'];
		  $parnav='<li><a href=\"'.url_admin('init',"category").'\" target=\"maincontent\">内容</a></li><li><a href=\"'.url_admin('lists','content',array("catid"=>$this->curcat['id'])).'\" target=\"maincontent\">'.$this->rootcat['title'].'</a></li><li class=\"active\">'.$this->curcat['title'].'</li>';
		   if(!getroot('users','logs')){
			  messagebox(Lan('no_permission'),'back',"warn");			
		   }
		 $modelfields=$this->db->select("select * from `#yunyecms_modelfields`  where modelid={$this->curmodel['modelid']}  and isdisplay=1 ");
		 $pagesize=20;
		 $sqlquery="select * from `#yunyecms_{$tablename}`  ";
		 $where=" where 1=1  ";
		 $sqlcnt=" select count(*) from `#yunyecms_{$tablename}` ";
		 $order=" order by `addtime` desc ";
		  if(isset($_REQUEST)){
		   if(!empty($_REQUEST["searchkey"])){
		        $searchkey=usafestr(trim($_REQUEST["searchkey"]));
		        $where=$where." and ( `title`  like '%{$searchkey}%' or  `title_en`  like '%{$searchkey}%' )";
			  }
		 }
			if(!empty($_REQUEST['catid'])){
			     $catid=trim($_REQUEST['catid']);
				 if(!is_numeric($catid)){
						messagebox("错误的参数","back",'warn');		
				  }
				 $catid=usafestr($catid);
				   $subcat=get_cat_child($catid);
	               $catidstr=implode(',', $subcat);
				   $newcat=array_intersect($subcat,$catpwerid);
				   $messagetip="暂时没有{$this->curcat["title"]}信息";
				   if(!empty($catpwerid)){
					     if(empty($newcat)){
							 messagebox("您没有管理{$this->curcat["title"]}信息的权限",'back',"warn");			
							}else{
							    $catidstr=implode(',', $newcat);
							    $where=$where." and catid in($catidstr)";
						   }
				   }else{
		              $where=$where." and catid in($catidstr)";
				   }
			 }
		
		 $pagearr=$this->db->pagelist($sqlcnt,$sqlquery,$where,$order,$pagesize);
		 if($pagearr["count"]!=0){
			 $list=$pagearr["query"];
			 foreach($list as $key=>$var){
				  $modelarr=$this->db->find("select modelid  from `#yunyecms_category` where `id`= {$var['catid']}");
				  if($modelarr){
				     $list[$key]["modelid"]=$modelarr["modelid"];
					 if(!empty($var["userid"])) {
				         $list[$key]["user"]=$this->db->getbyid($var["userid"],"member");
					 }
				  }
			 }
			 $pages=$pagearr["page"];
		 }
		 require tpl_adm('customform');
	  }
		
	
 public function customform_add(){
				$tablename="m_".$this->curmodel['tablename'];
				$modelfields=$this->db->select("select * from `#yunyecms_modelfields`  where modelid={$this->curmodel['modelid']}  and isadd=1 ");
			   if(!empty($_REQUEST["id"])){
						 $parnav='<li><a href=\"'.url_admin('init',"category").'\" target=\"maincontent\">内容</a></li><li><a href=\"'.url_admin('lists','content').'\" target=\"maincontent\">'.$this->rootcat['title'].'</a></li><li class=\"active\">'.$this->curcat['title'].'</li>';
						 $id=trim($_REQUEST["id"]);
						 if(!is_numeric($id)){
						   messagebox("参数必须为数字，谢谢!",$_SERVER['HTTP_REFERER']);
						 }
				   		 $id=usafestr(trim($id));
						 $rsinfo=$this->db->find("select * from `#yunyecms_{$tablename}` where `id`= {$id}");
						 if(empty($rsinfo)){
							   messagebox("栏目不存在",$_SERVER['HTTP_REFERER']);			
						  }
				        $catid=$this->curcat['id'];
						$yyact="edit";
				}else{
				     $yyact=yyact_get("add");
					 $parnav='<li><a href=\"'.url_admin('init',"category").'\" target=\"maincontent\">内容</a></li><li><a href=\"'.url_admin('lists','content').'\" target=\"maincontent\">'.$this->rootcat['title'].'</a></li><li class=\"active\">'.$this->curcat['title'].'</li>';
			   
				    $catid=$this->curcat['id'];
				   
			         if(!getroot('info','add')){
							messagebox(Lan('no_permission_infoadd'),'back',"warn");			
					  }else{
					    if(!get_cat_power($catid,"add")){
						    messagebox("您没有添加{$this->curcat["title"]}信息的权限",'back',"warn");			
					      }
					  }					   

		   }
	
	          if(isset($_POST["yyact"])){
				      $_POST=ustripslashes($_POST);
				 	  $data["catid"]=usafestr(trim($_POST["catid"]));
				 	  $data["addtime"]=time();
				 	  $data["status"]=1;
					  foreach($modelfields as $key=>$var){
						 $fdname=$var['fdname'];
						  if(isset($_POST[$fdname])){
						     $data[$fdname]=uhtmlspecialchars(trim($_POST[$fdname]));
						  }
					   }
					   if(empty($data["title"])){
							messagebox("标题不能为空，谢谢!");		
					   }
					   if(empty($data["catid"])){
							messagebox("栏目不能为空，请重新选择!");		
					   }
					   if(!is_numeric($data["catid"])){
							messagebox("栏目参数错误，请重新选择!");		
						 }
			$catid=$data["catid"];
			if($_POST["yyact"]=="add"){
				 	 $data["admin_userid"]=$this->admuser["userid"];
					 $retres=$this->db->insert($data,$tablename);
					 if($retres){
								$doing="添加{$this->curcat['title']}信息——".$data["title"];
								$yyact="addcustomform";
								insert_admlogs($doing,$yyact);
								messagebox("添加{$this->curcat['title']}信息成功！",url_admin('customform','content',array('catid'=>$catid)),"success");
					 }else{
								messagebox("添加{$this->curcat['title']}信息失败！",url_admin('customform','content',array('catid'=>$catid)),"error");
					 }
			  }
		      if($_POST["yyact"]=="edit"){
				  
				    if(!getroot('info','edit')){
							messagebox(Lan('no_permission_infoedit'),'back',"warn");			
					  }else{
					    if(!get_cat_power($catid,"edit")){
						    messagebox("您没有编辑{$this->curcat["title"]}信息的权限",'back',"warn");			
					      }
					  }					   
				  
					   $id=$_POST["id"];
					   if(!$this->check_exist($id)){
							messagebox("该内容不存在！",'back',"warn");
					   }
					   $retres=$this->db->update($data,$tablename,"id={$id}");
						if($retres){
								    $doing="更新{$this->curcat['title']}信息——".$data["title"];
									$yyact="updatecustomform";
									insert_admlogs($doing,$yyact);
									messagebox("更新{$this->curcat['title']}信息成功！",url_admin('customform','content',array('catid'=>$catid)),"success");
						 }else{
									messagebox("更新{$this->curcat['title']}信息失败！",url_admin('customform','content',array('catid'=>$catid)),"error");
						 }
			  }			  
		  }
		require tpl_adm('customform_add');
	 }	
	
	
 public function customform_view(){
				$tablename="m_".$this->curmodel['tablename'];
				$modelfields=$this->db->select("select * from `#yunyecms_modelfields`  where modelid={$this->curmodel['modelid']}  and isadd=1 ");
			   if(!empty($_REQUEST["id"])){
						 $parnav='<li><a href=\"'.url_admin('init',"category").'\" target=\"maincontent\">内容</a></li><li><a href=\"'.url_admin('lists','content').'\" target=\"maincontent\">'.$this->rootcat['title'].'</a></li><li class=\"active\">'.$this->curcat['title'].'</li>';
						 $id=trim($_REQUEST["id"]);
						 if(!is_numeric($id)){
						   messagebox("参数必须为数字，谢谢!",$_SERVER['HTTP_REFERER']);
						 }
				         $id=usafestr($id);
				        $rsinfo=$this->db->find("select * from `#yunyecms_{$tablename}` where `id`= {$id}");
						 if(empty($rsinfo)){
							   messagebox("栏目不存在",$_SERVER['HTTP_REFERER']);			
						  }else{
							$strsql="update `#yunyecms_{$tablename}` set isview=1 where id=".$id.""; 
							$this->db->query($strsql);	
							 
						 }
				       $catid=$this->curcat['id'];
		              if(!getroot('info','view')){
							messagebox(Lan('no_permission_infoview'),'back',"warn");			
					  }else{
					    if(!get_cat_power($catid,"view")){
						    messagebox("您没有查看{$this->curcat["title"]}信息的权限",'back',"warn");			
					      }
					  }					   
				   
				}
		require tpl_adm('customform_view');
	 }	
	
	
	  
  public function finaldelete() {
		  $tablename="m_".$this->curmodel['tablename'];
	  	  $catid = $this->curcat["id"];
		   if(!getroot('info','del')){
					messagebox(Lan('no_permission_infodel'),'back',"warn");			
			  }else{
				if(!get_cat_power($catid,"del")){
					 messagebox("您没有删除{$this->curcat["title"]}信息的权限",'back',"warn");			
				  }
			  }
            $id = $_REQUEST["id"];
		    if(!is_array($id)){
			  $id=compact('id');
			}
			$idarray=$id;
			 foreach($idarray as $key=>$var){
				 		if(!is_numeric($var)){
					        messagebox("错误的参数！",'back',"warn");			
					    }
                        $var = usafestr($var);
		                $curmodelnr=$this->db->find("select id,pic,content from `#yunyecms_{$tablename}` where `id`= {$var}");
						if($curmodelnr){
                          $thumb=$curmodelnr["pic"];
                          $ct_content=$curmodelnr["content"];
						  $ct_content_img=getcontentimg($ct_content);
							 if(!empty($thumb)){
								 file_delete($thumb);
							 }
							 foreach($ct_content_img as $key3=>$var3){
									file_delete($var3);
								 }
						}	
					}
            if (isset($idarray)){
				 $idarray=implode(",",$idarray);
				 $retres =$this->db->delete($tablename,"id in ({$idarray})");
                if ($retres !== false) {
						$yyact="infolistdel";
						$logcontent['tablename']=$this->curmodel['tablename'];
						$logcontent['action']=$yyact;
						$logcontent['ids']=serialize($id);
						$logcontent=serialize($logcontent);
						$doing="删除{$this->curcat['title']}信息：{$idarray} ";
						insert_admlogs($doing,$yyact,$logcontent);
				        messagebox(Lan('admin_delete_success'),$_SERVER['HTTP_REFERER'],"success");
                } else {
				    messagebox(Lan('admin_delete_error'),$_SERVER['HTTP_REFERER'],"warn");
                }
            } else {
				    messagebox(Lan('admin_delete_error'),$_SERVER['HTTP_REFERER'],"warn");
            }
    }	
	
	
	

	
	  
  public function customformdelete() {
		  $tablename="m_".$this->curmodel['tablename'];

			$catid = $this->curcat["id"];
		   if(!getroot('info','del')){
					messagebox(Lan('no_permission_infodel'),'back',"warn");			
			  }else{
				if(!get_cat_power($catid,"del")){
					 messagebox("您没有删除{$this->curcat["title"]}信息的权限",'back',"warn");			
				  }
			  }	  
	  
            $id = $_REQUEST["id"];
		    if(!is_array($id)){
			  $id=compact('id');
			}
			$idarray=$id;
            if (isset($idarray)){
					 foreach($idarray as $key=>$var){
				 		if(!is_numeric($var)){
					        messagebox("错误的参数！",'back',"warn");			
					    }
				        $idarray[$key]=usafestr($var);
			            }
				 $idarray=implode(",",$idarray);
				 $retres =$this->db->delete($tablename,"id in ({$idarray})");
                if ($retres !== false) {
						$yyact="customformdel";
						$logcontent['tablename']=$this->curmodel['tablename'];
						$logcontent['action']=$yyact;
						$logcontent['ids']=serialize($id);
						$logcontent=serialize($logcontent);
						$doing="删除{$this->curcat['title']}信息：{$idarray} ";
						insert_admlogs($doing,$yyact,$logcontent);
				    messagebox(Lan('admin_delete_success'),$_SERVER['HTTP_REFERER'],"success");
                } else {
				    messagebox(Lan('admin_delete_error'),$_SERVER['HTTP_REFERER'],"warn");
                }
            } else {
				    messagebox(Lan('admin_delete_error'),$_SERVER['HTTP_REFERER'],"warn");
            }
    }	
		
	
	
	
    public function check(){
		 if(!getroot('info','verify')){
			   messagebox(Lan('no_permission_infoverify'),'back',"warn");			
		 } 
		$tablename="m_".$this->curmodel['tablename'];
        parent::infocheck($tablename,$this->db);
    }	
	
    public function nocheck() {
		if(!getroot('info','verify')){
			  messagebox(Lan('no_permission_infoverify'),'back',"warn");			
		 } 
		$tablename="m_".$this->curmodel['tablename'];
        parent::infonocheck($tablename,$this->db);
    }		
	 
	private function check_exist($id) {
		$tablename="m_".$this->curmodel['tablename'];
		 $id = trim($id);
		 if(empty($id)){
		     return false;
			 }else{
			    if(!is_numeric($id)){
					  return false;
				 }
			  if ($this->db->find("select count(*) as cnt from `#yunyecms_{$tablename}` where `id`= {$id}")){
				  return true;
			  }				 
		  }
	  }	

	 
}
?>
