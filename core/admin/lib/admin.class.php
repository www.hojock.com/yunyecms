<?php
defined('IN_YUNYECMSAdmin') or exit('No direct script access allowed');
session_save_path(YUNYECMS_ADMSESSION);
session_start();
class YUNYE_admin {
    public $lang_menu;
	public $hashurl;
    function __construct() {
		$this->hashurl=GetyunyecmsHashStrAll();
    }
    protected function infocheck($tablename,$db) {
        $id = $_REQUEST ["id"];
		 if(!is_array($id)){
			  $id=compact('id');
		  }
		if(empty($tablename)){
			messagebox("非法操作!",$_SERVER['HTTP_REFERER'],"warn");		
		}
		$idarray=$id;
        if (isset($idarray)){
						foreach($idarray as $key=>$var){
				 		if(!is_numeric($var)){
					        messagebox("错误的参数！",'back',"warn");			
					    }
				        $idarray[$key]=usafestr($var);
			}
		$idarray=implode(",",$idarray);
		$retres=$db->query("update `#yunyecms_{$tablename}` set  status=1 where id in({$idarray}) ");
        if (false !==$retres) {
			messagebox("信息审核成功!",$_SERVER['HTTP_REFERER'],"success");		
        } else {
			messagebox("信息审核失败!",$_SERVER['HTTP_REFERER'],"warn");		
        }
	  }
    }
	
    protected function infonocheck($tablename,$db) {
         $id = $_REQUEST ["id"];
		 if(!is_array($id)){
			  $id=compact('id');
		  }
		 if(empty($tablename)){
			messagebox("非法操作!",$_SERVER['HTTP_REFERER'],"warn");		
		 }
		$idarray=$id;
        if (isset($idarray)){
						foreach($idarray as $key=>$var){
				 		if(!is_numeric($var)){
					        messagebox("错误的参数！",'back',"warn");			
					    }
				        $idarray[$key]=usafestr($var);
						}
		$idarray=implode(",",$idarray);
		$retres=$db->query("update `#yunyecms_{$tablename}` set  status=0 where id in({$idarray}) ");
        if (false !==$retres) {
			messagebox("信息取消审核成功!",$_SERVER['HTTP_REFERER'],"success");		
			
        } else {
			messagebox("信息取消审核失败!",$_SERVER['HTTP_REFERER'],"warn");		
        }
		}
    }	

	
}