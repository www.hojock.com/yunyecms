<?php
defined('IN_YUNYECMSAdmin') or exit('No permission.');
core::load_admin_class('admin');
class msg extends YUNYE_admin {
	private $db;
	private $admuser;
	function __construct() {
		$this->db = core::load_model('msg_model');
		$this->admuser=IsAdmLogin($this->db);
		parent::__construct();
	}
	 //加载首页
	 public function init() {
		 if(!getroot('system','msgview')){
			messagebox(Lan('no_permission_msgview'),'back',"warn");			
		 }	
		  $parnav='<li><a href=\"'.url_admin('init',"msg").'\" target=\"maincontent\">系统</a></li><li><a href=\"'.url_admin('init','msg').'\" target=\"maincontent\">系统消息管理</a></li><li class=\"active\">系统消息列表</li>';
		   if(!getroot('users','logs')){
			  messagebox(Lan('no_permission'),'back',"warn");			
		   }
		 $pagesize=20;
		 $sqlquery="select * from `#yunyecms_msg`  ";
		 $where=" where status=1 and title<>'' ";
		 $sqlcnt=" select count(*) from `#yunyecms_msg` ";
		 $order=" order by `addtime` desc ";
		  if(isset($_REQUEST)){
	     if(!empty($_REQUEST["msgdate"])){
			    $msgdate=trim($_REQUEST["msgdate"]);
				$msgdate=explode("-",$msgdate);
				$sdate=strtotime($msgdate[0]." 00:00:00");
				$edate=strtotime($msgdate[1]." 23:59:59");
				$sdatestr=date("Y/m/d",$sdate);
				$edatestr=date("Y/m/d",$edate);
				if($sdate>=$edate){
			        messagebox("开始时间不能大于结束时间",'back',"warn");
					}
					$where=$where." and ( addtime >={$sdate} and addtime <={$edate} ) ";
			   }			  
			  
		   if(!empty($_REQUEST["searchkey"])){
		        $searchkey=usafestr(trim($_REQUEST["searchkey"]));
		        $where=$where." and ( `title`  like '%{$searchkey}%')";
			  }
		 }
		 $pagearr=$this->db->pagelist($sqlcnt,$sqlquery,$where,$order,$pagesize);
		 if($pagearr["count"]!=0){
			 $list=$pagearr["query"];
			 $page=$pagearr["page"];
		 }
		 require tpl_adm('msg_list');
	  }
	
	
	
	
 public function view(){
	 	if(!getroot('system','msgview')){
			messagebox(Lan('no_permission_msgview'),'back',"warn");			
		 }	
		   if(!empty($_GET["id"])){
					$parnav='<li><a href=\"'.url_admin('init','notice').'\" target=\"maincontent\">系统消息管理</a></li><li class=\"active\">修改系统消息</li>';
					 $id=usafestr(trim($_GET["id"]));
					 if(!is_numeric($id)){
					   messagebox("系统消息参数错误",url_admin('init'));
					 }
					 $info=$this->db->find("select * from `#yunyecms_msg` where `id`= {$id}");
					 if(empty($info)){
						   messagebox("系统消息不存在",$_SERVER['HTTP_REFERER']);			
					  }else{
						    if($info["content"]) $info["content"]=unserialize($info["content"]);
							$strsql="update `#yunyecms_msg` set isview=1 where id=".$id.""; 
							$this->db->query($strsql);				 
					 }
			}
		require tpl_adm('msg_view');
	 }		
	
	

  public function finaldelete() {
	  	if(!getroot('system','msgdel')){
			messagebox(Lan('no_permission_msgdel'),'back',"warn");			
		 }	
            $id = $_REQUEST["id"];
		    if(!is_array($id)){
			  $id=compact('id');
			}
			$idarray=$id;
            if (isset($idarray)){
						foreach($idarray as $key=>$var){
				 		if(!is_numeric($var)){
					        messagebox("错误的参数！",'back',"warn");			
					    }
				        $idarray[$key]=usafestr($var);
						}
				 $idarray=implode(",",$idarray);
				 $retres =$this->db->delete("msg","id in ({$idarray})");
                if ($retres !== false) {
						$yyact="Msg_Del";
						$logcontent['tablename']="msg";
						$logcontent['action']=$yyact;
						$logcontent['ids']=serialize($id);
						$logcontent=serialize($logcontent);
						$doing="删除系统系统消息：{$idarray} ";
						insert_admlogs($doing,$yyact,$logcontent);
				    messagebox(Lan('admin_delete_success'),$_SERVER['HTTP_REFERER'],"success");
                } else {
				    messagebox(Lan('admin_delete_error'),$_SERVER['HTTP_REFERER'],"warn");
                }
            }else{
				    messagebox(Lan('admin_delete_error'),$_SERVER['HTTP_REFERER'],"warn");
            }
    }	
	
	private function check_exist($id) {
		 $id = trim($id);
		 if(empty($id)){
		     return false;
			 }else{
			    if(!is_numeric($id)){
					  return false;
				 }
			  if ($this->db->find("select count(*) as cnt from `#yunyecms_msg` where `id`= {$id}")){
				  return true;
			  }				 
		  }
	  }	

	 
}
?>
