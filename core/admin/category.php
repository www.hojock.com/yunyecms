<?php
defined('IN_YUNYECMSAdmin') or exit('No permission.');
core::load_admin_class('admin');
class category extends YUNYE_admin {
	private $db;
	private $admuser;
	private $modellist;
	function __construct() {
		$this->db = core::load_model('category_model');
		$this->admuser=IsAdmLogin($this->db);
		parent::__construct();
		$this->modellist=$this->db->select("select * from `#yunyecms_model` where modelname<>'' order by ordernum asc ");
		if(!getroot('column','view')){
					messagebox(Lan('no_permission_columnview'),'back',"warn");			
		 } 	
	}
	 //加载首页
	  public function init() {
			if(!getroot('column','view')){
					messagebox(Lan('no_permission_columnview'),'back',"warn");			
			 } 		  
		  $parnav='<li><a href=\"'.url_admin('init',"category").'\" target=\"maincontent\">内容</a></li><li><a href=\"'.url_admin('init','category').'\" target=\"maincontent\">栏目管理</a></li><li class=\"active\">栏目列表</li>';
		   if(!getroot('users','logs')){
			  messagebox(Lan('no_permission'),'back',"warn");			
		   }
		 $pagesize=20;
		 if(!empty($_REQUEST['parentid'])){
			  $parentid=usafestr($_REQUEST['parentid']);
		      $where=" where pid={$parentid} ";
		    } else{
		    $where=" where pid=0 ";
		  }
		 $sqlquery="select * from `#yunyecms_category`  ";
		 $sqlcnt=" select count(*) from `#yunyecms_category` ";
		 $order=" order by `ordernum` asc , `addtime` desc ";
		  if(isset($_REQUEST)){
		   if(!empty($_REQUEST["searchkey"])){
		        $searchkey=usafestr(trim($_REQUEST["searchkey"]));
		        $where=$where." and ( `title`  like '%{$searchkey}%' or  `title_en`  like '%{$searchkey}%' )";
			  }
		 }
		 $pagearr=$this->db->pagelist($sqlcnt,$sqlquery,$where,$order,$pagesize);
		 if($pagearr["count"]!=0){
			 $list=$pagearr["query"];
			 $pages=$pagearr["page"];
		 }
		 require tpl_adm('category_list');
	  }
	
    public function add(){
			if(!empty($_REQUEST ['pid'])){
				     $strpid=yytrim($_REQUEST["pid"]);
					 if(!is_numeric($strpid)){
					   messagebox("参数错误",url_admin('init'));
					 }
				$pcate=$this->db->select("select * from `#yunyecms_category` where `id`= {$strpid}");
			 }
		   if(!empty($_GET["id"])){
					$parnav='<li><a href=\"'.url_admin('init','category').'\" target=\"maincontent\">内容</a></li><li><a href=\"'.url_admin('init','category').'\" target=\"maincontent\">栏目管理</a></li><li class=\"active\">修改栏目</li>';
					 $id=trim($_GET["id"]);
					 if(!is_numeric($id)){
					   messagebox("栏目参数错误",url_admin('init'));
					 }
					 $row=$this->db->find("select * from `#yunyecms_category` where `id`= {$id}");
					 if(empty($row)){
						   messagebox("栏目不存在",$_SERVER['HTTP_REFERER']);			
					  }
					  if($row["id"]!=0){
						   $pcate1=$this->db->select("select * from `#yunyecms_category` where `id`= {$row["pid"]}");
						}			   
					$yyact="edit";
			}else{
				 if(!getroot('column','add')){
						messagebox(Lan('no_permission_columnadd'),'back',"warn");			
				 } 
				$yyact=yyact_get("add");
				$parnav='<li><a href=\"'.url_admin('init','category').'\" target=\"maincontent\">内容</a></li><li><a href=\"'.url_admin('init','category').'\" target=\"maincontent\">栏目管理</a></li><li class=\"active\">增加栏目</li>';
				if(!empty($strpid)){
							 $sqlordermax="select max(ordernum) as maxordernum from `#yunyecms_category` where `pid`= {$strpid}";
							 }else{
							 $sqlordermax="select max(ordernum) as maxordernum from `#yunyecms_category` where `pid`= 0";
							 }
						$ordermaxquery=$this->db->find($sqlordermax);
						$ordermax=$ordermaxquery["maxordernum"]+1;
				}
	        if(isset($_POST["yyact"])){
				      $pic=uhtmlspecialchars(trim($_POST["pic"]));
				      $_POST=yytrim($_POST);
				 	  $data["pid"]=$_POST["pid"];
					  $data["modelid"]=$_POST["modelid"];
					  $data["title"]=$_POST["title"];
					  $data["ftitle"]=$_POST["ftitle"];
					  $data["pic"]=$pic;
					  $data["exlink"]=$_POST["exlink"];
					  $data["ispower"]=$_POST["ispower"];
					  $data["pages"]=$_POST["pages"];
					  $data["ordernum"]=$_POST["ordernum"];
					  $data["isdisplay"]=$_POST["isdisplay"];
					  $data["isgood"]=$_POST["isgood"];
					  $data["seotitle"]=$_POST["seotitle"];
					  $data["keywords"]=$_POST["keywords"];
					  $data["description"]=$_POST["description"];
					  $data["tplhome"]=$_POST["tplhome"];
					  $data["tpllist"]=$_POST["tpllist"];
					  $data["tplcontent"]=$_POST["tplcontent"];
					  $data["title_en"]=$_POST["title_en"];
					  $data["ftitle_en"]=$_POST["ftitle_en"];
					  $data["exlink_en"]=$_POST["exlink_en"];
					  $data["seotitle_en"]=$_POST["seotitle_en"];
					  $data["keywords_en"]=$_POST["keywords_en"];
					  $data["description_en"]=$_POST["description_en"];
					  $data["keywords_en"]=$_POST["keywords_en"];
					  $data["keywords_en"]=$_POST["keywords_en"];
					  $data["status"]=1;
					   if(empty($data["title"])){
							messagebox("栏目标题不能为空，谢谢!",$_SERVER['HTTP_REFERER']);		
					   }
					   if(empty($data["modelid"])){
							messagebox("模型不能为空，请重新选择!",$_SERVER['HTTP_REFERER']);		
					   }
					   if(!is_numeric($data["modelid"])){
							messagebox("模型参数错误，请重新选择!",$_SERVER['HTTP_REFERER']);		
						 }	
			if($_POST["yyact"]=="add"){
					  $data["addtime"]=time();
					  $data["updatetime"]=time();
					 $retres=$this->db->insert($data);
					 if($retres){
								$doing="添加栏目—".$data["title"];
								$yyact="addcategory";
								insert_admlogs($doing,$yyact);
								messagebox("添加栏目成功！",url_admin('init'),"success");
					 }else{
								messagebox("添加栏目失败！",url_admin('init'),"error");
					 }
			  }
		      if($_POST["yyact"]=="edit"){
					  $data["updatetime"]=time();
				  	 if(!getroot('column','edit')){
							messagebox(Lan('no_permission_columnedit'),'back',"warn");			
					  } 
					   $id=$_POST["id"];
					   if(!$this->check_exist($id)){
							messagebox("该内容不存在！",url_admin('init'),"warn");
					   }
					   $retres=$this->db->update($data,"category","id={$id}");
						if($retres){
									$doing="更新栏目—".$data["title"];
									$yyact="updatecategory";
									insert_admlogs($doing,$yyact);
									messagebox("栏目更新成功！",url_admin('init'),"success");
						 }else{
									messagebox("栏目更新失败！",url_admin('init'),"error");
						 }
			  }			  
		  }
	 
		require tpl_adm('category_add');
	 }	
	
	
	  

    public function finaldelete() {
			 if(!getroot('column','del')){
					messagebox(Lan('no_permission_columndel'),'back',"warn");			
			  } 
            $id = $_REQUEST["id"];
		    if(!is_array($id)){
			  $id=compact('id');
			}
			$idarray=$id;
			foreach($idarray as $key=>$var){
				        if(!is_numeric($var)){
						  messagebox("错误的参数！",url_admin('init'),"error");
					    }
                        $var = usafestr($var);
		                $curcategory=$this->db->find("select id,modelid,pic from `#yunyecms_category` where `id`= {$var}");
						if($curcategory){
                          $mid=$curcategory["modelid"];
                          $picfile=$curcategory["pic"];
							 if(!empty($picfile)){
								 file_delete($picfile);
							 }
						    $modname=getmodel($mid);
						    $tablename=$modname['tablename'];
						    $modeltype=$modname['modeltype'];
						    $tablename="m_".$tablename;
						    if(!empty($tablename)){
								  if($modeltype!=3){
		                          $cur_ct=$this->db->select("select id,pic,content from `#yunyecms_{$tablename}` where `catid`= {$var}");
								  if($cur_ct){
									  foreach($cur_ct as $key2=>$var2){
										  $ct_thumb=$var2['pic'];
										  $ct_content=$var2['content'];
										  $ct_content_img=getcontentimg($ct_content);
										   if(!empty($ct_thumb)){
													 file_delete($ct_thumb);
											   }
										      foreach($ct_content_img as $key3=>$var3){
												   file_delete($var3);
											  }
									   }
								   }
								}
								
								$re = $this->db->delete($tablename,"catid={$var}");
						   }
						}	
					}
            if (isset($idarray)) {
				 $idarray=implode(",",$idarray);
				 $retres =$this->db->delete("category","id in ({$idarray})");
                if ($retres !== false) {
				    messagebox(Lan('admin_delete_success'),url_admin('init','category'),"success");
                } else {
				    messagebox(Lan('admin_delete_error'),url_admin('init','category'),"warn");
                }
            } else {
				   messagebox(Lan('admin_delall_lessone'),url_admin('init','category'),"warn");
            }
    }	
	
		
    public function check(){
		 if(!getroot('column','edit')){
			  messagebox(Lan('no_permission_columnedit'),'back',"warn");			
		  } 
        parent::infocheck("category",$this->db);
    }	
	
    public function nocheck() {
		 if(!getroot('column','edit')){
			messagebox(Lan('no_permission_columnedit'),'back',"warn");			
		 } 
        parent::infonocheck("category",$this->db);
    }		
	 
	private function check_exist($id) {
		 $id = trim($id);
		 if(empty($id)){
		     return false;
			 }else{
			    if(!is_numeric($id)){
					  return false;
				 }
			  if ($this->db->find("select count(*) as cnt from `#yunyecms_category` where `id`= {$id}")){
				  return true;
			  }				 
		  }
	  }	

	 
}
?>
