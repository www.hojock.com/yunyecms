<?php
$LANG['no_language'] = '缺少语言包';
$LANG['search_key_empty'] = '搜索关键词不能为空';
$LANG['user_already_exist'] = '该用户已经存在';
$LANG['user_add_success'] = '添加用户成功';
$LANG['user_username_empty'] = '用户名不能为空';
$LANG['user_pwd_empty'] = '密码不能为空';
$LANG['user_role_empty'] = '权限组不能为空';
$LANG['user_catid_empty'] = '部门不能为空';
$LANG['user_not_exist'] = '该用户不存在';
$LANG['user_pwd_less'] = '密码不能少于6位';
$LANG['user_edit_success'] = '用户修改成功';
$LANG['user_oldpwd_error'] = '旧密码不正确';
$LANG['user_delete_success'] = '用户删除成功';
$LANG['user_delete_error'] = '用户删除失败';
$LANG['user_userid_notnumber'] = '用户名必须为数字';
$LANG['user_islastuser'] = '已经是最后一个用户了，不能删除';
$LANG['department_id_notnumber'] = '部门ID必须为数字';
$LANG['department_edit_success'] = '部门修改成功';
$LANG['department_not_exist'] = '部门不存在';
$LANG['department_delete_success'] = '部门删除成功';
$LANG['department_delete_error'] = '部门删除失败';
$LANG['department_name_empty'] = '部门名称不能为空';
$LANG['department_already_exist'] = '部门已经存在';
$LANG['department_add_success'] = '部门添加成功';
$LANG['role_add_success'] = '权限添加成功';
$LANG['role_edit_success'] = '权限修改成功';
$LANG['id_notnumber'] = 'ID必须为数字';
$LANG['role_not_exist'] = '该权限不存在';
$LANG['role_not_exist'] = '该权限不存在';
$LANG['role_already_exist'] = '该权限已经存在了';
$LANG['role_name_empty'] = '权限名称不能为空';
$LANG['role_powers_empty'] = '您至少应该选中一种权限';
$LANG['role_info_allself'] = '可管理所选栏目所有信息和只能管理所选栏目自己发布的信息不能同时选中';


$LANG['adminlogin_Illegallogging'] = '非法登录后台，请联系网站管理员';
$LANG['adminlogin_username_error'] = '请输入合法的用户名';
$LANG['adminlogin_pwd_error'] = '请输入合法的密码';
$LANG['adminlogin_captcha_error'] = '您输入的验证码不正确，请重新输入';
$LANG['adminlogin_fails_time'] = '您已尝试5次输入，请3小时后再登录';
$LANG['adminlogin_usernotexist'] = '该管理员不存在，请确认输入是否正确！';
$LANG['adminlogin_pwderror'] = '管理员密码不正确！';
$LANG['adminlogin_success'] = '您已成功登录管理中心！';
$LANG['adminlogin_SingleUser'] = '同一帐号同时只能一个在线！';
$LANG['adminlogin_OneUser'] = '您的账号在其他地方登录，您被迫退出！';
$LANG['adminlogin_overtime'] = '登录超时！';
$LANG['adminlogin_notlogin'] = '您还没有登录，请先登录！';
$LANG['adminloginlog_not_exist'] = '该登录记录不存在！';



$LANG['model_edit_success'] = '模型修改成功！';
$LANG['model_already_exist'] = '该模型已经存在！';
$LANG['model_name_empty'] = '模型名不能为空！';
$LANG['model_add_success'] = '模型添加成功！';
$LANG['model_add_success'] = '模型添加成功！';
$LANG['modeltype_isnotnumber'] = '模型类型必须为数字！';
$LANG['model_tablename_empty'] = '表名不能为空！';
$LANG['model_name_empty'] = '模型名称为空为空！';
$LANG['model_createtable_error'] = '创建表失败！';
$LANG['model_table_exist'] = '该数据表已经存在！';
$LANG['model_delete_success'] = '模型删除成功！';
$LANG['model_delete_error'] = '模型删除失败！';


$LANG['admin_failhash'] = '非法请求，错误的来源！';
$LANG['info_already_exist'] = '该信息已经存在了';
$LANG['admin_logs_emptyact'] = '操作日志操作类型不能为空';
$LANG['startdate_gt_enddate'] = '开始时间不能大约结束时间';
$LANG['admin_deleteall_success'] = '批量删除成功';
$LANG['admin_deleteall_error'] = '批量删除失败';
$LANG['admin_delall_lessone'] = '删除失败，您至少应该选中一条记录！';
$LANG['admin_logout_success'] = '您已经成功退出系统！';
$LANG['admin_id_notnumber'] = 'ID必须为数字！';
$LANG['admin_delete_success'] = '删除成功！';
$LANG['admin_delete_error'] = '删除失败！';
$LANG['admin_user_notexist'] = '管理员不存在';
$LANG['no_permission'] = '您没有操作权限，请联系系统管理员';
$LANG['no_permission_modelview'] = '您没有模型管理操作权限，请联系系统管理员';
$LANG['no_permission_modeladd'] = '您没有模型添加操作权限，请联系系统管理员';
$LANG['no_permission_modeledit'] = '您没有模型修改操作权限，请联系系统管理员';
$LANG['no_permission_modeldel'] = '您没有模型删除操作权限，请联系系统管理员';
$LANG['no_permission_fieldsview'] = '您没有字段查看操作权限，请联系系统管理员';
$LANG['no_permission_fieldsadd'] = '您没有字段添加操作权限，请联系系统管理员';
$LANG['no_permission_fieldsedit'] = '您没有字段修改操作权限，请联系系统管理员';
$LANG['no_permission_fieldsdel'] = '您没有字段删除操作权限，请联系系统管理员';
$LANG['no_permission_langview'] = '您没有语言版管理操作权限，请联系系统管理员';
$LANG['no_permission_langadd'] = '您没有语言版添加操作权限，请联系系统管理员';
$LANG['no_permission_langedit'] = '您没有语言版修改操作权限，请联系系统管理员';
$LANG['no_permission_langdel'] = '您没有语言版删除操作权限，请联系系统管理员';
$LANG['no_permission_msgview'] = '您没有系统消息管理操作权限，请联系系统管理员';
$LANG['no_permission_msgdel'] = '您没有系统消息删除操作权限，请联系系统管理员';
$LANG['no_permission_sysconfig'] = '您没有系统配置信息设置操作权限，请联系系统管理员';
$LANG['no_permission_memberview'] = '您没有会员管理操作权限，请联系系统管理员';
$LANG['no_permission_memberadd'] = '您没有会员添加操作权限，请联系系统管理员';
$LANG['no_permission_memberedit'] = '您没有会员修改操作权限，请联系系统管理员';
$LANG['no_permission_memberdel'] = '您没有会员删除操作权限，请联系系统管理员';
$LANG['no_permission_adminuser'] = '您没有管理员管理操作权限，请联系系统管理员';
$LANG['no_permission_columnview'] = '您没有栏目管理操作权限，请联系系统管理员';
$LANG['no_permission_columnadd'] = '您没有栏目添加操作权限，请联系系统管理员';
$LANG['no_permission_columndit'] = '您没有栏目修改操作权限，请联系系统管理员';
$LANG['no_permission_columndel'] = '您没有栏目删除操作权限，请联系系统管理员';

$LANG['no_permission_infoview'] = '您没有内容管理操作权限，请联系系统管理员';
$LANG['no_permission_infoadd'] = '您没有内容添加操作权限，请联系系统管理员';
$LANG['no_permission_infoedit'] = '您没有内容修改操作权限，请联系系统管理员';
$LANG['no_permission_infodel'] = '您没有内容删除操作权限，请联系系统管理员';
$LANG['no_permission_infoverify'] = '您没有内容审核操作权限，请联系系统管理员';

 
?>