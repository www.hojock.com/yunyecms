<?php
defined('IN_YUNYECMSAdmin') or exit('No permission.');
core::load_admin_class('admin');
class notice extends YUNYE_admin {
	private $db;
	private $admuser;
	function __construct() {
		$this->db = core::load_model('notice_model');
		$this->admuser=IsAdmLogin($this->db);
		parent::__construct();
		 if(!getroot('member','notice')){
			messagebox(Lan('no_permission'),'back',"warn");			
		 } 
	}
	 //加载首页
	 public function init() {
		  $parnav='<li><a href=\"'.url_admin('init',"notice").'\" target=\"maincontent\">系统</a></li><li><a href=\"'.url_admin('init','notice').'\" target=\"maincontent\">通知消息管理</a></li><li class=\"active\">通知消息列表</li>';
		   if(!getroot('users','logs')){
			  messagebox(Lan('no_permission'),'back',"warn");			
		   }
		 $pagesize=20;
		 $sqlquery="select * from `#yunyecms_notice`  ";
		 $where=" where 1=1 and title<>'' ";
		 $sqlcnt=" select count(*) from `#yunyecms_notice` ";
		 $order=" order by `addtime` desc ";
		  if(isset($_REQUEST)){
		   if(!empty($_REQUEST["searchkey"])){
		        $searchkey=usafestr(trim($_REQUEST["searchkey"]));
		        $where=$where." and ( `title`  like '%{$searchkey}%' or  `title_en`  like '%{$searchkey}%' )";
			  }
		 }
		 $pagearr=$this->db->pagelist($sqlcnt,$sqlquery,$where,$order,$pagesize);
		 if($pagearr["count"]!=0){
			 $list=$pagearr["query"];
			 $page=$pagearr["page"];
		 }
		 require tpl_adm('notice_list');
	  }
	
	
 public function add(){
		   if(!empty($_GET["id"])){
					$parnav='<li><a href=\"'.url_admin('init','notice').'\" target=\"maincontent\">通知消息管理</a></li><li class=\"active\">修改通知消息</li>';
					$id=trim($_GET["id"]);
					 if(!is_numeric($id)){
					   messagebox("通知消息参数错误",url_admin('init'));
					 }
					 $rsinfo=$this->db->find("select * from `#yunyecms_notice` where `id`= {$id}");
					 if(empty($rsinfo)){
						   messagebox("通知消息不存在",$_SERVER['HTTP_REFERER']);			
					  }
					$yyact="edit";
			}else{
				$yyact=yyact_get("add");
				$parnav='<li><a href=\"'.url_admin('init','member').'\" target=\"maincontent\">用户</a></li><li><a href=\"'.url_admin('init','notice').'\" target=\"maincontent\">通知消息管理</a></li><li class=\"active\">增加通知消息</li>';
				$sqlordermax="select max(ordernum) as maxordernum from `#yunyecms_notice`";
				$ordermaxquery=$this->db->find($sqlordermax);
				$ordermax=$ordermaxquery["maxordernum"]+1;
				}
	        if(isset($_POST["yyact"])){
				      $_POST=ustripslashes($_POST);
					  $data["isgood"]=empty($_POST["isgood"])?0:usafestr(trim($_POST["isgood"]));
				 	  $data["title"]=usafestr(trim($_POST["title"]));
				 	  $data["pic"]=uhtmlspecialchars(trim($_POST["pic"]));
				      $data["exlink"]=uhtmlspecialchars(trim($_POST["exlink"]));
				 	  $data["picfile"]=uhtmlspecialchars(trim($_POST["picfile"]));
				 	  $data["summary"]=usafestr(trim($_POST["summary"]));
				 	  $data["seotitle"]=usafestr(trim($_POST["seotitle"]));
				 	  $data["seokeywords"]=usafestr(trim($_POST["seokeywords"]));
				 	  $data["seodesc"]=usafestr(trim($_POST["seodesc"]));
				 	  $data["ordernum"]=usafestr(trim($_POST["ordernum"]));
				 	  $data["title_en"]=usafestr(trim($_POST["title_en"]));
				 	  $data["exlink_en"]=uhtmlspecialchars(trim($_POST["exlink_en"]));
				 	  $data["summary_en"]=usafestr(trim($_POST["summary_en"]));
				 	  $data["seotitle_en"]=usafestr(trim($_POST["seotitle_en"]));
				 	  $data["seokeywords_en"]=usafestr(trim($_POST["seokeywords_en"]));
				 	  $data["seodesc_en"]=usafestr(trim($_POST["seodesc_en"]));
				 	  $data["addtime"]=time();
				 	  $data["status"]=1;
				      if(isset($_POST["content"])){
				 	     $data["content"]=uhtmlspecialchars(trim($_POST["content"]));
						  
					  }
		              if(isset($_POST["content_en"])){
				 	     $data["content_en"]=uhtmlspecialchars(trim($_POST["content_en"]));
					  }		 
					  if(empty($data["title"])){
							messagebox("通知消息标题不能为空，谢谢!",$_SERVER['HTTP_REFERER']);		
					   }
			 if($_POST["yyact"]=="add"){
					 $retres=$this->db->insert($data);
					 if($retres){
								$doing="添加通知消息—".$data["title"];
								$yyact="Add_Notice";
								insert_admlogs($doing,$yyact);
								messagebox("添加通知消息成功！",url_admin('init'),"success");
					 }else{
								messagebox("添加通知消息失败！",url_admin('init'),"error");
					 }
			  }
		      if($_POST["yyact"]=="edit"){
					   $id=$_POST["id"];
					   if(!$this->check_exist($id)){
							messagebox("该消息不存在！",url_admin('init'),"warn");
					   }
					   $retres=$this->db->update($data,"notice","id={$id}");
						if($retres){
									$doing="更新通知消息—".$data["title"];
									$yyact="Update_Notice";
									insert_admlogs($doing,$yyact);
									messagebox("通知消息更新成功！",url_admin('init'),"success");
						 }else{
									messagebox("通知消息更新失败！",url_admin('init'),"error");
						 }
			  }			  
		  }
		require tpl_adm('notice_add');
	 }	
	
  public function finaldelete() {
		  if(!getroot('info','del')){
			    messagebox(Lan('no_permission'),'back');			
		   }
            $id = $_REQUEST["id"];
		    if(!is_array($id)){
			  $id=compact('id');
			}
			$idarray=$id;
			 foreach($idarray as $key=>$var){
				 		if(!is_numeric($var)){
					        messagebox("错误的参数！",'back',"warn");			
					    }
                        $var = usafestr($var);
				        $idarray[$key]=$var;
		                $curmodelnr=$this->db->find("select id,pic,content from `#yunyecms_notice` where `id`= {$var}");
						if($curmodelnr){
                          $thumb=$curmodelnr["pic"];
                          $ct_content=$curmodelnr["content"];
						  $ct_content_img=getcontentimg($ct_content);
							 if(!empty($thumb)){
								 file_delete($thumb);
							 }
							 foreach($ct_content_img as $key3=>$var3){
									file_delete($var3);
								 }
						}	
					}
            if (isset($idarray)){
				 $idarray=implode(",",$idarray);
				 $retres =$this->db->delete("notice","id in ({$idarray})");
                if ($retres !== false) {
						$yyact="Notice_Del";
						$logcontent['tablename']="notice";
						$logcontent['action']=$yyact;
						$logcontent['ids']=serialize($id);
						$logcontent=serialize($logcontent);
						$doing="删除通知消息：{$idarray} ";
						insert_admlogs($doing,$yyact,$logcontent);
				    messagebox(Lan('admin_delete_success'),$_SERVER['HTTP_REFERER'],"success");
                } else {
				    messagebox(Lan('admin_delete_error'),$_SERVER['HTTP_REFERER'],"warn");
                }
            } else {
				    messagebox(Lan('admin_delete_error'),$_SERVER['HTTP_REFERER'],"warn");
            }
    }	
	
	
	
	
    public function check(){
        parent::infocheck("notice",$this->db);
    }	
	
    public function nocheck() {
        parent::infonocheck("notice",$this->db);
    }		
	 
	private function check_exist($id) {
		 $id = trim($id);
		 if(empty($id)){
		     return false;
			 }else{
			    if(!is_numeric($id)){
					  return false;
				 }
			  if ($this->db->find("select count(*) as cnt from `#yunyecms_notice` where `id`= {$id}")){
				  return true;
			  }				 
		  }
	  }	

	 
}
?>
