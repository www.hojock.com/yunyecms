<?php
defined('IN_YUNYECMSAdmin') or exit('No permission.');
core::load_admin_class('admin');
class main extends YUNYE_admin {
	private $db;
	private $admuser;
	function __construct() {
		$this->db = core::load_model('user_model');
		$this->admuser=IsAdmLogin($this->db);
		parent::__construct();
		}
	//加载首页
	  public function init() {
		if(!empty($this->admuser["userid"])) {
		     $config=getcfg();
			 $strsql="select `realname`,`prelogintime` from `#yunyecms_user` where `userid`={$this->admuser['userid']}";
			 $mainuser=$this->db->find($strsql);
			 $prelgtime=date("Y-m-d H:i:s",$mainuser['prelogintime']);
			 $admrealname=$mainuser['realname'];
			 $mainrole=$this->db->find("select `rolename` from `#yunyecms_admin_role` where `roleid`={$this->admuser['roleid']}");
			 if(!empty($mainrole))$admrolename=$mainrole['rolename'];
			 $maindepartment=$this->db->find("select `departmentname` from `#yunyecms_department` where `departmentid`={$this->admuser['catid']}");
			 if(!empty($maindepartment))$admpartment=$maindepartment['departmentname'];	
			 //系统消息
			 $msgpower=getroot('system','msgview');
			 $msgto=$this->db->select("select * from `#yunyecms_msg` where status=1 and isview=0 order by addtime desc ");
			 $msgnum=count($msgto);
			 foreach($msgto as $key=>$var){
				     $msgto[$key]['content']=unserialize($var['content']);
				     $msgto[$key]['url']=url_admin('view','msg',array("id"=>$var["id"]));
					 switch($var['flag']){
					  case 1:	
					  $msgto[$key]['icon']='<i class="fa fa-bell-o text-yellow"></i>';
					  $msgto[$key]['icon2']='<i class="menu-icon fa fa-bell-o bg-yellow"></i>';
 					  break; 
					  case 2:	
					  $msgto[$key]['icon']='<i class="fa fa-user text-aqua"></i> ';
					  $msgto[$key]['icon2']='<i class="menu-icon fa fa-user bg-aqua"></i>';
					  break;  
					  case 3:	
					  $msgto[$key]['icon']='<i class="fa fa-shopping-cart text-green"></i>';
					  $msgto[$key]['icon2']='<i class="menu-icon fa fa-shopping-cart bg-green"></i>';
					  break;
					  case 4:	
					  $msgto[$key]['icon']='<i class="icon-envelope  text-red"></i>';
					  $msgto[$key]['icon2']='<i class="menu-icon fa fa-envelope bg-light-blue"></i>';
					  break;  
					  default:
					  $msgto[$key]['icon']='<i class="fa fa-bell-o text-yellow"></i>';
					  $msgto[$key]['icon2']='<i class="menu-icon fa fa-bell-o bg-yellow"></i>';
				 }
			 }
		    //系统未读消息
			$modellist=$this->db->select("select * from `#yunyecms_model` where modeltype=3 order by ordernum asc");
			$customfromto=array();
			foreach($modellist as $key=>$var){
				$tablename="m_".$var["tablename"];
			    $fromlist=$this->db->select("select * from `#yunyecms_{$tablename}` where isview=0 order by addtime desc");
				foreach($fromlist as $k=>$v){
					$customfromto[]=$v;
				}
				$formcat=$this->db->find("select * from `#yunyecms_category` where modelid={$var["modelid"]} order by id desc limit 0,1");
			}
			if($formcat){
				     if(!getroot('info','view')){
							$formpower=false;	
					  }else{
					    if(!get_cat_power($formcat["id"],"view")){
						    $formpower=false;	
					      }else{
						    $formpower=true;	
						 }
					  }		
					$formurl=url_admin('customform','content',array("catid"=>$formcat["id"]));
			}
            $customfromnum=count($customfromto);
			}
		require tpl_adm("main");
	 }
	  public function home() {
			$parnav='<li class=\"active\">管理中心首页</li>';
					if(!empty($this->admuser["userid"])) {
						 $strsql="select `realname`,`prelogintime` from `#yunyecms_user` where `userid`={$this->admuser['userid']}";
						 $mainuser=$this->db->find($strsql);
						 $prelgtime=date("Y-m-d H:i:s",$mainuser['prelogintime']);
						 $admrealname=$mainuser['realname'];
						 $mainrole=$this->db->find("select `rolename` from `#yunyecms_admin_role` where `roleid`={$this->admuser['roleid']}");
						 if(!empty($mainrole))$admrolename=$mainrole['rolename'];
						 $maindepartment=$this->db->find("select `departmentname` from `#yunyecms_department` where `departmentid`={$this->admuser['catid']}");
						 if(!empty($maindepartment))$admpartment=$maindepartment['departmentname'];			 
						}
		  
		 $category=$this->db->select("select id,title,modelid from `#yunyecms_category` where status=1 and pid=0 order by ordernum asc");
		 foreach ($category as $key => $val) {
			if (is_array($val)) {
				 $chid= $val["id"];
				 $modelid= $val["modelid"];
				 $cattitle=$val["title"];
				   if(stripos($cattitle,'产品')!==false){
					    $category[$key]["colcss"]="icon-layers";
				   }elseif(stripos($cattitle,'新闻')!==false){
					    $category[$key]["colcss"]="icon-list";
				    }elseif(stripos($cattitle,'公司')!==false){
					    $category[$key]["colcss"]="icon-users";
				    }elseif(stripos($cattitle,'关于我们'||stripos($cattitle,'about')!==false)!==false){
					    $category[$key]["colcss"]="icon-users";
				    }elseif(stripos($cattitle,'案例')!==false||stripos($cattitle,'case')!==false){
					    $category[$key]["colcss"]="icon-book-open";
				    }elseif(stripos($cattitle,'联系我们')!==false||stripos($cattitle,'contact')!==false){
					    $category[$key]["colcss"]="icon-call-end";
				    }elseif(stripos($cattitle,'服务')!==false){
					    $category[$key]["colcss"]="icon-emoticon-smile";
				    }elseif(stripos($cattitle,'留言')!==false||stripos($cattitle,'反馈')!==false||stripos($cattitle,'feedback')!==false){
					    $category[$key]["colcss"]="icon-bubble";
				    }elseif(stripos($cattitle,'下载')!==false||stripos($cattitle,'download')!==false){
					    $category[$key]["colcss"]="icon-cloud-download";
				    }elseif(stripos($cattitle,'链接')!==false){
					    $category[$key]["colcss"]="icon-link";
				    }else{
					    $category[$key]["colcss"]="icon-globe";
				    }
				 $modelinfo=getmodel($modelid);
				 $category[$key]["cnt"]=0;
				 $modeltype=getmodeltype($modelid);
				if($modeltype=='singlepage'){
				   $category[$key]["url"]=url_admin('init','category',array("parentid"=>$chid));
				 }else{
				   $category[$key]["url"]=url_admin($modeltype,'content',array("catid"=>$chid));
				 }
				 $catcharr=$this->db->select("select id,title,modelid from `#yunyecms_category` where status=1 and pid={$chid} order by ordernum asc");
				 if(!empty($catcharr)){
					 $category[$key]['catlist']=$catcharr;
					 foreach($category[$key]['catlist'] as $k=>$v ){
						  $subcatids=getcatin($chid);
						  $category[$key]["cnt"]=$this->db->GetCount("select count(*) from `#yunyecms_m_{$modelinfo["tablename"]}` where catid in ($subcatids)");
					 }
				 }
			 }
		 }
		  
			 $msgto=$this->db->select("select * from `#yunyecms_msg` where status=1 and isview=0 order by addtime desc limit 0,10 ");
			 foreach($msgto as $key=>$var){
				     $msgto[$key]['content']=unserialize($var['content']);
				     $msgto[$key]['url']=url_admin('view','msg',array("id"=>$var["id"]));
					 switch($var['flag']){
					  case 1:	
					  $msgto[$key]['icon']='<i class="fa fa-bell-o text-yellow"></i>';
 					  break; 
					  case 2:	
					  $msgto[$key]['icon']='<i class="fa fa-user text-aqua"></i> ';
					  break;  
					  case 3:	
					  $msgto[$key]['icon']='<i class="fa fa-shopping-cart text-green"></i>';
					  break;
					  case 4:	
					  $msgto[$key]['icon']='<i class="icon-envelope  text-red"></i>';
					  break;  
					  default:
					  $msgto[$key]['icon']='<i class="fa fa-bell-o text-yellow"></i>';
				 }
			 }
		  
	    //系统未读消息
			$modellist=$this->db->select("select * from `#yunyecms_model` where modeltype=3 order by ordernum asc");
			foreach($modellist as $key=>$var){
				$tablename="m_".$var["tablename"];
			    $fromlist=$this->db->select("select * from `#yunyecms_{$tablename}` where isview=0 order by addtime desc");
				foreach($fromlist as $k=>$v){
					$customfromto[]=$v;
				}
				$formcat=$this->db->find("select * from `#yunyecms_category` where modelid={$var["modelid"]} order by ordernum desc limit 0,1");
			}
			if($formcat){
					$formurl=url_admin('customform','content',array("catid"=>$formcat["id"]));
			}		  
		  
			$newmember=$this->db->select("select * from `#yunyecms_member` where username<>'' order by addtime desc limit 0,8 ");
			$neworders=$this->db->select("select * from `#yunyecms_orders` where goods<>'' order by addtime desc limit 0,8 ");
		    foreach($neworders as $key=>$var){
				  $goods= unserialize($var["goods"]);
				  $neworders[$key]["goods"]=$goods;
				  $neworders[$key]["member"]=$this->db->getbyid($var["userid"],"member");
				  foreach($neworders[$key]["goods"] as $k=>$v){
				     $neworders[$key]["goods"][$k]["url"]=url('content/index/show',array("catid"=>$v["catid"],"id"=>$v["gid"]));
				  }
			    }
		  $cur_ver=getversion(YUNYECMS_SOFT_TYPE,strtoupper(YUNYECMS_VERSION),strtotime(YUNYECMS_VERSION_TIME),strtotime(YUNYECMS_BUILD_TIME));
		  $cur_soft_name=getsoftname(YUNYECMS_SOFT_TYPE,strtoupper(YUNYECMS_VERSION),strtotime(YUNYECMS_VERSION_TIME),strtotime(YUNYECMS_BUILD_TIME));
		   require tpl_adm("home");
	    }
	
		public function getver() {
		  //获取官方最新版本
		   $remoteurl="http://www.yunyecms.com/index.php?m=version&c=index&a=getver";
		   $remotedata=curl_post($remoteurl,$postdata);
		   $remotedata=yunyecms_json_decode($remotedata);
		   $soft_type=$remotedata["soft_type"];
		   $version_time=$remotedata["version_time"];
		   $build_time=$remotedata["build_time"];
		   $soft_name=$remotedata["soft_name"];
		   $version_name=$remotedata["version_name"];
		   $downurl=$remotedata["downurl"];
		   $ver_time=getsofttime($soft_type, $version_time,$build_time);
		   $ver_name=strtoupper($remotedata["version_name"]);			  
		   //$guanfang_ver=empty($remotedata["version_time"])?$ver_name:$ver_name."_".$ver_time;
		   $cur_ver=getversion(YUNYECMS_SOFT_TYPE,strtoupper(YUNYECMS_VERSION),strtotime(YUNYECMS_VERSION_TIME),strtotime(YUNYECMS_BUILD_TIME));
		   $guanfang_ver=getversion($soft_type,$version_name,$version_time,$build_time);  
		  if(YUNYECMS_SOFT_TYPE==1){
			  $cur_ver_time=YUNYECMS_VERSION_TIME;
		  }elseif(YUNYECMS_SOFT_TYPE==2){
			  $cur_ver_time=YUNYECMS_BUILD_TIME;
		  }else{
			  $cur_ver_time=YUNYECMS_VERSION_TIME;
		  }
		  if($ver_name==strtoupper(YUNYECMS_VERSION) && $ver_time==$cur_ver_time){
			  $ver_isnew=0;
			  $verdata["cur_ver"]=$cur_ver;
		   }else{
			 $ver_isnew=1;
			 $verdata["cur_ver"]=$cur_ver."&nbsp;&nbsp;<span class='text-red'> 官方新版：<a href='{$downurl}' class='btn btn-danger btn-xs btn-flat' target='_blank'>{$guanfang_ver} </a></span>";
		   }
			 //$verdata["guanfang_ver"]=$guanfang_ver;
		   exit(json_encode($verdata));
		   //商业授权结束 
		  }
	   	
	
		  public function getgrant() {
		   //获取商业授权信息
		   $shouquanurl="http://www.yunyecms.com/index.php?m=grant&c=index&a=getgrant";
		   $strsql="select grantcode from `#yunyecms_config` order by id desc limit 0,1";
		   $curconfig=$this->db->find($strsql);
		   $postdata['grantcode']=$curconfig["grantcode"];
		   $postdata['clientip']=getip();
		   $postdata['clienthost']=getserver_domain();
		   $grantdata=curl_post($shouquanurl,$postdata);
			if(!empty($grantdata)){
			  $grantdata=yunyecms_json_decode($grantdata);
		      $grantdata['company']=empty($grantdata['company'])?$grantdata['name']:$grantdata['company'];
		      $grantdata['granttime']=date("Y-m-d",$grantdata['granttime']);	
		      $grantdata['isgrant']=1;	
			}else{
		      $grantdata['isgrant']=0;	
			}
		    exit(json_encode($grantdata));
		   //商业授权结束 
		  }
	   
	
	  public function cacheclear() {
			 $parnav='<li class=\"active\">清理缓存</li>';
		      $tpl_cache = CACHE_ROOT . 'templates';

			if (!is_writable($tpl_cache)) {
					messagebox($tpl_cache . ' 目录不可写','back',"warn");			
				}
		   $caches= getDir($tpl_cache);
		   if(!empty( $caches)){
			$stroutput="<h4 class='text-aqua'>清理缓存开始</h1>";
			$doc_root=$_SERVER['DOCUMENT_ROOT'];
            if(!empty($doc_root)&&substr($doc_root,-1)=="/"){$doc_root=substr($doc_root, 0, -1);}
			if(strpos($doc_root,"\\")){ $doc_root=str_ireplace('\\','/',$doc_root);}
			 foreach($caches as  $key=>$var){
				  $file=str_replace('\\','/',str_replace($doc_root,'',$var));
				  $stroutput.="成功清理缓存文件 {$file} <br/>";
				  @unlink($var);
			 } 
		    $stroutput .="<h4 class='text-aqua'>清理缓存完成</h1>";
		   }else{
			  $stroutput="<h4 class='text-aqua'>没有要清理的缓存文件！</h1>";
		   }
		     require tpl_adm("clear");
	  }		
	
	
	
   public function logout(){
	    $this->admlogout($this->admuser["userid"],$this->admuser["username"] ,$this->admuser["rnd"]);
	   }	  
	  
private function admlogout($userid,$username,$rnd){
	$userid=(int)$userid;
	if(!$userid||!$username)
	{
		messagebox(Lan('adminlogin_notlogin'),url_admin('init','login'),"warn");	
	}
	usetcookie("admuserid","",1);
	usetcookie("admusername","",1);
	usetcookie("admloginrnd","",1);
	usetcookie("admroleid","",1);
	usetcookie("loginyunyecmsckpass",'',1);
	usetcookie("loginyunyecmsfilernd",'',1);
	usetcookie("admlogintruetime",'',1);
	usetcookie("admlogintime",'',1);
	usetcookie("admloginlicense",'',1);
	$rnd=make_rand(20);
	$sql=$this->db->query("update `#yunyecms_user` set `rnd`='$rnd' where userid='$userid'");
	DelADMFileRnd($userid);
	$doing="管理员退出系统";
	$yyact="logout";
	insert_admlogs($doing,$yyact,"",$userid);
	messagebox(Lan('admin_logout_success'),url_admin('init','login'),"warn");	
}	 
	 
	 
	 
	 
}
?>
