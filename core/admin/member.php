<?php
defined('IN_YUNYECMSAdmin') or exit('No permission.');
core::load_admin_class('admin');
class member extends YUNYE_admin {
	private $db;
	private $admuser;
	function __construct() {
		$this->db = core::load_model('member_model');
		$this->admuser=IsAdmLogin($this->db);
		parent::__construct();
		 if(!getroot('member','view')){
			messagebox(Lan('no_permission_memberview'),'back',"warn");			
		 } 
	}
	 //加载首页
	  public function init() {
		  $parnav='<li><a href=\"'.url_admin('init',"member").'\" target=\"maincontent\">用户</a></li><li><a href=\"'.url_admin('init','member').'\" target=\"maincontent\">会员管理</a></li><li class=\"active\">会员列表</li>';
		  if(!getroot('member','view')){
			messagebox(Lan('no_permission_memberview'),'back',"warn");			
		    } 
		 $pagesize=20;
		 $sqlquery="select * from `#yunyecms_member`  ";
		 $where=" where username<>'' ";
		 $sqlcnt=" select count(*) from `#yunyecms_member` ";
		 $order=" order by `addtime` desc,`id` desc ";
		  if(isset($_REQUEST)){
		   if(!empty($_REQUEST["searchkey"])){
		        $searchkey=usafestr(trim($_REQUEST["searchkey"]));
		        $where=$where." and ( `username`  like '%{$searchkey}%' )";
			  }
		 }
		 $pagearr=$this->db->pagelist($sqlcnt,$sqlquery,$where,$order,$pagesize);
		 if($pagearr["count"]!=0){
			 $list=$pagearr["query"];
			 	foreach($list as $key=>$var){
				   $list[$key]["membergroupname"]=GetMemberGroupName($var["groupid"]);
			    }
			 $page=$pagearr["page"];
		 }
		 require tpl_adm('member_list');
	  }
	
 public function add(){
	       $grouplist=$this->db->select("select * from `#yunyecms_membergroup` where `status`=1 ");
		   if(!empty($_GET["id"])){
					$parnav='<li><a href=\"'.url_admin('init','member').'\" target=\"maincontent\">会员管理</a></li><li class=\"active\">修改会员</li>';
					$id=trim($_GET["id"]);
					 if(!is_numeric($id)){
					   messagebox("会员参数错误",url_admin('init'));
					 }
					 $row=$this->db->find("select * from `#yunyecms_member` where `id`= {$id}");
					 if(empty($row)){
						   messagebox("会员不存在",$_SERVER['HTTP_REFERER']);			
					  }
					$yyact="edit";
			}else{
				$yyact=yyact_get("add");
				$parnav='<li><a href=\"'.url_admin('init','member').'\" target=\"maincontent\">会员</a></li><li><a href=\"'.url_admin('init','member').'\" target=\"maincontent\">会员管理</a></li><li class=\"active\">增加会员</li>';
				}
	        if(isset($_POST["yyact"])){
				      $curtime=time();
				      $_POST=ustripslashes($_POST);
				      $pic=uhtmlspecialchars(trim($_POST["pic"]));
				      $_POST=yytrim($_POST);
					  $data["pic"]=$pic;
					  $data["name"]=$_POST["name"];
					  $data["email"]=$_POST["email"];
					  $data["phone"]=$_POST["phone"];
					  $data["mobile"]=$_POST["mobile"];
					  $data["address"]=$_POST["address"];
					  $data["sex"]=$_POST["sex"];
					  $data["company"]=$_POST["company"];
					  $data["groupid"]=$_POST["groupid"];
					  $data["position"]=$_POST["position"];
					  $data["department"]=$_POST["department"];
					  $data["qq"]=$_POST["qq"];
					  $data["website"]=$_POST["website"];
					  $data["updatetime"]=$curtime;
					  if($_POST["yyact"]=="add"){
					  if(!getroot('member','add')){
						messagebox(Lan('no_permission_memberadd'),'back',"warn");			
						} 						
						  
						 if(empty($_POST["username"])){
						    messagebox("用户名不能为空，谢谢!");		
					     }    
						if(empty($_POST["pwd"])){
						    messagebox("会员密码不能为空，谢谢!");		
					     }
					      $data["username"]=$_POST["username"];
						  $pwd=yypwd(trim($_POST["pwd"]));
					      $data["pwd"]=$pwd;
					      $data["status"]=1;
					      $data["addtime"]=$curtime;
							 $retres=$this->db->insert($data);
							 if($retres){
										$doing="添加会员—".$data["username"];
										$yyact="AddMember";
										insert_admlogs($doing,$yyact);
										messagebox("添加会员成功！",url_admin('init'),"success");
							 }else{
										messagebox("添加会员失败！",url_admin('init'),"error");
							 }
					  }
		        if($_POST["yyact"]=="edit"){
					
				  if(!getroot('member','edit')){
					messagebox(Lan('no_permission_memberedit'),'back',"warn");			
					} 						
					
					   $id=$_POST["id"];
					   if(!empty($_POST["pwd"])){
				          $pwd=yypwd(trim($_POST["pwd"]));
					      $data["pwd"]=$pwd;
					   }
					   if(!$this->check_exist($id)){
							messagebox("该会员不存在！",url_admin('init'),"warn");
					   }
					   $retres=$this->db->update($data,"member","id={$id}");
						if($retres){
									$doing="更新会员—".$_POST["username"];
									$yyact="UpdateMember";
									insert_admlogs($doing,$yyact);
									messagebox("会员更新成功！",url_admin('init'),"success");
						 }else{
									messagebox("会员更新失败！",url_admin('init'),"error");
						 }
			  }			  
		  }
		require tpl_adm('member_add');
	 }	
	
    public function finaldelete() {
		  if(!getroot('member','del')){
			messagebox(Lan('no_permission_memberdel'),'back',"warn");			
			} 	
            $id = $_REQUEST["id"];
		    if(!is_array($id)){
			  $id=compact('id');
			}
			$idarray=$id;
            if (isset($idarray)) {
				foreach($idarray as $key=>$var){
				 		if(!is_numeric($var)){
					        messagebox("错误的参数！",'back',"warn");			
					    }
				        $idarray[$key]=usafestr($var);
						}
				 $idarray=implode(",",$idarray);
				 $retres =$this->db->delete("member","id in ({$idarray})");
                if ($retres!== false) {
				    messagebox(Lan('admin_delete_success'),url_admin('init','member'),"success");
                } else {
				    messagebox(Lan('admin_delete_error'),url_admin('init','member'),"warn");
                }
            } else {
				   messagebox(Lan('admin_delall_lessone'),url_admin('init','member'),"warn");
            }
    }	
	
    function export_excel() {
		 if(!getroot('member','view')){
	 		messagebox(Lan('no_permission_memberview'),'back',"warn");			
		   } 
		    $strsql="select username,name,mobile,phone,email,company,address,status,addtime,updatetime,lastlogintime from `#yunyecms_member` order by addtime desc";
		    $xlsData=$this->db->select($strsql); 
			$xlsName  = "会员";
			foreach($xlsData as $k=>$var){
				 $status=$var['status'];
				 $strstus="";
				 switch($status){
					  case 0:	
					  $strstus="未审核";
					  break; 
					  case 1:	
					  $strstus="已审核";
					  break;  
					  default:
					  $strstus="未审核";
				 }
				 $xlsData[$k]['addtime']=date("Y-m-d h:i:s",$xlsData[$k]['addtime']);
				 $xlsData[$k]['updatetime']=date("Y-m-d h:i:s",$xlsData[$k]['updatetime']);
				 $xlsData[$k]['lastlogintime']=date("Y-m-d h:i:s",$xlsData[$k]['lastlogintime']);
				 $xlsData[$k]['status']=$strstus;
				 }
			     $xlsCell  = array(
				  array('username','用户名'),
				  array('name','姓名'),
				  array('mobile','手机'),
				  array('phone','电话'),
				  array('email','邮箱'),
				  array('company','公司'),
				  array('address','地址'),
				  array('status','状态'),
				  array('addtime','注册时间'),
				  array('updatetime','更新时间'),
				  array('lastlogintime','上次登录时间'),
			  );
			  exportExcel($xlsName,$xlsCell,$xlsData);
      }			
	
		
    public function check(){
	    if(!getroot('member','edit')){
		messagebox(Lan('no_permission_memberedit'),'back',"warn");			
		} 		
        parent::infocheck("member",$this->db);
    }	
	
    public function nocheck() {
		if(!getroot('member','edit')){
			messagebox(Lan('no_permission_memberedit'),'back',"warn");			
		} 		
        parent::infonocheck("member",$this->db);
    }	
	
	private function check_exist($id) {
		 $id = trim($id);
		 if(empty($id)){
		     return false;
			 }else{
			    if(!is_numeric($id)){
					  return false;
				 }
			  if ($this->db->find("select count(*) as cnt from `#yunyecms_member` where `id`= {$id}")){
				  return true;
			  }				 
		  }
	  }	

	 
}
?>
