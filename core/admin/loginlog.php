<?php
defined('IN_YUNYECMSAdmin') or exit('No permission.');
core::load_admin_class('admin');
class loginlog extends YUNYE_admin {
	private $db;
	private $admuser;
	function __construct() {
		$this->db = core::load_model('adminloginlog_model');
		$this->admuser=IsAdmLogin($this->db);
		parent::__construct();
	}
	 //加载首页
	  public function init() {
		  $parnav='<li><a href=\"'.url_admin('init','user','',$this->hashurl['usvg']).'\" target=\"maincontent\">管理员</a></li><li><a href=\"'.url_admin('init','loginlog','',$this->hashurl['usvg']).'\" target=\"maincontent\">登录日志</a></li><li class=\"active\">登录日志管理</li>';
		  if(!getroot('users','logs')){
			  messagebox(Lan('no_permission'),'back',"warn");			
		   }
		 $pagesize=20;
		 $sqlquery="select * from `#yunyecms_adminloginlog`  ";
		 $where=" where `username`<>'' ";
		 $sqlcnt=" select count(*) from `#yunyecms_adminloginlog` ";
		 $order=" order by `loginid` desc ";
		  if(isset($_REQUEST)){
		   if(!empty($_REQUEST["logindate"])){
			    $logindate=trim($_REQUEST["logindate"]);
				$logindate=explode("-",$logindate);
				$sdate=strtotime($logindate[0]." 00:00:00");
				$edate=strtotime($logindate[1]." 23:59:59");
				$sdatestr=date("Y/m/d",$sdate);
				$edatestr=date("Y/m/d",$edate);
				if($sdate>=$edate){
			        messagebox(Lan('startdate_gt_enddate'),'back',"warn");
					}
					$where=$where." and ( logintime >={$sdate} and logintime <={$edate} ) ";
			   }
		   if(!empty($_REQUEST["searchkey"])){
		        $searchkey=usafestr(trim($_REQUEST["searchkey"]));
		        $where=$where." and ( `username`  like '%{$searchkey}%' or  `loginip`  like '%{$searchkey}%' )";
			  }
			  
		    if(isset($_REQUEST["status"])){
				  if($_REQUEST["status"]!=''){
					   $status=usafestr(trim($_REQUEST["status"]));
					   $where=$where." and `status` = {$status} ";
					}
			  }
		 }
		 
		 $pagearr=$this->db->pagelist($sqlcnt,$sqlquery,$where,$order,$pagesize);
		  
		 if($pagearr["count"]!=0){
		 $list=$pagearr["query"];
		 $pages=$pagearr["page"];
		 }
		 require tpl_adm('adminloginlog_list');
	  }
	  

	  public function adminloginlog_delete(){
		   if(!getroot('users','logs')){
			  messagebox(Lan('no_permission'),'back',"warn");			
		   }
		 if(!empty($_GET["id"])){
			 $id=trim($_GET["id"]);
			 if(!is_numeric($id)){
			   messagebox(Lan('admin_id_notnumber'),url_admin('init','','',$this->hashurl['usvg']),"warn");
			 }
			 $cadminloginlog=$this->db->find("select * from `#yunyecms_adminloginlog` where `loginid`= {$id}");
			 if(empty($cadminloginlog)){
				 messagebox(Lan('adminloginlog_not_exist'),url_admin('init','','',$this->hashurl['usvg']),"warn");	
			  }
			$query=$this->db->query("delete from `#yunyecms_adminloginlog` where loginid='$id'");
			if($query){
				    messagebox(Lan('admin_delete_success'),url_admin('init','','',$this->hashurl['usvg']),"success");
				}else{
				    messagebox(Lan('admin_delete_error'),url_admin('init','','',$this->hashurl['usvg']),"warn");
					}
			}
	 }	
	 
	  public function deleteall(){
		  if(!getroot('users','logs')){
			  messagebox(Lan('no_permission'),'back',"warn");			
		   }
		 if(!empty($_POST["adminloginid"])){
			 $ids=$_POST["adminloginid"];
			 $idstr="";
			    foreach($ids as $key=>$var){
				  if(!is_numeric($var)){
					   messagebox(Lan('adminloginlog_id_notnumber'),url_admin('init','','',$this->hashurl['usvg']),"warn");
				   }
				        $ids[$key]=usafestr($var);
				 }
			 $idstr=implode(",",$ids);
			 $query=$this->db->query("delete from `#yunyecms_adminloginlog` where loginid in({$idstr})");
			  if($query){
				    messagebox(Lan('admin_deleteall_success'),url_admin('init','','',$this->hashurl['usvg']),"success");
				}else{
				    messagebox(Lan('admin_deleteall_error'),url_admin('init','','',$this->hashurl['usvg']),"warn");
					}
			}else{
			   messagebox(Lan('admin_delall_lessone'),url_admin('init','','',$this->hashurl['usvg']),"warn");
		    }
	 }	 	 
	  
	 
	 
	 
	 
	 
	 
	private function checkadminloginlog_exist($loginid) {
		$loginid =  trim($loginid);
		 if(empty($loginid)){
		     return false;
			 }else{
			  if ($this->db->find("select `adminloginlogname` from `#yunyecms_adminloginlog` where `loginid`= {$loginid}")){
				  return true;
			  }				 
		  }
	  }	

	 
	 
	 
	 
}
?>
