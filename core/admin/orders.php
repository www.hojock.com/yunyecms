<?php
defined('IN_YUNYECMSAdmin') or exit('No permission.');
core::load_admin_class('admin');
class orders extends YUNYE_admin {
	private $db;
	private $admuser;
	function __construct() {
		$this->db = core::load_model('orders_model');
		$this->admuser=IsAdmLogin($this->db);
		parent::__construct();
	}
	 //加载首页
	  public function init() {
		  if(!getroot('shop','ordersview')){
			  messagebox(Lan('no_permission'),'back',"warn");			
		   } 
		  $parnav='<li><a href=\"'.url_admin('init',"lang").'\" target=\"maincontent\">系统</a></li><li><a href=\"'.url_admin('init','orders').'\" target=\"maincontent\">云商城</a></li><li class=\"active\">订单列表</li>';
		   if(!getroot('users','logs')){
			  messagebox(Lan('no_permission'),'back',"warn");			
		   }
		 $pagesize=20;
		 $sqlquery="select * from `#yunyecms_orders`  ";
		 $where=" where sn<>'' ";
		 $sqlcnt=" select count(*) from `#yunyecms_orders` ";
		 $order=" order by `updatetime` desc,`addtime` desc,`id` desc ";
		  if(isset($_REQUEST)){
		   if(!empty($_REQUEST["searchkey"])){
		        $searchkey=usafestr(trim($_REQUEST["searchkey"]));
		        $where=$where." and ( `sn`  like '%{$searchkey}%' or  `title`  like '%{$searchkey}%' )";
			  }
		 }
		 $pagearr=$this->db->pagelist($sqlcnt,$sqlquery,$where,$order,$pagesize);
		 if($pagearr["count"]!=0){
			 $list=$pagearr["query"];
			     foreach($list as $key=>$var){
				  $goods= unserialize($var["goods"]);
				  $list[$key]["goods"]=$goods;
				  $list[$key]["member"]=$this->db->getbyid($var["userid"],"member");
				  foreach($list[$key]["goods"] as $k=>$v){
				     $list[$key]["goods"][$k]["url"]=url('content/index/show',array("catid"=>$v["catid"],"id"=>$v["gid"]));
				  }
			    }
			 $page=$pagearr["page"];
		 }
		 require tpl_adm('orders_list');
	  }
	
 public function add(){
		   if(!empty($_GET["id"])){
					$parnav='<li><a href=\"'.url_admin('init','lang').'\" target=\"maincontent\">云商城</a></li><li class=\"active\">修改订单</li>';
					 $id=trim($_GET["id"]);
					 if(!is_numeric($id)){
					   messagebox("订单参数错误",url_admin('init'));
					 }
				     $id=usafestr($id);
					 $row=$this->db->find("select * from `#yunyecms_orders` where `id`= {$id}");
					 if(empty($row)){
						   messagebox("该订单不存在",$_SERVER['HTTP_REFERER']);			
					  }
					$yyact="edit";
			 }
	        if(isset($_POST["yyact"])){
				      $_POST=ustripslashes($_POST);
					  $data["title"]=usafestr($_POST["title"]);
					  $data["mobile"]=usafestr($_POST["mobile"]);
					  $data["name"]=usafestr($_POST["name"]);
					  $data["address"]=usafestr($_POST["address"]);
					  $data["remark"]=usafestr($_POST["remark"]);
					  $data["updatetime"]=time();
					  $data["status"]=usafestr($_POST["status"]);
					  if(empty($data["title"])){
							messagebox("订单标题不能为空，谢谢!",$_SERVER['HTTP_REFERER']);		
					   }
		       if($_POST["yyact"]=="edit"){
					  if(!getroot('shop','ordersedit')){
						  messagebox(Lan('no_permission'),'back',"warn");			
					   } 
					   $id=usafestr($_POST["id"]);
					   if(!$this->check_exist($id)){
							messagebox("该订单不存在！",url_admin('init'),"warn");
					   }
					   $retres=$this->db->update($data,"orders","id={$id}");
					   if($retres){
									$doing="修改订单—".$data["title"];
									$yyact="Update_Orders";
									insert_admlogs($doing,$yyact);
									messagebox("订单信息更新成功！",url_admin('init'),"success");
						 }else{
									messagebox("订单信息更新失败！",url_admin('init'),"error");
						 }
			  }			  
		  }
		require tpl_adm('orders_edit');
	 }	
	
    public function finaldelete() {
		   if(!getroot('shop','ordersdel')){
			  messagebox(Lan('no_permission'),'back',"warn");			
		    }
            $id = $_REQUEST["id"];
		    if(!is_array($id)){
			  $id=compact('id');
			}
			$idarray=$id;
            if (isset($idarray)) {
				   foreach($idarray as $key=>$var){
				 		if(!is_numeric($var)){
					        messagebox("错误的参数！",'back',"warn");			
					    }
				        $idarray[$key]=usafestr($var);
						}
				 $idarray=implode(",",$idarray);
				 $retres =$this->db->delete("orders","id in ({$idarray})");
                if ($retres!== false) {
				    messagebox(Lan('admin_delete_success'),url_admin('init','orders'),"success");
                } else {
				    messagebox(Lan('admin_delete_error'),url_admin('init','orders'),"warn");
                }
            } else {
				   messagebox(Lan('admin_delall_lessone'),url_admin('init','orders'),"warn");
            }
    }	
	
    function export_excel() {
			 if(!getroot('shop','ordersview')){
					messagebox(Lan('no_permission'),'back',"warn");			
			 } 
		    $strsql="select sn,money,goods,userid,name,address,mobile,addtime,status from `#yunyecms_orders` order by addtime desc";
		    $xlsData=$this->db->select($strsql); 
			$xlsName  = "订单";
			foreach($xlsData as $k=>$var){
				 $status=$var['status'];
				 $goods=$var['goods'];
				 $strstus="";
				 switch($status){
					  case 0:	
					  $strstus="未付款";
					  break; 
					  case 1:	
					  $strstus="已付款";
					  break;  
					  case 2:	
					  $strstus="已完成";
					  break;  
					  default:
					  $strstus="未付款";
				 }
                     $goods=unserialize($goods);
				     $xlsData[$k]["goods"]=$goods;
				     $strgoods="";
				     foreach($xlsData[$k]["goods"] as $kk=>$vv){
						 $gid=$vv['gid'];
						 $num=$vv['num'];
						 $strgoods=$strgoods."产品名称：".$vv['title']."——单价：".$vv['price']."——数量：".$vv['num']."\r\n";
					   }
				 $xlsData[$k]["goods"]=$strgoods;
				 $xlsData[$k]['addtime']=date("Y-m-d h:i:s",$xlsData[$k]['addtime']);
				 $xlsData[$k]['status']=$strstus;
				 }
			     $xlsCell  = array(
				  array('sn','订单号'),
				  array('money','价格'),
				  array('goods','商品'),
				  array('name','收货人姓名'),
				  array('mobile','电话'),
				  array('address','收货地址'),
				  array('status','订单状态'),
				  array('addtime','时间'),
			  );
			  exportExcel($xlsName,$xlsCell,$xlsData);
      }			
	
		
    public function check(){
		if(!getroot('shop','ordersedit')){
			 messagebox(Lan('no_permission'),'back',"warn");			
		 } 
        parent::infocheck("orders",$this->db);
    }	
	
    public function nocheck() {
		if(!getroot('shop','ordersedit')){
				 messagebox(Lan('no_permission'),'back',"warn");			
		 } 
        parent::infonocheck("orders",$this->db);
    }	
	
	 
	private function check_exist($id) {
		 $id = trim($id);
		 if(empty($id)){
		     return false;
			 }else{
			    if(!is_numeric($id)){
					  return false;
				 }
			  if ($this->db->find("select count(*) as cnt from `#yunyecms_orders` where `id`= {$id}")){
				  return true;
			  }				 
		  }
	  }	

	 
}
?>
