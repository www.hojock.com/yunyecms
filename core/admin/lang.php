<?php
defined('IN_YUNYECMSAdmin') or exit('No permission.');
core::load_admin_class('admin');
class lang extends YUNYE_admin {
	private $db;
	private $admuser;
	function __construct() {
		$this->db = core::load_model('lang_model');
		$this->admuser=IsAdmLogin($this->db);
		parent::__construct();
	}
	 //加载首页
	  public function init() {
		if(!getroot('lang','view')){
			messagebox(Lan('no_permission_langview'),'back',"warn");			
		 }		  
		  $parnav='<li><a href=\"'.url_admin('init',"lang").'\" target=\"maincontent\">系统</a></li><li><a href=\"'.url_admin('init','lang').'\" target=\"maincontent\">语言版管理</a></li><li class=\"active\">语言版列表</li>';
		   if(!getroot('users','logs')){
			  messagebox(Lan('no_permission'),'back',"warn");			
		   }
		 $pagesize=20;
		 $sqlquery="select * from `#yunyecms_lang`  ";
		 $where=" where 1=1 and title<>'' ";
		 $sqlcnt=" select count(*) from `#yunyecms_lang` ";
		 $order=" order by `ordernum` asc ";
		  if(isset($_REQUEST)){
		   if(!empty($_REQUEST["searchkey"])){
		        $searchkey=usafestr(trim($_REQUEST["searchkey"]));
		        $where=$where." and ( `title`  like '%{$searchkey}%' or  `title_en`  like '%{$searchkey}%' )";
			  }
		 }
		 $pagearr=$this->db->pagelist($sqlcnt,$sqlquery,$where,$order,$pagesize);
		 if($pagearr["count"]!=0){
			 $list=$pagearr["query"];
			 $pages=$pagearr["page"];
		 }
		 require tpl_adm('lang_list');
	  }
	
	
 public function add(){
		   if(!empty($_GET["id"])){
				if(!getroot('lang','edit')){
					messagebox(Lan('no_permission_langedit'),'back',"warn");			
				 }				   
					$parnav='<li><a href=\"'.url_admin('init','lang').'\" target=\"maincontent\">语言版管理</a></li><li class=\"active\">修改语言版</li>';
					$id=trim($_GET["id"]);
					 if(!is_numeric($id)){
					   messagebox("语言版参数错误",url_admin('init'));
					 }
					 $row=$this->db->find("select * from `#yunyecms_lang` where `id`= {$id}");
					 if(empty($row)){
						   messagebox("语言版不存在",$_SERVER['HTTP_REFERER']);			
					  }
					$yyact="edit";
			}else{
			   	if(!getroot('lang','add')){
					messagebox(Lan('no_permission_langadd'),'back',"warn");			
				 }	
				$yyact=yyact_get("add");
				$parnav='<li><a href=\"'.url_admin('init','lang').'\" target=\"maincontent\">内容</a></li><li><a href=\"'.url_admin('init','lang').'\" target=\"maincontent\">语言版管理</a></li><li class=\"active\">增加语言版</li>';
						$sqlordermax="select max(ordernum) as maxordernum from `#yunyecms_lang`";
						$ordermaxquery=$this->db->find($sqlordermax);
						$ordermax=$ordermaxquery["maxordernum"]+1;
				}
	        if(isset($_POST["yyact"])){
				      $_POST=ustripslashes($_POST);
				      $logo=uhtmlspecialchars(trim($_POST["logo"]));
				      $qrcode=uhtmlspecialchars(trim($_POST["qrcode"]));
				      $copyright=!empty($_POST["copyright"])?uhtmlspecialchars(trim($_POST["copyright"])):"";
				      $_POST=yytrim($_POST);
					  $data["title"]=$_POST["title"];
					  $data["title_en"]=$_POST["title_en"];
					  $data["theme"]=$_POST["theme"];
					  $data["landir"]=$_POST["landir"];
					  $data["sitename"]=$_POST["sitename"];
					  $data["seotitle"]=$_POST["seotitle"];
					  $data["seokey"]=$_POST["seokey"];
					  $data["seodesc"]=$_POST["seodesc"];
					  $data["copyright"]=$copyright;
					  $data["ordernum"]=$_POST["ordernum"];
					  $data["isdefault"]=$_POST["isdefault"];
				      $data["tel"]=$_POST["tel"];
				      $data["mobile"]=$_POST["mobile"];
				      $data["mail"]=$_POST["mail"];
				      $data["qq"]=$_POST["qq"];
				      $data["address"]=$_POST["address"];
				 	  $data["logo"]=$logo;
				 	  $data["qrcode"]=$qrcode;
					  if(empty($data["title"])){
							messagebox("语言版名称不能为空，谢谢!");		
					   }
				      if(empty($data["theme"])){
							messagebox("当前模板风格不能为空，谢谢!");		
					   }
					  if(empty($data["landir"])){
							messagebox("模板文件或语言包目录不能为空，谢谢!");		
					   }
			        if($_POST["yyact"]=="add"){
						
					 if($data["isdefault"]==1){
						  $check=$this->db->GetCount("select count(*) from `#yunyecms_lang` where isdefault=1 ");
						  if($check>0){
							  messagebox("默认语言已存在,请重新选择！",'back',"warn");
						   }
					  } 	
		
					 if($this->db->GetCount("select count(*) from `#yunyecms_lang` where  title='{$data["title"]}'")){
						 messagebox("该语言版已经存在，请重新填写!");		
					 }
					 if($this->db->GetCount("select count(*) from `#yunyecms_lang` where  landir='{$data["landir"]}'")){
						 messagebox("模板文件或语言包目录已经存在，请重新填写!");		
					 }	
					 $data["status"]=1;
					 $data["addtime"]=time();
					 $data["updatetime"]=time();
					 $retres=$this->db->insert($data);
					 if($retres){
								$doing="添加语言版—".$data["title"];
								$yyact="addlang";
								insert_admlogs($doing,$yyact);
								messagebox("添加语言版成功！",url_admin('init'),"success");
					 }else{
								messagebox("添加语言版失败！",url_admin('init'),"error");
					 }
			  }
		         if($_POST["yyact"]=="edit"){
					  $id=$_POST["id"];
					  $row=$this->db->find("select * from `#yunyecms_lang` where `id`= {$id}");
					  if(empty($row)){
						  messagebox("该语言版不存在！",url_admin('init'),"warn");
					   }
					  if($data["isdefault"]==1){
						  if($row["isdefault"]!=1){
							   $check=$this->db->GetCount("select count(*) from `#yunyecms_lang` where isdefault=1 and id<>{$id} ");
							   if($check>0){
								  messagebox("默认语言已存在,请重新选择！",'back',"warn");
							   }
						  }
					   } 	
					 if($_POST["oldtitle"]!=$data["title"]){
							 if($this->db->GetCount("select count(*) from `#yunyecms_lang` where  title='{$data["title"]}'")){
								 messagebox("该语言版已经存在，请重新填写!");		
							 } 
					 }
					if($_POST["oldlandir"]!=$data["landir"]){
						  if($this->db->GetCount("select count(*) from `#yunyecms_lang` where  landir='{$data["landir"]}'")){
						   messagebox("模板文件或语言包目录已经存在，请重新填写!");		
					      }		
					 }
					  $data["updatetime"]=time();
					   $retres=$this->db->update($data,"lang","id={$id}");
						if($retres){
									$doing="更新语言版—".$data["title"];
									$yyact="updatelang";
									insert_admlogs($doing,$yyact);
									messagebox("语言版更新成功！",url_admin('init'),"success");
						 }else{
									messagebox("语言版更新失败！",url_admin('init'),"error");
						 }
			  }			  
		  }
		require tpl_adm('lang_add');
	 }	
	
    public function finaldelete() {
		     $count_lang=$this->db->GetCount("select count(*) from `#yunyecms_lang` where status=1 ");
		     if($count_lang<=1){
				   messagebox("对不起,您必须至少保留一个已开启的语言版!");		
			  }
			if(!getroot('lang','del')){
				messagebox(Lan('no_permission_langdel'),'back',"warn");			
			 }
            $id = $_REQUEST["id"];
		    if(!is_array($id)){
			  $id=compact('id');
			}
			$idarray=$id;
			 foreach($idarray as $key=>$var){
				 		if(!is_numeric($var)){
					        messagebox("错误的参数！",'back',"warn");			
					    }
                        $var = usafestr($var);
		                $curlang=$this->db->find("select id,qrcode,logo,copyright,isdefault from `#yunyecms_lang` where `id`= {$var}");
						if($curlang){
							if($curlang["isdefault"]==1){
								 $count_lang_def=$this->db->GetCount("select count(*) from `#yunyecms_lang` where isdefault=1 ");
								 if($count_lang_def<=1){
									   messagebox("对不起,默认语言版不能删除!");		
								  }	
							}
                          $logo=$curlang["logo"];
                          $qrcode=$curlang["qrcode"];
                          $ct_content=$curlang["copyright"];
						  $ct_content_img=getcontentimg($ct_content);
							 if(!empty($picfile)){
								 file_delete($picfile);
							 }
							 if(!empty($qrcode)){
								 file_delete($qrcode);
							 }
							 foreach($ct_content_img as $key3=>$var3){
									file_delete($var3);
								 }							
						}
					}
            if (isset($idarray)) {
				 $idarray=implode(",",$idarray);
				 $retres =$this->db->delete("lang","id in ({$idarray})");
                if ($retres!== false) {
				    messagebox(Lan('admin_delete_success'),url_admin('init','lang'),"success");
                } else {
				    messagebox(Lan('admin_delete_error'),url_admin('init','lang'),"warn");
                }
            } else {
				   messagebox(Lan('admin_delall_lessone'),url_admin('init','lang'),"warn");
            }
    }	
	
		
    public function check(){
		if(!getroot('lang','edit')){
			messagebox(Lan('no_permission_langedit'),'back',"warn");			
		 }	
        parent::infocheck("lang",$this->db);
    }	
	
    public function nocheck() {
		if(!getroot('lang','edit')){
			messagebox(Lan('no_permission_langedit'),'back',"warn");			
		 }
        parent::infonocheck("lang",$this->db);
    }		
	 
	private function check_exist($id) {
		 $id = trim($id);
		 if(empty($id)){
		     return false;
			 }else{
			    if(!is_numeric($id)){
					  return false;
				 }
			  if ($this->db->find("select count(*) as cnt from `#yunyecms_lang` where `id`= {$id}")){
				  return true;
			  }				 
		  }
	  }	

	 
}
?>
