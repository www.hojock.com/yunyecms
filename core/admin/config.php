<?php
defined('IN_YUNYECMSAdmin') or exit('No permission.');
core::load_admin_class('admin');
class config extends YUNYE_admin {
	private $db;
	private $admuser;
	function __construct() {
		$this->db = core::load_model('config_model');
		$this->admuser=IsAdmLogin($this->db);
		parent::__construct();
	}
	 //加载首页
	  public function init() {
		  $parnav='<li><a href=\"'.url_admin('save',"config").'\" target=\"maincontent\">系统</a></li><li><a href=\"'.url_admin('save','config').'\" target=\"maincontent\">系统配置信息</a></li>';
		   if(!getroot('users','logs')){
			  messagebox(Lan('no_permission'),'back',"warn");			
		   }
		 require tpl_adm('config');
	  }
	
 public function save(){
		 if(!getroot('system','setting')){
			messagebox(Lan('no_permission_sysconfig'),'back',"warn");			
		 }	
		  $yyact="edit";
		  $parnav='<li><a href=\"'.url_admin('save',"config").'\" target=\"maincontent\">系统</a></li><li><a href=\"'.url_admin('save','config').'\" target=\"maincontent\">系统配置信息</a></li>';
				$sqlcnt="select count(*) from `#yunyecms_config`";
                $checkcnt=$this->db->GetCount($sqlcnt);
				if($checkcnt>0){
				  $strsql="select * from `#yunyecms_config` order by id desc limit 0,1";
				  $curconfig=$this->db->find($strsql);
				  if(!empty($curconfig)){
					  $curconfig['mobilecfg']=unserialize($curconfig['mobilecfg']);
					  $curconfig['content']=unserialize($curconfig['content']);
					  $curconfig['email']=unserialize($curconfig['email']);
					  $curconfig['sms']=unserialize($curconfig['sms']);
					  $curconfig['weixin']=unserialize($curconfig['weixin']);
					  $curconfig['alipay']=unserialize($curconfig['alipay']);
					  $curconfig['upload']=unserialize($curconfig['upload']);
					  $rs=$curconfig;
				   }else{
					  messagebox("配置信息错误！");
				  }
				}	 
	 
	        if(isset($_POST["yyact"])){
			  $_POST=ustripslashes($_POST);
			  $content=TrimArray($_POST['content']);
			  $mobilecfg=TrimArray($_POST['mobilecfg']);
			  $email=TrimArray($_POST['email']);
		      $sms=TrimArray($_POST['sms']);
		      $weixin=TrimArray($_POST['weixin']);
		      $alipay=TrimArray($_POST['alipay']);
		      $upload=TrimArray($_POST['upload']);
		      if(!empty($email)) { 
				  if(!empty($email["pwd"])){
					  $email["pwd"]=yunyecms_strencode($email["pwd"]);
				  }else{
					  $strsql="select * from `#yunyecms_config` order by id desc limit 0,1";
					  $curconfig=$this->db->find($strsql);
					  $curconfig['email']=unserialize($curconfig['email']);
					  $email["pwd"]=$curconfig['email']['pwd'];
				  }
					  $email=serialize($email); }else{ $email=''; }
					  if(!empty($content)) { $content=serialize($content);  }else{ $content=''; }
					  if(!empty($mobilecfg)) { $mobilecfg=serialize($mobilecfg);  }else{ $mobilecfg=''; }
					  if(!empty($sms)) { $sms=serialize($sms);  }else{ $sms=''; }
					  if(!empty($weixin)) { $weixin=serialize($weixin); }else{ $weixin=''; }
					  if(!empty($alipay)) { $alipay=serialize($alipay);  }else{ $alipay=''; }
					  if(!empty($upload)) { $upload=serialize($upload); }else{ $upload=''; }
				      $data["isclose"]=usafestr(trim($_POST["isclose"]));
				      $data["iskefu"]=usafestr(trim($_POST["iskefu"]));
				      $data["ismall"]=usafestr(trim($_POST["ismall"]));
				      $data["isquicknav"]=usafestr(trim($_POST["isquicknav"]));
				      $data["ismailsub"]=usafestr(trim($_POST["ismailsub"]));
				      $data["ismailreg"]=usafestr(trim($_POST["ismailreg"]));
				      $data["isfeedbackmail"]=usafestr(trim($_POST["isfeedbackmail"]));
				      $data["ishtml"]=usafestr(trim($_POST["ishtml"]));
				      $data["verifymode"]=usafestr(trim($_POST["verifymode"]));
				      $data["tel"]=usafestr(trim($_POST["tel"]));
				      $data["mobile"]=usafestr(trim($_POST["mobile"]));
				      $data["mail"]=usafestr(trim($_POST["mail"]));
				      $data["qq"]=usafestr(trim($_POST["qq"]));
				      $data["grantcode"]=usafestr(trim($_POST["grantcode"]));
				 	  $data["logo"]=uhtmlspecialchars(trim($_POST["logo"]));
				 	  $data["qrcode"]=uhtmlspecialchars(trim($_POST["qrcode"]));
				      $data["sitename"]=usafestr(trim($_POST["sitename"]));
					  $data["seotitle"]=usafestr(trim($_POST["seotitle"]));
					  $data["seokey"]=usafestr(trim($_POST["seokey"]));
					  $data["seodesc"]=usafestr(trim($_POST["seodesc"]));
				      $data["email"]= $email;
				      $data["content"]= $content;
				      $data["mobilecfg"]= $mobilecfg;
				      $data["sms"]= $sms;
				      $data["weixin"]= $weixin;
				      $data["alipay"]= $alipay;
				      $data["upload"]= $upload;
		            if($_POST["yyact"]=="edit"){
					   $id=$_POST["id"];
					   if(!$this->check_exist($id)){
							messagebox("配置信息错误！",url_admin('init'),"warn");
					   }
					   $retres=$this->db->update($data,"config","id={$id}");
					   if($retres){
									$doing="更新系统配置";
									$yyact="UpdateConfig";
									insert_admlogs($doing,$yyact);
									messagebox("系统配置信息更新成功！",url_admin('save','config'),"success");
						 }else{
									messagebox("系统配置信息更新失败！",url_admin('save','config'),"error");
						 }
			  }	
				
		  }
		require tpl_adm('config');
	 }	
	
	
	
public function savetop(){
					if(!getroot('system','setting')){
						messagebox(Lan('no_permission_sysconfig'),'back',"warn");			
					 }	
		            if(empty($_POST['isclose'])){$isclose=2;} else { $isclose=1; }
					if(empty($_POST['iskefu'])){$iskefu=2;} else { $iskefu=1; }
					if(empty($_POST['isfeedbackmail'])){$isfeedbackmail=2;} else { $isfeedbackmail=1; }
					if(empty($_POST['ismailreg'])){$ismailreg=2;} else { $ismailreg=1; }
					if(empty($_POST['ismailsub'])){$ismailsub=2;} else { $ismailsub=1; }
					if(empty($_POST['ishtml'])){$ishtml=2;} else { $ishtml=1; }
					if(empty($_POST['ismall'])){$ismall=2;} else { $ismall=1; }
					if(empty($_POST['verifymode'])){$verifymode=2;} else { $verifymode=1; }
					  $data["isclose"]=$isclose;
				      $data["iskefu"]=$iskefu;
				      $data["ismall"]=$ismall;
				      $data["ismailsub"]=$ismailsub;
				      $data["ismailreg"]=$ismailreg;
				      $data["isfeedbackmail"]=$isfeedbackmail;
				      $data["ishtml"]=$ishtml;
				      $data["verifymode"]=$verifymode;
					  if(empty($_POST['upload']['isthumb'])){$_POST['upload']['isthumb']=0;}else { $_POST['upload']['isthumb']=1; }
					  if(!empty($upload)) { $upload=serialize($upload); }else{ $upload=''; }
					  $upload=TrimArray($_POST['upload']);
					  $upload=serialize($_POST['upload']);	
				      $data["upload"]= $upload;
		            if($_POST["yyact"]=="edit"){
					   $id=$_POST["id"];
					   if(!$this->check_exist($id)){
							messagebox("配置信息错误！",url_admin('init'),"warn");
					   }
					   $retres=$this->db->update($data,"config","id={$id}");
					   if($retres){
									$doing="更新系统配置";
									$yyact="UpdateConfig";
									insert_admlogs($doing,$yyact);
									messagebox("系统配置信息更新成功！",url_admin('save','config'),"success");
						 }else{
									messagebox("系统配置信息更新失败！",url_admin('save','config'),"error");
						 }
			  }
				
	 }	
		
		
    public function check(){
        parent::infocheck("lang",$this->db);
    }	
	
    public function nocheck() {
        parent::infonocheck("lang",$this->db);
    }		
	 
	private function check_exist($id) {
		 $id = trim($id);
		 if(empty($id)){
		     return false;
			 }else{
			    if(!is_numeric($id)){
					  return false;
				 }
			  if ($this->db->find("select count(*) as cnt from `#yunyecms_config` where `id`= {$id}")){
				  return true;
			  }				 
		  }
	  }	

	 
}
?>
