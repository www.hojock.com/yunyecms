<?php
defined('IN_YUNYECMSAdmin') or exit('No permission.');
core::load_admin_class('admin');
class department extends YUNYE_admin {
	private $db;
	private $admuser;
	function __construct() {
		$this->db = core::load_model('department_model');
		$this->admuser=IsAdmLogin($this->db);
		parent::__construct();
		if(!getroot('users','department')){
			messagebox(Lan('no_permission'),'back',"warn");			
		 } 
	}
	 //加载首页
	  public function init() {
		$parnav='<li><a href=\"'.url_admin('init','user','',$this->hashurl['usvg']).'\" target=\"maincontent\">管理员</a></li><li><a href=\"'.url_admin('init','department','',$this->hashurl['usvg']).'\" target=\"maincontent\">部门</a></li><li class=\"active\">部门管理</li>';
		 $pagesize=20;
		 $sqlquery="select `departmentid`, `departmentname`,`status` from `#yunyecms_department`  ";
		 $where=" where `departmentname`<>'' ";
		 $sqlcnt=" select count(*) from `#yunyecms_department` ";
		 $order=" order by `departmentid` asc ";
		  if(isset($_POST["searchkey"])){
		   $searchkey=usafestr(trim($_POST["searchkey"]));
		   if(empty($searchkey)){
			   messagebox(Lan('search_key_empty'),url_admin('init','','',$this->hashurl['usvg']),"warn");
			   exit;		
			   }else{
		        $where=" where `departmentname`  like'%{$searchkey}%' ";
				   }
		 }
		 $pagearr=$this->db->pagelist($sqlcnt,$sqlquery,$where,$order,$pagesize);
		 if($pagearr["count"]!=0){
		 $list=$pagearr["query"];
		 $pages=$pagearr["page"];
		 }
		 require tpl_adm('department_list');
	  }
	  
	 public function department_add(){
		 if(!empty($_GET["id"])){
			$parnav='<li><a href=\"'.url_admin('init','user','',$this->hashurl['usvg']).'\" target=\"maincontent\">管理员</a></li><li><a href=\"'.url_admin('init','department','',$this->hashurl['usvg']).'\" target=\"maincontent\">部门</a></li><li class=\"active\">修改部门</li>';

			$departmentid=trim($_GET["id"]);
			 if(!is_numeric($departmentid)){
			   messagebox(Lan('department_id_notnumber'),url_admin('init','','',$this->hashurl['usvg']),"warn");
			 }
			$cdepartment=$this->db->find("select * from `#yunyecms_department` where `departmentid`= {$departmentid}");
			 if(empty($cdepartment)){
				   messagebox(Lan('department_not_exist'),url_admin('department_add','department',array('id'=>$departmentid),$this->hashurl['usvg']),"warn");			
			  }
		    $yyact="edit";
			}else{
		    $yyact=yyact_get("add");
			$parnav='<li><a href=\"'.url_admin('init','user','',$this->hashurl['usvg']).'\" target=\"maincontent\">管理员</a></li><li><a href=\"'.url_admin('init','department','',$this->hashurl['usvg']).'\" target=\"maincontent\">部门</a></li><li class=\"active\">添加部门</li>';
				}
	    if(isset($_POST["yyact"])){
			       $departmentname=$_POST["departmentname"];
				   if(array_key_exists("status",$_POST)){$status=$_POST["status"];}else{
					   $status=0;
					   }
			if($_POST["yyact"]=="add"){
				   $this->add_admin_department($departmentname,$status);
			  }
		    if($_POST["yyact"]=="edit"){
			       $id=$_POST["id"];
			       $olddepartmentname=$_POST["olddepartmentname"];
				   $this->edit_admin_department($id,$departmentname,$olddepartmentname,$status);
			  }			  
		  }
		require tpl_adm('department_add');
	 }
	 
	  public function department_delete(){
		 if(!empty($_GET["id"])){
			 $id=trim($_GET["id"]);
			 if(!is_numeric($id)){
			   messagebox(Lan('department_id_notnumber'),url_admin('init','','',$this->hashurl['usvg']),"warn");
			 }
			 $id=usafestr($id);
			 $cdepartment=$this->db->find("select * from `#yunyecms_department` where `departmentid`= {$id}");
			 if(empty($cdepartment)){
				 messagebox(Lan('department_not_exist'),url_admin('init','','',$this->hashurl['usvg']),"warn");	
			  }
			$query=$this->db->query("delete from `#yunyecms_department` where departmentid='$id'");
			if($query){
				    messagebox(Lan('department_delete_success'),url_admin('init','','',$this->hashurl['usvg']),"success");
				}else{
				    messagebox(Lan('department_delete_error'),url_admin('init','','',$this->hashurl['usvg']),"warn");
					}
			}
	 }	 
	 
	private function checkdepartment_exist($departmentname) {
		$departmentname =  trim($departmentname);
		 if(empty($departmentname)){
		     return false;
			 }else{
			  if ($this->db->find("select `departmentname` from `#yunyecms_department` where `departmentname`= '{$departmentname}'")){
				  return true;
			  }				 
		  }
	  }	
	private function add_admin_department($departmentname,$status=1) {
		 $departmentname=usafestr(trim($departmentname));
		 if(empty($departmentname)){
			   messagebox(Lan('department_name_empty'),url_admin('department_add','','',$this->hashurl['usvg']),"error");			
			 }
		 $status=usafestr(trim($status));
		 if(empty($status)){
			 $status=0;
			 }else{
		     $status=(int)trim($status);
				 }
		  if($this->checkdepartment_exist($departmentname)){
				         messagebox(Lan('department_already_exist'),url_admin('department_add','','',$this->hashurl['usvg']),"warn");			
					}else{
				    $strsql="insert into `#yunyecms_department`(`departmentname`,`status`) values('$departmentname',$status)";
					$query=$this->db->query($strsql);
	                $departmentid=$this->db->insert_id();
					if($departmentid){
				         messagebox(Lan('department_add_success'),url_admin('init','','',$this->hashurl['usvg']),"success");			
					}
		    }
	 }	
	 
	private function edit_admin_department($id,$departmentname,$olddepartmentname,$status) {
		 $departmentname=usafestr(trim($departmentname));
		 $olddepartmentname=usafestr(trim($olddepartmentname));
		 $id=usafestr(trim(intval($id)));
		 if(empty($departmentname)||empty($id)||!is_numeric($id)){
			   messagebox(Lan('department_name_empty'),url_admin('department_add','department',array('id'=>$id),$this->hashurl['usvg']),"warn");				
			 }
		  if($departmentname!=$olddepartmentname){
			   $num=$this->db->GetCount("select count(*) as total from `#yunyecms_department` where departmentname='$departmentname' and departmentid<>$id limit 1");
			   if($num){ messagebox(Lan('department_already_exist'),url_admin('department_add','','',$this->hashurl['usvg']),"warn");	}
				   }
		 $status=usafestr(trim($status));
		 if(empty($status)){
			 $status=0;
			 }else{
		     $status=(int)$status;
				 }
				  $strsql="update `#yunyecms_department`  set `departmentname`='$departmentname',`status`=$status where departmentid='$id'";
				  $query=$this->db->query($strsql);
				   if($query){
					   messagebox(Lan('department_edit_success'),url_admin('init','','',$this->hashurl['usvg']),"success");			
					  }
	   }		 
	 
}
?>
