<?php
defined('IN_YUNYECMSAdmin') or exit('No permission.');
core::load_admin_class('admin');
class menu extends YUNYE_admin {
	private $db;
	private $admuser;
	function __construct() {
		$this->db = core::load_model("menu_model");
		$this->admuser=IsAdmLogin($this->db);
		parent::__construct();
	}
	 //加载首页
	  public function init() {
		  $parnav='<li><a href=\"'.url_admin('init',"category").'\" target=\"maincontent\">内容</a></li><li><a href=\"'.url_admin('init','category').'\" target=\"maincontent\">栏目管理</a></li><li class=\"active\">栏目列表</li>';
		   if(!getroot('users','logs')){
			  messagebox(Lan('no_permission'),'back',"warn");			
		   }
		 $pagesize=20;
		 $sqlquery="select * from `#yunyecms_category`  ";
		 $where=" where 1=1 and pid=0 ";
		 $sqlcnt=" select count(*) from `#yunyecms_category` ";
		 $order=" order by `addtime` desc ";
		  if(isset($_REQUEST)){
		   if(!empty($_REQUEST["searchkey"])){
		        $searchkey=usafestr(trim($_REQUEST["searchkey"]));
		        $where=$where." and ( `title`  like '%{$searchkey}%' or  `title_en`  like '%{$searchkey}%' )";
			  }
		 }
		 $pagearr=$this->db->pagelist($sqlcnt,$sqlquery,$where,$order,$pagesize);
		 if($pagearr["count"]!=0){
			 $list=$pagearr["query"];
			 $pages=$pagearr["page"];
		 }
		 require tpl_adm('category_list');
	  }
	
	
     public function getmenu(){
	   $menuvar='var menu_yyinfo,menu_yymodel,menu_yymember,menu_yyapps;';
	   $menu_slider='menu_yymodel="<li class=\"header\" id=\"yunyetophead\">系统面板</li>\n";
	   menu_yymodel+="<li class=\"treeview active\">\n";
	   menu_yymodel+="<a href=\"#\">\n";
	   menu_yymodel+="<i class=\"fa fa-cog\"></i> <span>系统</span>\n";
	   menu_yymodel+="<span class=\"pull-right-container\">\n";
	   menu_yymodel+="<i class=\"fa fa-angle-left pull-right\"></i>\n";
	   menu_yymodel+="</span></a>\n";
	   menu_yymodel+="<ul class=\"treeview-menu\">\n";
	   menu_yymodel+="<li ><a href=\"'.url_admin('save','config').'\" target=\"maincontent\"><i class=\"fa fa-gear\"></i> 系统设置</a></li>\n";
	   menu_yymodel+="<li><a href=\"'.url_admin('init','msg').'\" target=\"maincontent\"><i class=\"fa fa-commenting\"></i> 系统消息</a></li>\n";
	   menu_yymodel+="</ul>\n";
	   menu_yymodel+="</li>\n";	   
	   menu_yymodel+="<li class=\"treeview\">\n";
	   menu_yymodel+="<a href=\"#\">\n";
	   menu_yymodel+="<i class=\"fa fa-cubes\"></i> <span>模型管理</span>\n";
	   menu_yymodel+="<span class=\"pull-right-container\">\n";
	   menu_yymodel+="<i class=\"fa fa-angle-left pull-right\"></i>\n";
	   menu_yymodel+="</span></a>\n";
	   menu_yymodel+="<ul class=\"treeview-menu\">\n";
	   menu_yymodel+="<li class=\"active\"><a href=\"'.url_admin('init','yunyecmsmodel').'\" target=\"maincontent\"><i class=\"fa fa-list\"></i> 模型管理</a></li>\n";
	   menu_yymodel+="<li><a href=\"'.url_admin('model_add','yunyecmsmodel').'\" target=\"maincontent\"><i class=\"fa fa-plus\"></i> 添加模型</a></li>\n";
	   menu_yymodel+="</ul>\n";
	   menu_yymodel+="</li>\n";
       menu_yymodel+="<li class=\"treeview\">\n";
	   menu_yymodel+="<a href=\"#\">\n";
	   menu_yymodel+="<i class=\"fa fa-globe\"></i> <span>语言版管理</span>\n";
	   menu_yymodel+="<span class=\"pull-right-container\">\n";
	   menu_yymodel+="<i class=\"fa fa-angle-left pull-right\"></i>\n";
	   menu_yymodel+="</span></a>\n";
	   menu_yymodel+="<ul class=\"treeview-menu\">\n";
	   menu_yymodel+="<li ><a href=\"'.url_admin('init','lang').'\" target=\"maincontent\"><i class=\"fa fa-list\"></i> 语言版管理</a></li>\n";
	   menu_yymodel+="<li><a href=\"'.url_admin('add','lang').'\" target=\"maincontent\"><i class=\"fa fa-plus\"></i> 添加语言版</a></li>\n";
	   menu_yymodel+="</ul>\n";
	   menu_yymodel+="</li>\n";	   
	   menu_yymember="<li class=\"header\" id=\"yunyetophead\">用户面板</li>\n";
	   menu_yymember+="<li class=\"treeview active\">\n";
	   menu_yymember+="<a href=\"#\">\n";
	   menu_yymember+="<i class=\"fa fa-users\"></i> <span>管理员管理</span>\n";
	   menu_yymember+="<span class=\"pull-right-container\">\n";
	   menu_yymember+="<i class=\"fa fa-angle-left pull-right\"></i>\n";
	   menu_yymember+="</span></a>\n";
	   menu_yymember+="<ul class=\"treeview-menu\">\n";
	   menu_yymember+="<li class=\"active\"><a href=\"'.url_admin('user_pwd','user',array('userid'=>$this->admuser['userid'])).'\" target=\"maincontent\"><i class=\"fa fa-circle-o\"></i> 修改密码</a></li>\n";
	   menu_yymember+="<li><a href=\"'.url_admin('init','user').'\" target=\"maincontent\"><i class=\"fa fa-circle-o\"></i> 管理员管理</a></li>\n";
	   menu_yymember+="<li><a href=\"'.url_admin('init','role').'\" target=\"maincontent\"><i class=\"fa fa-circle-o\"></i> 权限管理</a></li>\n";
	   menu_yymember+="<li><a href=\"'.url_admin('init','department').'\" target=\"maincontent\"><i class=\"fa fa-circle-o\"></i> 部门管理</a></li>\n";
	   menu_yymember+="<li><a href=\"'.url_admin('init','loginlog').'\" target=\"maincontent\"><i class=\"fa fa-circle-o\"></i> 登录日志</a></li>\n";
	   menu_yymember+="<li><a href=\"'.url_admin('init','adminlogs').'\" target=\"maincontent\"><i class=\"fa fa-circle-o\"></i> 操作日志</a></li>\n";
	   menu_yymember+="</ul>\n";
	   menu_yymember+="</li>\n";
	   menu_yymember+="<li class=\"treeview\">\n";
	   menu_yymember+="<a href=\"#\">\n";
	   menu_yymember+="<i class=\"fa fa-user-circle-o\"></i> <span>会员管理</span>\n";
	   menu_yymember+="<span class=\"pull-right-container\">\n";
	   menu_yymember+="<i class=\"fa fa-angle-left pull-right\"></i>\n";
	   menu_yymember+="</span></a>\n";
	   menu_yymember+="<ul class=\"treeview-menu\">\n";	
	   menu_yymember+="<li><a href=\"'.url_admin('init','member').'\"  target=\"maincontent\"><i class=\"fa fa-circle-o\"></i> 会员管理</a></li>\n";	
	   menu_yymember+="<li><a href=\"'.url_admin('init','membergroup').'\"  target=\"maincontent\"><i class=\"fa fa-circle-o\"></i> 会员组</a></li>\n";	
	   menu_yymember+="<li><a href=\"'.url_admin('init','notice').'\"  target=\"maincontent\"><i class=\"fa fa-circle-o\"></i> 通知消息</a></li>\n";	
	   menu_yymember+="</ul>\n";
	   menu_yymember+="</li>\n"; 
	   menu_yyapps+="<li class=\"treeview active\">\n";
	   menu_yyapps+="<a href=\"#\">\n";
	   menu_yyapps+="<i class=\"fa  fa-shopping-cart\"></i> <span>云商城</span>\n";
	   menu_yyapps+="<span class=\"pull-right-container\">\n";
	   menu_yyapps+="<i class=\"fa fa-angle-left pull-right\"></i>\n";
	   menu_yyapps+="</span></a>\n";
	   menu_yyapps+="<ul class=\"treeview-menu\">\n";
	   menu_yyapps+="<li ><a href=\"'.url_admin('init','orders').'\" target=\"maincontent\"><i class=\"fa fa-list\"></i> 订单管理 </a></li>\n";
	   menu_yyapps+="<li><a href=\"'.url_admin('save','config').'\" target=\"maincontent\"><i class=\"fa fa-money\"></i> 支付方式</a></li>\n";
	   menu_yyapps+="</ul>\n";
	   menu_yyapps+="</li>\n";
	   '; 
	   $menu_info=$this->getcatmenu();
	   $menu_slider=$menuvar.$menu_info.$menu_slider;
	   exit($menu_slider); 
	 }	
	
  private function getcatmenu(){
      $menuleft=$this->db->select("select `id`,`title`,`modelid`,`tplhome` from `#yunyecms_category` where `pid`= 0 order by ordernum asc");
	  $menu_info='
	   menu_yyinfo="<li class=\"header\" id=\"yunyetophead\">内容面板</li>\n";
		 menu_yyinfo+="<li class=\"treeview active\">\n";
         menu_yyinfo+="<a href=\"#\">\n";
             menu_yyinfo+="<i class=\"fa fa-info\"></i> <span>内容管理</span>\n";
             menu_yyinfo+="<span class=\"pull-right-container\">\n";
               menu_yyinfo+="<i class=\"fa fa-angle-left pull-right\"></i>\n";
             menu_yyinfo+="</span>\n";
           menu_yyinfo+="</a>\n";
           menu_yyinfo+="<ul class=\"treeview-menu menu-open\" style=\"display: block;\">\n";
            ';
		  $loopmenu="";
		   foreach ($menuleft as $key => $val){
			if (is_array($val)) {
			  $menuid=$val['id'];
				$catwo=$this->db->select("select `id`,`title`,`modelid`,`tplhome` from `#yunyecms_category` where `pid`= {$menuid} order by ordernum asc");
			  $menuleft[$key]['catlist']=$catwo;
				if(!empty($catwo)){
					$link_one="javascript:;";
				}else{
			        $link_one=url_admin(getmodeltype($val["modelid"]),'content',array("catid"=>$val["id"]));
				}
			  $loopmenu=$loopmenu.'
			  menu_yyinfo+="<li>\n";
              menu_yyinfo+="<a href=\"'.$link_one.'\" target=\"maincontent\"><i class=\"fa fa-circle-o\"></i> '.$val['title'].'\n";
                menu_yyinfo+="<span class=\"pull-right-container\">\n";
                  menu_yyinfo+="<i class=\"fa fa-angle-left pull-right\"></i>\n";
                menu_yyinfo+="</span>\n";
              menu_yyinfo+="</a>\n";';
			  if($catwo){
			   $loopmenu=$loopmenu.'  
              menu_yyinfo+="<ul class=\"treeview-menu\">\n";';
						foreach ($menuleft[$key]['catlist'] as $k => $v) {
						  $curmid=$v["modelid"];
						  if (is_array($v)) {
	                       $cathree=$this->db->select("select `id`,`title`,`modelid`,`tplhome` from `#yunyecms_category` where `pid`= {$v["id"]} order by ordernum asc");
							 $menuleft[$key]['catlist'][$k]['sublist']=$cathree;	
						   if(!empty($cathree)){
								$link_two="javascript:;";
								$link_two_click='';
							}else{
								$link_two=url_admin(getmodeltype($v["modelid"]),'content',array("catid"=>$v["id"]));
								$link_two_click='onClick=\"leftsidebar(this);\"';
							}	  											  
						  $loopmenu=$loopmenu.'
						  menu_yyinfo+="<li>\n";
						  menu_yyinfo+="<a href=\"'.$link_two.'\" target=\"maincontent\" '.$link_two_click.'><i class=\"fa fa-circle-o\"></i>'.$v["title"].'\n";';
						   if(!empty($cathree)){
							$loopmenu=$loopmenu.'menu_yyinfo+="<span class=\"pull-right-container\">\n";
							menu_yyinfo+=" <i class=\"fa fa-angle-left pull-right\"></i>\n";
							menu_yyinfo+="</span>\n";';
						   }
						   $loopmenu=$loopmenu.' menu_yyinfo+=" </a>\n";';
							  if($cathree){
								$loopmenu=$loopmenu.'  menu_yyinfo+="<ul class=\"treeview-menu\">\n";';
									 foreach ($cathree as $ks => $vs) {
											 $cursmid=$vs["modelid"];
								             $link_three=url_admin(getmodeltype($vs["modelid"]),'content',array("catid"=>$vs["id"]));
											 $loopmenu=$loopmenu.'menu_yyinfo+="<li><a href=\"'.$link_three.'\"  onClick=\"leftsidebar(this);\"  target=\"maincontent\"><i class=\"fa fa-circle-o\"></i>'.$vs["title"].'</a></li>\n";';
									   }
							    $loopmenu=$loopmenu.'menu_yyinfo+="</ul>\n";';
								  	}	
							 $loopmenu=$loopmenu.'menu_yyinfo+="</li>\n";';
							}
						  }
                $loopmenu=$loopmenu.'menu_yyinfo+="</ul>\n";';
			       }
               $loopmenu=$loopmenu.'menu_yyinfo+="</li>\n";';
		   }	
		 }	
       $menu_info=$menu_info.$loopmenu.'menu_yyinfo+="</ul></li>\n";';		
	   $menu_column='
	   menu_yyinfo+="<li class=\"treeview active\">\n";
	   menu_yyinfo+="<a href=\"#\">\n";
	   menu_yyinfo+="<i class=\"fa fa-columns\"></i> <span>栏目管理</span>\n";
	   menu_yyinfo+="<span class=\"pull-right-container\">\n";
	   menu_yyinfo+="<i class=\"fa fa-angle-left pull-right\"></i>\n";
	   menu_yyinfo+="</span></a>\n";
	   menu_yyinfo+="<ul class=\"treeview-menu\">\n";
	   menu_yyinfo+="<li class=\"active\"><a href=\"'.url_admin('init','category').'\" target=\"maincontent\"><i class=\"fa fa-circle-o\"></i> 栏目管理</a></li>\n";
	   menu_yyinfo+="<li><a href=\"'.url_admin('add','category').'\" target=\"maincontent\"><i class=\"fa fa-circle-o\"></i> 添加栏目</a></li>\n";
	   menu_yyinfo+="</ul>\n";
	   menu_yyinfo+="</li>\n";';
	   $retmenuinfo=$menu_info.$menu_column;;
	   return $retmenuinfo;
	}
	
	public function getmenuout(){
	    $menuvar='var menu_yyinfo;
		';
		$menu_info=$this->getcatmenu();
		exit($menuvar.$menu_info);
	}
	
  public function getmenux(){
         $menuleft=$this->db->select("select `id`,`title`,`modelid`,`tplhome` from `#yunyecms_category` where `pid`= 0 order by ordernum asc");
		 $menu_info='
		 <li class="header" id="yunyetophead">内容面板</li>
		 <li class="treeview active">
          <a href="#">
            <i class="fa fa-info"></i> <span>内容管理</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu menu-open" style="display: block;">
            ';
		   $loopmenu="";
		   foreach ($menuleft as $key => $val){
			if (is_array($val)) {
			  $menuid=$val['id'];
			$catwo=$this->db->select("select `id`,`title`,`modelid`,`tplhome` from `#yunyecms_category` where `pid`= {$menuid} order by ordernum asc");
			  $menuleft[$key]['catlist']=$catwo;
				if(!empty($catwo)){
					$link_one="javascript:;";
				}else{
			        $link_one=url_admin(getmodeltype($val["modelid"]),'content',array("catid"=>$val["id"]));
				}
			  $loopmenu=$loopmenu.'<li>
              <a href="'.$link_one.'"><i class="fa fa-circle-o"></i> '.$val['title'].'
                <span class="pull-right-container">
                  <i class="fa fa-angle-left pull-right"></i>
                </span>
              </a>';
			  if($catwo){
			   $loopmenu=$loopmenu.'  
              <ul class="treeview-menu">';
						foreach ($menuleft[$key]['catlist'] as $k => $v) {
						  $curmid=$v["modelid"];
						  if (is_array($v)) {
							$cathree=$this->db->select("select `id`,`title`,`modelid`,`tplhome` from `#yunyecms_category` where `pid`= {$v["id"]} order by ordernum asc");
							 $menuleft[$key]['catlist'][$k]['sublist']=$cathree;	
						   if(!empty($cathree)){
								$link_two="javascript:;";
							}else{
								$link_two=url_admin(getmodeltype($v["modelid"]),'content',array("catid"=>$v["id"]));
							}	  							  
						  $loopmenu=$loopmenu.'<li>
						  <a href="'.$link_two.'"><i class="fa fa-circle-o"></i>'.$v["title"].'
							<span class="pull-right-container">
							  <i class="fa fa-angle-left pull-right"></i>
							</span>
						  </a>';
							  if($menuleft[$key]['catlist'][$k]['sublist']){
								$loopmenu=$loopmenu.'  <ul class="treeview-menu">';
									 foreach ($menuleft[$key]['catlist'][$k]['sublist'] as $ks => $vs) {
											 $cursmid=$vs["modelid"];
								             $link_three=url_admin(getmodeltype($vs["modelid"]),'content',array("catid"=>$vs["id"]));
											 $loopmenu=$loopmenu.'<li><a href="'.$link_three.'"><i class="fa fa-circle-o"></i>'.$vs["title"].'</a></li>';
									   }
							    $loopmenu=$loopmenu.'</ul>';
								  	}	
							 $loopmenu=$loopmenu.'</li>';
							}
						  }
                $loopmenu=$loopmenu.'</ul>';
			       }
               $loopmenu=$loopmenu.'</li>';
		   }	
		 }	
      $menu_info=$menu_info.$loopmenu.'</ul></li>';		 
	  exit($menu_info);
	 }	
	
	 	
	
	
	 
}
?>
