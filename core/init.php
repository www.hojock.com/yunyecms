<?php
define('InYUNYECMS',TRUE);
if(!defined('YUNYECMS_ROOT')){exit("YUNYECMS_ROOT is not defined");}
require(YUNYECMS_CONFIG."config.php");
core::load_fun("basic");
core::load_fun("core");
core::load_fun("custom");
core::load_class("safestring");
final class core { 
	 public static function &load_class($class_name='',$initialize = 1){ 
		static $classarr = array(); 
		$path=YUNYECMS_CORE.'lib'.DIRECTORY_SEPARATOR.$class_name.'.class.php'; 
		$key=md5($class_name.$path); 
		if (isset($classarr[$key])) { return $classarr[$key]; } 
		if(file_exists($path)){ 
			include_once $path; 
			if($initialize){ 
				$classarr[$key] = new $class_name; 
			}else{$classarr[$key]=true; } return $classarr[$key];
		}else{ 
         echo 'Unable to load system class: '.$path;
		 } 
}
	
public static function &load_parma_class($class_name='',$initialize = 1,$parma=''){ 
		static $classarr = array(); 
		if($parma=="") exit('Unable to load system class with parameter: '.$path);
		$path=YUNYECMS_CORE.'lib'.DIRECTORY_SEPARATOR.$class_name.'.class.php'; 
		$key=md5($class_name.$path); 
		if (isset($classarr[$key])) { return $classarr[$key]; } 
		if(file_exists($path)){ 
			include_once $path; 
			if($initialize){ 
				$classarr[$key] = new $class_name($parma); 
			}else{ $classarr[$key]=true; } return $classarr[$key]; 
		}else{ 
         echo 'Unable to load system class with parameter: '.$path;
		 }
} 

public static function &load_app_class($class_name='',$module='',$initialize = 1){ 
		static $classarr = array(); 
		if(empty($module)){ $module=ROUTE_M; } 		$path=YUNYECMS_CORE.'app'.DIRECTORY_SEPARATOR.$module.DIRECTORY_SEPARATOR."lib".DIRECTORY_SEPARATOR.$class_name.'.class.php'; 
		$key=md5($class_name.$path); 
		if (isset($classarr[$key])) { return $classarr[$key]; } 
		if(file_exists($path)){ 
			include_once $path; 
			if($initialize){ 
				$classarr[$key] = new $class_name; 
			}else{ $classarr[$key]=true; } return $classarr[$key]; 
		}else{ 
        echo 'Unable to load application class: '.$path;
	} 
} 
   public static function &load_admin_class($class_name='',$initialize = false){ 
		static $classarr = array(); 
		$path=YUNYECMS_CORE.'admin'.DIRECTORY_SEPARATOR.'lib'.DIRECTORY_SEPARATOR.$class_name.'.class.php'; 
		$key=md5($class_name.$path); 
		if (isset($classarr[$key])) { return $classarr[$key]; } 
		if(file_exists($path)){ 
			include_once $path; 
			if($initialize){ 
				$classarr[$key] = new $class_name; 
			}else{ $classarr[$key]=true; } return $classarr[$key]; 
		}else{ 
         echo 'Unable to load system class: '.$path;
	} 
} 

public static function load_config($file, $key = '', $default = '', $reload = false) {
		static $configs = array();
		if (!$reload && isset($configs[$file])) {
			if (empty($key)) {
				return $configs[$file];
			} elseif (isset($configs[$file][$key])) {
				return $configs[$file][$key];
			} else {
				return $default;
			}
		}
	    $path=YUNYECMS_CONFIG.$file.'.cfg.php'; 
		if (file_exists($path)) {
			$configs[$file] = include $path;
		}
		if (empty($key)) {
			return $configs[$file];
		} elseif (isset($configs[$file][$key])) {
			return $configs[$file][$key];
		} else {
			return $default;
		}
		echo 'Unable to load config  file: '.$path;
	}
	
	public static function &load_base_class($class_name='',$classpath = '',$initialize = 1){ 
		static $classarr = array(); 
		$path=YUNYECMS_CORE.$classpath.DIRECTORY_SEPARATOR.$class_name.'.class.php'; 
		$key=md5($class_name.$path); 
		if (isset($classarr[$key])) {return $classarr[$key]; } 
		if(file_exists($path)){ 
			include_once $path; 
			if($initialize){ 
				$classarr[$key] = new $class_name; 
			}else{ $classarr[$key]=true; } return $classarr[$key]; 
		}else{ 
         echo 'Unable to loads system model: '.$path;
		 }
   } 
	public static function load_model($classname) {
		 return self::load_base_class($classname,'model');
	}
    public static function load_app_config($filename,$keys='',$module=''){ 
	static $configs = array(); 
	if(isset($configs[$filename])){ 
		if (empty($keys)) { return $configs[$filename]; } 
		else if (isset($configs[$filename][$keys])) { return $configs[$filename][$keys]; }
		else{ return $configs[$filename]; } 
	} 
	 if(empty($module)){$module=ROUTE_M; }
	 $path=YUNYECMS_CORE.'app'.DIRECTORY_SEPARATOR.$module.DIRECTORY_SEPARATOR.'lib'.DIRECTORY_SEPARATOR.$filename.'.cfg.php';
	if (file_exists($path)){ 
		$configs[$filename]=include $path; 
		if(empty($keys)){ return $configs[$filename]; }
		else{ return $configs[$filename][$keys]; } 
	} 
    echo 'Unable to load application config  file: '.$path;
} 
public static function load_base_fun($fun_name,$path,$mode="system"){ 
	static $func = array(); 
	$key = md5($path); 
	if (isset($func[$key])) return true; 
	if(file_exists($path)){ 
		$func[$key] = true; return include $path; 
		}
	else{ 
	$func[$key] = false; 
    echo 'Unable to load '.$mode.' function: '.$path;
	} 
}
public static function load_fun($fun_name){ 
	$path=YUNYECMS_FUN.$fun_name.'.fun.php'; 
	self::load_base_fun($fun_name,$path);
} 
public static function load_app_fun($fun_name){ 
	if(empty($module)){$module=ROUTE_M; } 	$path=YUNYECMS_CORE.'app'.DIRECTORY_SEPARATOR.$module.DIRECTORY_SEPARATOR.'func'.DIRECTORY_SEPARATOR.$fun_name.'.fun.php'; 
	self::load_base_fun($fun_name,$path,"application");
} 
public static function load_admin_fun($fun_name){ 
	if(empty($module)){$module=ROUTE_M;} 
	$path=YUNYECMS_CORE.'admin'.DIRECTORY_SEPARATOR.'func'.DIRECTORY_SEPARATOR.$fun_name.'.fun.php'; 
	self::load_base_fun($fun_name,$path,"admin");
}
public static function &load_app_model($model_name='',$module='',$initialize = 1){ 
	static $models=array(); 
	$key=md5($module.$model_name); 
	$path=YUNYECMS_CORE.'app'.DIRECTORY_SEPARATOR.$module.DIRECTORY_SEPARATOR.'model'.DIRECTORY_SEPARATOR.$model_name.'.php'; 
	if (file_exists($path)){ 
		include $path; 
		if($initialize){ 
			$models[$key]=new $model_name; 
		}else{ 
			$models[$key]=true; 
		} return $models[$key]; 
    } 
    echo 'Unable to load app model: '.$path;
}
public static function InitApp(){return self::load_class('application');}
public static function InitYUNYECMSAdmin(){return self::load_class('YUNYECMSAdmin');}
}
?>