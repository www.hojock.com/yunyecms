<?php
if(PHP_VERSION < '5.2.0') die('require  php version >5.2.0 ');
$doc_root=$_SERVER['DOCUMENT_ROOT'];
if(!empty($doc_root)&&substr($doc_root,-1)=="/"){$doc_root=substr($doc_root, 0, -1);}
$filename=$_SERVER['SCRIPT_FILENAME'];
if(strpos($doc_root,"\\")){ $doc_root=str_ireplace('\\','/',$doc_root);}
if(strpos($filename,"\\")){ $filename=str_ireplace('\\','/',$filename);}
$site_root=str_ireplace($doc_root,'',$filename);
$site_root=substr($site_root, 0, strrpos($site_root, '/')+1);
define('ROOT',$site_root);
strlen(ROOT)>1 ? define('RD',substr(ROOT, 0, -1)):define('RD',"");
define('YUNYECMS_ROOT',str_ireplace('\\','/',realpath(dirname(__FILE__).'/../../')).'/');
define('YUNYECMS_CORE',YUNYECMS_ROOT."core/");
define('YUNYECMS_CLASS',YUNYECMS_ROOT."core/lib/");
define('YUNYECMS_CONFIG',YUNYECMS_ROOT."core/config/");
define('YUNYECMS_FUN',YUNYECMS_ROOT."core/func/");
define('SITE_URL', (isset($_SERVER['HTTP_HOST']) ? $_SERVER['HTTP_HOST'] : ''));
define('YUNYECMS_DATA',YUNYECMS_ROOT."data/");
define('YUNYECMS_ADMSESSION',YUNYECMS_ROOT."data/admin/admsess/");
define('YUNYECMS_SESSION',YUNYECMS_ROOT."data/sessions/");
define('YUNYECMS_LAN',"gb");
define('YUNYECMS_PUBLIC',ROOT."public/");
define('YUNYECMS_UI',ROOT."public/ui/");
define('THEME',ROOT."theme/");
define('CACHE_ROOT',YUNYECMS_DATA."caches/");
define('CACHE_AUTO_UPDATE',false);
define('APP_DEBUG',0);
define('APP_PATH',YUNYECMS_ROOT."core/app/");
define('APP_SUB_DOMAIN_DEPLOY',false);
define('URL_MODEL',0);
define('URL_PATHINFO_DEPR',"/");
define('URL_HTML_SUFFIX',"html");
define('YUNYECMS_VERSION',"V2.0.2");
define('YUNYECMS_SOFT_TYPE',"2");
define('YUNYECMS_VERSION_TIME',"2019-12-02");
define('YUNYECMS_BUILD_TIME',"2020-01-13");
?>