<?php
defined('InYUNYECMS') or exit('No permission resources.');
core::load_class("model",0);
class model_model extends model{
	public function __construct() {
		$this->tablename = 'model';
		parent::__construct();
	}
   public function getsql_create_infomode($tbname){
   if(!empty($tbname)){
		   $sql_create_info_table="CREATE TABLE  `".$tbname."` (
		 	  `id` int(11) NOT NULL AUTO_INCREMENT,
			  `catid` int(11) DEFAULT '0',
			  `title` varchar(500) DEFAULT '',
			  `seotitle` varchar(500) DEFAULT '',
			  `seokeywords` varchar(500) DEFAULT '',
			  `seodesc` varchar(500) DEFAULT '',
			  `source` varchar(200) DEFAULT '',
			  `author` varchar(200) DEFAULT '',
			  `template` varchar(50) DEFAULT '',
			  `morepic` varchar(800) DEFAULT '',
			  `pic` varchar(500) DEFAULT '',
			  `picfile` varchar(500) DEFAULT '',
			  `summary` varchar(500) DEFAULT '',
			  `exlink` varchar(300) DEFAULT '',
			   `content` text,
			  `hits` int(11) DEFAULT '0',
			  `downnum` int(11) DEFAULT '0',
			  `userid` int(11) DEFAULT '0',
			  `adminuserid` int(11) DEFAULT '0',
			  `groupid` int(11) DEFAULT '0',
			  `ordernum` int(10) unsigned DEFAULT '0',
			  `addtime` int(11) unsigned DEFAULT '0',
			  `updatetime` int(11) DEFAULT '0',
			  `status` tinyint(1) unsigned zerofill DEFAULT '0',
			  `isgood` tinyint(1) unsigned zerofill DEFAULT '0',
			  `istop` tinyint(1) unsigned zerofill DEFAULT '0',
			  `ishead` tinyint(1) unsigned zerofill DEFAULT '0',
			  `ispower` tinyint(1) unsigned zerofill DEFAULT '0',
			  `admin_userid` int(11) DEFAULT '0',
			  `title_en` varchar(500) DEFAULT '',
			  `seotitle_en` varchar(500) DEFAULT '',
			  `seokeywords_en` varchar(500) DEFAULT '',
			  `seodesc_en` varchar(500) DEFAULT '',
			  `source_en` varchar(200) DEFAULT '',
			  `author_en` varchar(200) DEFAULT '',
			  `summary_en` varchar(500) DEFAULT '',
			  `exlink_en` varchar(300) DEFAULT '',
			   `content_en` text,
			   PRIMARY KEY (`id`),
			   KEY `status` (`status`,`ordernum`,`id`),
			   KEY `sort` (`catid`,`status`,`ordernum`,`id`),
			   KEY `catid` (`catid`,`status`,`id`)
		  ) ENGINE=MyISAM  DEFAULT CHARSET=utf8 ";
	      return $sql_create_info_table;  
	   }else{
	      return false;  
     }
	}
	
	  public function getsql_create_infomodedata($tbname){
		if(!empty($tbname)){
			   $sql_create_infodata_table="CREATE TABLE  `".$tbname."` (
			  `kid` int(11) NOT NULL AUTO_INCREMENT,
			  `id` int(11) DEFAULT '0',
			  `content` mediumtext NOT NULL,
			   PRIMARY KEY (`kid`),
			   UNIQUE KEY `id` (`id`)
			   ) ENGINE=MyISAM  DEFAULT CHARSET=utf8";
		  return $sql_create_infodata_table;
		}else{
			  return false;  
	   }
	  }
	
	   public function getsql_create_singlemode($tbname){
	   if(!empty($tbname)){
			   $sql_create_single_table="CREATE TABLE  `".$tbname."` (
			  `id` int(11) NOT NULL AUTO_INCREMENT,
			  `catid` int(11) DEFAULT '0',
			  `title` varchar(500) DEFAULT '',
			  `seotitle` varchar(500) DEFAULT '',
			  `seokeywords` varchar(500) DEFAULT '',
			  `seodesc` varchar(500) DEFAULT '',
			  `pic` varchar(500) DEFAULT '',
			  `summary` varchar(500) DEFAULT '',
			   `content` text,
			  `ordernum` int(10) unsigned DEFAULT '0',
			  `addtime` int(11) unsigned DEFAULT '0',
			  `title_en` varchar(500) DEFAULT '',
			  `seotitle_en` varchar(500) DEFAULT '',
			  `seokeywords_en` varchar(500) DEFAULT '',
			  `seodesc_en` varchar(500) DEFAULT '',
			  `summary_en` varchar(500) DEFAULT '',
			   `content_en` text,
			   PRIMARY KEY (`id`)
			  ) ENGINE=MyISAM  DEFAULT CHARSET=utf8 ";
			  return $sql_create_single_table;  
		   }else{
			  return false;  
		 }
		}

	
	   public function getsql_create_formmode($tbname){
	   if(!empty($tbname)){
			   $sql_create_form_table="CREATE TABLE  `".$tbname."` (
			  `id` int(11) NOT NULL AUTO_INCREMENT,
			  `catid` int(11) DEFAULT NULL,
			  `title` varchar(50) DEFAULT NULL,
			  `name` varchar(200) DEFAULT NULL,
			  `sex` varchar(200) DEFAULT '',
			  `mobile` varchar(200) DEFAULT '',
			  `phone` varchar(50) DEFAULT NULL,
			  `email` varchar(50) DEFAULT NULL,
			  `company` varchar(200) DEFAULT '',
			  `fax` varchar(200) DEFAULT '',
			  `address` varchar(200) DEFAULT '',
			  `website` varchar(200) DEFAULT '',
			  `zipcode` varchar(200) DEFAULT '',
			  `age` varchar(45) DEFAULT NULL,
			  `qq` varchar(255) DEFAULT NULL,
			  `weixin` varchar(255) DEFAULT NULL,
			  `userid` int(10) unsigned DEFAULT '0',
			  `adminuserid` int(10) unsigned DEFAULT '0',
			  `remark` text,
			  `addtime` int(11) DEFAULT NULL,
			  `status` int(11) DEFAULT NULL,
			  `ip` varchar(255) DEFAULT NULL,
			  `isview` tinyint(1) unsigned zerofill DEFAULT '0',
			  `ispower` tinyint(1) unsigned zerofill DEFAULT '0',
			  PRIMARY KEY (`id`)
			) ENGINE=MyISAM  DEFAULT CHARSET=utf8 ";
			  return $sql_create_form_table;  
		   }else{
			  return false;  
		 }
		}
		
	
	public function getsql_infomodefields($modelid){
			if(!empty($modelid)){
				$sql_infomodelfields="INSERT INTO `#yunyecms_modelfields` (`modelid`, `fdname`, `fdtitle`, `fdtype`, `fdlen`, `ismt`, `isindex`, `isunique`, `ispage`, `tobr`, `showhtml`, `ordernum`, `formctrl`, `formsize`, `editorstyle`, `defaultvalue`, `isrequired`, `istougao`, `issearch`, `issys`, `isallowdel`, `remark`, `isdisplay`, `language`, `formwidth`, `formheight`, `formvalue`,`isadd`) VALUES
		({$modelid}, 'title', '标题', 'VARCHAR', 500, 1, 0, 0, 0, 0, 1, 1, 'text', '', 0, '', 0, 0, 0, 1, 0, '', 1, 1, NULL, NULL, NULL,1),
		({$modelid}, 'morepic', '更多图片', 'VARCHAR', 800, 1, 0, 0, 0, 0, 1, 3, 'text', '', 0, '', 0, 0, 0, 1, 0, '', 1, 1, NULL, NULL, NULL,2),
		({$modelid}, 'pic', '图片', 'VARCHAR', 500, 1, 0, 0, 0, 0, 1, 2, 'text', '', 0, '', 0, 0, 0, 1, 0, '', 1, 1, NULL, NULL, NULL,1),
		({$modelid}, 'picfile', '文件', 'VARCHAR', 500, 1, 0, 0, 0, 0, 1, 4, 'text', '', 0, '', 0, 0, 0, 1, 0, '', 1, 1, NULL, NULL, NULL,1),
		({$modelid}, 'exlink', '外部链接', 'VARCHAR', 250, 1, 0, 0, 0, 0, 1, 5, 'text', '', 0, '', 0, 1, 0, 1, 0, '', 1, 1, NULL, NULL, NULL,1),
		({$modelid}, 'summary', '简介', 'VARCHAR', 500, 1, 0, 0, 0, 0, 1, 6, 'textarea', '', 0, '', 0, 1, 0, 1, 0, '', 1, 1, NULL, NULL, NULL,1),
		({$modelid}, 'source', '来源', 'VARCHAR', 100, 1, 0, 0, 0, 0, 1, 7, 'text', '', 0, '', 0, 0, 0, 0, 1, '', 1, 1, NULL, NULL, NULL,1),
		({$modelid}, 'content', '内容', 'TEXT', 8000, 1, 0, 0, 0, 0, 1, 8, 'editor', '', 0, '', 0, 0, 0, 0, 1, '', 1, 1, NULL, NULL, NULL,1),
		({$modelid}, 'title_en', '标题（English）', 'VARCHAR', 500, 1, 0, 0, 0, 0, 1, 9, 'text', '', 0, '', 0, 1, 0, 1, 0, '', 1, 2, NULL, NULL, NULL,1),
		({$modelid}, 'exlink_en', '外部链接（English）', 'VARCHAR', 500, 1, 0, 0, 0, 0, 1, 10, 'text', '', 0, '', 0, 1, 0, 1, 0, '', 1, 2, NULL, NULL, NULL,1),
		({$modelid}, 'summary_en', '简介（English）', 'VARCHAR', 500, 1, 0, 0, 0, 0, 1, 11, 'textarea', '', 0, '', 0, 1, 0, 1, 0, '', 1, 2, NULL, NULL, NULL,1),
		({$modelid}, 'source', '来源（English）', 'VARCHAR', 100, 1, 0, 0, 0, 0, 1, 12, 'text', '', 0, '', 0, 0, 0, 0, 1, '', 1, 2, NULL, NULL, NULL,1),
		({$modelid}, 'content_en', '内容（English）', 'TEXT', 8000, 1, 0, 0, 0, 0, 1, 13, 'editor', '', 0, '', 0, 0, 0, 0, 1, '', 1, 2, NULL, NULL, NULL,1) ; ";
			  return $sql_infomodelfields;
			}else{
				 return false;  
			 }
		  }	
	
		public function getsql_singlemodefields($modelid){
			if(!empty($modelid)){
				   $sql_singlemodelfields="INSERT INTO  `#yunyecms_modelfields` (`modelid`, `fdname`, `fdtitle`, `fdtype`, `fdlen`, `ismt`, `isindex`, `isunique`, `ispage`, `tobr`, `showhtml`, `ordernum`, `formctrl`, `formsize`, `editorstyle`, `defaultvalue`, `isrequired`, `istougao`, `issearch`, `issys`, `isallowdel`, `remark`, `isdisplay`, `language`, `formwidth`, `formheight`, `formvalue`,`isadd`) VALUES
		({$modelid}, 'title', '标题', 'VARCHAR', 500, 1, 0, 0, 0, 0, 1, 1, 'text', '', 0, '', 0, 0, 0, 1, 0, '', 1, 1, NULL, NULL, NULL,1),
		({$modelid}, 'pic', '图片', 'VARCHAR', 300, 1, 0, 0, 0, 0, 1, 2, 'text', '', 0, '', 0, 0, 0, 1, 0, '', 1, 1, NULL, NULL, NULL,1),
		({$modelid}, 'summary', '简介', 'VARCHAR', 500, 1, 0, 0, 0, 0, 1, 3, 'textarea', '', 0, '', 0, 1, 0, 1, 0, '', 1, 1, NULL, NULL, NULL,1),
		({$modelid}, 'content', '内容', 'TEXT', 8000, 1, 0, 0, 0, 0, 1, 4, 'editor', '', 0, '', 0, 0, 0, 1, 0, '', 1, 1, NULL, NULL, NULL,1),
		({$modelid}, 'title_en', '标题（English）', 'VARCHAR', 500, 1, 0, 0, 0, 0, 1, 5, 'text', '', 0, '', 0, 1, 0, 1, 0, '', 1, 2, NULL, NULL, NULL,1),
		({$modelid}, 'summary_en', '简介（English）', 'VARCHAR', 500, 1, 0, 0, 0, 0, 1, 6, 'textarea', '', 0, '', 0, 1, 0, 1, 0, '', 1, 2, NULL, NULL, NULL,1),
		({$modelid}, 'content_en', '内容（English）', 'TEXT', 8000, 1, 0, 0, 0, 0, 1, 7, 'editor', '', 0, '', 0, 0, 0, 1, 0, '', 1, 2, NULL, NULL, NULL,1); ";
			  return $sql_singlemodelfields;
			}else{
				 return false;  
			 }
		  }		
	
	  public function getsql_formmodefields($modelid){
			if(!empty($modelid)){
				   $sql_formmodelfields="INSERT INTO `#yunyecms_modelfields` (`modelid`, `fdname`, `fdtitle`, `fdtitle_en`, `fdtype`, `fdlen`, `ismt`, `isindex`, `isunique`, `ispage`, `tobr`, `showhtml`, `ordernum`, `formctrl`, `formsize`, `editorstyle`, `defaultvalue`, `isrequired`, `istougao`, `issearch`, `issys`, `isallowdel`, `remark`, `isdisplay`, `language`, `formwidth`, `formheight`, `formvalue`,`isadd`) VALUES
		({$modelid}, 'title', '标题','Title', 'VARCHAR', 500, 1, 0, 0, 0, 0, 1, 1, 'text', '', 0, '', 1, 0, 0, 1, 0, '', 1, 1, NULL, NULL, NULL,1),
		({$modelid}, 'name', '姓名', 'Name','VARCHAR', 100, 1, 0, 0, 0, 0, 1, 2, 'text', '', 0, '', 1, 0, 0, 1, 0, '', 1, 1, NULL, NULL, NULL,1),
		({$modelid}, 'sex', '性别','Sex', 'VARCHAR', 50, 1, 0, 0, 0, 0, 1, 3, 'text', '', 0, '', 2, 0, 0, 1, 0, '', 0, 1, NULL, NULL, NULL,1),
		({$modelid}, 'mobile', '手机','Mobile', 'VARCHAR', 100, 1, 0, 0, 0, 0, 1, 4, 'text', '', 0, '', 1, 0, 0, 1, 0, '', 1, 1, NULL, NULL, NULL,1),
		({$modelid}, 'phone', 'Telephone','标题', 'VARCHAR', 100, 1, 0, 0, 0, 0, 1, 5, 'text', '', 0, '', 1, 0, 0, 1, 0, '', 1, 1, NULL, NULL, NULL,1),
		({$modelid}, 'email', 'E-mail','E-mail', 'VARCHAR', 100, 1, 0, 0, 0, 0, 1, 6, 'text', '', 0, '', 1, 0, 0, 1, 0, '', 1, 1, NULL, NULL, NULL,1),
		({$modelid}, 'company', '公司','Company', 'VARCHAR', 200, 1, 0, 0, 0, 0, 1, 7, 'text', '', 0, '', 1, 0, 0, 1, 0, '', 1, 1, NULL, NULL, NULL,1),
		({$modelid}, 'fax', '传真', 'Fax','VARCHAR', 200, 1, 0, 0, 0, 0, 1, 8, 'text', '', 0, '', 0, 0, 0, 1, 0, '', 0, 1, NULL, NULL, NULL,1),
		({$modelid}, 'address', '地址','Address', 'VARCHAR', 500, 1, 0, 0, 0, 0, 1, 9, 'textarea', '', 0, '', 0, 0, 0, 1, 0, '', 0, 1, NULL, NULL, NULL,1),
		({$modelid}, 'website', '网址','Website', 'VARCHAR', 300, 1, 0, 0, 0, 0, 1, 10, 'text', '', 0, '', 0, 0, 0, 1, 0, '', 0, 1, NULL, NULL, NULL,1),
		({$modelid}, 'zipcode', '邮编', 'Zip code','VARCHAR', 50, 1, 0, 0, 0, 0, 1, 11, 'text', '', 0, '', 0, 0, 0, 1, 0, '', 0, 1, NULL, NULL, NULL,1),
		({$modelid}, 'age', '年龄','Age', 'VARCHAR', 50, 1, 0, 0, 0, 0, 1, 12, 'text', '', 0, '', 0, 0, 0, 1, 0, '', 0, 1, NULL, NULL, NULL,1),
		({$modelid}, 'qq', 'QQ', 'QQ','VARCHAR', 50, 1, 0, 0, 0, 0, 1, 13, 'text', '', 0, '', 0, 0, 0, 1, 0, '', 0, 1, NULL, NULL, NULL,1),
		({$modelid}, 'weixin', '微信','WeChat', 'VARCHAR', 50, 1, 0, 0, 0, 0, 1, 14, 'text', '', 0, '', 0, 0, 0, 1, 0, '', 0, 1, NULL, NULL, NULL,1),
		({$modelid}, 'remark', '内容', 'Content','TEXT', 8000, 1, 0, 0, 0, 0, 1, 15, 'textarea', '', 0, '', 1, 0, 0, 1, 0, '', 1, 1, NULL, NULL, NULL,1); ";
			  return $sql_formmodelfields;
			}else{
				 return false;  
			 }

		  }		
	
}
?>