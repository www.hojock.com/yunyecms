<?php
defined('InYUNYECMS')or exit('No permission resources.');
class safestring{
/**
* 安全过滤类-过滤javascript,css,iframes,object等不安全参数 过滤级别高
* @param  string $value 需要过滤的值
* @return string
*/
public static function fliter_script($value){
$value = preg_replace("/(javascript:)?on(click|load|key|mouse|error|abort|move|unload|change|dblclick|move|reset|resize|submit)/i","&111n\\2",$value);
$value = preg_replace("/(.*?)<\/script>/si","",$value);
$value = preg_replace("/(.*?)<\/iframe>/si","",$value);
$value = preg_replace ("//isU", '', $value);
$scriptstr = array("alert", 'cookie', "script", "delete", "XSS","eval");
$script_re = array("","","","","","","");
$returnstr=str_ireplace($scriptstr, $script_re, $value);  	
for($i=1;$i<=3;$i++){
  $returnstr= str_ireplace($scriptstr, $script_re, $returnstr);	
}
return  $returnstr;	
}
/**
* 安全过滤类-过滤HTML标签
* @param  string $value 需要过滤的值
* @return string
*/
public static function fliter_html($value) {
if (function_exists('htmlspecialchars')) return htmlspecialchars($value);
return str_ireplace(array("&", '"', "'", "<", ">"), array("&amp;", "&quot;", "&#039;", "&lt", "&gt;"), $value);
}
/**
* 安全过滤类-对进入的数据加下划线 防止SQL注入
* @param  string $value 需要过滤的值
* @return string
*/
public static function fliter_sql($value) {
$sql = array("select", 'insert', "update", "delete","\'", "\/\*", 
     "\.\.\/", "\.\/", "union", "into", "load_file", "outfile","drop","modify","create","alter","exec","regxp","database()","sleep(");
$sql_re = array("&nbsp;","&nbsp;","&nbsp;","&nbsp;","&nbsp;","&nbsp;","&nbsp;","&nbsp;","&nbsp;","&nbsp;","&nbsp;","&nbsp;","&nbsp;","&nbsp;","&nbsp;","&nbsp;","&nbsp;","&nbsp;","&nbsp;","&nbsp;");
$returnstr=str_ireplace($sql, $sql_re, $value);  	
for($i=1;$i<=3;$i++){
  $returnstr= str_ireplace($sql, $sql_re, $returnstr);	
}
return  $returnstr;
}
/**
* 安全过滤类-通用数据过滤
* @param string $value 需要过滤的变量
* @return string|array
*/
public static function fliter_escape($value) {
if (is_array($value)) {
  foreach ($value as $k => $v) {
   $value[$k] = self::fliter_str($v);
  }
} else {
  $value = self::fliter_str($value);
}
return $value;
}
/**
* 安全过滤类-字符串过滤 过滤特殊有危害字符
* @param  string $value 需要过滤的值
* @return string
*/
public static  function fliter_str($value) {
 $badstr = array("\0", "%00", "\r", '&', ' ', '"', "'", "<", ">", "   ", "%3C", "%3E");
 $newstr = array('', '', '', '&', ' ', '"', "'", "<", ">", "   ", "<", ">");
 $value  = str_ireplace($badstr, $newstr, $value);
 $value  = preg_replace('/&((#(\d{3,5}|x[a-fA-F0-9]{4}));)/', '&\\1', $value);
 $value=self::filter_phptag($value);
 return $value;
}

/**
* 私有路劲安全转化
* @param string $fileName
* @return string
*/
public static function filter_dir($fileName) {
$tmpname = strtolower($fileName);
$temp = array(':/',"\0", "..");
if (str_ireplace($temp, '', $tmpname) !== $tmpname) {
  return false;
}
return $fileName;
}
/**
* 过滤目录
* @param string $path
* @return array
*/
public static function filter_path($path) {
$path = str_ireplace(array("'",'#','=','`','$','%','&',';'), '', $path);
return rtrim(preg_replace('/(\/){2,}|(\\\){1,}/', '/', $path), '/');
}
/**
* 过滤PHP标签
* @param string $string
* @return string
*/
public static function filter_phptag($string) {
return str_ireplace(array(''), array('<?', '?>'), $string);
}
/**
* 安全过滤类-返回函数
* @param  string $value 需要过滤的值
* @return string
*/
public static function str_out($value) {
$badstr = array("<", ">", "%3C", "%3E");
$newstr = array("<", ">", "<", ">");
$value  = str_ireplace($newstr, $badstr, $value);
return stripslashes($value); //下划线
}   

}