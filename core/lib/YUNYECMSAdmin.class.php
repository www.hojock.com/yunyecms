<?php
define('IN_YUNYECMSAdmin',true);
define('IN_YUNYECMS',true);
class YUNYECMSAdmin {
	/**
	 * 构造函数
	 */
	public function __construct() {
	    $param = core::load_parma_class('param',1,"admin");
		define('ROUTE_M', $param->route_m());
		define('ROUTE_C', $param->route_c());
		define('ROUTE_A', $param->route_a());
		$this->init();
	}
	/**
	 * 调用件事
	 */
	private function init() {
		$controller = $this->load_controller();
		core::load_config('loaded');
		if (method_exists($controller, ROUTE_A)) {
			if (preg_match('/^[_]/i', ROUTE_A)) {
				exit('What you are visiting is a private action.');
			} else {
				call_user_func(array($controller,ROUTE_A));
			}
		} else {
			exit('Action does not exist.');
		}
	}
	/**
	 * 加载控制器
	 * @param string $filename
	 * @param string $m
	 * @return obj
	 */
	private function load_controller() {
		core::load_admin_fun('admin');
		$filepath = YUNYECMS_CORE.'admin'.DIRECTORY_SEPARATOR.ROUTE_C.'.php';
		if(file_exists($filepath)) {
			$classname = ROUTE_C;
			include $filepath;
			if(class_exists($classname)){
				return new $classname;
			}else{
			  exit('Controller file '.$filepath.' does not exist.');
 			}
		   }else {
			exit('Controller file '.$filepath.' does not exist.');
		}
	}
}
