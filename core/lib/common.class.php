<?php
defined('IN_YUNYECMS') or exit('No direct script access allowed');
session_save_path(YUNYECMS_SESSION);
session_start();
class common {
	public $lang;
	public $cat;
	public $rootcat;
	public $cfg;
	public $member;
	public $db;
    function __construct() {
	     $this->db = core::load_model('common_model');
		 $this->cfg=getcfg();
		 if(!empty($_REQUEST['lang'])&&is_numeric($_REQUEST['lang'])){
	        $this->lang=getcurlang(usafestr(trim($_REQUEST['lang'])));
		}else{
	        $this->lang=getdefaultlang();
		}
		$this->lang=dolang($this->lang,$this->cfg);
		 if(strlen(RD)>0){
			   if(stripos($this->lang['logo'],RD)===FALSE) $this->lang['logo']=RD.$this->lang['logo'];
			   if(stripos($this->lang['qrcode'],RD)===FALSE) $this->lang['qrcode']=RD.$this->lang['qrcode'];
		  };
        define('LAN',empty($this->lang['landir'])?"cn":$this->lang['landir']);
        define('CTD',empty($this->lang['theme'])?"default":$this->lang['theme']);
		define('TPL',THEME.CTD."/".LAN."/");
	    if(!empty($_REQUEST['catid'])){
			  $catid=trim($_REQUEST['catid']);
			  if(!is_numeric($catid)){
					messagebox("错误的参数","back",'warn');		
			  }
		   $catid=usafestr($catid);
		   $this->cat=getbyid($catid);
		   $this->rootcat=getbyid(getppid($catid));
				if(strlen(RD)>0&&stripos($this->rootcat["pic"],RD)===FALSE){
					$this->rootcat["pic"]=!empty($this->rootcat["pic"])?RD.$this->rootcat["pic"]:''; RD.$this->rootcat["pic"];
				};
	    }
		if($this->cfg['isclose']==1){
			include tpl("closesite");
			exit;
		}
	    $userid=usafestr(yunyecms_strdecode(ugetcookie("userid")));
		if($userid&&is_numeric($userid)){
		    $this->member=getbyid($userid,"member");
			if($this->member["id"]){
			  $this->member['cartnum']=getcount("select count(*) from `#yunyecms_cart` where userid={$this->member["id"]}");
		      $this->member['ordernum']=getcount("select count(*) from `#yunyecms_orders` where userid={$this->member["id"]}");	
			}
		    $this->member['noticenum']=getcount("select count(*) from `#yunyecms_notice` where status=1");
		}
    }
}