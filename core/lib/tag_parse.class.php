<?php
class tag_parse {
	private $db;
	public function __construct() {
		$this->db = core::load_model("model_tag");
	}

	public function list_tag($data) {
		if(empty($data['catid'])) exit("list标签解析错误:catid为空");
		$catid = intval($data['catid']);
		if(!empty($data['page'])){
		    $page = $data['page'];
		}
		$modelid=getmodelid($catid);
		if(empty($modelid)){
			 exit("list标签解析错误:模型不存在");			
		}else{
			$curmodel=getmodel($modelid);
		    $tablename="m_".$curmodel['tablename'];
		 }
		 $sqlquery="select * from `#yunyecms_{$tablename}`  ";
		 $catidstr= getcatin($catid);
		 $where=" where  status=1 and catid in($catidstr)  ";
		 $limit="";
		if(!empty($data['where'])){
		   $where=$where.trim($data['where']);
		}
		if(isset($data['pic'])){
			$pic=intval($data['pic']);
			if(!empty($pic)) $where=$where." and pic !='' ";
		}
		//推荐
		if($curmodel["modeltype"]==1){
			if(isset($data['isgood'])){
				$isgood=intval(usafestr(trim($data['isgood'])));
				if(is_numeric($isgood)) $where=$where." and isgood =$isgood ";
			}
			//置顶
			if(isset($data['istop'])){
				$istop=intval(usafestr(trim($data['istop'])));
				if(is_numeric($istop)) $where=$where." and istop =$istop ";
			}
			//头条
			if(isset($data['ishead'])){
				$ishead=intval(usafestr(trim($data['ishead'])));
				if(is_numeric($ishead)) $where=$where." and ishead =$ishead ";
			}
		}
		if(!empty($data['order'])){
		   $orderstr = trim($data['order']);
		   $order=" order by $orderstr ";
		}else{
		   $order=" order by `addtime` desc ";
		}
		$isen=!empty($_REQUEST['lang'])&&is_numeric($_REQUEST['lang'])?isenglish($_REQUEST['lang']):false;
		if(!empty($page)){
		        $sqlcnt=" select count(*) from `#yunyecms_{$tablename}` ";
					if(!empty($data['num'])){
					   $num = intval($data['num']);
					   $pagesize=$num;
					   }else{
					   $pagesize=20;
					  }	
				$pagearr=$this->db->pagelist($sqlcnt,$sqlquery,$where,$order,$pagesize);
			    if(isset($pagearr["query"])){
			    foreach($pagearr["query"] as $key=>$var){
					  if(strlen(RD)>0&&stripos($pagearr["query"][$key]["pic"],RD)===FALSE){
						    $pagearr["query"][$key]["pic"]=!empty($var['pic'])?RD.$var['pic']:''; 
						    $pagearr["query"][$key]["picfile"]=!empty($var['picfile'])?RD.$var['picfile']:''; 
					   };
					  if($isen){
							 if(!empty($var['title_en']))$pagearr["query"][$key]["title"]=$var['title_en'];
						      $pagearr["query"][$key]["content"]=!empty($var['content_en'])?uhtmlspecialchars_decode($var["content_en"]):uhtmlspecialchars_decode($var["content"]);
							 if(!empty($var['summary_en']))$pagearr["query"][$key]["summary"]=$var['summary_en'];
							 $pagearr["query"][$key]["url"]=!empty($var['exlink_en'])?$var['exlink_en']:url('show',array('catid'=>$var['catid'],'id'=>$var['id']));
						 }else{
							 $pagearr["query"][$key]["url"]=!empty($var['exlink'])?$var['exlink']:url('show',array('catid'=>$var['catid'],'id'=>$var['id']));
							 if(!empty($var['content']))$pagearr["query"][$key]["content"]=uhtmlspecialchars_decode($var["content"]);
						}
				   }
				}
				return $pagearr;
		  }else{
			if(!empty($data['num'])){
			   $num = intval($data['num']);
			   $limit=" limit 0, $num";
			}
			$strsql=$sqlquery.$where.$order.$limit;
			$return = $this->db->select($strsql);
			if(isset($return)){
			foreach($return as $key=>$var){
				      if(strlen(RD)>0&&stripos($return[$key]["pic"],RD)===FALSE){
						    $return[$key]["pic"]=!empty($var['pic'])?RD.$var['pic']:''; 
						    $return[$key]["picfile"]=!empty($var['picfile'])?RD.$var['picfile']:''; 
					   };
				      if($isen){
							 if(!empty($var['title_en']))$return[$key]["title"]=$var['title_en'];
						     $return[$key]["content"]=!empty($var['content_en'])?uhtmlspecialchars_decode($var["content_en"]):(!empty($var['content'])?uhtmlspecialchars_decode($var["content"]):'');
							 if(!empty($var['summary_en']))$return[$key]["summary"]=$var['summary_en'];
							  $return[$key]["url"]=!empty($var['exlink_en'])?$var['exlink_en']:url('show',array('catid'=>$var['catid'],'id'=>$var['id']));
						 }else{
							  $return[$key]["url"]=!empty($var['exlink'])?$var['exlink']:url('show',array('catid'=>$var['catid'],'id'=>$var['id']));
							  if(!empty($var['content']))$return[$key]["content"]=uhtmlspecialchars_decode($var["content"]);
						}
				  }
			  }
			return $return;
		 }
	}
	/**
	 * 单条信息标签
	 * @param $data
	 */
	public function find_tag($data) {
		if(!empty($data['catid'])){
		    $catid = intval($data['catid']);
		    $modelid=getmodelid($catid);
		    $catidstr= getcatin($catid);
		    $where=" where catid in($catidstr)  ";
		}else{
			$modelid=$data['modelid'];
			if(!empty($data['id'])){
			  $id=$data['id'];
		      $where=" where id=$id  ";
			}else{
			  exit("find标签解析错误:信息ID不存在");			
			}
		}
		if(empty($modelid)){
			 exit("find标签解析错误:模型不存在");			
		}else{
			$curmodel=getmodel($modelid);
		    $tablename="m_".$curmodel['tablename'];
		 }
		$isen=!empty($_REQUEST['lang'])&&is_numeric($_REQUEST['lang'])?isenglish($_REQUEST['lang']):false;
		$modelfields_all=$this->db->select("select * from `#yunyecms_modelfields`  where modelid={$modelid}  and issys=0 ");				
		
		 $sqlquery="select * from `#yunyecms_{$tablename}`  ";
		if(!empty($data['where'])){
		   $where=$where.trim($data['where']);
		}
		if(isset($data['pic'])){
			$pic=intval($data['pic']);
			if(!empty($pic)) $where=$where." and pic !='' ";
		}
		//推荐
		if($curmodel["modeltype"]==1){
				if(isset($data['isgood'])){
					 /* if(!$this->db->field_exists('isgood',$tablename)){
						exit("find标签解析错误:字段isgood不存在");
						}*/
					$isgood=intval(usafestr(trim($data['isgood'])));
					if(is_numeric($isgood)) $where=$where." and isgood =$isgood ";
				}
				//置顶
				if(isset($data['istop'])){
					$istop=intval(usafestr(trim($data['istop'])));
					if(is_numeric($istop)) $where=$where." and istop =$istop ";
				}
				//头条
				if(isset($data['ishead'])){
					$ishead=intval(usafestr(trim($data['ishead'])));
					if(is_numeric($ishead)) $where=$where." and ishead =$ishead ";
				}			
		}
	    $limit=" limit 0, 1";
		if(!empty($data['order'])){
		   $orderstr = trim($data['order']);
		   $order=" order by $orderstr ";
		}else{
		   $order=" order by `addtime` desc ";
		}	
		$strsql=$sqlquery.$where.$order.$limit;
		$return = $this->db->find($strsql);
		     if(strlen(RD)>0&&stripos($return["pic"],RD)===FALSE){
				 $return["pic"]=!empty($return["pic"])?RD.$return["pic"]:''; 
				 $return[$key]["picfile"]=!empty($return['picfile'])?RD.$return['picfile']:''; 
			 };
				 if($isen){
						 if(!empty($return['title_en']))$return["title"]=$return['title_en'];
						 $return["content"]=!empty($return['content_en'])?uhtmlspecialchars_decode($return["content_en"]):(!empty($return['content'])?uhtmlspecialchars_decode($return["content"]):'');
						 if(!empty($return['summary_en']))$return["summary"]=$return['summary_en'];
					 }else{
						 if(isset($return['content']))$return["content"]=uhtmlspecialchars_decode($return["content"]);
					}		
				  foreach($modelfields_all as $key=>$var){
					 $fdname=$var['fdname'];
					  if(isset($return[$fdname])){
						 $return[$fdname]=uhtmlspecialchars_decode($return[$fdname]);
					  }
				   }
		return $return;
	}	
/**
	 * 直接执行sql标签
	 * @param $data
	 */
	public function query_tag($data) {
		if(empty($data['sql'])) exit("sql标签解析错误:sql参数为空");
		if(!empty($data['page'])){
		    $page = $data['page'];
		}
		$where="";
		$order="";
		 global $yunyecms_dbconfig;
		 $sqlquery=$data['sql'];
	     $sqlquery=str_ireplace('#yunyecms_',$yunyecms_dbconfig['tablepre'],trim($sqlquery));
		 $isen=!empty($_REQUEST['lang'])&&is_numeric($_REQUEST['lang'])?isenglish($_REQUEST['lang']):false;
		 if(!empty($page)){
			    $list=$this->db->select($sqlquery);
			    $sqlcnt=count($list);
					if(!empty($data['num'])){
					   $num = intval($data['num']);
					   $pagesize=$num;
					   }else{
					   $pagesize=20;
					  }
				$pagearr=$this->db->pagelist($sqlcnt,$sqlquery,$where,$order,$pagesize);
				if(isset($pagearr["query"])){
					foreach($pagearr["query"] as $key=>$var){
						  if(strlen(ROOT)>1){ $picpath=substr(ROOT, 0, -1); $pagearr["query"][$key]["pic"]=!empty($var['pic'])?$picpath.$var['pic']:''; }
						  if($isen){
								 if(!empty($var['title_en']))$pagearr["query"][$key]["title"]=$var['title_en'];
								 if(!empty($var['content_en']))$pagearr["query"][$key]["content"]=uhtmlspecialchars_decode($var["content_en"]);
								 if(!empty($var['summary_en']))$pagearr["query"][$key]["summary"]=$var['summary_en'];
								 $pagearr["query"][$key]["url"]=!empty($var['exlink_en'])?$var['exlink_en']:url('show',array('id'=>$var['id']));
							 }else{
								 $pagearr["query"][$key]["url"]=!empty($var['exlink'])?$var['exlink']:url('show',array('id'=>$var['id']));
								 if(!empty($var['content']))$pagearr["query"][$key]["content"]=uhtmlspecialchars_decode($var["content"]);
							}
					   }
					}
				return $pagearr;
		  }else{
			if(!empty($data['num'])){
			   $num = intval($data['num']);
			   $limit=" limit 0, $num";
			}
			$strsql=$sqlquery.$limit;
			$return = $this->db->select($strsql);
			if(isset($return)){
			foreach($return as $key=>$var){
						if(strlen(ROOT)>1){ $picpath=substr(ROOT, 0, -1); $return[$key]["pic"]=!empty($var['pic'])?$picpath.$var['pic']:''; }
				      if($isen){
							 if(!empty($var['title_en']))$return[$key]["title"]=$var['title_en'];
						     $return[$key]["content"]=!empty($var['content_en'])?uhtmlspecialchars_decode($var["content_en"]):(!empty($var['content'])?uhtmlspecialchars_decode($var["content"]):'');
							 if(!empty($var['summary_en']))$return[$key]["summary"]=$var['summary_en'];
							  $return[$key]["url"]=!empty($var['exlink_en'])?$var['exlink_en']:url('show',array('id'=>$var['id'],'catid'=>$var['catid']));
						 }else{
						  
						      if(!empty($var['catid'])){
  $return[$key]["url"]=!empty($var['exlink'])?$var['exlink']:url('show',array('id'=>$var['id'],'catid'=>$var['catid']));								  
							  }else{
  $return[$key]["url"]=!empty($var['exlink'])?$var['exlink']:url('show',array('id'=>$var['id']));								  
								  
							  }
							
							  if(!empty($var['content']))$return[$key]["content"]=uhtmlspecialchars_decode($var["content"]);
						}
				  }
			  } 
			return $return;
		 }
	}
		/**
	 * 导航信息标签
	 * @param $data
	 */
	public function cat_tag($data) {
		$pid=empty($data['pid'])?0:$data['pid'];
		$level=empty($data['level'])?0:$data['level'];
		$num=empty($data['num'])?0:$data['num'];
		$catid=empty($data['catid'])?'':$data['catid'];
		 if(!is_numeric($pid)){
			exit("error pid");
		  }
		$return=getcat($pid,$level,$num,$catid);
		return $return;
	}	
	/**
	 * 面包屑导航信息标签
	 * @param $data
	 */
	public function breadcumb_tag($data) {
		$catid=empty($data['catid'])?0:$data['catid'];
		  if(!is_numeric($catid)){
			 exit("error catid");
		  }
		$parent=getbreadcumb($catid);
		return $parent;
	}	
	/**
	 * 导航信息标签
	 * @param $data
	 */
	public function lang_tag($data) {
		$return=getlang();
		return $return;
	}		
	/**
	 * 相关文章标签
	 * @param $data
	 */
	public function relation_tag($data) {
		if(!empty($data['catid'])){
		    $catid = intval($data['catid']);
		}
		if(!empty($data['modelid'])){
		   $modelid = intval($data['modelid']);
		}else{
		   $modelid=getmodelid($catid);
		}
		if(empty($modelid)){
			 exit("relation标签解析错误:模型不存在");			
		}else{
			$curmodel=getmodel($modelid);
		    $tablename="m_".$curmodel['tablename'];
		 }	
		$sqlquery="select * from `#yunyecms_{$tablename}`  ";
	    if(!empty($data['order'])){
		   $orderstr = trim($data['order']);
		   $order=" order by $orderstr ";
		}else{
		   $order=" order by `addtime` desc ";
		}	
		$isen=!empty($_REQUEST['lang'])&&is_numeric($_REQUEST['lang'])?isenglish($_REQUEST['lang']):false;
		$limit = $data['id'] ? $data['num']+1 : $data['num'];
		$limit=" limit 0,{$limit}";
		$returnarray1=array();
		$returnarray2=array();
		if(!empty($data['relation'])) {
		    $where=" where status=1 ";
			$relations = explode('|',trim($data['relation'],'|'));
			$relations = array_diff($relations, array(null));
			$relations = implode(',',$relations);
			$where =$where. " and `id` IN ($relations)";
			$returnarray1 = $this->db->select($sqlquery.$where.$order.$limit);
		 }elseif(!empty($data['keywords'])) {
			$keywords = str_replace(array('%',"'"), '',$data['keywords']);
			$keywords_arr = explode(' ',$keywords);
			$key_array = array();
			$number = 0;
			$i =1;
			$catidstr= getcatin($catid);
		    $where2 =" where 1 ";
		    //$where2 =" where catid in($catidstr) ";
			if(!empty($relations)){
		    $where2 .="  and `id` NOT IN ($relations) ";
			}
			 if(!empty($keywords_arr)){
				$sqlkey=" and ( ";
				foreach ($keywords_arr as $_k) {
					if($i==1){
						$sqlkey = $sqlkey."  `seokeywords` LIKE '%$_k%' ";
					 }else{
						$sqlkey = $sqlkey." or `seokeywords` LIKE '%$_k%' ";
					}
					$i=$i+1;
			    }
				$sqlkey= $sqlkey. " ) ";
                $where2=$where2.$sqlkey.(isset($data['id']) && intval($data['id']) ? " AND `id` != '".abs(intval($data['id']))."'" : '');
			 }
			$returnarray2 = $this->db->select($sqlquery.$where2.$order.$limit);
		}
		$return=array_merge($returnarray1,$returnarray2);
		if(isset($return)){
		foreach($return as $key=>$var){
					if(strlen(ROOT)>1){ $picpath=substr(ROOT, 0, -1); $return[$key]["pic"]=!empty($var['pic'])?$picpath.$var['pic']:''; }
				  if($isen){
						 if(!empty($var['title_en']))$return[$key]["title"]=$var['title_en'];
						  $return[$key]["content"]=!empty($var['content_en'])?uhtmlspecialchars_decode($var["content_en"]):(!empty($var['content'])?uhtmlspecialchars_decode($var["content"]):'');
						 if(!empty($var['summary_en']))$return[$key]["summary"]=$var['summary_en'];
						  $return[$key]["url"]=!empty($var['exlink_en'])?$var['exlink_en']:url('show',array('catid'=>$var['catid'],'id'=>$var['id']));
					 }else{
						  $return[$key]["url"]=!empty($var['exlink'])?$var['exlink']:url('show',array('catid'=>$var['catid'],'id'=>$var['id']));
						  if(!empty($var['content']))$return[$key]["content"]=uhtmlspecialchars_decode($var["content"]);
					}
			  }
		  }	
		return $return;
	}
	/**
	 * 单条信息标签
	 * @param $data
	 */
	public function form_tag($data) {
		if(!empty($data['catid'])){
		    $catid = intval($data['catid']);
		    $modelid=getmodelid($catid);
		}else{
			  exit("form标签解析错误:信息ID不存在");			
		}
		if(empty($modelid)){
			 exit("form标签解析错误:模型不存在");			
		}else{
			$curmodel=getmodel($modelid);
		    $tablename="m_".$curmodel['tablename'];
		 }
		 $isen=!empty($_REQUEST['lang'])&&is_numeric($_REQUEST['lang'])?isenglish($_REQUEST['lang']):false;
		 $modelfields_all=$this->db->select("select * from `#yunyecms_modelfields`  where modelid={$modelid}  and isadd=1 ");
		 if($isen){
			  foreach($modelfields_all as $key=>$var){
			   $modelfields_all[$key]["fdtitle"]=$var["fdtitle_en"];
		      }    
		 }
		return $modelfields_all;
	}	


}